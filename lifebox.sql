-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lifebox
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `academics_professor`
--

DROP TABLE IF EXISTS `academics_professor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academics_professor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_university_id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `academics_professor_a8ebd7b9` (`current_university_id`),
  CONSTRAINT `D12faad85e3c4b33773b3d0d2e362223` FOREIGN KEY (`current_university_id`) REFERENCES `academics_university` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academics_professor`
--

LOCK TABLES `academics_professor` WRITE;
/*!40000 ALTER TABLE `academics_professor` DISABLE KEYS */;
INSERT INTO `academics_professor` VALUES (1,1,'Ken','Train'),(2,2,'Eric','Grimson'),(3,3,'Dan','Grossman'),(4,4,'Martin','Haugh'),(5,5,'Ben','Polak');
/*!40000 ALTER TABLE `academics_professor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `academics_question`
--

DROP TABLE IF EXISTS `academics_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academics_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(256) NOT NULL,
  `answer` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academics_question`
--

LOCK TABLES `academics_question` WRITE;
/*!40000 ALTER TABLE `academics_question` DISABLE KEYS */;
INSERT INTO `academics_question` VALUES (1,'1) What is data structure?\n','Data structure refers to the way data is organized and manipulated. It seeks to find ways to make data access more efficient. When dealing with data structure, we not only focus on one piece of data, but rather different set of data and how they can relate to one another in an organized manner.\n'),(2,'2) Differentiate file structure from storage structure.\n','Basically, the key difference is the memory area that is being accessed. When dealing with the structure that resides the main memory of the computer system, this is referred to as storage structure. When dealing with an auxiliary structure, we refer to it as file structure.\n'),(3,'3) When is a binary search best applied?\n','A binary search is an algorithm that is best applied to search a list when the elements are already in order or sorted. The list is search starting in the middle, such that if that middle value is not the target search key, it will check to see if it will continue the search on the lower half of the list or the higher half. The split and search will then continue in the same manner.\n'),(4,'4) What is a linked list?\n','A linked list is a sequence of nodes in which each node is connected to the node following it. This forms a chain-like link of data storage.\n'),(5,'5) How do you reference all the elements in a one-dimension array?\n','To do this, an indexed loop is used, such that the counter runs from 0 to the array size minus one. In this manner, we are able to reference all the elements in sequence by using the loop counter as the array subscript.\n'),(6,'6) In what areas do data structures applied?\n','Data structure is important in almost every aspect where data is involved. In general, algorithms that involve efficient data structure is applied in the following areas: numerical analysis, operating system, A.I., compiler design, database management, graphics, and statistical analysis, to name a few.\n'),(7,'7) What is LIFO?\n','LIFO is short for Last In First Out, and refers to how data is accessed, stored and retrieved. Using this scheme, data that was stored last , should be the one to be extracted first. This also means that in order to gain access to the first data, all the other data that was stored before this first data must first be retrieved and extracted.\n'),(8,'8 ) What is a queue?\n','A queue is a data structure that can simulates a list or stream of data. In this structure, new elements are inserted at one end and existing elements are removed from the other end.\n'),(9,'9) What are binary trees?\n','A binary tree is one type of data structure that has two nodes, a left node and a right node. In programming, binary trees are actually an extension of the linked list structures.\n'),(10,'10) Which data structure is applied when dealing with a recursive function?\n','Recursion, which is basically a function that calls itself based on a terminating condition, makes use of the stack. Using LIFO, a call to a recursive function saves the return address so that it knows how to return to the calling function after the call terminates.\n'),(11,'11) What is a stack?\n','A stack is a data structure in which only the top element can be accessed. As data is stored in the stack, each data is pushed downward, leaving the most recently added data on top.\n'),(12,'12) Explain Binary Search Tree\n','A binary search tree stores data in such a way that they can be retrieved very efficiently. The left subtree contains nodes whose keys are less than the node’s key value, while the right subtree contains nodes whose keys are greater than or equal to the node’s key value. Moreover, both subtrees are also binary search trees.\n'),(13,'13) What are multidimensional arrays?\n','Multidimensional arrays make use of multiple indexes to store data. It is useful when storing data that cannot be represented using a single dimensional indexing, such as data representation in a board game, tables with data stored in more than one column.\n'),(14,'14) Are linked lists considered linear or non-linear data structure?\n','It actually depends on where you intend to apply linked lists. If you based it on storage, a linked list is considered non-linear. On the other hand, if you based it on access strategies, then a linked list is considered linear.\n'),(15,'15) How does dynamic memory allocation help in managing data?\n','Aside from being able to store simple structured data types, dynamic memory allocation can combine separately allocated structured blocks to form composite structures that expand and contract as needed.\n'),(16,'16) What is FIFO?\n','FIFO is short for First-in, First-out, and is used to represent how data is accessed in a queue. Data has been inserted into the queue list the longest is the one that is removed first.\n'),(17,'17) What is an ordered list?\n','An ordered list is a list in which each node’s position in the list is determined by the value of its key component, so that the key values form an increasing sequence, as the list is traversed.\n'),(18,'18) What is merge sort?\n','Merge sort takes a divide-and-conquer approach to sorting data. In a sequence of data, adjacent ones are merged and sorted to create bigger sorted lists. These sorted lists are then merged again to form an even bigger sorted list, which continuous until you have one single sorted list.\n'),(19,'19) Differentiate NULL and VOID.\n','Null is actually a value, whereas Void is a data type identifier. A variable that is given a Null value simply indicates an empty value. Void is used to identify pointers as having no initial size.\n'),(20,'20) What is the primary advantage of a linked list?\n','A linked list is a very ideal data structure because it can be modified easily. This means that modifying a linked list works regardless of how many elements are in the list.\n'),(21,'21) What is the difference between a PUSH and a POP?\n','Pushing and popping applies to the way data is stored and retrieved in a stack. A push denotes data being added to it, meaning data is being “pushed” into the stack. On the other hand, a pop denotes data retrieval, and in particular refers to the topmost data being accessed.\n'),(22,'22) What is a linear search?\n','A linear search refers to the way a target key is being searched in a sequential data structure. Using this method, each element in the list is checked and compared against the target key, and is repeated until found or if the end of the list has been reached.\n'),(23,'23) How does variable declaration affect memory allocation?\n','The amount of memory to be allocated or reserved would depend on the data type of the variable being declared. For example, if a variable is declared to be of integer type, then 32 bits of memory storage will be reserved for that variable.\n'),(24,'24) What is the advantage of the heap over a stack?\n','Basically, the heap is more flexible than the stack. That’s because memory space for the heap can be dynamically allocated and de-allocated as needed. However, memory of the heap can at times be slower when compared to that stack.\n'),(25,'25) What is a postfix expression?\n','A postfix expression is an expression in which each operator follows its operands. The advantage of this form is that there is no need to group sub-expressions in parentheses or to consider operator precedence.\n'),(26,'26) What is Data abstraction?\n','Data abstraction is a powerful tool for breaking down complex data problems into manageable chunks. This is applied by initially specifying the data objects involved and the operations to be performed on these data objects without being overly concerned with how the data objects will be represented and stored in memory.\n'),(27,'27) How do you insert a new item in a binary search tree?\n','Assuming that the data to be inserted is a unique value (that is, not an existing entry in the tree), check first if the tree is empty. If it’s empty, just insert the new item in the root node. If it’s not empty, refer to the new item’s key. If it’s smaller than the root’s key, insert it into the root’s left subtree, otherwise, insert it into the root’s right subtree.\n'),(28,'28) How does a selection sort work for an array?\n','The selection sort is a fairly intuitive sorting algorithm,, though not necessarily efficient. To perform this, the smallest element is first located and switched with the element at subscript zero, thereby placing the smallest element in the first position. The smallest element remaining in the subarray is then located next with subscripts 1 through n-1 and switched with the element at subscript 1, thereby placing the second smallest element in the second position. The steps are repeated in the same manner till the last element.\n'),(29,'29) How do signed and unsigned numbers affect memory?\n','In the case of signed numbers, the first bit is used to indicate whether positive or negative, which leaves you with one bit short. With unsigned numbers, you have all bits available for that number. The effect is best seen in the number range (unsigned 8 bit number has a range 0-255, while 8-bit signed number has a range -128 to +127.\n'),(30,'30) What is the minimum number of nodes that a binary tree can have?\n','A binary tree can have a minimum of zero nodes, which occurs when the nodes have NULL values. Furthermore, a binary tree can also have 1 or 2 nodes.\n'),(31,'31) What are dynamic data structures?\n','Dynamic data structures are structures that expand and contract as a program runs. It provides a flexible means of manipulating data because it can adjust according to the size of the data.\n'),(32,'32) In what data structures are pointers applied?\n','Pointers that are used in linked list have various applications in data structure. Data structures that make use of this concept include the Stack, Queue, Linked List and Binary Tree.\n'),(33,'33) Do all declaration statements result in a fixed reservation in memory?\n','Most declarations do, with the exemption of pointers. Pointer declaration does not allocate memory for data, but for the address of the pointer variable. Actual memory allocation for the data comes during run-time.\n'),(34,'34) What are ARRAYs?\n','When dealing with arrays, data is stored and retrieved using an index that actually refers to the element number in the data sequence. This means that data can be accessed in any order. In programming, an array is declared as a variable having a number of indexed elements.\n'),(35,'35) What is the minimum number of queues needed when implementing a priority queue?\n','The minimum number of queues needed in this case is two. One queue is intended for sorting priorities while the other queue is intended for actual storage of data.\n'),(36,'36) Which sorting algorithm is considered the fastest?\n','There are many types of sorting algorithms: quick sort, bubble sort, balloon sort, radix sort, merge sort, etc. Not one can be considered the fastest because each algorithm is designed for a particular data structure and data set. It would depend on the data set that you would want to sort.\n'),(37,'37) Differentiate STACK from ARRAY.\n','Data that is stored in a stack follows a LIFO pattern. This means that data access follows a sequence wherein the last data to be stored will the first one to be extracted. Arrays, on the other hand, does not follow a particular order and instead can be accessed by referring to the indexed element within the array.\n'),(38,'38) Give a basic algorithm for searching a binary search tree.\n','1. if the tree is empty, then the target is not in the tree, end search; 2. if the tree is not empty, the target is in the tree; 3. check if the target is in the root item; 4. if target is not in the root item, check if target is smaller than the root’s value; 5. if target is smaller than the root’s value, search the left subtree; 6. else, search the right subtree\n'),(39,'39) What is a dequeue?\n','A dequeue is a double-ended queue. This is a structure wherein elements can be inserted or removed from either end.\n'),(40,'40) What is a bubble sort and how do you perform it?\n','A bubble sort is one sorting technique that can be applied to data structures such as an array. It works by comparing adjacent elements and exchanges their values if they are out of order. This method lets the smaller values “bubble” to the top of the list, while the larger value sinks to the bottom.\n'),(41,'41) What are the parts of a linked list?\n','A linked list typically has two parts: the head and the tail. Between the head and tail lie the actual nodes, with each node being linked in a sequential manner.\n'),(42,'42) How does selection sort work?\n','Selection sort works by picking the smallest number from the list and placing it at the front. This process is repeated for the second position towards the end of the list. It is the simplest sort algorithm.\n'),(43,'43) What is a graph?\n','A graph is one type of data structure that contains a set of ordered pairs. These ordered pairs are also referred to as edges or arcs, and are used to connect nodes where data can be stored and retrieved.\n'),(44,'44) Differentiate linear from non linear data structure.\n','Linear data structure is a structure wherein data elements are adjacent to each other. Examples of linear data structure include arrays, linked lists, stacks and queues. On the other hand, non-linear data structure is a structure wherein each data element can connect to more than two adjacent data elements. Examples of non linear data structure include trees and graphs.\n'),(45,'45) What is an AVL tree?\n','An AVL tree is a type of binary search tree that is always in a state of partially balanced. The balance is measured as a difference between the heights of the subtrees from the root. This self-balancing tree was known to be the first data structure to be designed as such.\n'),(46,'46) What are doubly linked lists?\n','Doubly linked lists are a special type of linked list wherein traversal across the data elements can be done in both directions. This is made possible by having two links in every node, one that links to the next node and other one that links to the previous node.\n'),(47,'47) What is Huffman’s algorithm?\n','Huffman’s algorithm is associated in creating extended binary trees that has minimum weighted path lengths from the given weights. It makes use of a table that contains frequency of occurrence for each data element.\n'),(48,'48) What is Fibonacci search?\n','Fibonacci search is a search algorithm that applies to a sorted array. It makes use of a divide-and-conquer approach that can greatly reduce the time needed in order to reach the target element.\n'),(49,'49) Briefly explain recursive algorithm.\n','Recursive algorithm targets a problem by dividing it into smaller, manageable sub-problems. The output of one recursion after processing one sub-problem becomes the input to the next recursive process.\n'),(50,'50) How do you search for a target key in a linked list?\n','To find the target key in a linked list, you have to apply sequential search. Each node is traversed and compared with the target key, and if it is different, then it follows the link to the next node. This traversal continues until either the target key is found or if the last node is reached.\n');
/*!40000 ALTER TABLE `academics_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `academics_term`
--

DROP TABLE IF EXISTS `academics_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academics_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academics_term`
--

LOCK TABLES `academics_term` WRITE;
/*!40000 ALTER TABLE `academics_term` DISABLE KEYS */;
INSERT INTO `academics_term` VALUES (1,'Spring'),(2,'Fall');
/*!40000 ALTER TABLE `academics_term` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `academics_university`
--

DROP TABLE IF EXISTS `academics_university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academics_university` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academics_university`
--

LOCK TABLES `academics_university` WRITE;
/*!40000 ALTER TABLE `academics_university` DISABLE KEYS */;
INSERT INTO `academics_university` VALUES (1,'University of California, Berkeley'),(2,'Massachusetts Institute of Technology'),(3,'University of Washington'),(4,'Columbia University'),(5,'Yale University');
/*!40000 ALTER TABLE `academics_university` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add television show',7,'add_televisionshow'),(20,'Can change television show',7,'change_televisionshow'),(21,'Can delete television show',7,'delete_televisionshow'),(22,'Can add season',8,'add_season'),(23,'Can change season',8,'change_season'),(24,'Can delete season',8,'delete_season'),(25,'Can add episode',9,'add_episode'),(26,'Can change episode',9,'change_episode'),(27,'Can delete episode',9,'delete_episode'),(28,'Can add academic course',10,'add_academiccourse'),(29,'Can change academic course',10,'change_academiccourse'),(30,'Can delete academic course',10,'delete_academiccourse'),(31,'Can add lecture',11,'add_lecture'),(32,'Can change lecture',11,'change_lecture'),(33,'Can delete lecture',11,'delete_lecture'),(34,'Can add professor',12,'add_professor'),(35,'Can change professor',12,'change_professor'),(36,'Can delete professor',12,'delete_professor'),(37,'Can add university',13,'add_university'),(38,'Can change university',13,'change_university'),(39,'Can delete university',13,'delete_university'),(40,'Can add term',14,'add_term'),(41,'Can change term',14,'change_term'),(42,'Can delete term',14,'delete_term'),(43,'Can add workout',15,'add_workout'),(44,'Can change workout',15,'change_workout'),(45,'Can delete workout',15,'delete_workout'),(46,'Can add exercise',16,'add_exercise'),(47,'Can change exercise',16,'change_exercise'),(48,'Can delete exercise',16,'delete_exercise'),(49,'Can add exercise set',17,'add_exerciseset'),(50,'Can change exercise set',17,'change_exerciseset'),(51,'Can delete exercise set',17,'delete_exerciseset'),(55,'Can add walk',19,'add_walk'),(56,'Can change walk',19,'change_walk'),(57,'Can delete walk',19,'delete_walk'),(58,'Can add food',20,'add_food'),(59,'Can change food',20,'change_food'),(60,'Can delete food',20,'delete_food'),(61,'Can add sleep',21,'add_sleep'),(62,'Can change sleep',21,'change_sleep'),(63,'Can delete sleep',21,'delete_sleep'),(73,'Can add daily survey',25,'add_dailysurvey'),(74,'Can change daily survey',25,'change_dailysurvey'),(75,'Can delete daily survey',25,'delete_dailysurvey'),(76,'Can add unit',26,'add_unit'),(77,'Can change unit',26,'change_unit'),(78,'Can delete unit',26,'delete_unit'),(79,'Can add conversion',27,'add_conversion'),(80,'Can change conversion',27,'change_conversion'),(81,'Can delete conversion',27,'delete_conversion'),(82,'Can add chemical',28,'add_chemical'),(83,'Can change chemical',28,'change_chemical'),(84,'Can delete chemical',28,'delete_chemical'),(85,'Can add content',29,'add_content'),(86,'Can change content',29,'change_content'),(87,'Can delete content',29,'delete_content'),(88,'Can add caffeine',30,'add_caffeine'),(89,'Can change caffeine',30,'change_caffeine'),(90,'Can delete caffeine',30,'delete_caffeine'),(91,'Can add pantry',31,'add_pantry'),(92,'Can change pantry',31,'change_pantry'),(93,'Can delete pantry',31,'delete_pantry'),(94,'Can add pantry item',32,'add_pantryitem'),(95,'Can change pantry item',32,'change_pantryitem'),(96,'Can delete pantry item',32,'delete_pantryitem'),(103,'Can add recipe',35,'add_recipe'),(104,'Can change recipe',35,'change_recipe'),(105,'Can delete recipe',35,'delete_recipe'),(106,'Can add recipe food',36,'add_recipefood'),(107,'Can change recipe food',36,'change_recipefood'),(108,'Can delete recipe food',36,'delete_recipefood'),(109,'Can add food weight',37,'add_foodweight'),(110,'Can change food weight',37,'change_foodweight'),(111,'Can delete food weight',37,'delete_foodweight'),(112,'Can add meal',38,'add_meal'),(113,'Can change meal',38,'change_meal'),(114,'Can delete meal',38,'delete_meal'),(115,'Can add category',39,'add_category'),(116,'Can change category',39,'change_category'),(117,'Can delete category',39,'delete_category'),(118,'Can add future purchase',40,'add_futurepurchase'),(119,'Can change future purchase',40,'change_futurepurchase'),(120,'Can delete future purchase',40,'delete_futurepurchase'),(124,'Can add recurring bill',42,'add_recurringbill'),(125,'Can change recurring bill',42,'change_recurringbill'),(126,'Can delete recurring bill',42,'delete_recurringbill'),(127,'Can add budget allocation',43,'add_budgetallocation'),(128,'Can change budget allocation',43,'change_budgetallocation'),(129,'Can delete budget allocation',43,'delete_budgetallocation'),(130,'Can add happiness moment',44,'add_happinessmoment'),(131,'Can change happiness moment',44,'change_happinessmoment'),(132,'Can delete happiness moment',44,'delete_happinessmoment'),(133,'Can add note card',45,'add_notecard'),(134,'Can change note card',45,'change_notecard'),(135,'Can delete note card',45,'delete_notecard'),(136,'Can add author',46,'add_author'),(137,'Can change author',46,'change_author'),(138,'Can delete author',46,'delete_author'),(139,'Can add book',47,'add_book'),(140,'Can change book',47,'change_book'),(141,'Can delete book',47,'delete_book'),(142,'Can add person',48,'add_person'),(143,'Can change person',48,'change_person'),(144,'Can delete person',48,'delete_person'),(148,'Can add artist',50,'add_artist'),(149,'Can change artist',50,'change_artist'),(150,'Can delete artist',50,'delete_artist'),(151,'Can add album',51,'add_album'),(152,'Can change album',51,'change_album'),(153,'Can delete album',51,'delete_album'),(154,'Can add song',52,'add_song'),(155,'Can change song',52,'change_song'),(156,'Can delete song',52,'delete_song'),(157,'Can add possession',53,'add_possession'),(158,'Can change possession',53,'change_possession'),(159,'Can delete possession',53,'delete_possession'),(160,'Can add linear mood',54,'add_linearmood'),(161,'Can change linear mood',54,'change_linearmood'),(162,'Can delete linear mood',54,'delete_linearmood'),(163,'Can add cleaning',55,'add_cleaning'),(164,'Can change cleaning',55,'change_cleaning'),(165,'Can delete cleaning',55,'delete_cleaning'),(166,'Can add room',56,'add_room'),(167,'Can change room',56,'change_room'),(168,'Can delete room',56,'delete_room'),(169,'Can add water pitcher',57,'add_waterpitcher'),(170,'Can change water pitcher',57,'change_waterpitcher'),(171,'Can delete water pitcher',57,'delete_waterpitcher'),(172,'Can add linear energy',58,'add_linearenergy'),(173,'Can change linear energy',58,'change_linearenergy'),(174,'Can delete linear energy',58,'delete_linearenergy'),(175,'Can add substance',59,'add_substance'),(176,'Can change substance',59,'change_substance'),(177,'Can delete substance',59,'delete_substance'),(178,'Can add purchase',60,'add_purchase'),(179,'Can change purchase',60,'change_purchase'),(180,'Can delete purchase',60,'delete_purchase'),(181,'Can add movie',61,'add_movie'),(182,'Can change movie',61,'change_movie'),(183,'Can delete movie',61,'delete_movie'),(184,'Can add rule',62,'add_rule'),(185,'Can change rule',62,'change_rule'),(186,'Can delete rule',62,'delete_rule'),(187,'Can add task',63,'add_task'),(188,'Can change task',63,'change_task'),(189,'Can delete task',63,'delete_task'),(190,'Can add date',64,'add_date'),(191,'Can change date',64,'change_date'),(192,'Can delete date',64,'delete_date'),(193,'Can add journal',65,'add_journal'),(194,'Can change journal',65,'change_journal'),(195,'Can delete journal',65,'delete_journal'),(196,'Can add water filter',66,'add_waterfilter'),(197,'Can change water filter',66,'change_waterfilter'),(198,'Can delete water filter',66,'delete_waterfilter'),(199,'Can add conversation',67,'add_conversation'),(200,'Can change conversation',67,'change_conversation'),(201,'Can delete conversation',67,'delete_conversation'),(202,'Can add label',68,'add_label'),(203,'Can change label',68,'change_label'),(204,'Can delete label',68,'delete_label'),(205,'Can add attribute',69,'add_attribute'),(206,'Can change attribute',69,'change_attribute'),(207,'Can delete attribute',69,'delete_attribute'),(208,'Can add composer',70,'add_composer'),(209,'Can change composer',70,'change_composer'),(210,'Can delete composer',70,'delete_composer'),(211,'Can add node',71,'add_node'),(212,'Can change node',71,'change_node'),(213,'Can delete node',71,'delete_node'),(214,'Can add binary mood',72,'add_binarymood'),(215,'Can change binary mood',72,'change_binarymood'),(216,'Can delete binary mood',72,'delete_binarymood'),(223,'Can add container possession',75,'add_containerpossession'),(224,'Can change container possession',75,'change_containerpossession'),(225,'Can delete container possession',75,'delete_containerpossession'),(226,'Can add location',76,'add_location'),(227,'Can change location',76,'change_location'),(228,'Can delete location',76,'delete_location'),(229,'Can add activity',77,'add_activity'),(230,'Can change activity',77,'change_activity'),(231,'Can delete activity',77,'delete_activity'),(235,'Can add atom',79,'add_atom'),(236,'Can change atom',79,'change_atom'),(237,'Can delete atom',79,'delete_atom'),(238,'Can add question',80,'add_question'),(239,'Can change question',80,'change_question'),(240,'Can delete question',80,'delete_question'),(241,'Can add project',81,'add_project'),(242,'Can change project',81,'change_project'),(243,'Can delete project',81,'delete_project');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$TXM8LOzcinf5$Gp21llf4N1cDEI1xzZqIPMbEPyKiIGOmozNxEesLcas=','2014-12-23 23:55:28',1,'admin','','','admin@example.com',1,1,'2014-12-14 05:15:17'),(3,'pbkdf2_sha256$12000$3mfOb3EdKWfu$wiqjFF0wSD0Bp9x7PHTJ+A22AIekwH01DNY4ZTODZwk=','2015-02-14 22:50:07',1,'joshua','','','',1,1,'2014-12-21 02:02:45'),(4,'pbkdf2_sha256$12000$xS5d8kyawiza$9A5oDvbivkG6M+xVvdyOgauA6gPnTsXWd2R7Xdhfphw=','2014-12-21 02:03:09',1,'ryan','','','',1,1,'2014-12-21 02:03:09');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_node`
--

DROP TABLE IF EXISTS `core_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_node`
--

LOCK TABLES `core_node` WRITE;
/*!40000 ALTER TABLE `core_node` DISABLE KEYS */;
INSERT INTO `core_node` VALUES (1,'Felix Mendelssohn'),(2,'Max Bruch');
/*!40000 ALTER TABLE `core_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_person`
--

DROP TABLE IF EXISTS `core_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_person`
--

LOCK TABLES `core_person` WRITE;
/*!40000 ALTER TABLE `core_person` DISABLE KEYS */;
INSERT INTO `core_person` VALUES (1,'Ken','Train'),(2,'Eric','Grimson'),(3,'Dan','Grossman'),(4,'Martin','Haugh'),(5,'Ben','Polak');
/*!40000 ALTER TABLE `core_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corsheaders_corsmodel`
--

DROP TABLE IF EXISTS `corsheaders_corsmodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corsheaders_corsmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cors` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corsheaders_corsmodel`
--

LOCK TABLES `corsheaders_corsmodel` WRITE;
/*!40000 ALTER TABLE `corsheaders_corsmodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `corsheaders_corsmodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=503 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-12-14 05:58:08','1','Psycho-Pass Season 1 Episode 1',2,'Changed viewed.',9,1),(2,'2014-12-14 05:58:08','2','Psycho-Pass Season 1 Episode 2',2,'Changed viewed.',9,1),(3,'2014-12-14 05:58:08','3','Psycho-Pass Season 1 Episode 3',2,'Changed viewed.',9,1),(4,'2014-12-14 05:58:08','4','Psycho-Pass Season 1 Episode 4',2,'Changed viewed.',9,1),(5,'2014-12-14 05:58:08','5','Psycho-Pass Season 1 Episode 5',2,'Changed viewed.',9,1),(6,'2014-12-14 05:58:08','6','Psycho-Pass Season 1 Episode 6',2,'Changed viewed.',9,1),(7,'2014-12-14 05:58:08','7','Psycho-Pass Season 1 Episode 7',2,'Changed viewed.',9,1),(8,'2014-12-14 05:58:08','8','Psycho-Pass Season 1 Episode 8',2,'Changed viewed.',9,1),(9,'2014-12-14 05:58:09','9','Psycho-Pass Season 1 Episode 9',2,'Changed viewed.',9,1),(10,'2014-12-14 06:11:07','92','Mad Men Season 1 Episode 1',2,'Changed viewed.',9,1),(11,'2014-12-14 06:11:07','93','Mad Men Season 1 Episode 2',2,'Changed viewed.',9,1),(12,'2014-12-14 06:11:07','94','Mad Men Season 1 Episode 3',2,'Changed viewed.',9,1),(13,'2014-12-14 06:11:07','95','Mad Men Season 1 Episode 4',2,'Changed viewed.',9,1),(14,'2014-12-14 06:11:08','96','Mad Men Season 1 Episode 5',2,'Changed viewed.',9,1),(15,'2014-12-14 06:11:08','97','Mad Men Season 1 Episode 6',2,'Changed viewed.',9,1),(16,'2014-12-14 06:11:08','98','Mad Men Season 1 Episode 7',2,'Changed viewed.',9,1),(17,'2014-12-14 06:11:08','99','Mad Men Season 1 Episode 8',2,'Changed viewed.',9,1),(18,'2014-12-14 06:11:08','100','Mad Men Season 1 Episode 9',2,'Changed viewed.',9,1),(19,'2014-12-14 06:11:08','101','Mad Men Season 1 Episode 10',2,'Changed viewed.',9,1),(20,'2014-12-14 06:11:08','102','Mad Men Season 1 Episode 11',2,'Changed viewed.',9,1),(21,'2014-12-14 06:11:08','103','Mad Men Season 1 Episode 12',2,'Changed viewed.',9,1),(22,'2014-12-14 06:11:08','104','Mad Men Season 1 Episode 13',2,'Changed viewed.',9,1),(23,'2014-12-14 18:46:46','1','University object',1,'',13,1),(24,'2014-12-14 18:47:15','1','Professor object',1,'',12,1),(25,'2014-12-14 18:48:12','1','Term object',1,'',14,1),(26,'2014-12-14 18:48:15','2','Term object',1,'',14,1),(27,'2014-12-14 18:50:01','1','AcademicCourse object',1,'',10,1),(28,'2014-12-14 18:51:43','1','Introduction to Economics',2,'Changed term_taught.',10,1),(29,'2014-12-14 18:52:34','1','Introduction to Economics',2,'Changed lecture_count.',10,1),(30,'2014-12-14 19:03:56','1','Introduction to Economics',3,'',10,1),(31,'2014-12-14 19:04:11','2','Introduction to Economics',1,'',10,1),(32,'2014-12-14 19:04:40','3','Introduction to Economics Lecture 1',2,'Changed viewed.',11,1),(33,'2014-12-14 19:04:40','4','Introduction to Economics Lecture 2',2,'Changed viewed.',11,1),(34,'2014-12-14 19:04:40','5','Introduction to Economics Lecture 3',2,'Changed viewed.',11,1),(35,'2014-12-14 19:06:53','2','Massachusetts Institute of Technology',1,'',13,1),(36,'2014-12-14 19:07:03','2','Eric Grimson',1,'',12,1),(37,'2014-12-14 19:07:30','3','Introduction to Computer Science and Programming',1,'',10,1),(38,'2014-12-14 19:08:48','3','Introduction to Computer Science and Programming',2,'Changed lecture_count.',10,1),(39,'2014-12-14 19:09:05','3','Introduction to Computer Science and Programming',2,'Changed lecture_count.',10,1),(40,'2014-12-14 19:09:11','51','Introduction to Computer Science and Programming Lecture 25',2,'Changed viewed.',11,1),(41,'2014-12-14 19:10:41','3','Introduction to Computer Science and Programming',2,'Changed completed.',10,1),(42,'2014-12-14 19:12:00','3','University of Washington',1,'',13,1),(43,'2014-12-14 19:12:09','3','Dan Grossman',1,'',12,1),(44,'2014-12-14 19:16:53','4','Programming Languages',1,'',10,1),(45,'2014-12-14 19:17:44','4','Columbia University',1,'',13,1),(46,'2014-12-14 19:17:58','4','Martin Haugh',1,'',12,1),(47,'2014-12-14 19:18:57','5','Financial Engineering and Risk Management Part 1',1,'',10,1),(48,'2014-12-14 19:19:33','5','Yale University',1,'',13,1),(49,'2014-12-14 19:19:58','5','Ben Polak',1,'',12,1),(50,'2014-12-14 19:20:27','6','Game Theory',1,'',10,1),(51,'2014-12-14 20:23:09','6','Introduction to Economics Lecture 4',2,'Changed viewed.',11,1),(52,'2014-12-14 23:52:53','312','Glee Season 3 Episode 13',2,'Changed viewed.',9,1),(53,'2014-12-15 02:06:46','1','Exercise object',1,'',16,1),(54,'2014-12-15 02:06:57','2','Exercise object',1,'',16,1),(55,'2014-12-15 02:07:04','3','Exercise object',1,'',16,1),(56,'2014-12-15 02:07:54','1','Workout object',1,'',15,1),(57,'2014-12-15 02:12:31','1','ExerciseSet object',1,'',17,1),(58,'2014-12-15 02:12:40','2','ExerciseSet object',1,'',17,1),(59,'2014-12-15 02:12:47','3','ExerciseSet object',1,'',17,1),(60,'2014-12-15 02:12:54','4','ExerciseSet object',1,'',17,1),(61,'2014-12-15 02:13:03','5','ExerciseSet object',1,'',17,1),(62,'2014-12-15 02:13:08','6','ExerciseSet object',1,'',17,1),(63,'2014-12-15 02:13:15','7','ExerciseSet object',1,'',17,1),(64,'2014-12-15 02:13:23','8','ExerciseSet object',1,'',17,1),(65,'2014-12-15 02:13:31','9','ExerciseSet object',1,'',17,1),(66,'2014-12-15 02:13:37','10','ExerciseSet object',1,'',17,1),(67,'2014-12-15 02:13:43','11','ExerciseSet object',1,'',17,1),(68,'2014-12-15 02:13:49','12','ExerciseSet object',1,'',17,1),(69,'2014-12-15 15:59:16','1','Walk object',1,'',19,1),(70,'2014-12-15 16:14:35','1','Walk object',2,'Changed start_time and end_time.',19,1),(71,'2014-12-15 22:05:17','2','Walk object',1,'',19,1),(72,'2014-12-15 22:05:17','3','Walk object',1,'',19,1),(73,'2014-12-15 22:05:32','3','Walk object',3,'',19,1),(74,'2014-12-15 22:05:38','2','Walk object',3,'',19,1),(75,'2014-12-15 22:05:41','4','Walk object',1,'',19,1),(76,'2014-12-15 22:06:02','4','Walk object',2,'Changed start_time and blocks.',19,1),(77,'2014-12-15 23:45:39','515','Reign Season 1 Episode 1',2,'Changed viewed.',9,1),(78,'2014-12-15 23:45:59','7','Reign',3,'',7,1),(79,'2014-12-15 23:46:20','10','Psycho-Pass Season 1 Episode 10',2,'Changed viewed.',9,1),(80,'2014-12-15 23:46:20','11','Psycho-Pass Season 1 Episode 11',2,'Changed viewed.',9,1),(81,'2014-12-16 07:41:26','26','House',2,'Changed season_count.',7,1),(82,'2014-12-16 07:41:50','26','House',2,'No fields changed.',7,1),(83,'2014-12-16 07:43:29','1165','House Season 1 Episode 1',2,'Changed viewed.',9,1),(84,'2014-12-16 20:17:32','1166','House Season 1 Episode 2',2,'Changed viewed.',9,1),(85,'2014-12-16 20:18:41','2','Workout 2014-12-16 02:41:53-05:00',1,'',15,1),(86,'2014-12-16 20:18:51','2','Workout 2014-12-16 07:41:53+00:00',3,'',15,1),(87,'2014-12-20 17:45:38','12','Psycho-Pass Season 2 Episode 1',2,'Changed viewed.',9,1),(88,'2014-12-20 17:45:38','13','Psycho-Pass Season 2 Episode 2',2,'Changed viewed.',9,1),(89,'2014-12-20 17:47:51','1167','House Season 1 Episode 3',2,'Changed viewed.',9,1),(90,'2014-12-20 18:19:04','7','Introduction to Economics Lecture 5',2,'Changed viewed.',11,1),(91,'2014-12-23 04:27:04','27','White Collar',1,'',7,3),(92,'2014-12-23 04:29:29','27','White Collar',3,'',7,3),(93,'2014-12-23 04:31:09','1342','White Collar Season 1 Episode 1',2,'Changed viewed.',9,3),(94,'2014-12-23 04:31:09','1343','White Collar Season 1 Episode 2',2,'Changed viewed.',9,3),(95,'2014-12-23 04:31:09','1344','White Collar Season 1 Episode 3',2,'Changed viewed.',9,3),(96,'2014-12-23 04:31:09','1345','White Collar Season 1 Episode 4',2,'Changed viewed.',9,3),(97,'2014-12-23 04:31:09','1346','White Collar Season 1 Episode 5',2,'Changed viewed.',9,3),(98,'2014-12-23 04:31:09','1347','White Collar Season 1 Episode 6',2,'Changed viewed.',9,3),(99,'2014-12-23 04:31:09','1348','White Collar Season 1 Episode 7',2,'Changed viewed.',9,3),(100,'2014-12-23 04:31:09','1349','White Collar Season 1 Episode 8',2,'Changed viewed.',9,3),(101,'2014-12-23 04:31:09','1350','White Collar Season 1 Episode 9',2,'Changed viewed.',9,3),(102,'2014-12-23 04:31:09','1351','White Collar Season 1 Episode 10',2,'Changed viewed.',9,3),(103,'2014-12-23 04:31:09','1352','White Collar Season 1 Episode 11',2,'Changed viewed.',9,3),(104,'2014-12-23 04:31:09','1353','White Collar Season 1 Episode 12',2,'Changed viewed.',9,3),(105,'2014-12-23 04:31:09','1354','White Collar Season 1 Episode 13',2,'Changed viewed.',9,3),(106,'2014-12-23 04:31:09','1355','White Collar Season 1 Episode 14',2,'Changed viewed.',9,3),(107,'2014-12-23 04:31:39','1356','White Collar Season 2 Episode 1',2,'Changed viewed.',9,3),(108,'2014-12-23 04:31:39','1357','White Collar Season 2 Episode 2',2,'Changed viewed.',9,3),(109,'2014-12-23 04:31:39','1358','White Collar Season 2 Episode 3',2,'Changed viewed.',9,3),(110,'2014-12-23 04:31:39','1359','White Collar Season 2 Episode 4',2,'Changed viewed.',9,3),(111,'2014-12-23 04:31:39','1360','White Collar Season 2 Episode 5',2,'Changed viewed.',9,3),(112,'2014-12-23 04:31:39','1361','White Collar Season 2 Episode 6',2,'Changed viewed.',9,3),(113,'2014-12-23 04:31:39','1362','White Collar Season 2 Episode 7',2,'Changed viewed.',9,3),(114,'2014-12-23 04:31:39','1363','White Collar Season 2 Episode 8',2,'Changed viewed.',9,3),(115,'2014-12-23 04:31:39','1364','White Collar Season 2 Episode 9',2,'Changed viewed.',9,3),(116,'2014-12-23 04:31:39','1365','White Collar Season 2 Episode 10',2,'Changed viewed.',9,3),(117,'2014-12-23 04:31:39','1366','White Collar Season 2 Episode 11',2,'Changed viewed.',9,3),(118,'2014-12-23 04:31:39','1367','White Collar Season 2 Episode 12',2,'Changed viewed.',9,3),(119,'2014-12-23 04:31:39','1368','White Collar Season 2 Episode 13',2,'Changed viewed.',9,3),(120,'2014-12-23 04:31:39','1369','White Collar Season 2 Episode 14',2,'Changed viewed.',9,3),(121,'2014-12-23 04:31:39','1370','White Collar Season 2 Episode 15',2,'Changed viewed.',9,3),(122,'2014-12-23 04:31:39','1371','White Collar Season 2 Episode 16',2,'Changed viewed.',9,3),(125,'2014-12-24 04:15:14','4','DailySurvey object',1,'',25,1),(126,'2014-12-24 04:15:23','4','DailySurvey object',3,'',25,1),(127,'2014-12-24 04:15:23','3','DailySurvey object',3,'',25,1),(128,'2014-12-24 04:15:23','2','DailySurvey object',3,'',25,1),(129,'2014-12-24 04:15:23','1','DailySurvey object',3,'',25,1),(130,'2014-12-24 04:16:47','5','DailySurvey object',1,'',25,1),(131,'2014-12-24 04:40:28','5','DailySurvey object',2,'No fields changed.',25,1),(132,'2014-12-24 05:02:48','313','Glee Season 3 Episode 14',2,'Changed viewed.',9,1),(133,'2014-12-24 05:02:48','314','Glee Season 3 Episode 15',2,'Changed viewed.',9,1),(134,'2014-12-24 05:02:48','315','Glee Season 3 Episode 16',2,'Changed viewed.',9,1),(135,'2014-12-24 05:02:48','316','Glee Season 3 Episode 17',2,'Changed viewed.',9,1),(136,'2014-12-24 05:02:48','317','Glee Season 3 Episode 18',2,'Changed viewed.',9,1),(137,'2014-12-24 05:02:48','318','Glee Season 3 Episode 19',2,'Changed viewed.',9,1),(138,'2014-12-24 05:03:04','516','Reign Season 1 Episode 2',2,'Changed viewed.',9,1),(139,'2014-12-24 05:05:09','1','Psycho-Pass',2,'Changed active.',7,1),(140,'2014-12-24 05:05:09','4','The IT Crowd',2,'Changed active.',7,1),(141,'2014-12-24 05:05:09','5','Fairy Tail',2,'Changed active.',7,1),(142,'2014-12-24 05:05:09','6','Glee',2,'Changed active.',7,1),(143,'2014-12-24 05:05:09','12','Reign',2,'Changed active.',7,1),(144,'2014-12-24 05:05:09','26','House',2,'Changed active.',7,1),(145,'2014-12-24 18:41:31','14','Psycho-Pass Season 2 Episode 3',2,'Changed viewed.',9,1),(146,'2014-12-24 18:42:06','225','Fairy Tail Season 1 Episode 18',2,'Changed viewed.',9,1),(147,'2014-12-24 18:42:06','226','Fairy Tail Season 1 Episode 19',2,'Changed viewed.',9,1),(148,'2014-12-24 18:42:06','227','Fairy Tail Season 1 Episode 20',2,'Changed viewed.',9,1),(149,'2014-12-24 18:42:06','228','Fairy Tail Season 1 Episode 21',2,'Changed viewed.',9,1),(150,'2014-12-24 18:42:06','229','Fairy Tail Season 1 Episode 22',2,'Changed viewed.',9,1),(151,'2014-12-24 18:42:06','230','Fairy Tail Season 1 Episode 23',2,'Changed viewed.',9,1),(152,'2014-12-24 23:22:09','1','Unit object',1,'',26,3),(153,'2014-12-24 23:23:00','2','Unit object',1,'',26,3),(154,'2014-12-24 23:26:31','1','Conversion object',1,'',27,3),(155,'2014-12-24 23:33:22','1','Caffeine',1,'',28,3),(156,'2014-12-24 23:38:07','3','Milligram',1,'',26,3),(157,'2014-12-24 23:40:56','1','Content object',1,'',29,3),(158,'2014-12-25 00:28:19','1','Caffeine object',1,'',30,3),(159,'2014-12-25 06:03:46','2','Workout 2014-12-24 16:30:00-05:00',1,'',15,3),(160,'2014-12-25 06:04:37','13','Bench Press - 12x75',1,'',17,3),(161,'2014-12-25 06:04:45','14','Bench Press - 12x75',1,'',17,3),(162,'2014-12-25 06:04:52','15','Bench Press - 12x75',1,'',17,3),(163,'2014-12-25 06:05:03','16','Squats - 8x75',1,'',17,3),(164,'2014-12-25 06:05:11','17','Squats - 8x75',1,'',17,3),(165,'2014-12-25 06:05:17','18','Squats - 8x75',1,'',17,3),(166,'2014-12-25 06:05:24','19','Deadlift - 4x75',1,'',17,3),(167,'2014-12-25 06:05:31','20','Deadlift - 4x75',1,'',17,3),(168,'2014-12-25 06:05:38','21','Deadlift - 4x75',1,'',17,3),(169,'2014-12-25 06:06:43','7','2014-12-24',1,'',25,3),(170,'2014-12-25 06:06:59','7','2014-12-24',3,'',25,3),(171,'2014-12-25 06:07:42','5','Walk 2014-12-25 12:30:00-05:00',1,'',19,3),(172,'2014-12-25 15:25:55','2','Caffeine object',1,'',30,3),(173,'2014-12-25 17:38:09','4','Ice Cube Tray',1,'',26,3),(174,'2014-12-25 17:38:23','5','Ice Cube',1,'',26,3),(175,'2014-12-25 17:39:45','2','Conversion object',1,'',27,3),(176,'2014-12-25 17:40:03','2','Content object',1,'',29,3),(177,'2014-12-25 17:41:27','3','Caffeine object',1,'',30,3),(178,'2014-12-25 19:43:56','4','Caffeine object',1,'',30,3),(179,'2014-12-26 15:39:58','5','Caffeine object',1,'',30,3),(180,'2014-12-26 15:39:58','6','Caffeine object',1,'',30,3),(183,'2014-12-26 15:41:20','8','2014-12-25',1,'',25,3),(184,'2014-12-26 17:40:34','5','Caffeine object',3,'',30,3),(185,'2014-12-26 17:40:57','7','Caffeine object',1,'',30,3),(186,'2014-12-26 21:14:04','8','Caffeine object',1,'',30,3),(187,'2014-12-26 22:38:26','9','Caffeine object',1,'',30,3),(188,'2014-12-26 22:49:55','3','Workout 2014-12-26 17:30:00-05:00',1,'',15,3),(189,'2014-12-26 22:54:40','3','Workout 2014-12-26 17:30:00-05:00',2,'Added exercise set \"Deadlift - 8x75\". Added exercise set \"Deadlift - 8x75\". Added exercise set \"Deadlift - 8x75\".',15,3),(190,'2014-12-26 23:00:15','1','The Kitchen at 1021 Pennsylvania Ave East, Apartment 3',1,'',31,3),(191,'2014-12-26 23:02:54','1','The Kitchen at 1021 Pennsylvania Ave East, Apartment 3',2,'Added pantry item \"PantryItem object\". Added pantry item \"PantryItem object\".',31,3),(192,'2014-12-26 23:05:03','6','Unit',1,'',26,3),(193,'2014-12-26 23:05:17','6','Unit',2,'Changed description.',26,3),(194,'2014-12-26 23:05:30','7','Pound',1,'',26,3),(195,'2014-12-26 23:05:58','8','Stalk',1,'',26,3),(196,'2014-12-26 23:11:33','1','The Kitchen at 1021 Pennsylvania Ave East, Apartment 3',2,'No fields changed.',31,3),(197,'2014-12-26 23:13:40','9','Gallon',1,'',26,3),(198,'2014-12-26 23:13:43','1','The Kitchen at 1021 Pennsylvania Ave East, Apartment 3',2,'Added pantry item \"PantryItem object\". Added pantry item \"PantryItem object\".',31,3),(199,'2014-12-26 23:14:42','1','The Kitchen at 1021 Pennsylvania Ave East, Apartment 3',2,'Added pantry item \"PantryItem object\".',31,3),(200,'2014-12-26 23:16:25','3','Workout 2014-12-26 17:30:00-05:00',2,'Added exercise set \"Squats - 8x75\". Added exercise set \"Squats - 8x75\". Added exercise set \"Squats - 8x75\".',15,3),(201,'2014-12-26 23:22:28','1','Juice',1,'',35,3),(202,'2014-12-26 23:23:38','1','Juice',2,'Added recipe food \"RecipeFood object\". Added recipe food \"RecipeFood object\". Added recipe food \"RecipeFood object\".',35,3),(203,'2014-12-26 23:25:02','10','Cup',1,'',26,3),(204,'2014-12-26 23:25:17','3','Conversion object',1,'',27,3),(205,'2014-12-26 23:26:07','2','Smoothie',1,'',35,3),(206,'2014-12-26 23:31:16','3','Workout 2014-12-26 17:30:00-05:00',2,'Added exercise set \"Bench Press - 12x75\". Added exercise set \"Bench Press - 12x75\". Added exercise set \"Bench Press - 12x75\".',15,3),(207,'2014-12-27 02:10:15','10','Caffeine object',1,'',30,3),(208,'2014-12-27 16:28:40','11','Caffeine object',1,'',30,3),(209,'2014-12-27 16:28:49','12','Caffeine object',1,'',30,3),(210,'2014-12-27 16:42:25','11','Gram',1,'',26,3),(211,'2014-12-27 16:42:31','1','FoodWeight object',1,'',37,3),(212,'2014-12-27 16:44:32','4','Conversion object',1,'',27,3),(213,'2014-12-27 16:44:32','5','Conversion object',1,'',27,3),(214,'2014-12-27 16:44:44','4','Conversion object',3,'',27,3),(215,'2014-12-27 16:44:52','6','Conversion object',1,'',27,3),(216,'2014-12-27 16:44:57','6','Conversion object',3,'',27,3),(217,'2014-12-27 16:59:49','2','FoodWeight object',1,'',37,3),(218,'2014-12-27 17:08:04','3','FoodWeight object',1,'',37,3),(219,'2014-12-27 17:08:41','3','PantryItem object',2,'Changed unit.',32,3),(220,'2014-12-27 17:54:08','1','Juice',2,'Changed unit for recipe food \"RecipeFood object\".',35,3),(221,'2014-12-27 18:17:15','1','The Kitchen at 1021 Pennsylvania Ave East, Apartment 3',2,'Changed quantity for pantry item \"PantryItem object\".',31,3),(222,'2014-12-27 18:18:07','1','The Kitchen at 1021 Pennsylvania Ave East, Apartment 3',2,'Changed quantity for pantry item \"PantryItem object\".',31,3),(223,'2014-12-27 18:37:36','4','FoodWeight object',1,'',37,3),(224,'2014-12-27 18:48:23','5','Conversion object',2,'Changed multiplier.',27,3),(225,'2014-12-27 18:53:43','5','FoodWeight object',1,'',37,3),(226,'2014-12-27 18:54:48','5','FoodWeight object',2,'Changed multiplier and unit.',37,3),(227,'2014-12-27 18:58:37','1','Meal object',1,'',38,3),(228,'2014-12-27 19:02:20','3','Juice',1,'',38,3),(229,'2014-12-27 19:02:54','4','Juice',1,'',38,3),(230,'2014-12-27 19:04:21','4','Juice',3,'',38,3),(231,'2014-12-27 19:04:39','4','Juice',3,'',38,3),(232,'2014-12-27 19:04:50','3','Juice',3,'',38,3),(233,'2014-12-27 19:04:50','1','Juice',3,'',38,3),(234,'2014-12-27 19:05:23','5','Juice',1,'',38,3),(235,'2014-12-27 19:05:31','5','Juice',3,'',38,3),(236,'2014-12-27 19:05:40','6','Juice',1,'',38,3),(237,'2014-12-27 19:05:56','7','Smoothie',1,'',38,3),(238,'2014-12-27 20:15:44','9','2014-12-26',1,'',25,3),(239,'2014-12-27 20:16:56','10','2014-12-27',1,'',25,3),(240,'2014-12-27 20:22:27','1','Category object',1,'',39,3),(241,'2014-12-27 20:23:23','1','FuturePurchase object',1,'',40,3),(242,'2014-12-27 20:23:31','2','FuturePurchase object',1,'',40,3),(243,'2014-12-27 20:23:59','3','Keyboard',1,'',40,3),(244,'2014-12-27 20:24:11','4','Mouse',1,'',40,3),(245,'2014-12-27 20:24:23','5','Office chair',1,'',40,3),(252,'2014-12-27 20:35:53','1','Rent',1,'',42,3),(253,'2014-12-27 20:38:27','2','Federal loan payment',1,'',42,3),(254,'2014-12-27 20:38:43','3','Private loan payment',1,'',42,3),(255,'2014-12-27 20:38:57','4','Carnegie Mellon loan payment',1,'',42,3),(256,'2014-12-27 20:41:36','1','Food',1,'',43,3),(257,'2014-12-27 20:42:09','2','Travel',1,'',43,3),(258,'2014-12-27 20:42:19','3','Dog',1,'',43,3),(259,'2014-12-27 20:42:28','4','Spending',1,'',43,3),(260,'2014-12-27 20:42:28','5','Spending',1,'',43,3),(261,'2014-12-27 20:42:44','5','Spending',3,'',43,3),(262,'2014-12-27 22:06:12','13','Caffeine object',1,'',30,3),(263,'2014-12-27 22:06:21','14','Caffeine object',1,'',30,3),(264,'2014-12-27 22:06:21','15','Caffeine object',1,'',30,3),(265,'2014-12-27 22:07:15','6','Walk 2014-12-27 16:52:13-05:00',1,'',19,3),(266,'2014-12-27 22:07:15','7','Walk 2014-12-27 16:52:13-05:00',1,'',19,3),(267,'2014-12-27 22:07:51','1','Sleep object',1,'',21,3),(268,'2014-12-27 22:08:47','1','Sleep object',2,'No fields changed.',21,3),(269,'2014-12-27 22:18:12','5','Cable internet',1,'',42,3),(270,'2014-12-27 22:21:03','6','Netflix',1,'',42,3),(271,'2014-12-27 22:21:27','7','Spotify',1,'',42,3),(272,'2014-12-27 23:06:21','15','Caffeine object',3,'',30,3),(273,'2014-12-27 23:38:44','16','Caffeine object',1,'',30,3),(274,'2014-12-27 23:38:55','16','Caffeine object',2,'Changed datetime.',30,3),(275,'2014-12-28 18:15:40','17','Caffeine object',1,'',30,3),(276,'2014-12-28 18:15:52','18','Caffeine object',1,'',30,3),(277,'2014-12-28 18:16:20','10','2014-12-27',2,'Changed nap, lunch, dinner, television, piano, evening_caffeine and calm.',25,3),(278,'2014-12-28 18:16:26','10','2014-12-27',2,'Changed calm.',25,3),(279,'2014-12-28 18:16:49','8','Juice',1,'',38,3),(280,'2014-12-28 18:17:04','9','Smoothie',1,'',38,3),(281,'2014-12-28 20:00:27','19','Caffeine object',1,'',30,3),(282,'2014-12-28 20:01:06','11','2014-12-28',1,'',25,3),(283,'2014-12-28 22:19:37','11','2014-12-28',2,'Changed peak_happiness and peak_energy.',25,3),(284,'2014-12-28 22:19:40','11','2014-12-28',2,'No fields changed.',25,3),(285,'2014-12-28 22:20:17','2','Sleep object',1,'',21,3),(286,'2014-12-28 23:22:02','20','Caffeine object',1,'',30,3),(287,'2014-12-28 23:49:17','21','Caffeine object',1,'',30,3),(288,'2014-12-29 02:45:20','22','Caffeine object',1,'',30,3),(289,'2014-12-29 04:24:38','517','Reign Season 1 Episode 3',2,'Changed viewed.',9,3),(290,'2014-12-29 04:24:38','518','Reign Season 1 Episode 4',2,'Changed viewed.',9,3),(291,'2014-12-29 04:25:18','233','Fairy Tail Season 1 Episode 26',2,'Changed viewed.',9,3),(292,'2014-12-29 04:25:18','234','Fairy Tail Season 1 Episode 27',2,'Changed viewed.',9,3),(293,'2014-12-29 06:41:35','23','Caffeine object',1,'',30,3),(294,'2014-12-29 11:17:05','24','Caffeine object',1,'',30,3),(295,'2014-12-29 11:20:23','25','Caffeine object',1,'',30,3),(296,'2014-12-29 11:37:58','26','Caffeine object',1,'',30,3),(297,'2014-12-29 19:57:00','27','Caffeine object',1,'',30,3),(298,'2014-12-29 19:57:22','28','Caffeine object',1,'',30,3),(299,'2014-12-30 00:17:24','29','Caffeine object',1,'',30,3),(300,'2014-12-30 00:56:25','30','Caffeine object',1,'',30,3),(301,'2014-12-30 01:02:56','31','Caffeine object',1,'',30,3),(302,'2014-12-30 01:15:35','32','Caffeine object',1,'',30,3),(303,'2014-12-30 02:48:59','33','Caffeine object',1,'',30,3),(304,'2014-12-30 18:21:23','34','Caffeine object',1,'',30,3),(305,'2014-12-30 18:21:35','35','Caffeine object',1,'',30,3),(306,'2014-12-30 18:52:59','36','Caffeine object',1,'',30,3),(307,'2014-12-30 18:54:41','10','Smoothie',1,'',38,3),(308,'2014-12-30 18:55:07','11','Juice',1,'',38,3),(309,'2014-12-30 18:55:35','10','Smoothie',2,'Changed datetime.',38,3),(310,'2014-12-30 18:57:29','12','Juice',1,'',38,3),(311,'2014-12-30 20:01:38','37','Caffeine object',1,'',30,3),(312,'2014-12-30 21:32:11','38','Caffeine object',1,'',30,3),(313,'2014-12-30 22:38:11','12','2014-12-29',1,'',25,3),(314,'2014-12-31 01:21:40','39','Caffeine object',1,'',30,3),(315,'2014-12-31 01:53:29','40','Caffeine object',1,'',30,3),(316,'2015-01-01 00:50:54','41','Caffeine object',1,'',30,3),(317,'2015-01-01 00:51:02','42','Caffeine object',1,'',30,3),(318,'2015-01-01 00:51:13','43','Caffeine object',1,'',30,3),(319,'2015-01-01 00:51:24','44','Caffeine object',1,'',30,3),(320,'2015-01-01 00:51:36','45','Caffeine object',1,'',30,3),(321,'2015-01-01 00:52:10','13','2014-12-30',1,'',25,3),(322,'2015-01-01 00:52:27','14','2014-12-31',1,'',25,3),(323,'2015-01-01 00:52:48','1','Cell phone',2,'No fields changed.',40,3),(324,'2015-01-01 00:52:55','6','Laptop case',1,'',40,3),(325,'2015-01-01 02:06:06','4','Workout 2014-12-31 21:04:29-05:00',1,'',15,3),(326,'2015-01-01 02:07:41','1','HappinessMoment object',1,'',44,3),(327,'2015-01-01 02:12:05','4','Workout 2014-12-31 21:04:29-05:00',2,'Added exercise set \"Squats - 8x75\". Added exercise set \"Squats - 8x75\".',15,3),(328,'2015-01-01 02:15:20','4','Workout 2014-12-31 21:04:29-05:00',2,'Added exercise set \"Squats - 8x75\".',15,3),(329,'2015-01-01 02:22:00','4','Workout 2014-12-31 21:04:29-05:00',2,'Added exercise set \"Deadlift - 4x75\". Added exercise set \"Deadlift - 4x75\".',15,3),(330,'2015-01-01 02:26:09','4','Workout 2014-12-31 21:04:29-05:00',2,'Added exercise set \"Deadlift - 4x75\".',15,3),(331,'2015-01-01 02:29:06','1','NoteCard object',1,'',45,3),(332,'2015-01-01 02:29:16','2','NoteCard object',1,'',45,3),(333,'2015-01-01 02:41:51','1','Author object',1,'',46,3),(334,'2015-01-01 02:42:00','1','Book object',1,'',47,3),(335,'2015-01-01 02:42:55','1','Think and Grow Rich',2,'No fields changed.',47,3),(336,'2015-01-01 02:43:19','2','George  Leonard',1,'',46,3),(337,'2015-01-01 02:43:21','2','Mastery: The Keys to Success and Long-Term Fulfillment ',1,'',47,3),(338,'2015-01-01 02:43:57','3','Melvin Helitzer',1,'',46,3),(339,'2015-01-01 02:43:58','3','Comedy Writing Secrets',1,'',47,3),(340,'2015-01-01 02:44:28','4','Carol Fleming',1,'',46,3),(341,'2015-01-01 02:44:30','4','The Sound of Your Voice',1,'',47,3),(342,'2015-01-01 02:46:44','3','Use up what you have first',1,'',45,3),(343,'2015-01-01 02:47:00','4','I can leave Warren when I have a plan and a living',1,'',45,3),(344,'2015-01-01 02:47:15','5','I can leave warren when I find myself and by debt is paid off',1,'',45,3),(345,'2015-01-01 02:47:24','5','I can leave warren when I find myself and my debt is paid off',2,'Changed subject.',45,3),(346,'2015-01-01 02:47:33','6','Develop a reputation',1,'',45,3),(347,'2015-01-01 02:47:42','7','Stay in one place',1,'',45,3),(348,'2015-01-01 02:47:48','8','Exercise good posture',1,'',45,3),(349,'2015-01-01 02:48:07','9','Research, mathematics, memory, and language',1,'',45,3),(350,'2015-01-01 02:48:19','10','I have a lot to say, and I love people',1,'',45,3),(351,'2015-01-01 02:48:44','11','Fill the walls with cards',1,'',45,3),(352,'2015-01-01 03:01:23','1','Train',1,'',50,3),(353,'2015-01-01 03:02:49','1','Hey, Soul Sister',1,'',51,3),(354,'2015-01-01 03:03:03','1','Hey, Soul Sister',1,'',52,3),(355,'2015-01-01 03:03:41','2','Florence + The Machine',1,'',50,3),(356,'2015-01-01 03:03:58','2','Lungs',1,'',51,3),(357,'2015-01-01 03:04:05','2','Dog Days Are Over',1,'',52,3),(358,'2015-01-01 03:13:40','12','Things I love',1,'',45,3),(359,'2015-01-01 03:14:17','13','My goal with women',1,'',45,3),(360,'2015-01-01 03:15:18','14','Type of girl I want',1,'',45,3),(361,'2015-01-01 03:15:29','14','Qualities I want in a girl',2,'Changed subject.',45,3),(362,'2015-01-01 03:19:05','2','Clothing',1,'',39,3),(363,'2015-01-01 03:19:12','3','Furniture',1,'',39,3),(364,'2015-01-01 03:19:21','4','Stationary',1,'',39,3),(365,'2015-01-01 03:19:27','5','Trinkets',1,'',39,3),(366,'2015-01-01 03:19:31','6','Books',1,'',39,3),(367,'2015-01-01 03:50:54','1','Lenovo T430',1,'',53,3),(368,'2015-01-01 03:52:11','2','Amazon Bluetooth Speaker',1,'',53,3),(369,'2015-01-01 03:53:01','2','Amazon Bluetooth Speaker',2,'Changed condition.',53,3),(370,'2015-01-01 03:53:08','1','Lenovo T430',2,'Changed condition.',53,3),(371,'2015-01-01 03:53:43','3','Apple iPhone',1,'',53,3),(372,'2015-01-01 04:00:05','2','HappinessMoment object',1,'',44,3),(373,'2015-01-01 17:36:55','46','Caffeine object',1,'',30,3),(374,'2015-01-01 17:37:01','47','Caffeine object',1,'',30,3),(375,'2015-01-01 19:41:33','48','Caffeine object',1,'',30,3),(376,'2015-01-01 21:36:16','235','Fairy Tail Season 1 Episode 28',2,'Changed viewed.',9,3),(377,'2015-01-01 21:36:16','236','Fairy Tail Season 1 Episode 29',2,'Changed viewed.',9,3),(378,'2015-01-01 21:36:16','237','Fairy Tail Season 1 Episode 30',2,'Changed viewed.',9,3),(379,'2015-01-01 21:36:16','238','Fairy Tail Season 1 Episode 31',2,'Changed viewed.',9,3),(380,'2015-01-01 23:55:55','1','LinearMood object',1,'',54,3),(381,'2015-01-01 23:58:04','2','LinearMood object',2,'Changed mood.',54,3),(382,'2015-01-01 23:58:04','2','LinearMood object',2,'No fields changed.',54,3),(383,'2015-01-02 07:09:52','3','HappinessMoment object',1,'',44,3),(384,'2015-01-02 07:10:17','1170','House Season 1 Episode 6',2,'Changed viewed.',9,3),(385,'2015-01-02 07:10:17','1171','House Season 1 Episode 7',2,'Changed viewed.',9,3),(386,'2015-01-02 07:10:17','1172','House Season 1 Episode 8',2,'Changed viewed.',9,3),(387,'2015-01-02 07:10:17','1173','House Season 1 Episode 9',2,'Changed viewed.',9,3),(388,'2015-01-02 17:06:40','56','Caffeine object',1,'',30,3),(389,'2015-01-02 17:09:41','15','2015-01-01',1,'',25,3),(390,'2015-01-02 17:09:57','16','2015-01-02',1,'',25,3),(391,'2015-01-02 17:17:46','1','Wipe the kitchen countertops',1,'',55,3),(392,'2015-01-02 17:17:52','2','Put away the dishes',1,'',55,3),(393,'2015-01-02 17:19:04','1','Bedroom',1,'',56,3),(394,'2015-01-02 17:19:07','2','Kitchen',1,'',56,3),(395,'2015-01-02 17:19:09','3','Bathroom',1,'',56,3),(396,'2015-01-02 17:19:14','4','Living room',1,'',56,3),(397,'2015-01-02 17:19:25','2','Put away the dishes',2,'Changed room.',55,3),(398,'2015-01-02 17:19:38','1','Wipe the kitchen countertops',2,'Changed room.',55,3),(399,'2015-01-02 17:25:24','2','Put away the dishes',2,'Changed frequency.',55,3),(400,'2015-01-02 17:25:29','1','Wipe the kitchen countertops',2,'Changed frequency.',55,3),(401,'2015-01-02 17:25:48','3','Vacuum',1,'',55,3),(402,'2015-01-02 17:28:19','4','Clean out cupboard',1,'',55,3),(403,'2015-01-02 17:28:39','5','Wipe down shower',1,'',55,3),(404,'2015-01-02 17:28:57','6','Do laundry',1,'',55,3),(405,'2015-01-02 17:29:17','7','Filter boxes',1,'',55,3),(406,'2015-01-02 17:39:19','5','Room agnostic',1,'',56,3),(407,'2015-01-02 17:39:33','8','Take out the trash',1,'',55,3),(408,'2015-01-03 22:53:01','16','Rotini Pasta',1,'',20,3),(409,'2015-01-03 22:53:06','17','Cream Cheese',1,'',20,3),(410,'2015-01-03 22:53:17','12','Box',1,'',26,3),(411,'2015-01-03 22:53:34','13','Spoon',1,'',26,3),(412,'2015-01-03 22:53:44','18','Tomato Paste',1,'',20,3),(413,'2015-01-03 22:53:59','14','Can',1,'',26,3),(414,'2015-01-03 22:54:07','19','Onion',1,'',20,3),(415,'2015-01-03 22:55:12','3','Tomato and cream pasta',1,'',35,3),(416,'2015-01-04 01:07:44','21','Tomato and cream pasta',1,'',38,3),(417,'2015-01-04 01:08:07','22','Tomato and cream pasta',1,'',38,3),(418,'2015-01-04 01:20:33','20','Pancake Flour',1,'',20,3),(419,'2015-01-04 01:21:12','4','Pancakes',1,'',35,3),(420,'2015-01-04 01:49:29','3','Squat',2,'Changed name.',16,3),(421,'2015-01-04 01:55:20','3','Madeon',1,'',50,3),(422,'2015-01-04 01:55:31','3','Icarus',1,'',51,3),(423,'2015-01-04 01:55:43','3','Icarus',1,'',52,3),(424,'2015-01-04 01:56:01','4','The City',1,'',51,3),(425,'2015-01-04 01:56:08','4','The City',1,'',52,3),(426,'2015-01-04 02:12:08','15','Programmers are the modern day composers',1,'',45,3),(427,'2015-01-04 02:15:20','4','The City',2,'Changed rating.',52,3),(428,'2015-01-04 02:15:29','4','The City',2,'No fields changed.',52,3),(429,'2015-01-04 06:25:13','21','Brown Rice',1,'',20,3),(430,'2015-01-04 06:25:23','22','Blank Beans',1,'',20,3),(431,'2015-01-04 06:25:54','5','Rice with beans, cheese, and eggs',1,'',35,3),(432,'2015-01-04 06:26:06','27','Rice with beans, cheese, and eggs',1,'',38,3),(433,'2015-01-04 06:41:12','1','Bobs Red Mill Honey Oat Granola, 12-Ounce (Pack of 4)',1,'',60,3),(434,'2015-01-04 06:41:18','1','Bobs Red Mill Honey Oat Granola, 12-Ounce (Pack of 4)',2,'No fields changed.',60,3),(435,'2015-01-04 06:51:57','23','Yogurt',1,'',20,3),(436,'2015-01-04 06:52:03','24','Granola',1,'',20,3),(437,'2015-01-04 06:52:10','25','Maple Syrup',1,'',20,3),(438,'2015-01-04 06:52:19','15','Tablespoon',1,'',26,3),(439,'2015-01-04 06:52:21','6','Yogurt and granola',1,'',35,3),(440,'2015-01-04 06:55:04','1','Saturday Night Fever',1,'',61,3),(441,'2015-01-04 06:55:14','2','The Wolf of Wallstreet',1,'',61,3),(442,'2015-01-04 06:55:18','2','The Wolf of Wallstreet',2,'Changed rating.',61,3),(443,'2015-01-04 18:24:57','26','Clif Bar',1,'',20,3),(444,'2015-01-04 18:25:05','7','Clif bar',1,'',35,3),(445,'2015-01-05 16:21:35','16','Adjusting schedule backwards is near impossible',1,'',45,3),(446,'2015-01-07 21:40:10','17','A daily decision',1,'',45,3),(447,'2015-01-08 05:17:23','17','A daily decision',2,'Changed notes.',45,3),(448,'2015-01-14 19:37:42','2','Coffee',1,'',28,3),(449,'2015-01-14 19:39:02','2','Coffee',3,'',28,3),(450,'2015-01-20 16:51:50','18','Little poem about the night',1,'',45,3),(451,'2015-01-26 19:59:24','1','Program one year worth of tasks, work, and scheduling',1,'',63,3),(452,'2015-01-26 20:01:18','2','Create a schedule for the week of January 26th through February 1st, 2015',1,'',63,3),(453,'2015-01-26 20:04:05','2','Create a schedule for the week of January 26th through February 1st, 2015',2,'No fields changed.',63,3),(454,'2015-01-26 20:04:28','2','Create a schedule for the week of January 26th through February 1st, 2015',2,'Changed parent.',63,3),(455,'2015-01-26 20:04:45','3','Create a schedule for January 26th',1,'',63,3),(456,'2015-01-26 20:04:58','4','Create a schedule for January 27th',1,'',63,3),(457,'2015-01-26 20:05:57','3','Create a schedule for January 26th',2,'No fields changed.',63,3),(458,'2015-01-26 20:06:15','5','Consider all projects, contracts, clients, and prospects',1,'',63,3),(459,'2015-01-26 20:06:36','6','Check email',1,'',63,3),(460,'2015-01-26 20:07:30','7','Write an ordered list of the most important projects, contracts, clients, and prospects',1,'',63,3),(461,'2015-01-26 20:25:16','7','Write an ordered list of the most important projects, contracts, clients, and prospects',2,'Changed position.',63,3),(462,'2015-01-26 20:25:16','5','Consider all projects, contracts, clients, and prospects',2,'Changed position.',63,3),(463,'2015-01-26 20:34:45','1','Date object',1,'',64,3),(464,'2015-01-26 20:39:03','8','Fix Consumers Win shopping site',1,'',63,3),(465,'2015-01-26 20:41:40','9','Transfer tiercom to OVH',1,'',63,3),(466,'2015-01-26 20:53:33','10','Migrate Mission Bowling Club to OVH',1,'',63,3),(467,'2015-01-26 20:54:21','11','Test rlmohl.com, casademetta.org, and photosbymohl.com on OVH',1,'',63,3),(468,'2015-01-26 20:57:07','12','Test juicingwithmia.com and chain2prosper.com on OVH',1,'',63,3),(469,'2015-01-26 22:37:56','13','Clear error alert from LMS assignment creation template',1,'',63,3),(470,'2015-01-26 22:53:58','12','Test juicingwithmia.com and chain2prosper.com on OVH',2,'Changed complete.',63,3),(471,'2015-01-27 00:51:25','11','Test rlmohl.com, casademetta.org, and photosbymohl.com on OVH',2,'Changed complete.',63,3),(472,'2015-01-28 15:16:03','1','On the flight back from San Francisco',1,'',65,3),(473,'2015-01-28 15:19:39','1','On the flight back from San Francisco',2,'Changed content.',65,3),(474,'2015-01-28 15:31:50','1','On the flight back from San Francisco',2,'Changed content.',65,3),(475,'2015-01-28 15:34:33','1','On the flight back from San Francisco',2,'Changed content.',65,3),(476,'2015-01-28 15:36:59','1','On the flight back from San Francisco',2,'Changed content.',65,3),(477,'2015-01-28 15:39:43','1','On the flight back from San Francisco',2,'Changed content.',65,3),(478,'2015-01-28 15:41:27','1','On the flight back from San Francisco',2,'Changed content.',65,3),(479,'2015-01-28 15:45:07','1','On the flight back from San Francisco',2,'Changed content.',65,3),(480,'2015-01-28 15:47:26','1','On the flight back from San Francisco',2,'Changed content.',65,3),(481,'2015-01-28 15:50:34','1','On the flight back from San Francisco',2,'Changed content.',65,3),(482,'2015-01-28 15:51:21','1','On the flight back from San Francisco',2,'Changed content.',65,3),(483,'2015-01-28 15:52:51','1','On the flight back from San Francisco',2,'Changed content.',65,3),(484,'2015-01-28 15:56:47','1','On the flight back from San Francisco',2,'Changed content.',65,3),(485,'2015-01-28 16:00:02','1','On the flight back from San Francisco',2,'Changed content.',65,3),(486,'2015-01-28 16:00:34','1','The Project (on the flight back from San Francisco)',2,'Changed title.',65,3),(487,'2015-01-28 16:02:36','1','The Project (on the flight back from San Francisco)',2,'Changed content.',65,3),(488,'2015-01-28 16:03:29','1','The Project (on the flight back from San Francisco)',2,'No fields changed.',65,3),(489,'2015-02-14 22:51:17','4','Gabriel Faure',1,'',50,3),(490,'2015-02-14 22:51:19','5','Faure: Requiem & Messe Basse',1,'',51,3),(491,'2015-02-14 22:51:25','5','Faure\'s Requiem in D Minor',1,'',52,3),(492,'2015-02-18 04:23:23','10','Workout 2015-02-17 22:49:50-05:00',2,'Added exercise set \"Deadlift - 4x75\". Changed repetitions for exercise set \"Squat - 8x75\". Changed repetitions for exercise set \"Squat - 8x75\". Changed exercise for exercise set \"Squat - 8x75\". Changed repetitions for exercise set \"Deadlift - 4x75\". Changed repetitions for exercise set \"Deadlift - 4x75\".',15,3),(493,'2015-02-21 23:07:54','1','Psycho-Pass',2,'Changed active.',7,3),(494,'2015-02-21 23:07:54','4','The IT Crowd',2,'Changed active.',7,3),(495,'2015-02-21 23:07:54','6','Glee',2,'Changed active.',7,3),(496,'2015-02-21 23:07:54','28','White Collar',2,'Changed active.',7,3),(497,'2015-02-21 23:08:26','243','Fairy Tail Season 1 Episode 36',2,'Changed viewed.',9,3),(498,'2015-02-21 23:08:26','244','Fairy Tail Season 1 Episode 37',2,'Changed viewed.',9,3),(499,'2015-02-21 23:08:26','245','Fairy Tail Season 1 Episode 38',2,'Changed viewed.',9,3),(500,'2015-02-21 23:08:26','246','Fairy Tail Season 1 Episode 39',2,'Changed viewed.',9,3),(501,'2015-02-21 23:08:57','322','Glee Season 4 Episode 1',2,'Changed viewed.',9,3),(502,'2015-02-21 23:08:57','323','Glee Season 4 Episode 2',2,'Changed viewed.',9,3);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'television show','media','televisionshow'),(8,'season','media','season'),(9,'episode','media','episode'),(10,'academic course','media','academiccourse'),(11,'lecture','media','lecture'),(12,'professor','academics','professor'),(13,'university','academics','university'),(14,'term','academics','term'),(15,'workout','fitness','workout'),(16,'exercise','fitness','exercise'),(17,'exercise set','fitness','exerciseset'),(19,'walk','fitness','walk'),(20,'food','food','food'),(21,'sleep','fitness','sleep'),(25,'daily survey','schedule','dailysurvey'),(26,'unit','units','unit'),(27,'conversion','units','conversion'),(28,'chemical','units','chemical'),(29,'content','units','content'),(30,'caffeine','fitness','caffeine'),(31,'pantry','food','pantry'),(32,'pantry item','food','pantryitem'),(35,'recipe','food','recipe'),(36,'recipe food','food','recipefood'),(37,'food weight','food','foodweight'),(38,'meal','food','meal'),(39,'category','inventory','category'),(40,'future purchase','inventory','futurepurchase'),(42,'recurring bill','finances','recurringbill'),(43,'budget allocation','finances','budgetallocation'),(44,'happiness moment','psychology','happinessmoment'),(45,'note card','psychology','notecard'),(46,'author','media','author'),(47,'book','media','book'),(48,'person','core','person'),(50,'artist','media','artist'),(51,'album','media','album'),(52,'song','media','song'),(53,'possession','inventory','possession'),(54,'linear mood','psychology','linearmood'),(55,'cleaning','inventory','cleaning'),(56,'room','inventory','room'),(57,'water pitcher','fitness','waterpitcher'),(58,'linear energy','psychology','linearenergy'),(59,'substance','fitness','substance'),(60,'purchase','inventory','purchase'),(61,'movie','media','movie'),(62,'rule','psychology','rule'),(63,'task','tasks','task'),(64,'date','tasks','date'),(65,'journal','psychology','journal'),(66,'water filter','fitness','waterfilter'),(67,'conversation','psychology','conversation'),(68,'label','inventory','label'),(69,'attribute','inventory','attribute'),(70,'composer','media','composer'),(71,'node','core','node'),(72,'binary mood','psychology','binarymood'),(75,'container possession','inventory','containerpossession'),(76,'location','inventory','location'),(77,'activity','tasks','activity'),(79,'atom','schedule','atom'),(80,'question','academics','question'),(81,'project','projects','project');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2014-12-14 04:35:02'),(2,'auth','0001_initial','2014-12-14 04:35:06'),(3,'admin','0001_initial','2014-12-14 04:35:08'),(4,'sessions','0001_initial','2014-12-14 04:35:08'),(5,'media','0001_initial','2014-12-14 04:41:57'),(6,'media','0002_auto_20141214_0442','2014-12-14 04:42:46'),(7,'media','0003_televisionshow_season_count','2014-12-14 04:50:16'),(8,'media','0004_academiccourse_lecture','2014-12-14 18:36:08'),(9,'academics','0001_initial','2014-12-14 18:42:00'),(10,'academics','0002_term_university','2014-12-14 18:43:28'),(11,'academics','0003_auto_20141214_1846','2014-12-14 18:46:09'),(12,'media','0005_auto_20141214_1849','2014-12-14 18:49:41'),(13,'media','0006_academiccourse_term_taught','2014-12-14 18:50:44'),(14,'media','0007_lecture_viewed','2014-12-14 18:59:14'),(15,'media','0008_academiccourse_completed','2014-12-14 19:09:44'),(16,'fitness','0001_initial','2014-12-15 02:05:04'),(17,'fitness','0002_auto_20141215_0209','2014-12-15 02:09:52'),(18,'fitness','0003_auto_20141215_0211','2014-12-15 02:11:28'),(19,'fitness','0004_auto_20141215_1558','2014-12-15 15:58:06'),(20,'food','0001_initial','2014-12-20 20:52:25'),(21,'fitness','0005_auto_20141223_1857','2014-12-23 23:57:19'),(22,'fitness','0006_auto_20141223_1900','2014-12-24 00:00:51'),(23,'fitness','0007_auto_20141223_1903','2014-12-24 00:03:26'),(24,'schedule','0001_initial','2014-12-24 03:31:05'),(25,'schedule','0002_auto_20141223_2247','2014-12-24 03:47:16'),(26,'schedule','0003_auto_20141223_2251','2014-12-24 03:51:46'),(27,'schedule','0004_auto_20141223_2256','2014-12-24 03:56:38'),(28,'schedule','0005_auto_20141223_2300','2014-12-24 04:00:20'),(29,'schedule','0006_auto_20141223_2306','2014-12-24 04:06:41'),(30,'schedule','0007_auto_20141223_2308','2014-12-24 04:08:18'),(31,'schedule','0008_auto_20141223_2314','2014-12-24 04:14:58'),(32,'schedule','0009_dailysurvey_calm','2014-12-24 04:18:42'),(33,'schedule','0010_dailysurvey_date','2014-12-24 04:38:13'),(34,'media','0009_televisionshow_active','2014-12-24 05:03:46'),(35,'units','0001_initial','2014-12-24 23:20:54'),(36,'units','0002_conversion','2014-12-24 23:26:03'),(37,'units','0003_chemical','2014-12-24 23:32:53'),(38,'units','0004_content','2014-12-24 23:38:20'),(39,'units','0005_content_chemical','2014-12-24 23:40:37'),(40,'fitness','0008_auto_20141224_1926','2014-12-25 00:26:39'),(41,'fitness','0009_auto_20141225_1249','2014-12-25 17:49:28'),(42,'fitness','0010_auto_20141226_1809','2014-12-26 23:09:30'),(43,'fitness','0011_auto_20141226_1809','2014-12-26 23:09:40'),(44,'fitness','0012_auto_20141226_1810','2014-12-26 23:10:33'),(45,'fitness','0013_auto_20141226_1811','2014-12-26 23:11:05'),(46,'fitness','0014_auto_20141226_1811','2014-12-26 23:11:11'),(47,'units','0006_auto_20141227_1144','2014-12-27 16:44:06'),(48,'food','0002_auto_20141227_1319','2014-12-27 18:19:08'),(49,'food','0003_auto_20141227_1319','2014-12-27 18:19:21'),(50,'food','0004_auto_20141227_1321','2014-12-27 18:21:30'),(51,'food','0005_meal','2014-12-27 18:58:18'),(52,'inventory','0001_initial','2014-12-27 20:21:20'),(53,'inventory','0002_auto_20141227_1526','2014-12-27 20:26:35'),(54,'finances','0001_initial','2014-12-27 20:34:30'),(55,'inventory','0003_delete_recurringbill','2014-12-27 20:39:32'),(56,'finances','0002_budgetallocation','2014-12-27 20:40:59'),(57,'fitness','0015_auto_20141227_1708','2014-12-27 22:08:42'),(58,'schedule','0011_dailysurvey_peak_happiness','2014-12-28 22:18:54'),(59,'schedule','0012_dailysurvey_peak_energy','2014-12-28 22:19:26'),(60,'psychology','0001_initial','2015-01-01 02:05:38'),(61,'psychology','0002_notecard','2015-01-01 02:28:11'),(62,'psychology','0003_auto_20141231_2129','2015-01-01 02:29:03'),(63,'media','0010_author_book','2015-01-01 02:41:11'),(64,'core','0001_initial','2015-01-01 02:54:37'),(65,'media','0011_artist','2015-01-01 02:54:37'),(66,'media','0012_auto_20141231_2200','2015-01-01 03:00:50'),(67,'media','0013_artist','2015-01-01 03:01:00'),(68,'media','0014_album_song','2015-01-01 03:02:01'),(69,'inventory','0004_possession','2015-01-01 03:49:33'),(70,'inventory','0005_auto_20141231_2252','2015-01-01 03:52:51'),(71,'psychology','0004_linearmood','2015-01-01 23:55:15'),(72,'inventory','0006_cleaning','2015-01-02 17:16:58'),(73,'inventory','0007_auto_20150102_1218','2015-01-02 17:18:47'),(74,'inventory','0008_auto_20150102_1225','2015-01-02 17:25:17'),(75,'fitness','0016_auto_20150102_1230','2015-01-02 17:31:17'),(76,'fitness','0017_auto_20150102_1231','2015-01-02 17:31:23'),(77,'fitness','0018_auto_20150102_1231','2015-01-02 17:31:58'),(78,'fitness','0019_auto_20150102_1232','2015-01-02 17:32:11'),(79,'fitness','0020_auto_20150102_1234','2015-01-02 17:34:43'),(80,'fitness','0021_auto_20150102_1234','2015-01-02 17:34:46'),(81,'fitness','0022_auto_20150102_1235','2015-01-02 17:35:41'),(82,'food','0006_auto_20150103_1755','2015-01-03 22:55:09'),(83,'psychology','0005_auto_20150103_1831','2015-01-03 23:31:36'),(84,'food','0007_auto_20150103_2000','2015-01-04 01:00:46'),(85,'fitness','0023_exerciseset_datetime','2015-01-04 01:43:23'),(86,'media','0015_auto_20150103_2054','2015-01-04 01:54:16'),(87,'fitness','0024_substance','2015-01-04 02:02:54'),(88,'fitness','0025_substance_substance_type','2015-01-04 02:09:31'),(89,'fitness','0026_auto_20150103_2114','2015-01-04 02:14:03'),(90,'fitness','0027_auto_20150103_2117','2015-01-04 02:17:07'),(91,'inventory','0009_purchase','2015-01-04 06:39:19'),(92,'inventory','0010_purchase_reference','2015-01-04 06:40:37'),(93,'media','0016_movie','2015-01-04 06:53:24'),(94,'media','0017_auto_20150104_0154','2015-01-04 06:54:53'),(95,'psychology','0006_auto_20150112_1620','2015-01-12 21:20:37'),(96,'tasks','0001_initial','2015-01-26 19:51:18'),(97,'tasks','0002_task_parent','2015-01-26 20:02:35'),(98,'tasks','0003_auto_20150126_1524','2015-01-26 20:24:38'),(99,'tasks','0004_date','2015-01-26 20:34:18'),(100,'tasks','0005_task_date','2015-01-26 20:38:18'),(101,'tasks','0006_task_complete','2015-01-26 20:58:41'),(102,'psychology','0007_journal','2015-01-28 15:15:07'),(103,'psychology','0008_auto_20150128_1015','2015-01-28 15:15:57'),(104,'fitness','0028_auto_20150213_2215','2015-02-14 03:15:58'),(105,'psychology','0009_conversation','2015-02-16 01:20:23'),(106,'academics','0004_auto_20150215_2034','2015-02-16 01:41:21'),(107,'academics','0005_auto_20150215_2039','2015-02-16 01:41:21'),(108,'inventory','0011_label','2015-02-21 21:27:57'),(109,'inventory','0012_possession_labels','2015-02-21 21:28:17'),(110,'inventory','0013_attribute','2015-02-21 23:22:13'),(111,'inventory','0014_possession_attributes','2015-02-21 23:22:41'),(112,'media','0018_composer','2015-02-21 23:27:26'),(113,'core','0002_node','2015-02-21 23:32:06'),(114,'inventory','0015_auto_20150221_1842','2015-02-21 23:42:25'),(115,'psychology','0010_binarymood','2015-02-22 04:07:58'),(116,'inventory','0016_auto_20150308_1210','2015-03-08 17:10:13'),(117,'inventory','0017_auto_20150308_1259','2015-03-08 17:59:20'),(118,'inventory','0018_auto_20150308_1259','2015-03-08 17:59:34'),(119,'inventory','0019_auto_20150308_1300','2015-03-08 18:00:21'),(120,'inventory','0020_location','2015-03-08 18:04:00'),(121,'inventory','0021_possession_location','2015-03-08 18:07:51'),(122,'tasks','0007_auto_20150411_1214','2015-04-11 17:14:32'),(123,'schedule','0013_intervallog','2015-04-11 22:05:56'),(124,'schedule','0014_intervallog_mood','2015-04-11 22:52:19'),(125,'schedule','0015_intervallog_notes','2015-04-11 23:01:23'),(126,'schedule','0016_auto_20150411_1847','2015-04-11 23:47:19'),(127,'core','0003_auto_20150411_1914','2015-04-12 00:14:31'),(128,'finances','0003_auto_20150411_1914','2015-04-12 00:14:31'),(129,'fitness','0029_auto_20150411_1914','2015-04-12 00:14:31'),(130,'food','0008_auto_20150411_1914','2015-04-12 00:14:31'),(131,'inventory','0022_auto_20150411_1914','2015-04-12 00:14:32'),(132,'media','0019_auto_20150411_1914','2015-04-12 00:14:32'),(133,'units','0007_auto_20150411_1914','2015-04-12 00:14:32'),(134,'schedule','0017_remove_atom_notes','2015-04-12 00:20:51'),(135,'schedule','0018_atom_notes','2015-04-12 00:20:57'),(136,'schedule','0019_atom_absolute_index','2015-04-12 01:01:17'),(137,'tasks','0008_task_atom','2015-04-12 01:05:19'),(138,'tasks','0009_auto_20150411_2007','2015-04-12 01:07:37'),(139,'tasks','0010_task_time_estimate','2015-04-12 01:11:50'),(140,'tasks','0011_task_dependencies','2015-04-12 01:27:40'),(141,'tasks','0012_remove_task_atoms','2015-04-12 02:25:33'),(142,'schedule','0020_atom_tasks','2015-04-12 02:25:40'),(143,'schedule','0021_atom_activity','2015-04-12 02:26:42'),(144,'academics','0006_question','2015-04-12 18:36:25'),(145,'projects','0001_initial','2015-04-13 04:50:58'),(146,'schedule','0022_atom_project','2015-04-13 04:54:29');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('28mjafzrpnt997bcywo9mqjajkts3f7d','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-11 20:00:12'),('2b6ojj6k586wb184svn90jtqtsos4i4p','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-02-11 15:13:39'),('2l3kgvs7qen6a783gfgsj2z2ktf54pzv','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 19:19:54'),('39b7ub1e0htpoby7ykbjua7mbbyfgrsi','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-03 17:52:07'),('429j1g7aawa2a8z4ttwu5zseb3fhg5et','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-09 21:13:43'),('4fd383ov099cggvxbqet5c6y1tg2133e','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-10 16:28:12'),('4ntqcbpubjkdihhd6nvnfy4pdz24bmff','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 20:16:19'),('4wxeac6575ijp5lu8r08sxentowrzt7m','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-09 15:39:36'),('653ynh2k82jm2ajue3bjdqw9c8kw7jt4','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-04-23 03:10:00'),('7acvg7i4mq8umt7mg350hahojq4h9tlu','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 22:05:00'),('8dy1h17rbpkg9urbq0hj7p9g4bx0rshi','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-09 17:38:50'),('a4ay1mtxkj663zkru7z0ida48gs0qk8u','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-02-28 22:50:07'),('agglfjr54pn6ia3n79guhqv6w80ftqiw','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-07 23:16:14'),('aubcdlxxqzbt5memdxetfss20zxnhx54','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-08 06:01:46'),('azekjoc9uzxypchbrio4sd5hvj6j4fmk','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-04-18 23:05:05'),('b9rip46uknb0mftgfzkgzad8fv7ilmqa','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-06 04:25:25'),('bamgm1i9jpw0jfhqpp0fbnpib54vurfb','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 01:59:38'),('btkq01xncn9dqz7kyqfk2fas186j3p7d','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-14 03:57:25'),('c8gwc5ur3z6yk0b7g0inf6qw5q5552ju','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-03 17:44:05'),('cmifj5t89zltx4m1fhqux2rcw4qnz46e','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-19 14:32:20'),('db6mp58yzqkhd8qvnewizou1n5rs8a05','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 20:16:10'),('drny0daswu6q447jhxk9ji5bodur57xs','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-11 20:00:18'),('dyia8782amp6c9pg9wa5j37cb3bpz0t8','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-04 21:05:35'),('f1vvdy9snpzg21xdea7zqckhha3jjqlz','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-12 11:16:57'),('fqfzq2x2ek2pxn9q87bnc689u4fthk8m','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-10 23:06:10'),('give7n5nlt7tzrj5qqtipejomlf4f3sn','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-07 23:16:09'),('gocsgvpdu4v0jasmrwx54hcbylgzincf','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-12 19:56:39'),('h6houlnxnhxrec57fbt6wiykcihl8xon','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-10 23:38:34'),('hoiel1orb4505amao9paedx49kwugnb5','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-06 04:22:37'),('icg7ayl5u1jwu0qhgl1ptvu5jkgbb5u9','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-10 02:10:00'),('ign7bo9c50vc26sxsikif4gtjvspd0oe','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-21 04:34:00'),('ilofrm1l1r5f7h863i11cnscg3fyoz2m','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-12 11:37:48'),('kdduk8flz68jxbxjnr2mja0ubjt8d4oi','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 15:58:52'),('lg8j2o8p9u1rzvzxx5h9kw6waa22w8dp','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-28 16:52:09'),('m2787ciihpxymxyuzmdb8mjaa8kt41hs','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-06 23:55:28'),('maltmctl4dpm87n5b1s7z7z7pw0avdp1','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-19 16:21:18'),('mfv2vyukdsl5buehnevwhvsaqxzxg3cb','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-02-09 19:34:56'),('mgkl08zl7dxs935ftanuinblipmrg7n0','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-02-03 16:49:37'),('obfw0yrw24zqj8kqn2ixjcbrfw4ml2xh','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-28 23:51:12'),('og3ga8u3o63dy8ohymi12bvx6hwnvs03','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-13 18:03:11'),('oho2rxka63fhzcjcd4a1awu64l839l09','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 02:22:32'),('om2anu6a9ron7d8wf1ap9futmbmujigo','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-02-22 03:53:00'),('pix6tqgxq5e6yqdhgxvda6igt7cgma33','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-10 23:38:38'),('q5nuqe1qixi65b9xmuua744xuws8wj1v','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-18 18:24:21'),('qviwsmflca06lhjwk4gjtabvgf9k7z9p','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-06 17:12:37'),('r5yryca2q21a2ovn6o1q51v19077gkqs','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-18 06:24:45'),('rkqhinaqk8ikxjq5z597zuavstvl9idp','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-08 22:47:47'),('rwqwz0nf82tsvma3jqlwerxojc0gdhou','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-28 19:36:39'),('spjx25i2lwfsv6n0a242j1zg998i0b7i','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-11 22:19:01'),('u9362utnxnnthcgksdpsei9pcyidr7c9','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-12 11:20:15'),('v6chkgg6pbjc6hscpwzpusxvigbn5eay','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-04-26 18:40:42'),('v993xtxm79dl0kmemd7y2wm5u12ecj3w','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-29 00:57:51'),('vqa1lej19ywf3zj97je4oue8za0dc0b2','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-12 17:32:30'),('wbmdtgj89hxozeyhnmss5g7tj8p408dx','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-11 18:08:50'),('x3ihchue6cv5zbna9i58tti2heurdfm4','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 05:28:19'),('xn2123ot9n5i0e40xe5cnt68pg8ra2hh','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-13 02:48:53'),('ye0fqbjnxcnq3re28lcutdmdy7eq43yt','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2014-12-30 20:16:06'),('z30fphzs7r8ph244p8ggp69x6wz1klvb','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 19:44:04');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finances_budgetallocation`
--

DROP TABLE IF EXISTS `finances_budgetallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finances_budgetallocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `amount` decimal(6,2) NOT NULL,
  `periods_per_year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finances_budgetallocation`
--

LOCK TABLES `finances_budgetallocation` WRITE;
/*!40000 ALTER TABLE `finances_budgetallocation` DISABLE KEYS */;
INSERT INTO `finances_budgetallocation` VALUES (1,'Food',350.00,12),(2,'Travel',100.00,12),(3,'Dog',100.00,12),(4,'Spending',300.00,12);
/*!40000 ALTER TABLE `finances_budgetallocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finances_recurringbill`
--

DROP TABLE IF EXISTS `finances_recurringbill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finances_recurringbill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `amount` decimal(6,2) NOT NULL,
  `periods_per_year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finances_recurringbill`
--

LOCK TABLES `finances_recurringbill` WRITE;
/*!40000 ALTER TABLE `finances_recurringbill` DISABLE KEYS */;
INSERT INTO `finances_recurringbill` VALUES (1,'Rent',350.00,12),(2,'Federal loan payment',325.00,12),(3,'Private loan payment',825.18,12),(4,'Carnegie Mellon loan payment',152.79,12),(5,'Cable internet',44.99,12),(6,'Netflix',8.99,12),(7,'Spotify',10.59,12);
/*!40000 ALTER TABLE `finances_recurringbill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_caffeine`
--

DROP TABLE IF EXISTS `fitness_caffeine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_caffeine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` decimal(6,2) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fitness_caffeine_e8175980` (`unit_id`)
) ENGINE=MyISAM AUTO_INCREMENT=717 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_caffeine`
--

LOCK TABLES `fitness_caffeine` WRITE;
/*!40000 ALTER TABLE `fitness_caffeine` DISABLE KEYS */;
INSERT INTO `fitness_caffeine` VALUES (1,5.00,2,'2014-12-25 17:49:23'),(2,3.00,2,'2014-12-25 17:49:23'),(3,1.00,5,'2014-12-25 17:49:23'),(4,2.00,2,'2014-12-25 19:43:49'),(7,1.00,5,'2014-12-26 16:00:00'),(6,3.00,2,'2014-12-26 15:00:00'),(8,3.00,2,'2014-12-26 17:30:00'),(9,1.00,5,'2014-12-26 22:38:25'),(10,2.00,2,'2014-12-27 02:10:08'),(11,4.00,2,'2014-12-27 14:30:00'),(12,1.00,5,'2014-12-27 16:28:43'),(13,2.00,2,'2014-12-27 09:30:00'),(14,4.00,2,'2014-12-27 22:05:23'),(16,2.00,2,'2014-12-27 23:38:53'),(17,3.00,2,'2014-12-28 17:05:00'),(18,1.00,5,'2014-12-28 18:08:43'),(19,2.00,2,'2014-12-28 20:00:23'),(20,3.00,2,'2014-12-28 23:22:01'),(21,2.00,2,'2014-12-28 23:49:13'),(22,2.00,2,'2014-12-29 02:45:17'),(23,2.00,2,'2014-12-29 06:41:32'),(24,1.00,2,'2014-12-29 11:17:02'),(25,1.00,2,'2014-12-29 11:20:19'),(26,1.00,2,'2014-12-29 11:37:56'),(27,2.00,2,'2014-12-29 19:00:00'),(28,2.00,2,'2014-12-29 17:30:00'),(29,3.00,2,'2014-12-30 00:17:23'),(30,2.00,2,'2014-12-30 00:56:21'),(31,1.00,2,'2014-12-30 01:02:53'),(32,2.00,2,'2014-12-30 01:15:33'),(33,1.00,2,'2014-12-30 02:48:56'),(34,3.00,2,'2014-12-30 16:21:13'),(35,1.00,5,'2014-12-30 18:00:00'),(36,2.00,2,'2014-12-30 18:52:56'),(37,3.00,2,'2014-12-30 20:01:35'),(38,2.00,2,'2014-12-30 21:32:07'),(39,3.00,2,'2014-12-31 01:21:36'),(40,3.00,2,'2014-12-31 01:53:26'),(41,3.00,2,'2014-12-31 16:00:00'),(42,1.00,5,'2014-12-31 17:30:00'),(43,1.00,2,'2014-12-31 06:00:00'),(44,3.00,2,'2014-12-31 10:00:00'),(45,3.00,2,'2014-12-31 12:30:00'),(46,3.00,2,'2015-01-01 16:30:00'),(47,1.00,5,'2015-01-01 17:36:55'),(48,2.00,2,'2015-01-01 19:41:29'),(49,2.00,2,'2015-01-01 23:53:45'),(50,3.00,2,'2015-01-01 23:54:20'),(51,2.00,2,'2015-01-02 01:10:27'),(52,1.00,2,'2015-01-02 01:43:13'),(53,2.00,2,'2015-01-02 02:13:06'),(54,1.00,2,'2015-01-02 03:11:58'),(55,2.00,2,'2015-01-02 16:51:50'),(56,3.00,2,'2015-01-02 14:45:00'),(57,0.75,5,'2015-01-02 17:52:05'),(58,1.00,2,'2015-01-02 18:27:03'),(60,1.00,2,'2015-01-02 18:30:33'),(61,3.00,2,'2015-01-02 20:56:38'),(62,2.00,2,'2015-01-02 22:04:27'),(63,1.00,2,'2015-01-02 23:40:21'),(64,3.00,2,'2015-01-03 17:21:17'),(65,1.00,5,'2015-01-03 19:00:02'),(66,2.00,2,'2015-01-03 19:42:31'),(67,2.00,2,'2015-01-03 20:54:31'),(68,2.00,2,'2015-01-04 00:26:23'),(69,3.00,2,'2015-01-04 00:50:29'),(70,4.00,2,'2015-01-04 00:58:35'),(71,0.50,5,'2015-01-04 01:28:44'),(72,1.00,2,'2015-01-04 02:10:30'),(73,3.00,2,'2015-01-04 18:04:59'),(74,2.00,2,'2015-01-04 18:29:33'),(75,2.00,2,'2015-01-04 19:06:52'),(76,3.00,2,'2015-01-04 22:26:48'),(77,2.00,2,'2015-01-04 22:40:05'),(78,2.00,2,'2015-01-05 14:12:37'),(79,2.00,2,'2015-01-05 14:33:43'),(80,1.00,2,'2015-01-05 14:56:30'),(81,2.00,2,'2015-01-05 16:08:05'),(82,1.00,5,'2015-01-05 15:09:39'),(83,2.00,2,'2015-01-05 16:33:30'),(84,2.00,2,'2015-01-05 22:49:35'),(85,3.00,2,'2015-01-05 23:19:38'),(86,2.00,2,'2015-01-05 23:59:31'),(87,2.00,2,'2015-01-06 00:21:25'),(88,2.00,2,'2015-01-06 00:51:42'),(89,2.00,2,'2015-01-06 01:29:58'),(90,2.00,2,'2015-01-06 15:27:54'),(91,2.00,2,'2015-01-06 16:09:31'),(92,1.00,5,'2015-01-06 17:52:52'),(93,2.00,2,'2015-01-06 20:06:34'),(94,1.00,2,'2015-01-06 20:32:44'),(95,2.00,2,'2015-01-06 21:07:14'),(96,2.00,2,'2015-01-06 21:42:14'),(97,2.00,2,'2015-01-07 15:46:15'),(98,2.00,2,'2015-01-07 15:46:15'),(99,2.00,2,'2015-01-07 18:14:31'),(100,3.00,2,'2015-01-07 23:50:16'),(101,2.00,2,'2015-01-07 23:58:30'),(102,1.00,2,'2015-01-08 00:18:23'),(103,3.00,2,'2015-01-08 01:46:31'),(104,2.00,2,'2015-01-08 14:26:30'),(105,1.00,2,'2015-01-08 15:42:04'),(106,3.00,2,'2015-01-08 16:13:35'),(107,2.00,2,'2015-01-08 16:53:27'),(108,2.00,2,'2015-01-08 17:19:43'),(109,1.00,2,'2015-01-08 17:48:32'),(110,2.00,2,'2015-01-08 22:10:35'),(111,2.00,2,'2015-01-08 22:21:45'),(112,1.00,2,'2015-01-08 22:37:09'),(113,3.00,2,'2015-01-09 00:12:52'),(114,2.00,2,'2015-01-09 14:54:18'),(115,2.00,2,'2015-01-09 14:59:21'),(116,1.00,2,'2015-01-09 21:32:50'),(117,1.00,2,'2015-01-09 21:53:30'),(118,2.00,2,'2015-01-09 21:58:33'),(119,1.00,2,'2015-01-09 22:08:32'),(120,2.00,2,'2015-01-10 15:41:03'),(121,1.00,2,'2015-01-10 16:21:16'),(122,3.00,2,'2015-01-10 17:13:37'),(123,2.00,2,'2015-01-10 18:35:54'),(124,2.00,2,'2015-01-10 19:31:17'),(125,2.00,2,'2015-01-11 00:42:41'),(126,2.00,2,'2015-01-11 00:53:34'),(127,2.00,2,'2015-01-11 02:00:40'),(128,2.00,2,'2015-01-11 15:19:23'),(129,1.00,2,'2015-01-11 15:28:34'),(130,2.00,2,'2015-01-11 17:13:56'),(131,2.00,2,'2015-01-11 18:19:44'),(132,1.00,2,'2015-01-11 20:30:16'),(133,2.00,2,'2015-01-11 23:37:17'),(134,1.00,2,'2015-01-11 23:37:41'),(135,2.00,2,'2015-01-12 17:04:10'),(136,1.00,2,'2015-01-12 17:04:11'),(137,1.00,5,'2015-01-12 17:04:13'),(138,2.00,2,'2015-01-12 18:15:42'),(139,2.00,2,'2015-01-12 18:15:47'),(140,2.00,2,'2015-01-12 18:44:26'),(141,1.00,2,'2015-01-12 18:58:33'),(142,1.00,2,'2015-01-12 19:50:09'),(143,2.00,2,'2015-01-13 00:29:52'),(144,2.00,2,'2015-01-13 01:50:21'),(145,2.00,2,'2015-01-13 15:45:23'),(146,2.00,2,'2015-01-13 16:10:11'),(147,1.00,2,'2015-01-13 18:01:35'),(148,3.00,2,'2015-01-13 18:41:08'),(149,2.00,2,'2015-01-14 19:36:16'),(150,2.00,2,'2015-01-14 19:36:18'),(151,2.00,2,'2015-01-14 19:36:20'),(152,1.00,2,'2015-01-14 19:36:23'),(153,4.00,2,'2015-01-14 21:30:48'),(154,2.00,2,'2015-01-14 21:30:51'),(155,2.00,2,'2015-01-15 16:51:48'),(156,2.00,2,'2015-01-15 16:51:49'),(157,2.00,2,'2015-01-16 16:57:20'),(158,2.00,2,'2015-01-19 21:04:33'),(159,3.00,2,'2015-01-19 23:48:12'),(160,2.00,2,'2015-01-19 23:48:19'),(161,1.00,2,'2015-01-20 00:02:44'),(162,1.00,2,'2015-01-20 16:49:32'),(163,2.00,2,'2015-01-20 16:53:39'),(164,1.00,2,'2015-01-20 17:42:42'),(165,2.00,2,'2015-01-20 18:58:55'),(166,2.00,2,'2015-01-20 21:13:12'),(167,2.00,2,'2015-01-21 04:39:06'),(168,2.00,2,'2015-01-21 17:34:11'),(169,2.00,2,'2015-01-21 22:48:19'),(170,3.00,2,'2015-01-22 00:45:09'),(171,2.00,2,'2015-01-24 19:39:09'),(172,2.00,2,'2015-01-24 19:39:15'),(173,3.00,2,'2015-01-24 19:39:16'),(174,2.00,2,'2015-01-24 19:39:17'),(175,2.00,2,'2015-01-26 19:00:06'),(176,2.00,2,'2015-01-26 19:09:20'),(177,1.00,2,'2015-01-26 19:09:42'),(178,2.00,2,'2015-01-29 15:53:48'),(179,2.00,2,'2015-01-29 16:54:37'),(180,1.00,2,'2015-01-29 17:22:25'),(181,2.00,2,'2015-01-29 17:45:14'),(182,1.00,2,'2015-01-29 18:06:04'),(183,1.00,2,'2015-01-29 23:13:53'),(184,2.00,2,'2015-01-29 23:34:07'),(185,2.00,2,'2015-01-30 00:26:15'),(186,2.00,2,'2015-01-30 17:00:30'),(187,2.00,2,'2015-01-30 18:34:36'),(188,2.00,2,'2015-01-30 18:54:47'),(189,1.00,2,'2015-01-30 20:15:24'),(190,1.00,2,'2015-01-30 20:15:24'),(191,2.00,2,'2015-01-30 21:27:28'),(192,1.00,2,'2015-01-31 15:43:11'),(193,1.00,2,'2015-01-31 16:50:34'),(194,3.00,2,'2015-01-31 18:19:37'),(195,1.00,2,'2015-01-31 19:00:36'),(196,1.00,2,'2015-01-31 20:41:50'),(197,1.00,2,'2015-02-01 00:00:26'),(198,1.00,2,'2015-02-01 00:06:06'),(199,1.00,2,'2015-02-01 01:55:42'),(200,1.00,2,'2015-02-01 02:23:10'),(201,1.00,2,'2015-02-01 16:05:53'),(202,1.00,2,'2015-02-01 16:20:25'),(203,1.00,2,'2015-02-01 16:52:05'),(204,1.00,2,'2015-02-01 17:47:23'),(205,2.00,2,'2015-02-01 18:28:50'),(206,2.00,2,'2015-02-02 00:10:00'),(207,1.00,2,'2015-02-02 01:19:34'),(208,1.00,2,'2015-02-02 15:15:27'),(209,1.00,2,'2015-02-02 15:16:16'),(210,1.00,2,'2015-02-02 15:39:34'),(211,1.00,2,'2015-02-02 16:25:11'),(212,1.00,2,'2015-02-02 16:31:08'),(213,1.00,2,'2015-02-02 17:00:53'),(214,2.00,2,'2015-02-02 19:53:19'),(215,2.00,2,'2015-02-02 20:36:27'),(216,1.00,2,'2015-02-02 21:16:16'),(217,1.00,2,'2015-02-02 21:31:33'),(218,1.00,2,'2015-02-03 00:18:46'),(219,1.00,2,'2015-02-03 00:54:45'),(220,1.00,2,'2015-02-03 02:17:30'),(221,1.00,2,'2015-02-03 15:13:35'),(222,1.00,2,'2015-02-03 15:37:21'),(223,1.00,2,'2015-02-03 16:45:01'),(224,1.00,2,'2015-02-03 16:45:14'),(225,2.00,2,'2015-02-03 21:01:31'),(226,2.00,2,'2015-02-03 21:01:43'),(227,1.00,2,'2015-02-03 23:58:36'),(228,1.00,2,'2015-02-04 00:24:05'),(229,1.00,2,'2015-02-04 14:28:25'),(230,1.00,2,'2015-02-04 14:50:16'),(231,1.00,2,'2015-02-04 15:52:27'),(232,1.00,2,'2015-02-04 16:53:40'),(233,1.00,2,'2015-02-04 17:29:57'),(234,2.00,2,'2015-02-04 18:54:01'),(235,1.00,2,'2015-02-04 22:58:52'),(236,1.00,2,'2015-02-04 22:58:58'),(237,1.00,2,'2015-02-05 02:45:44'),(238,1.00,2,'2015-02-05 15:03:16'),(239,1.00,2,'2015-02-05 15:03:16'),(240,0.75,5,'2015-02-05 15:03:21'),(241,1.00,2,'2015-02-05 21:17:20'),(242,1.00,2,'2015-02-05 21:44:24'),(243,1.00,2,'2015-02-05 23:17:55'),(244,1.00,2,'2015-02-05 23:17:56'),(245,2.00,2,'2015-02-06 15:44:29'),(246,2.00,2,'2015-02-06 16:41:24'),(247,2.00,2,'2015-02-06 18:37:04'),(248,2.00,2,'2015-02-06 20:52:19'),(249,1.00,2,'2015-02-06 21:19:30'),(250,1.00,2,'2015-02-06 21:54:29'),(251,1.00,2,'2015-02-07 00:51:40'),(252,1.00,2,'2015-02-07 01:00:31'),(253,1.00,2,'2015-02-07 01:20:18'),(254,2.00,2,'2015-02-07 17:12:47'),(255,2.00,2,'2015-02-07 21:10:21'),(256,2.00,2,'2015-02-07 23:24:08'),(257,1.00,2,'2015-02-08 00:47:16'),(258,1.00,2,'2015-02-08 02:39:48'),(259,1.00,2,'2015-02-08 15:03:04'),(260,1.00,2,'2015-02-08 15:04:20'),(261,1.00,2,'2015-02-08 16:16:41'),(262,1.00,2,'2015-02-08 16:43:31'),(263,1.00,2,'2015-02-08 19:16:57'),(264,1.00,2,'2015-02-08 23:29:53'),(265,1.00,2,'2015-02-09 00:34:30'),(266,2.00,2,'2015-02-09 00:45:31'),(267,1.00,2,'2015-02-09 01:08:18'),(268,2.00,2,'2015-02-09 17:19:40'),(269,2.00,2,'2015-02-09 21:04:34'),(270,1.00,2,'2015-02-09 22:04:34'),(271,2.00,2,'2015-02-10 15:48:53'),(272,2.00,2,'2015-02-10 18:18:36'),(273,1.00,2,'2015-02-10 18:35:41'),(274,0.60,5,'2015-02-10 19:17:34'),(275,1.00,2,'2015-02-10 21:34:32'),(276,1.00,2,'2015-02-10 21:51:52'),(277,0.50,2,'2015-02-10 22:13:06'),(278,1.00,2,'2015-02-11 00:37:00'),(279,1.00,2,'2015-02-11 00:45:23'),(280,2.00,2,'2015-02-11 15:41:25'),(281,2.00,2,'2015-02-11 16:23:27'),(282,1.00,2,'2015-02-11 17:17:01'),(283,2.00,2,'2015-02-11 18:25:00'),(284,2.00,2,'2015-02-11 22:15:57'),(285,2.00,2,'2015-02-11 22:16:00'),(286,1.00,2,'2015-02-12 00:41:41'),(287,2.00,2,'2015-02-12 02:10:39'),(288,1.00,2,'2015-02-12 02:10:41'),(289,2.00,2,'2015-02-12 15:40:10'),(290,1.00,2,'2015-02-12 16:43:27'),(291,2.00,2,'2015-02-12 18:02:42'),(292,2.00,2,'2015-02-12 20:19:28'),(293,1.00,2,'2015-02-12 20:42:07'),(294,2.00,2,'2015-02-12 21:58:44'),(295,1.00,2,'2015-02-12 23:37:38'),(296,2.00,2,'2015-02-13 15:11:15'),(297,1.00,2,'2015-02-13 15:26:27'),(298,1.00,2,'2015-02-13 15:34:13'),(299,2.00,2,'2015-02-13 20:07:37'),(300,1.00,2,'2015-02-13 21:02:33'),(301,1.00,2,'2015-02-13 21:41:02'),(302,2.00,2,'2015-02-14 16:04:31'),(303,1.00,2,'2015-02-14 17:17:59'),(304,1.00,2,'2015-02-14 17:35:17'),(305,1.00,2,'2015-02-14 18:43:03'),(306,2.00,2,'2015-02-14 19:09:07'),(307,1.00,2,'2015-02-14 19:44:14'),(308,1.00,2,'2015-02-14 20:17:07'),(309,2.00,2,'2015-02-14 22:41:56'),(310,2.00,2,'2015-02-15 15:58:51'),(311,1.00,2,'2015-02-15 16:55:22'),(312,1.00,2,'2015-02-15 18:15:33'),(313,1.00,2,'2015-02-15 19:19:47'),(314,1.00,2,'2015-02-15 19:24:52'),(315,2.00,2,'2015-02-15 20:59:54'),(316,2.00,2,'2015-02-16 01:12:10'),(317,2.00,2,'2015-02-16 14:42:16'),(318,1.00,2,'2015-02-16 15:49:11'),(319,1.00,2,'2015-02-16 15:49:12'),(320,2.00,2,'2015-02-16 16:51:32'),(321,1.00,2,'2015-02-16 19:41:49'),(322,1.00,2,'2015-02-16 21:05:20'),(323,1.00,2,'2015-02-16 21:13:26'),(324,1.00,2,'2015-02-16 22:22:29'),(325,1.00,2,'2015-02-16 23:09:07'),(326,2.00,2,'2015-02-17 15:14:39'),(327,1.00,2,'2015-02-17 15:58:27'),(328,2.00,2,'2015-02-17 17:47:59'),(329,2.00,2,'2015-02-17 21:23:59'),(330,1.00,2,'2015-02-17 21:47:02'),(331,1.00,2,'2015-02-17 22:46:13'),(332,2.00,2,'2015-02-18 14:55:43'),(333,1.00,2,'2015-02-18 15:10:23'),(334,1.00,2,'2015-02-18 16:25:52'),(335,2.00,2,'2015-02-18 17:12:16'),(336,1.00,2,'2015-02-18 18:25:36'),(337,1.00,2,'2015-02-18 19:25:27'),(338,1.00,2,'2015-02-18 19:36:42'),(339,2.00,2,'2015-02-18 19:59:12'),(340,1.00,2,'2015-02-18 20:17:12'),(341,1.00,2,'2015-02-18 22:48:07'),(342,2.00,2,'2015-02-18 23:28:07'),(343,1.00,2,'2015-02-18 23:48:50'),(344,2.00,2,'2015-02-19 16:20:58'),(345,1.00,2,'2015-02-19 16:40:23'),(346,1.00,2,'2015-02-19 17:04:06'),(347,1.00,2,'2015-02-19 18:17:51'),(348,1.00,2,'2015-02-19 18:33:19'),(349,1.00,2,'2015-02-19 19:00:54'),(350,1.00,2,'2015-02-19 19:19:19'),(351,1.00,2,'2015-02-19 20:22:39'),(352,2.00,2,'2015-02-19 22:04:55'),(353,1.00,2,'2015-02-19 22:06:28'),(354,1.00,2,'2015-02-19 22:49:51'),(355,2.00,2,'2015-02-20 13:47:29'),(356,1.00,2,'2015-02-20 13:50:10'),(357,1.00,2,'2015-02-20 15:13:53'),(358,1.00,2,'2015-02-20 15:46:57'),(359,1.00,2,'2015-02-20 16:04:12'),(360,1.00,2,'2015-02-20 16:19:39'),(361,2.00,2,'2015-02-20 17:08:58'),(362,1.00,2,'2015-02-20 17:38:21'),(364,1.00,2,'2015-02-20 18:54:15'),(365,1.00,2,'2015-02-20 19:04:10'),(366,2.00,2,'2015-02-20 20:04:38'),(367,1.00,2,'2015-02-20 20:26:38'),(368,1.00,2,'2015-02-20 20:56:48'),(369,2.00,2,'2015-02-20 21:08:44'),(370,1.00,2,'2015-02-20 21:30:10'),(371,2.00,2,'2015-02-21 15:47:48'),(372,1.00,2,'2015-02-21 16:45:40'),(373,6.00,2,'2015-02-21 22:41:25'),(374,3.00,2,'2015-02-21 22:57:42'),(375,2.00,2,'2015-02-22 13:45:52'),(376,1.00,2,'2015-02-22 14:15:09'),(377,1.00,2,'2015-02-22 14:47:36'),(378,1.00,2,'2015-02-22 15:45:29'),(379,2.00,2,'2015-02-22 18:26:28'),(380,2.00,2,'2015-02-23 01:04:51'),(381,2.00,2,'2015-02-23 14:25:19'),(382,2.00,2,'2015-02-23 19:08:44'),(383,2.00,2,'2015-02-23 23:51:01'),(384,2.00,2,'2015-02-24 14:05:13'),(385,1.00,2,'2015-02-24 15:03:09'),(386,4.00,2,'2015-02-24 21:28:07'),(387,1.00,2,'2015-02-25 00:10:36'),(388,2.00,2,'2015-02-25 16:13:50'),(389,2.00,2,'2015-02-25 16:13:51'),(390,2.00,2,'2015-02-25 17:03:51'),(391,1.00,2,'2015-02-25 17:03:52'),(392,2.00,2,'2015-02-25 20:01:02'),(393,1.00,2,'2015-02-25 20:39:46'),(394,2.00,2,'2015-02-26 14:34:52'),(395,3.00,2,'2015-02-26 17:20:00'),(396,1.00,2,'2015-02-26 22:01:08'),(397,1.00,2,'2015-02-26 23:12:01'),(398,2.00,2,'2015-02-27 16:40:24'),(399,2.00,2,'2015-02-27 16:40:25'),(400,1.00,2,'2015-02-27 21:10:32'),(401,1.00,2,'2015-02-27 21:10:33'),(402,1.00,2,'2015-02-27 21:58:22'),(403,1.00,2,'2015-02-28 02:09:08'),(404,1.00,2,'2015-02-28 02:09:09'),(405,1.00,2,'2015-02-28 02:09:09'),(406,2.00,2,'2015-02-28 14:15:02'),(407,1.00,2,'2015-02-28 15:11:09'),(408,1.00,2,'2015-02-28 19:29:20'),(409,1.00,2,'2015-02-28 19:29:20'),(410,1.00,2,'2015-02-28 19:54:11'),(411,1.00,2,'2015-02-28 23:36:48'),(412,1.00,2,'2015-02-28 23:36:48'),(413,1.00,2,'2015-02-28 23:36:48'),(414,1.00,2,'2015-02-28 23:37:00'),(415,1.00,2,'2015-03-01 00:12:09'),(416,1.00,2,'2015-03-01 00:29:59'),(417,2.00,2,'2015-03-01 14:58:48'),(418,1.00,2,'2015-03-01 15:14:22'),(419,1.00,2,'2015-03-01 16:40:53'),(420,1.00,2,'2015-03-01 16:40:53'),(421,1.00,2,'2015-03-01 16:40:55'),(422,1.00,2,'2015-03-01 17:36:50'),(423,1.00,2,'2015-03-01 18:07:04'),(424,1.00,2,'2015-03-01 18:57:27'),(425,1.00,2,'2015-03-01 18:57:28'),(426,2.00,2,'2015-03-01 23:58:26'),(427,2.00,2,'2015-03-02 14:29:01'),(428,1.00,2,'2015-03-02 15:55:34'),(429,1.00,2,'2015-03-02 17:33:16'),(430,1.00,2,'2015-03-02 17:33:16'),(431,1.00,2,'2015-03-02 17:33:17'),(432,1.00,2,'2015-03-02 17:33:17'),(433,1.00,2,'2015-03-02 19:44:03'),(434,1.00,2,'2015-03-02 19:50:53'),(435,1.00,2,'2015-03-02 19:57:15'),(436,1.00,2,'2015-03-02 20:33:39'),(437,1.00,2,'2015-03-02 21:34:46'),(438,2.00,2,'2015-03-03 14:21:00'),(439,1.00,2,'2015-03-03 15:26:29'),(440,1.00,2,'2015-03-03 15:55:39'),(441,1.00,2,'2015-03-03 16:51:33'),(442,1.00,2,'2015-03-03 17:23:19'),(443,1.00,2,'2015-03-03 19:40:25'),(444,1.00,2,'2015-03-03 20:49:40'),(445,1.00,2,'2015-03-03 20:49:40'),(446,1.00,2,'2015-03-03 20:49:43'),(447,1.00,2,'2015-03-03 21:43:03'),(448,1.00,2,'2015-03-03 21:55:49'),(449,2.00,2,'2015-03-04 13:49:15'),(450,1.00,2,'2015-03-04 15:30:48'),(451,1.00,2,'2015-03-04 15:58:49'),(452,1.00,2,'2015-03-04 16:34:37'),(453,1.00,2,'2015-03-04 17:14:55'),(454,1.00,2,'2015-03-04 17:27:23'),(455,1.00,2,'2015-03-04 17:48:46'),(456,1.00,2,'2015-03-04 18:07:44'),(457,1.00,2,'2015-03-04 21:00:41'),(458,1.00,2,'2015-03-04 21:16:49'),(459,1.00,2,'2015-03-04 21:16:52'),(460,1.00,2,'2015-03-04 22:52:36'),(461,1.00,2,'2015-03-04 22:52:37'),(462,1.00,2,'2015-03-04 23:18:06'),(463,2.00,2,'2015-03-05 14:26:58'),(464,2.00,2,'2015-03-05 18:15:46'),(465,1.00,2,'2015-03-06 14:03:20'),(466,1.00,2,'2015-03-06 14:14:38'),(467,1.00,2,'2015-03-06 20:32:36'),(468,1.00,2,'2015-03-06 20:46:56'),(469,1.00,2,'2015-03-06 21:54:20'),(470,1.00,2,'2015-03-06 21:54:20'),(471,1.00,2,'2015-03-06 21:54:20'),(472,1.00,2,'2015-03-06 22:41:03'),(473,2.00,2,'2015-03-07 15:12:28'),(474,1.00,2,'2015-03-07 16:16:55'),(475,1.00,2,'2015-03-07 21:00:04'),(476,1.00,2,'2015-03-07 23:39:15'),(477,1.00,2,'2015-03-07 23:39:16'),(478,1.00,2,'2015-03-07 23:39:16'),(479,1.00,2,'2015-03-07 23:39:16'),(480,2.00,2,'2015-03-08 13:41:14'),(481,1.00,2,'2015-03-08 14:42:01'),(482,1.00,2,'2015-03-08 20:25:18'),(483,1.00,2,'2015-03-08 20:25:18'),(484,1.00,2,'2015-03-08 20:25:19'),(485,1.00,2,'2015-03-08 20:25:19'),(486,1.00,2,'2015-03-08 20:25:19'),(487,1.00,2,'2015-03-08 21:31:27'),(488,1.00,2,'2015-03-08 21:31:27'),(489,1.00,2,'2015-03-08 22:05:51'),(490,1.00,2,'2015-03-09 13:21:08'),(491,1.00,2,'2015-03-09 14:24:33'),(492,1.00,2,'2015-03-09 15:31:27'),(493,1.00,2,'2015-03-09 15:57:35'),(494,1.00,2,'2015-03-09 19:18:53'),(495,1.00,2,'2015-03-09 20:19:24'),(496,1.00,2,'2015-03-09 21:25:05'),(497,1.00,2,'2015-03-09 21:25:09'),(498,2.00,2,'2015-03-10 13:48:28'),(499,1.00,2,'2015-03-10 15:27:25'),(500,1.00,2,'2015-03-10 16:03:19'),(501,1.00,2,'2015-03-10 16:52:00'),(502,1.00,2,'2015-03-10 16:52:14'),(503,1.00,2,'2015-03-10 19:56:44'),(504,1.00,2,'2015-03-10 20:01:54'),(505,1.00,2,'2015-03-10 20:55:19'),(506,2.00,2,'2015-03-11 14:09:54'),(507,1.00,2,'2015-03-11 14:50:46'),(508,1.00,2,'2015-03-11 14:59:38'),(509,1.00,2,'2015-03-11 16:22:51'),(510,1.00,2,'2015-03-11 21:11:45'),(511,1.00,2,'2015-03-11 22:04:01'),(512,2.00,2,'2015-03-12 13:21:21'),(513,1.00,2,'2015-03-12 14:53:15'),(514,2.00,2,'2015-03-12 16:38:58'),(515,1.00,2,'2015-03-12 17:34:35'),(516,1.00,2,'2015-03-12 19:56:48'),(517,2.00,2,'2015-03-13 13:19:00'),(518,1.00,2,'2015-03-13 14:43:11'),(519,1.00,2,'2015-03-13 15:26:12'),(520,1.00,2,'2015-03-13 17:24:44'),(521,1.00,2,'2015-03-13 17:24:44'),(522,1.00,2,'2015-03-13 21:39:39'),(523,1.00,2,'2015-03-13 21:39:44'),(524,1.00,2,'2015-03-13 22:14:09'),(525,1.00,2,'2015-03-13 23:36:20'),(526,1.00,2,'2015-03-13 23:36:24'),(527,1.00,2,'2015-03-14 14:00:29'),(528,1.00,2,'2015-03-14 16:46:42'),(529,1.00,2,'2015-03-14 16:46:42'),(530,1.00,2,'2015-03-14 16:46:48'),(531,1.00,2,'2015-03-14 18:23:37'),(532,1.00,2,'2015-03-14 18:51:07'),(533,2.00,2,'2015-03-15 14:04:12'),(534,1.00,2,'2015-03-15 15:08:57'),(535,1.00,2,'2015-03-15 15:44:30'),(536,1.00,2,'2015-03-15 21:00:10'),(537,1.00,2,'2015-03-15 21:00:11'),(538,1.00,2,'2015-03-15 22:02:01'),(539,2.00,2,'2015-03-16 13:56:28'),(540,1.00,2,'2015-03-16 15:00:10'),(541,1.00,2,'2015-03-16 16:51:49'),(542,1.00,2,'2015-03-16 19:31:27'),(543,1.00,2,'2015-03-16 22:10:36'),(544,1.00,2,'2015-03-16 22:10:37'),(545,1.00,2,'2015-03-16 23:30:58'),(546,2.00,2,'2015-03-17 14:38:33'),(547,1.00,2,'2015-03-17 14:38:35'),(548,1.00,2,'2015-03-17 14:59:47'),(549,1.00,2,'2015-03-17 22:28:59'),(550,1.00,2,'2015-03-17 22:28:59'),(551,1.00,2,'2015-03-17 22:29:00'),(552,1.00,2,'2015-03-17 22:29:06'),(553,1.00,2,'2015-03-18 14:59:51'),(554,1.00,2,'2015-03-18 15:20:34'),(555,1.00,2,'2015-03-19 02:24:06'),(556,1.00,2,'2015-03-19 02:24:06'),(557,1.00,2,'2015-03-19 02:24:07'),(558,1.00,2,'2015-03-19 02:24:07'),(559,1.00,2,'2015-03-19 02:24:07'),(560,2.00,2,'2015-03-19 14:12:11'),(561,1.00,2,'2015-03-19 14:12:14'),(562,2.00,2,'2015-03-20 13:59:09'),(563,1.00,2,'2015-03-20 15:03:51'),(564,1.00,2,'2015-03-20 15:04:09'),(565,1.00,2,'2015-03-20 15:07:03'),(566,2.00,2,'2015-03-21 14:32:21'),(567,1.00,2,'2015-03-21 14:32:23'),(568,1.00,2,'2015-03-21 15:23:05'),(569,1.00,2,'2015-03-21 15:23:06'),(570,1.00,2,'2015-03-21 20:11:18'),(571,1.00,2,'2015-03-21 20:36:37'),(572,1.00,2,'2015-03-21 21:35:30'),(573,1.00,2,'2015-03-21 22:30:08'),(574,2.00,2,'2015-03-22 13:48:43'),(575,1.00,2,'2015-03-22 14:17:02'),(576,1.00,2,'2015-03-22 14:33:05'),(577,1.00,2,'2015-03-22 18:06:50'),(578,1.00,2,'2015-03-22 18:06:51'),(579,1.00,2,'2015-03-22 18:06:51'),(580,1.00,2,'2015-03-22 18:06:52'),(581,2.00,2,'2015-03-23 13:29:24'),(582,1.00,2,'2015-03-23 15:14:46'),(583,1.00,2,'2015-03-23 15:14:46'),(584,1.00,2,'2015-03-23 15:56:12'),(585,1.00,2,'2015-03-23 19:30:17'),(586,1.00,2,'2015-03-23 19:30:17'),(587,2.00,2,'2015-03-24 12:18:35'),(588,1.00,2,'2015-03-24 13:34:01'),(589,1.00,2,'2015-03-24 13:47:26'),(590,1.00,2,'2015-03-24 14:53:46'),(591,2.00,2,'2015-03-25 13:29:39'),(592,1.00,2,'2015-03-25 13:57:55'),(593,1.00,2,'2015-03-25 14:59:47'),(594,1.00,2,'2015-03-26 02:32:40'),(595,1.00,2,'2015-03-26 02:32:41'),(596,1.00,2,'2015-03-26 02:32:41'),(597,1.00,2,'2015-03-26 02:32:41'),(598,1.00,2,'2015-03-26 02:32:42'),(599,1.00,2,'2015-03-26 02:32:42'),(600,1.00,2,'2015-03-26 02:32:42'),(601,1.00,2,'2015-03-26 02:32:43'),(602,2.00,2,'2015-03-26 12:58:59'),(603,2.00,2,'2015-03-26 16:51:48'),(604,2.00,2,'2015-03-26 19:32:44'),(605,1.00,2,'2015-03-26 19:32:45'),(606,1.00,2,'2015-03-26 19:32:52'),(607,1.00,2,'2015-03-26 19:55:04'),(608,1.00,2,'2015-03-27 04:34:16'),(609,1.00,2,'2015-03-27 04:34:17'),(610,1.00,2,'2015-03-27 04:34:17'),(611,2.00,2,'2015-03-27 13:02:05'),(612,1.00,2,'2015-03-27 14:40:05'),(613,1.00,2,'2015-03-27 21:30:05'),(614,1.00,2,'2015-03-27 21:30:08'),(615,1.00,2,'2015-03-27 21:30:08'),(616,1.00,2,'2015-03-27 21:30:09'),(617,1.00,2,'2015-03-27 21:50:44'),(618,1.00,2,'2015-03-27 22:43:47'),(619,1.00,2,'2015-03-27 23:34:12'),(620,2.00,2,'2015-03-28 12:13:49'),(621,1.00,2,'2015-03-28 12:28:09'),(622,1.00,2,'2015-03-28 17:04:01'),(623,1.00,2,'2015-03-28 17:04:08'),(624,1.00,2,'2015-03-28 21:09:30'),(625,1.00,2,'2015-03-28 21:09:30'),(626,1.00,2,'2015-03-28 21:09:31'),(627,1.00,2,'2015-03-28 21:09:31'),(628,1.00,2,'2015-03-28 21:09:32'),(629,1.00,2,'2015-03-28 22:29:00'),(630,1.00,2,'2015-03-28 23:05:15'),(631,2.00,2,'2015-03-29 13:22:24'),(632,1.00,2,'2015-03-29 14:06:50'),(633,1.00,2,'2015-03-29 20:28:48'),(634,1.00,2,'2015-03-29 20:28:49'),(635,1.00,2,'2015-03-29 20:28:49'),(636,1.00,2,'2015-03-29 20:28:57'),(637,1.00,2,'2015-03-29 20:49:55'),(638,1.00,2,'2015-03-29 22:18:23'),(639,1.00,2,'2015-03-30 02:59:52'),(640,1.00,2,'2015-03-30 02:59:52'),(641,2.00,2,'2015-03-30 14:31:59'),(642,1.00,2,'2015-03-30 14:32:00'),(643,1.00,2,'2015-03-30 14:37:20'),(644,1.00,2,'2015-03-30 20:20:15'),(645,1.00,2,'2015-03-30 20:20:16'),(646,1.00,2,'2015-03-30 20:20:16'),(647,1.00,2,'2015-03-30 23:55:56'),(648,1.00,2,'2015-03-30 23:55:57'),(649,1.00,2,'2015-03-30 23:55:57'),(650,1.00,2,'2015-03-30 23:55:57'),(651,1.00,2,'2015-03-30 23:55:58'),(652,1.00,2,'2015-03-30 23:56:01'),(653,2.00,2,'2015-03-31 13:33:13'),(654,1.00,2,'2015-03-31 14:55:05'),(655,2.00,2,'2015-04-01 13:30:07'),(656,1.00,2,'2015-04-01 14:14:41'),(657,1.00,2,'2015-04-01 17:45:38'),(658,1.00,2,'2015-04-01 17:45:38'),(659,1.00,2,'2015-04-01 17:45:38'),(660,1.00,2,'2015-04-01 17:45:39'),(661,1.00,2,'2015-04-01 21:23:17'),(662,1.00,2,'2015-04-01 22:06:24'),(663,1.00,2,'2015-04-02 04:35:49'),(664,1.00,2,'2015-04-02 14:47:39'),(665,1.00,2,'2015-04-02 15:06:51'),(666,1.00,2,'2015-04-02 15:19:02'),(667,1.00,2,'2015-04-02 21:10:49'),(668,1.00,2,'2015-04-02 21:10:50'),(669,1.00,2,'2015-04-02 21:10:50'),(670,1.00,2,'2015-04-02 21:10:51'),(671,2.00,2,'2015-04-03 14:18:18'),(672,1.00,2,'2015-04-03 14:43:59'),(673,1.00,2,'2015-04-03 15:46:55'),(674,2.00,2,'2015-04-04 16:32:03'),(675,1.00,2,'2015-04-04 16:32:06'),(676,1.00,2,'2015-04-04 17:00:57'),(677,2.00,2,'2015-04-05 16:10:29'),(678,1.00,2,'2015-04-05 16:10:30'),(679,1.00,2,'2015-04-05 17:51:43'),(680,1.00,2,'2015-04-05 17:51:43'),(681,2.00,2,'2015-04-06 17:41:48'),(682,1.00,2,'2015-04-06 17:41:52'),(683,1.00,2,'2015-04-06 21:53:22'),(684,1.00,2,'2015-04-06 21:53:22'),(685,1.00,2,'2015-04-06 21:53:23'),(686,1.00,2,'2015-04-06 21:53:23'),(687,2.00,2,'2015-04-07 15:05:52'),(688,2.00,2,'2015-04-07 15:05:53'),(689,2.00,2,'2015-04-07 15:41:02'),(690,1.00,2,'2015-04-07 16:24:56'),(691,1.00,2,'2015-04-07 17:26:10'),(692,1.00,2,'2015-04-07 17:26:10'),(693,2.00,2,'2015-04-07 19:09:26'),(694,2.00,2,'2015-04-08 14:20:15'),(695,1.00,2,'2015-04-08 15:41:25'),(696,2.00,2,'2015-04-09 14:41:13'),(697,1.00,2,'2015-04-09 19:50:19'),(698,2.00,2,'2015-04-10 14:40:29'),(699,2.00,2,'2015-04-11 17:13:45'),(700,1.00,2,'2015-04-11 17:33:26'),(701,1.00,2,'2015-04-11 19:47:29'),(702,2.00,2,'2015-04-12 14:20:57'),(703,1.00,2,'2015-04-12 15:43:14'),(704,1.00,2,'2015-04-12 17:08:24'),(705,3.00,2,'2015-04-13 15:42:23'),(706,3.00,2,'2015-04-13 16:05:37'),(707,2.00,2,'2015-04-14 14:40:58'),(708,1.00,2,'2015-04-14 15:29:46'),(709,1.00,2,'2015-04-14 15:58:21'),(710,2.00,2,'2015-04-15 15:20:58'),(711,2.00,2,'2015-04-16 15:29:07'),(712,2.00,2,'2015-04-20 15:32:27'),(713,1.00,2,'2015-04-20 15:32:30'),(714,2.00,2,'2015-04-24 16:24:23'),(715,1.00,2,'2015-04-24 16:24:25'),(716,2.00,2,'2015-04-25 14:35:10');
/*!40000 ALTER TABLE `fitness_caffeine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_exercise`
--

DROP TABLE IF EXISTS `fitness_exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_exercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_exercise`
--

LOCK TABLES `fitness_exercise` WRITE;
/*!40000 ALTER TABLE `fitness_exercise` DISABLE KEYS */;
INSERT INTO `fitness_exercise` VALUES (1,'Bench Press'),(2,'Deadlift'),(3,'Squat');
/*!40000 ALTER TABLE `fitness_exercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_exerciseset`
--

DROP TABLE IF EXISTS `fitness_exerciseset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_exerciseset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repetitions` int(11) NOT NULL,
  `weight` int(11),
  `exercise_id` int(11) NOT NULL,
  `workout_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fitness_exerciseset_f57b1ff6` (`exercise_id`),
  KEY `fitness_exerciseset_c1bc7825` (`workout_id`),
  CONSTRAINT `fitness_exerci_workout_id_79cc1d3b42b702ac_fk_fitness_workout_id` FOREIGN KEY (`workout_id`) REFERENCES `fitness_workout` (`id`),
  CONSTRAINT `fitness_exer_exercise_id_73480ec38390ac7c_fk_fitness_exercise_id` FOREIGN KEY (`exercise_id`) REFERENCES `fitness_exercise` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_exerciseset`
--

LOCK TABLES `fitness_exerciseset` WRITE;
/*!40000 ALTER TABLE `fitness_exerciseset` DISABLE KEYS */;
INSERT INTO `fitness_exerciseset` VALUES (1,12,75,1,1,'2015-01-04 01:43:23'),(2,12,75,1,1,'2015-01-04 01:43:23'),(3,12,75,1,1,'2015-01-04 01:43:23'),(4,12,75,1,1,'2015-01-04 01:43:23'),(5,4,75,2,1,'2015-01-04 01:43:23'),(6,4,75,2,1,'2015-01-04 01:43:23'),(7,4,75,2,1,'2015-01-04 01:43:23'),(8,4,75,2,1,'2015-01-04 01:43:23'),(9,8,75,3,1,'2015-01-04 01:43:23'),(10,8,75,3,1,'2015-01-04 01:43:23'),(11,8,75,3,1,'2015-01-04 01:43:23'),(12,8,75,3,1,'2015-01-04 01:43:23'),(13,12,75,1,2,'2015-01-04 01:43:23'),(14,12,75,1,2,'2015-01-04 01:43:23'),(15,12,75,1,2,'2015-01-04 01:43:23'),(16,8,75,3,2,'2015-01-04 01:43:23'),(17,8,75,3,2,'2015-01-04 01:43:23'),(18,8,75,3,2,'2015-01-04 01:43:23'),(19,4,75,2,2,'2015-01-04 01:43:23'),(20,4,75,2,2,'2015-01-04 01:43:23'),(21,4,75,2,2,'2015-01-04 01:43:23'),(22,8,75,2,3,'2015-01-04 01:43:23'),(23,8,75,2,3,'2015-01-04 01:43:23'),(24,8,75,2,3,'2015-01-04 01:43:23'),(25,8,75,3,3,'2015-01-04 01:43:23'),(26,8,75,3,3,'2015-01-04 01:43:23'),(27,8,75,3,3,'2015-01-04 01:43:23'),(28,12,75,1,3,'2015-01-04 01:43:23'),(29,12,75,1,3,'2015-01-04 01:43:23'),(30,12,75,1,3,'2015-01-04 01:43:23'),(31,12,75,1,4,'2015-01-04 01:43:23'),(32,16,75,1,4,'2015-01-04 01:43:23'),(33,12,75,1,4,'2015-01-04 01:43:23'),(34,8,75,3,4,'2015-01-04 01:43:23'),(35,8,75,3,4,'2015-01-04 01:43:23'),(36,8,75,3,4,'2015-01-04 01:43:23'),(37,4,75,2,4,'2015-01-04 01:43:23'),(38,4,75,2,4,'2015-01-04 01:43:23'),(39,4,75,2,4,'2015-01-04 01:43:23'),(40,8,75,2,5,'2015-01-04 01:57:22'),(41,8,75,2,5,'2015-01-04 01:57:28'),(42,8,75,2,5,'2015-01-04 01:57:29'),(43,8,75,3,5,'2015-01-04 02:00:01'),(44,8,75,3,5,'2015-01-04 02:05:18'),(45,8,75,3,5,'2015-01-04 02:08:04'),(46,12,75,1,5,'2015-01-04 02:19:13'),(47,12,75,1,5,'2015-01-04 02:19:16'),(48,12,75,1,5,'2015-01-04 02:19:17'),(49,20,75,1,6,'2015-01-04 08:43:49'),(50,20,75,1,6,'2015-01-04 08:46:23'),(51,12,75,1,7,'2015-01-10 18:40:31'),(52,12,75,1,7,'2015-01-10 18:41:44'),(53,12,75,1,7,'2015-01-10 18:43:21'),(54,8,75,3,7,'2015-01-10 18:47:21'),(55,8,75,3,7,'2015-01-10 18:50:36'),(56,8,75,3,7,'2015-01-10 18:54:08'),(57,4,75,2,7,'2015-01-10 18:56:31'),(58,4,75,2,7,'2015-01-10 19:00:13'),(59,4,75,2,7,'2015-01-10 19:31:15'),(60,4,75,2,8,'2015-01-13 00:33:31'),(61,4,75,2,8,'2015-01-13 00:34:13'),(62,4,75,2,8,'2015-01-13 00:46:03'),(63,8,75,3,8,'2015-01-13 00:46:09'),(64,8,75,3,8,'2015-01-13 00:59:45'),(65,16,75,1,8,'2015-01-13 01:50:30'),(66,16,75,1,8,'2015-01-13 01:50:31'),(67,16,75,1,8,'2015-01-13 01:50:31'),(68,16,75,1,8,'2015-01-13 01:50:32'),(69,8,75,1,9,'2015-02-14 20:55:24'),(70,8,75,1,9,'2015-02-14 20:57:36'),(71,8,75,1,9,'2015-02-14 21:00:14'),(72,8,75,3,9,'2015-02-14 21:32:46'),(73,8,75,3,9,'2015-02-14 21:36:47'),(74,8,75,3,9,'2015-02-14 21:40:31'),(75,4,75,2,9,'2015-02-14 21:43:45'),(76,4,75,2,9,'2015-02-14 21:46:44'),(77,4,75,2,9,'2015-02-14 21:49:41'),(78,8,75,3,10,'2015-02-18 03:49:58'),(79,8,75,3,10,'2015-02-18 03:49:59'),(80,8,75,3,10,'2015-02-18 03:58:54'),(81,4,75,2,10,'2015-02-18 04:00:28'),(82,4,75,2,10,'2015-02-18 04:04:46'),(83,4,75,2,10,'2015-02-18 04:23:02'),(84,8,75,1,10,'2015-02-18 04:24:29'),(85,8,75,1,10,'2015-02-18 04:35:31'),(86,8,75,1,11,'2015-02-23 00:36:47'),(87,8,75,1,11,'2015-02-23 00:37:29'),(88,8,75,1,11,'2015-02-23 00:41:30'),(89,8,75,3,11,'2015-02-23 00:43:17'),(90,8,75,3,11,'2015-02-23 00:48:02'),(91,8,75,3,11,'2015-02-23 00:51:37'),(92,8,75,2,11,'2015-02-23 00:52:17'),(93,8,75,2,11,'2015-02-23 00:54:38'),(94,8,75,2,11,'2015-02-23 00:58:58'),(95,8,75,2,12,'2015-02-24 22:23:00'),(96,8,75,2,12,'2015-02-24 22:27:59'),(97,8,75,2,12,'2015-02-24 22:31:50'),(98,8,75,3,12,'2015-02-24 22:35:57'),(99,8,75,3,12,'2015-02-24 22:37:16'),(100,8,75,3,12,'2015-02-24 22:39:24'),(101,24,75,1,12,'2015-02-24 22:42:02'),(102,12,75,1,12,'2015-02-24 22:45:06'),(103,8,75,1,12,'2015-02-24 22:52:51'),(104,16,75,1,13,'2015-02-26 23:09:42'),(105,16,75,1,13,'2015-02-26 23:11:51'),(106,16,75,1,13,'2015-02-26 23:15:33'),(107,8,75,3,13,'2015-02-26 23:20:52'),(108,8,75,3,13,'2015-02-26 23:20:52'),(109,8,75,3,13,'2015-02-26 23:25:34'),(110,8,75,2,13,'2015-02-26 23:31:03'),(111,8,75,2,13,'2015-02-26 23:35:44'),(112,8,75,2,14,'2015-03-02 00:17:51'),(113,8,75,2,14,'2015-03-02 00:19:39'),(114,8,75,2,14,'2015-03-02 00:26:28'),(115,8,75,3,14,'2015-03-02 00:31:27'),(116,8,75,3,14,'2015-03-02 00:39:57'),(117,8,75,3,14,'2015-03-02 00:40:50'),(118,12,75,1,14,'2015-03-02 00:47:37'),(119,12,75,1,14,'2015-03-02 00:48:16'),(120,12,75,1,14,'2015-03-02 00:51:34'),(121,16,75,1,15,'2015-03-04 03:07:41'),(122,16,75,1,15,'2015-03-04 03:16:21'),(123,16,75,1,15,'2015-03-04 04:35:14'),(124,8,75,2,15,'2015-03-04 04:35:27'),(125,16,75,2,15,'2015-03-04 04:35:29'),(126,12,75,2,15,'2015-03-04 04:35:30'),(127,8,75,3,15,'2015-03-04 04:37:26'),(128,8,75,3,15,'2015-03-04 04:45:59'),(129,8,75,2,16,'2015-03-06 18:29:58'),(130,8,75,2,16,'2015-03-06 18:31:04'),(131,8,75,2,16,'2015-03-06 18:36:56'),(132,8,75,3,16,'2015-03-06 18:54:32'),(133,8,75,3,16,'2015-03-06 19:00:58'),(134,8,75,3,16,'2015-03-06 19:06:48'),(135,12,75,1,16,'2015-03-06 19:18:38'),(136,12,75,1,16,'2015-03-06 19:21:01'),(137,12,75,1,17,'2015-04-09 01:12:08'),(138,12,75,1,17,'2015-04-09 01:12:09'),(139,12,75,1,17,'2015-04-09 01:12:09'),(140,8,75,3,17,'2015-04-09 01:15:24'),(141,8,75,3,17,'2015-04-09 01:17:11'),(142,8,75,3,17,'2015-04-09 01:20:13'),(143,8,75,2,17,'2015-04-09 01:25:55'),(144,8,75,2,17,'2015-04-09 01:29:49'),(145,8,75,2,17,'2015-04-09 01:31:35'),(146,6,75,2,18,'2015-04-11 22:56:11'),(147,6,75,2,18,'2015-04-11 23:05:07'),(148,6,75,2,18,'2015-04-11 23:05:07'),(149,7,75,3,18,'2015-04-11 23:07:58'),(150,7,75,3,18,'2015-04-11 23:18:15'),(151,7,75,3,18,'2015-04-11 23:18:16'),(152,12,75,1,18,'2015-04-11 23:18:21'),(153,12,75,1,18,'2015-04-11 23:22:15'),(154,12,75,1,18,'2015-04-11 23:23:30'),(155,12,75,1,19,'2015-04-25 16:16:12'),(156,12,75,1,19,'2015-04-25 16:16:12'),(157,12,75,1,19,'2015-04-25 16:16:13'),(158,8,75,3,19,'2015-04-25 16:17:28'),(159,8,75,3,19,'2015-04-25 16:21:20'),(160,8,75,3,19,'2015-04-25 16:25:30');
/*!40000 ALTER TABLE `fitness_exerciseset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_sleep`
--

DROP TABLE IF EXISTS `fitness_sleep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_sleep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `slept_through` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_sleep`
--

LOCK TABLES `fitness_sleep` WRITE;
/*!40000 ALTER TABLE `fitness_sleep` DISABLE KEYS */;
INSERT INTO `fitness_sleep` VALUES (1,'2014-12-27 06:30:00','2014-12-27 14:30:00',0),(2,'2014-12-28 09:00:00','2014-12-28 17:00:00',0),(3,'2015-01-03 06:30:00','2015-01-03 17:00:00',0),(4,'2015-01-04 00:06:48','2015-01-04 00:26:48',0),(5,'2015-01-04 11:30:00','2015-01-04 16:40:00',0),(6,'2015-01-04 16:45:00','2015-01-04 17:45:00',0),(7,'2015-01-05 06:30:00','2015-01-05 14:00:00',0),(8,'2015-01-06 08:30:00','2015-01-06 15:00:00',0);
/*!40000 ALTER TABLE `fitness_sleep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_substance`
--

DROP TABLE IF EXISTS `fitness_substance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_substance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `substance_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_substance`
--

LOCK TABLES `fitness_substance` WRITE;
/*!40000 ALTER TABLE `fitness_substance` DISABLE KEYS */;
INSERT INTO `fitness_substance` VALUES (1,'2014-12-22 01:00:00',1),(2,'2014-12-22 06:00:00',2),(3,'2014-12-25 13:30:00',3),(4,'2014-12-25 14:00:00',3);
/*!40000 ALTER TABLE `fitness_substance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_walk`
--

DROP TABLE IF EXISTS `fitness_walk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_walk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `blocks` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_walk`
--

LOCK TABLES `fitness_walk` WRITE;
/*!40000 ALTER TABLE `fitness_walk` DISABLE KEYS */;
INSERT INTO `fitness_walk` VALUES (1,'2014-12-15 16:14:10','2014-12-15 16:29:10',6),(4,'2014-12-15 19:12:42','2014-12-15 19:34:42',11),(5,'2014-12-25 17:30:00','2014-12-25 17:45:00',6),(6,'2014-12-27 21:52:13','2014-12-27 22:07:13',6),(7,'2014-12-27 21:52:13','2014-12-27 22:07:13',6),(8,'2015-01-04 03:53:56','2015-01-04 04:08:56',6),(9,'2015-01-04 18:10:47','2015-01-04 18:25:47',6),(10,'2015-01-04 22:08:46','2015-01-04 22:23:46',6),(11,'2015-01-05 04:00:21','2015-01-05 04:15:21',6),(12,'2015-01-05 15:15:00','2015-01-05 15:30:00',6),(13,'2015-01-05 09:15:00','2015-01-05 09:30:00',6),(14,'2015-01-05 10:15:00','2015-01-05 10:30:00',6),(15,'2015-01-06 05:34:58','2015-01-06 05:49:58',6),(16,'2015-01-06 15:54:37','2015-01-06 16:09:37',6);
/*!40000 ALTER TABLE `fitness_walk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_waterfilter`
--

DROP TABLE IF EXISTS `fitness_waterfilter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_waterfilter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_waterfilter`
--

LOCK TABLES `fitness_waterfilter` WRITE;
/*!40000 ALTER TABLE `fitness_waterfilter` DISABLE KEYS */;
INSERT INTO `fitness_waterfilter` VALUES (1),(5),(6);
/*!40000 ALTER TABLE `fitness_waterfilter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_waterpitcher`
--

DROP TABLE IF EXISTS `fitness_waterpitcher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_waterpitcher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `water_filter_id` int(11),
  PRIMARY KEY (`id`),
  KEY `fitness_waterpitcher_b0ab8404` (`water_filter_id`),
  CONSTRAINT `fitne_water_filter_id_3c22083fc616d8b7_fk_fitness_waterfilter_id` FOREIGN KEY (`water_filter_id`) REFERENCES `fitness_waterfilter` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_waterpitcher`
--

LOCK TABLES `fitness_waterpitcher` WRITE;
/*!40000 ALTER TABLE `fitness_waterpitcher` DISABLE KEYS */;
INSERT INTO `fitness_waterpitcher` VALUES (1,'2015-01-02 17:36:56',1),(2,'2015-01-02 17:37:09',1),(3,'2015-01-02 17:37:10',1),(4,'2015-01-02 17:37:10',1),(5,'2015-01-02 17:37:11',1),(6,'2015-01-02 17:37:11',1),(7,'2015-01-02 17:37:12',1),(8,'2015-01-02 17:37:12',1),(9,'2015-01-02 17:37:12',1),(10,'2015-01-02 17:37:13',1),(11,'2015-01-02 17:37:13',1),(12,'2015-01-02 17:37:14',1),(13,'2015-01-02 17:37:14',1),(14,'2015-01-02 17:37:14',1),(15,'2015-01-02 17:37:15',1),(16,'2015-01-02 17:37:15',1),(17,'2015-01-02 17:37:16',1),(18,'2015-01-02 17:37:16',1),(19,'2015-01-02 17:37:16',1),(20,'2015-01-02 17:37:17',1),(21,'2015-01-02 17:37:17',1),(22,'2015-01-02 17:37:17',1),(23,'2015-01-02 17:37:18',1),(24,'2015-01-02 21:51:50',1),(25,'2015-01-03 19:02:28',1),(26,'2015-01-04 02:01:48',1),(27,'2015-01-04 21:11:00',1),(28,'2015-01-06 05:50:36',1),(29,'2015-01-06 19:37:17',1),(30,'2015-01-07 02:33:18',1),(31,'2015-01-07 22:19:26',1),(32,'2015-01-08 14:36:27',1),(33,'2015-01-09 21:55:34',1),(34,'2015-01-10 04:52:33',1),(35,'2015-01-10 23:37:56',1),(36,'2015-01-11 23:19:36',1),(37,'2015-01-12 21:34:59',1),(38,'2015-01-13 05:42:48',1),(39,'2015-01-29 04:16:46',1),(40,'2015-01-30 00:26:19',1),(41,'2015-01-30 20:56:57',1),(42,'2015-01-31 16:50:36',1),(43,'2015-01-31 19:45:26',1),(44,'2015-02-01 17:18:42',1),(45,'2015-02-02 16:31:14',1),(46,'2015-02-04 05:38:21',1),(47,'2015-02-04 14:51:36',1),(48,'2015-02-05 02:21:55',1),(49,'2015-02-06 02:41:16',1),(50,'2015-02-06 16:25:11',1),(51,'2015-02-07 00:51:25',1),(52,'2015-02-08 01:58:23',1),(53,'2015-02-09 00:19:26',1),(54,'2015-02-09 21:34:40',1),(55,'2015-02-10 19:24:18',1),(56,'2015-02-11 18:29:14',1),(57,'2015-02-12 02:22:44',1),(58,'2015-02-12 20:19:31',1),(59,'2015-02-13 22:14:44',1),(60,'2015-02-14 03:14:19',1),(62,'2015-02-14 03:21:55',5),(63,'2015-02-15 19:22:02',5),(64,'2015-02-16 05:00:47',5),(65,'2015-02-16 19:41:59',5),(66,'2015-02-17 19:34:09',5),(67,'2015-02-18 23:28:09',5),(68,'2015-02-19 04:02:13',5),(69,'2015-02-19 20:22:41',5),(70,'2015-02-20 03:12:12',5),(71,'2015-02-21 00:45:23',5),(72,'2015-02-21 22:41:23',5),(73,'2015-02-22 14:28:36',5),(74,'2015-02-24 01:15:55',5),(75,'2015-02-24 21:08:55',5),(76,'2015-02-25 02:40:16',5),(77,'2015-02-26 23:07:11',5),(78,'2015-02-28 02:09:11',5),(79,'2015-02-28 18:03:46',5),(80,'2015-03-01 04:58:00',5),(81,'2015-03-01 17:44:44',5),(82,'2015-03-02 05:54:40',5),(83,'2015-03-03 18:59:53',5),(84,'2015-03-03 22:15:25',5),(85,'2015-03-04 17:48:15',5),(86,'2015-03-05 16:31:07',5),(87,'2015-03-06 05:00:33',5),(88,'2015-03-07 05:09:29',5),(89,'2015-03-08 01:08:16',5),(90,'2015-03-08 20:47:50',5),(91,'2015-03-10 14:27:02',5),(92,'2015-03-11 14:51:14',5),(93,'2015-03-11 14:59:36',5),(94,'2015-03-12 03:55:19',5),(95,'2015-03-15 03:01:59',5),(96,'2015-03-16 13:56:33',5),(97,'2015-03-16 13:56:34',5),(98,'2015-03-17 17:01:48',5),(99,'2015-03-19 02:24:01',5),(100,'2015-03-19 17:22:49',5),(101,'2015-03-20 16:01:47',5),(102,'2015-03-20 22:18:33',5),(103,'2015-03-23 03:58:26',5),(104,'2015-03-24 03:50:45',5),(105,'2015-03-25 04:03:20',5),(106,'2015-03-26 02:32:55',5),(107,'2015-03-27 04:34:07',5),(108,'2015-03-27 21:30:26',5),(109,'2015-03-28 19:45:28',5),(110,'2015-03-30 02:59:34',5),(111,'2015-03-31 04:49:45',5),(112,'2015-03-31 23:12:00',5),(113,'2015-04-01 14:30:56',5),(114,'2015-04-02 04:35:45',5),(115,'2015-04-02 20:37:29',5),(116,'2015-04-04 18:23:34',5),(117,'2015-04-05 18:26:04',5),(118,'2015-04-08 05:05:45',5),(119,'2015-04-09 02:07:36',5),(120,'2015-04-09 02:07:44',5),(121,'2015-04-09 02:07:44',5),(122,'2015-04-09 02:07:50',6),(123,'2015-04-09 15:44:46',6),(124,'2015-04-19 23:57:48',6),(125,'2015-04-19 23:57:49',6),(126,'2015-04-19 23:57:50',6),(127,'2015-04-19 23:57:50',6),(128,'2015-04-19 23:57:51',6),(129,'2015-04-19 23:57:51',6);
/*!40000 ALTER TABLE `fitness_waterpitcher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_workout`
--

DROP TABLE IF EXISTS `fitness_workout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_workout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_workout`
--

LOCK TABLES `fitness_workout` WRITE;
/*!40000 ALTER TABLE `fitness_workout` DISABLE KEYS */;
INSERT INTO `fitness_workout` VALUES (1,'2014-12-14 20:30:00'),(2,'2014-12-24 21:30:00'),(3,'2014-12-26 22:30:00'),(4,'2015-01-01 02:04:29'),(5,'2015-01-04 01:44:22'),(6,'2015-01-04 08:43:31'),(7,'2015-01-10 18:39:42'),(8,'2015-01-13 00:31:06'),(9,'2015-02-14 20:53:26'),(10,'2015-02-18 03:49:50'),(11,'2015-02-23 00:36:30'),(12,'2015-02-24 22:20:29'),(13,'2015-02-26 23:07:14'),(14,'2015-03-02 00:15:16'),(15,'2015-03-04 03:06:38'),(16,'2015-03-06 18:29:51'),(17,'2015-04-09 01:04:12'),(18,'2015-04-11 22:55:32'),(19,'2015-04-25 16:16:00');
/*!40000 ALTER TABLE `fitness_workout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_food`
--

DROP TABLE IF EXISTS `food_food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_food` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_food`
--

LOCK TABLES `food_food` WRITE;
/*!40000 ALTER TABLE `food_food` DISABLE KEYS */;
INSERT INTO `food_food` VALUES (1,'banana'),(2,'apple'),(3,'carrot'),(4,'celery'),(5,'milk'),(6,'cheese'),(7,'egg'),(8,'kale'),(9,'ranch dressing'),(10,'clif bar'),(11,'nature valley bar'),(12,'chicken'),(13,'potato salad'),(14,'baked bean'),(15,'potato'),(16,'Rotini pasta'),(17,'Cream cheese'),(18,'Tomato paste'),(19,'Onion'),(20,'Pancake flour'),(21,'Brown rice'),(22,'Blank beans'),(23,'Yogurt'),(24,'Granola'),(25,'Maple syrup'),(26,'Clif bar');
/*!40000 ALTER TABLE `food_food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_foodweight`
--

DROP TABLE IF EXISTS `food_foodweight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_foodweight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `multiplier` decimal(14,8),
  `food_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `food_foodweight_380b3cb5` (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_foodweight`
--

LOCK TABLES `food_foodweight` WRITE;
/*!40000 ALTER TABLE `food_foodweight` DISABLE KEYS */;
INSERT INTO `food_foodweight` VALUES (1,60.00000000,3,11),(2,125.00000000,2,11),(3,40.00000000,4,11),(4,1.00000000,5,10),(5,118.00000000,1,11);
/*!40000 ALTER TABLE `food_foodweight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_meal`
--

DROP TABLE IF EXISTS `food_meal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_meal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `portion` decimal(3,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `food_meal_da50e9c3` (`recipe_id`),
  CONSTRAINT `food_meal_recipe_id_46c55800aa753725_fk_food_recipe_id` FOREIGN KEY (`recipe_id`) REFERENCES `food_recipe` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_meal`
--

LOCK TABLES `food_meal` WRITE;
/*!40000 ALTER TABLE `food_meal` DISABLE KEYS */;
INSERT INTO `food_meal` VALUES (6,'2014-12-27 16:00:00',1,1.00),(7,'2014-12-27 16:30:00',2,1.00),(8,'2014-12-28 18:08:43',1,1.00),(9,'2014-12-28 18:08:43',2,1.00),(10,'2014-12-30 06:00:00',2,1.00),(11,'2014-12-30 16:30:00',1,1.00),(12,'2014-12-29 06:00:00',1,1.00),(21,'2015-01-03 02:00:00',3,0.75),(22,'2015-01-03 23:30:00',3,0.25),(23,'2015-01-03 17:20:07',1,1.00),(24,'2015-01-03 18:52:11',4,1.00),(25,'2015-01-02 16:59:55',1,1.00),(26,'2015-01-04 01:28:07',2,1.00),(27,'2015-01-04 06:26:00',5,0.50),(28,'2015-01-04 06:52:36',6,1.00),(29,'2015-01-05 22:14:18',5,0.25),(30,'2015-01-05 15:14:48',1,1.00),(31,'2015-01-05 15:44:59',2,1.00);
/*!40000 ALTER TABLE `food_meal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_pantry`
--

DROP TABLE IF EXISTS `food_pantry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_pantry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_pantry`
--

LOCK TABLES `food_pantry` WRITE;
/*!40000 ALTER TABLE `food_pantry` DISABLE KEYS */;
INSERT INTO `food_pantry` VALUES (1,'The Kitchen at 1021 Pennsylvania Ave East, Apartment 3');
/*!40000 ALTER TABLE `food_pantry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_pantryitem`
--

DROP TABLE IF EXISTS `food_pantryitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_pantryitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` decimal(14,8),
  `food_id` int(11) NOT NULL,
  `pantry_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `food_pantryitem_380b3cb5` (`food_id`),
  KEY `food_pantryitem_e8175980` (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_pantryitem`
--

LOCK TABLES `food_pantryitem` WRITE;
/*!40000 ALTER TABLE `food_pantryitem` DISABLE KEYS */;
INSERT INTO `food_pantryitem` VALUES (1,-1.00000000,1,1,6),(2,15.00000000,2,1,6),(3,0.00000000,4,1,6),(4,0.68750000,5,1,9),(5,7.29623098,3,1,7);
/*!40000 ALTER TABLE `food_pantryitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_recipe`
--

DROP TABLE IF EXISTS `food_recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_recipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_recipe`
--

LOCK TABLES `food_recipe` WRITE;
/*!40000 ALTER TABLE `food_recipe` DISABLE KEYS */;
INSERT INTO `food_recipe` VALUES (1,'Juice'),(2,'Smoothie'),(3,'Tomato and cream pasta'),(4,'Pancakes'),(5,'Rice with beans, cheese, and eggs'),(6,'Yogurt and granola'),(7,'Clif bar');
/*!40000 ALTER TABLE `food_recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_recipefood`
--

DROP TABLE IF EXISTS `food_recipefood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_recipefood` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` decimal(3,2),
  `food_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `food_recipefood_380b3cb5` (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_recipefood`
--

LOCK TABLES `food_recipefood` WRITE;
/*!40000 ALTER TABLE `food_recipefood` DISABLE KEYS */;
INSERT INTO `food_recipefood` VALUES (1,1.00,2,1,6),(2,4.00,3,1,6),(3,1.00,4,1,6),(4,1.00,1,2,6),(5,1.00,5,2,10),(6,0.50,16,3,12),(7,1.00,17,3,13),(8,0.50,18,3,14),(9,1.00,19,3,6),(10,1.00,20,4,10),(11,1.00,5,4,14),(12,1.00,7,4,6),(13,1.00,21,5,10),(14,0.50,22,5,14),(15,0.20,6,5,10),(16,2.00,7,5,6),(17,1.00,23,6,10),(18,0.25,24,6,10),(19,2.00,25,6,15),(20,1.00,26,7,6);
/*!40000 ALTER TABLE `food_recipefood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geography_capital`
--

DROP TABLE IF EXISTS `geography_capital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geography_capital` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geography_capital`
--

LOCK TABLES `geography_capital` WRITE;
/*!40000 ALTER TABLE `geography_capital` DISABLE KEYS */;
/*!40000 ALTER TABLE `geography_capital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geography_city`
--

DROP TABLE IF EXISTS `geography_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geography_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `geography_city_5654bf12` (`state_id`),
  CONSTRAINT `state_id_refs_id_8fb8ecf4` FOREIGN KEY (`state_id`) REFERENCES `geography_state` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geography_city`
--

LOCK TABLES `geography_city` WRITE;
/*!40000 ALTER TABLE `geography_city` DISABLE KEYS */;
/*!40000 ALTER TABLE `geography_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geography_state`
--

DROP TABLE IF EXISTS `geography_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geography_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geography_state`
--

LOCK TABLES `geography_state` WRITE;
/*!40000 ALTER TABLE `geography_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `geography_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geography_territory`
--

DROP TABLE IF EXISTS `geography_territory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geography_territory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geography_territory`
--

LOCK TABLES `geography_territory` WRITE;
/*!40000 ALTER TABLE `geography_territory` DISABLE KEYS */;
/*!40000 ALTER TABLE `geography_territory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_attribute`
--

DROP TABLE IF EXISTS `inventory_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(200) NOT NULL,
  `value` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_attribute`
--

LOCK TABLES `inventory_attribute` WRITE;
/*!40000 ALTER TABLE `inventory_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_category`
--

DROP TABLE IF EXISTS `inventory_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6835 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_category`
--

LOCK TABLES `inventory_category` WRITE;
/*!40000 ALTER TABLE `inventory_category` DISABLE KEYS */;
INSERT INTO `inventory_category` VALUES (6819,'Memorabilia'),(6820,'Trinket'),(6821,'Electronics'),(6822,'Book'),(6823,'Utility'),(6824,'Disposable'),(6825,'Health'),(6826,'ContainerPossession'),(6827,'Cookware'),(6828,'Food'),(6829,'Clothing'),(6830,'Music'),(6831,'Stationary'),(6832,'Furniture'),(6833,'Gift'),(6834,'Accessory');
/*!40000 ALTER TABLE `inventory_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_cleaning`
--

DROP TABLE IF EXISTS `inventory_cleaning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_cleaning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `room_id` int(11),
  `complete` tinyint(1) NOT NULL,
  `frequency` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inventory_cleaning_8273f993` (`room_id`),
  CONSTRAINT `inventory_cleaning_room_id_36567d0af8844917_fk_inventory_room_id` FOREIGN KEY (`room_id`) REFERENCES `inventory_room` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_cleaning`
--

LOCK TABLES `inventory_cleaning` WRITE;
/*!40000 ALTER TABLE `inventory_cleaning` DISABLE KEYS */;
INSERT INTO `inventory_cleaning` VALUES (1,'Wipe the kitchen countertops',2,0,2),(2,'Put away the dishes',2,0,2),(3,'Vacuum',1,0,3),(4,'Clean out cupboard',4,0,1),(5,'Wipe down shower',3,1,3),(6,'Do laundry',1,1,3),(7,'Filter boxes',4,0,3),(8,'Take out the trash',5,0,2);
/*!40000 ALTER TABLE `inventory_cleaning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_containerpossession`
--

DROP TABLE IF EXISTS `inventory_containerpossession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_containerpossession` (
  `possession_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`possession_ptr_id`),
  CONSTRAINT `in_possession_ptr_id_64eb884826d2c25a_fk_inventory_possession_id` FOREIGN KEY (`possession_ptr_id`) REFERENCES `inventory_possession` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_containerpossession`
--

LOCK TABLES `inventory_containerpossession` WRITE;
/*!40000 ALTER TABLE `inventory_containerpossession` DISABLE KEYS */;
INSERT INTO `inventory_containerpossession` VALUES (27744);
/*!40000 ALTER TABLE `inventory_containerpossession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_futurepurchase`
--

DROP TABLE IF EXISTS `inventory_futurepurchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_futurepurchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `budget` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_futurepurchase`
--

LOCK TABLES `inventory_futurepurchase` WRITE;
/*!40000 ALTER TABLE `inventory_futurepurchase` DISABLE KEYS */;
INSERT INTO `inventory_futurepurchase` VALUES (1,'Cell phone',150.00),(2,'Tablet',100.00),(3,'Keyboard',50.00),(4,'Mouse',25.00),(5,'Office chair',50.00),(6,'Laptop case',100.00);
/*!40000 ALTER TABLE `inventory_futurepurchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_futurepurchase_categories`
--

DROP TABLE IF EXISTS `inventory_futurepurchase_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_futurepurchase_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `futurepurchase_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `futurepurchase_id` (`futurepurchase_id`,`category_id`),
  KEY `inventory_futurepurchase_categories_d1f7c05d` (`futurepurchase_id`),
  KEY `inventory_futurepurchase_categories_b583a629` (`category_id`),
  CONSTRAINT `D7be405610d8a84cd0c294b6597198a0` FOREIGN KEY (`futurepurchase_id`) REFERENCES `inventory_futurepurchase` (`id`),
  CONSTRAINT `inventory__category_id_1fcff7964342c0a7_fk_inventory_category_id` FOREIGN KEY (`category_id`) REFERENCES `inventory_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_futurepurchase_categories`
--

LOCK TABLES `inventory_futurepurchase_categories` WRITE;
/*!40000 ALTER TABLE `inventory_futurepurchase_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_futurepurchase_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_label`
--

DROP TABLE IF EXISTS `inventory_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_label`
--

LOCK TABLES `inventory_label` WRITE;
/*!40000 ALTER TABLE `inventory_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_location`
--

DROP TABLE IF EXISTS `inventory_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8941 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_location`
--

LOCK TABLES `inventory_location` WRITE;
/*!40000 ALTER TABLE `inventory_location` DISABLE KEYS */;
INSERT INTO `inventory_location` VALUES (8919,'standing desk shelf one'),(8920,'standing desk shelf two'),(8921,'standing desk shelf three'),(8922,'standing desk drawer one'),(8923,'standing desk drawer two'),(8924,'standing desk wall hook'),(8925,'standing desk bottom rack'),(8926,'standing desk floorspace'),(8927,'standing desk surrounding area'),(8928,'bathroom sink drawer one'),(8929,'bathroom sink drawer two'),(8930,'bathroom sink drawer three'),(8931,'bathroom sink cabinet'),(8932,'refrigerator top'),(8933,'dresser desk top'),(8934,'dresser cubby'),(8935,'dresser drawer one'),(8936,'dresser drawer two'),(8937,'dresser drawer three'),(8938,'dresser drawer four'),(8939,'dresser drawer five'),(8940,'dresser drawer six');
/*!40000 ALTER TABLE `inventory_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_possession`
--

DROP TABLE IF EXISTS `inventory_possession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_possession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `value` decimal(7,2) NOT NULL,
  `quality` decimal(3,2) NOT NULL,
  `utility` decimal(3,2) NOT NULL,
  `expected_expiration` date DEFAULT NULL,
  `sentimental` tinyint(1) NOT NULL,
  `practical` tinyint(1) NOT NULL,
  `condition` decimal(3,2) NOT NULL,
  `container_id` int(11),
  `location_id` int(11),
  PRIMARY KEY (`id`),
  KEY `inventory_possession_e71ddfe4` (`container_id`),
  KEY `inventory_possession_e274a5da` (`location_id`),
  CONSTRAINT `b7b3cc36a937b02471ffc380551aa86b` FOREIGN KEY (`container_id`) REFERENCES `inventory_containerpossession` (`possession_ptr_id`),
  CONSTRAINT `inventory__location_id_6b043218ba844c62_fk_inventory_location_id` FOREIGN KEY (`location_id`) REFERENCES `inventory_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27812 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_possession`
--

LOCK TABLES `inventory_possession` WRITE;
/*!40000 ALTER TABLE `inventory_possession` DISABLE KEYS */;
INSERT INTO `inventory_possession` VALUES (27744,'steel oats',0.00,0.50,0.50,NULL,0,0,0.50,NULL,NULL),(27745,'anime girl',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27746,'glass egg',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27747,'wolf medallion',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27748,'orange dice set',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27749,'gemstone',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27750,'harmonica',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27751,'carnegie mellon graduation pen',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27752,'graduation tassle',0.00,0.50,0.50,NULL,0,0,0.50,27744,NULL),(27753,'toe nail clippers',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8928),(27754,'loose index cards',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27755,'small portable clock',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8919),(27756,'amazon bluetooth speaker',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8919),(27757,'packaged index cards',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27758,'amazon bluetooth speaker charger',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8924),(27759,'lenovo t430 laptop',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8919),(27760,'lenovo t430 laptop charger',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8926),(27761,'logitech wireless mouse and keyboard',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8920),(27762,'liquid caffeine dropper bottle',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8920),(27763,'blue teapot',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8920),(27764,'red cup',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8920),(27765,'black pen',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27766,'postit notes',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27767,'code complete 2',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27768,'men of mathematics',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27769,'an introduction to database systems',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27770,'you are your own gym',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27771,'the little prince',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27772,'schumann piano concerto',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27773,'bach goldberg variations',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27774,'bach partitas',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27775,'chopin nocturnes',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27776,'yellow legal pads',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27777,'nexus 7',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27778,'nexus 7 charger block',0.00,0.50,0.50,NULL,0,0,0.50,NULL,NULL),(27779,'wrist watch',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27780,'wallet',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27781,'cell phone',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8921),(27782,'cell phone charger',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8926),(27783,'balancing board',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8926),(27784,'pet vacuum',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8925),(27785,'tall fan',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27786,'oil bin seat',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27787,'oil bin seat clothing pillow',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27788,'large surge protector',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27789,'power strip',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27790,'keyboard',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27791,'keyboard charger',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27792,'keyboard stand',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27793,'keyboard pedal',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27794,'keyboard chair',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8927),(27795,'cutting knife',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27796,'eating fork',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27797,'meat fork',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27798,'spoon',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27799,'bowl',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27800,'small plate',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27801,'large plate',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27802,'frying pan',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27803,'wok top',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27804,'large metal bowl',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27805,'salad bowl',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27806,'small metal bowl',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27807,'large measuring cup',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8932),(27808,'lenovo x61 laptop',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8933),(27809,'lenovo x61 laptop charger',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8933),(27810,'computer speakers',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8933),(27811,'audio- technica headphones',0.00,0.50,0.50,NULL,0,0,0.50,NULL,8934);
/*!40000 ALTER TABLE `inventory_possession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_possession_attributes`
--

DROP TABLE IF EXISTS `inventory_possession_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_possession_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `possession_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `possession_id` (`possession_id`,`attribute_id`),
  KEY `inventory_possession_attributes_3481396b` (`possession_id`),
  KEY `inventory_possession_attributes_e582ed73` (`attribute_id`),
  CONSTRAINT `inventor_attribute_id_44b745536907d7fb_fk_inventory_attribute_id` FOREIGN KEY (`attribute_id`) REFERENCES `inventory_attribute` (`id`),
  CONSTRAINT `invent_possession_id_38d04d1f89462db2_fk_inventory_possession_id` FOREIGN KEY (`possession_id`) REFERENCES `inventory_possession` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_possession_attributes`
--

LOCK TABLES `inventory_possession_attributes` WRITE;
/*!40000 ALTER TABLE `inventory_possession_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_possession_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_possession_categories`
--

DROP TABLE IF EXISTS `inventory_possession_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_possession_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `possession_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `possession_id` (`possession_id`,`category_id`),
  KEY `inventory_possession_categories_3481396b` (`possession_id`),
  KEY `inventory_possession_categories_b583a629` (`category_id`),
  CONSTRAINT `inventory__category_id_64f3c9c160815b33_fk_inventory_category_id` FOREIGN KEY (`category_id`) REFERENCES `inventory_category` (`id`),
  CONSTRAINT `invent_possession_id_39757dfec7d28c79_fk_inventory_possession_id` FOREIGN KEY (`possession_id`) REFERENCES `inventory_possession` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54540 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_possession_categories`
--

LOCK TABLES `inventory_possession_categories` WRITE;
/*!40000 ALTER TABLE `inventory_possession_categories` DISABLE KEYS */;
INSERT INTO `inventory_possession_categories` VALUES (54406,27745,6820),(54407,27746,6820),(54408,27747,6820),(54409,27748,6820),(54410,27749,6820),(54411,27750,6820),(54412,27750,6830),(54413,27751,6819),(54414,27751,6831),(54415,27752,6819),(54416,27753,6819),(54417,27754,6823),(54418,27754,6831),(54419,27755,6821),(54420,27755,6823),(54422,27756,6821),(54423,27756,6823),(54421,27756,6833),(54424,27757,6823),(54425,27757,6831),(54427,27758,6821),(54428,27758,6823),(54426,27758,6834),(54429,27759,6821),(54430,27759,6823),(54432,27760,6821),(54433,27760,6823),(54431,27760,6834),(54434,27761,6821),(54435,27761,6823),(54436,27762,6825),(54438,27763,6823),(54437,27763,6825),(54439,27763,6833),(54441,27764,6823),(54440,27764,6825),(54442,27765,6823),(54443,27765,6831),(54444,27766,6823),(54445,27766,6831),(54446,27767,6822),(54447,27767,6823),(54448,27768,6822),(54449,27768,6823),(54450,27769,6822),(54451,27769,6823),(54452,27770,6822),(54453,27770,6823),(54454,27771,6822),(54455,27771,6823),(54456,27772,6822),(54458,27772,6823),(54457,27772,6830),(54459,27773,6822),(54461,27773,6823),(54460,27773,6830),(54462,27774,6822),(54464,27774,6823),(54463,27774,6830),(54465,27775,6822),(54467,27775,6823),(54466,27775,6830),(54468,27776,6823),(54469,27776,6831),(54470,27777,6821),(54472,27778,6821),(54471,27778,6834),(54473,27779,6821),(54474,27779,6823),(54475,27780,6823),(54476,27781,6821),(54477,27781,6823),(54479,27782,6821),(54480,27782,6823),(54478,27782,6834),(54481,27783,6823),(54482,27784,6821),(54483,27784,6823),(54484,27785,6821),(54485,27785,6823),(54487,27786,6823),(54486,27786,6832),(54489,27787,6823),(54488,27787,6832),(54490,27788,6821),(54491,27788,6823),(54492,27789,6821),(54493,27789,6823),(54494,27790,6821),(54495,27790,6830),(54497,27791,6821),(54498,27791,6823),(54496,27791,6834),(54500,27792,6821),(54501,27792,6823),(54499,27792,6834),(54503,27793,6821),(54504,27793,6823),(54502,27793,6834),(54506,27794,6823),(54505,27794,6832),(54508,27795,6823),(54507,27795,6827),(54510,27796,6823),(54509,27796,6827),(54512,27797,6823),(54511,27797,6827),(54514,27798,6823),(54513,27798,6827),(54516,27799,6823),(54515,27799,6827),(54518,27800,6823),(54517,27800,6827),(54520,27801,6823),(54519,27801,6827),(54522,27802,6823),(54521,27802,6827),(54524,27803,6823),(54523,27803,6827),(54526,27804,6823),(54525,27804,6827),(54528,27805,6823),(54527,27805,6827),(54530,27806,6823),(54529,27806,6827),(54532,27807,6823),(54531,27807,6827),(54533,27808,6821),(54535,27809,6821),(54534,27809,6834),(54536,27810,6821),(54537,27810,6830),(54538,27811,6821),(54539,27811,6830);
/*!40000 ALTER TABLE `inventory_possession_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_possession_labels`
--

DROP TABLE IF EXISTS `inventory_possession_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_possession_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `possession_id` int(11) NOT NULL,
  `label_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `possession_id` (`possession_id`,`label_id`),
  KEY `inventory_possession_labels_3481396b` (`possession_id`),
  KEY `inventory_possession_labels_abec2aca` (`label_id`),
  CONSTRAINT `inventory_posses_label_id_10e333b695dde025_fk_inventory_label_id` FOREIGN KEY (`label_id`) REFERENCES `inventory_label` (`id`),
  CONSTRAINT `invent_possession_id_4de924dc0b350f4c_fk_inventory_possession_id` FOREIGN KEY (`possession_id`) REFERENCES `inventory_possession` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_possession_labels`
--

LOCK TABLES `inventory_possession_labels` WRITE;
/*!40000 ALTER TABLE `inventory_possession_labels` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_possession_labels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_purchase`
--

DROP TABLE IF EXISTS `inventory_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `cost` decimal(6,2) NOT NULL,
  `purchased_at` datetime NOT NULL,
  `reference` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_purchase`
--

LOCK TABLES `inventory_purchase` WRITE;
/*!40000 ALTER TABLE `inventory_purchase` DISABLE KEYS */;
INSERT INTO `inventory_purchase` VALUES (1,'Bobs Red Mill Honey Oat Granola, 12-Ounce (Pack of 4)',13.27,'2015-01-04 06:40:41','https://www.amazon.com/gp/css/order-history?ie=UTF8&ref_=pe_385040_127745480_TE_simp_on_sh&search=113-4038372-2179411');
/*!40000 ALTER TABLE `inventory_purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_room`
--

DROP TABLE IF EXISTS `inventory_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_room`
--

LOCK TABLES `inventory_room` WRITE;
/*!40000 ALTER TABLE `inventory_room` DISABLE KEYS */;
INSERT INTO `inventory_room` VALUES (1,'Bedroom'),(2,'Kitchen'),(3,'Bathroom'),(4,'Living room'),(5,'Room agnostic');
/*!40000 ALTER TABLE `inventory_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_academiccourse`
--

DROP TABLE IF EXISTS `media_academiccourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_academiccourse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `lecture_count` int(11) NOT NULL,
  `professor_id` int(11) NOT NULL,
  `university_id` int(11) NOT NULL,
  `year_taught` int(11) NOT NULL,
  `term_taught_id` int(11) NOT NULL,
  `completed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_academiccourse_fa68148b` (`professor_id`),
  KEY `media_academiccourse_890553cf` (`university_id`),
  KEY `media_academiccourse_a038e4cc` (`term_taught_id`),
  CONSTRAINT `media_acade_term_taught_id_12eef5812e130334_fk_academics_term_id` FOREIGN KEY (`term_taught_id`) REFERENCES `academics_term` (`id`),
  CONSTRAINT `media_ac_professor_id_358b591c883b5001_fk_academics_professor_id` FOREIGN KEY (`professor_id`) REFERENCES `academics_professor` (`id`),
  CONSTRAINT `media__university_id_32d46e89ec2151eb_fk_academics_university_id` FOREIGN KEY (`university_id`) REFERENCES `academics_university` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_academiccourse`
--

LOCK TABLES `media_academiccourse` WRITE;
/*!40000 ALTER TABLE `media_academiccourse` DISABLE KEYS */;
INSERT INTO `media_academiccourse` VALUES (2,'Introduction to Economics',24,1,1,2011,2,0),(3,'Introduction to Computer Science and Programming',24,2,2,2008,2,1),(4,'Programming Languages',11,3,3,2013,2,1),(5,'Financial Engineering and Risk Management Part 1',54,4,4,2013,2,0),(6,'Game Theory',24,5,5,2014,1,1);
/*!40000 ALTER TABLE `media_academiccourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_album`
--

DROP TABLE IF EXISTS `media_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `artist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_album_ca949605` (`artist_id`),
  CONSTRAINT `media_album_artist_id_149cba4dc5d08b46_fk_media_artist_id` FOREIGN KEY (`artist_id`) REFERENCES `media_artist` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_album`
--

LOCK TABLES `media_album` WRITE;
/*!40000 ALTER TABLE `media_album` DISABLE KEYS */;
INSERT INTO `media_album` VALUES (1,'Hey, Soul Sister',1),(2,'Lungs',2),(3,'Icarus',3),(4,'The City',3),(5,'Faure: Requiem & Messe Basse',4);
/*!40000 ALTER TABLE `media_album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_artist`
--

DROP TABLE IF EXISTS `media_artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_artist`
--

LOCK TABLES `media_artist` WRITE;
/*!40000 ALTER TABLE `media_artist` DISABLE KEYS */;
INSERT INTO `media_artist` VALUES (1,'Train'),(2,'Florence + The Machine'),(3,'Madeon'),(4,'Gabriel Faure');
/*!40000 ALTER TABLE `media_artist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_author`
--

DROP TABLE IF EXISTS `media_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_author`
--

LOCK TABLES `media_author` WRITE;
/*!40000 ALTER TABLE `media_author` DISABLE KEYS */;
INSERT INTO `media_author` VALUES (1,'Napolean','Hill'),(2,'George ','Leonard'),(3,'Melvin','Helitzer'),(4,'Carol','Fleming');
/*!40000 ALTER TABLE `media_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_book`
--

DROP TABLE IF EXISTS `media_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_book_4f331e2f` (`author_id`),
  CONSTRAINT `media_book_author_id_3a2fa3c5a8a60fc5_fk_media_author_id` FOREIGN KEY (`author_id`) REFERENCES `media_author` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_book`
--

LOCK TABLES `media_book` WRITE;
/*!40000 ALTER TABLE `media_book` DISABLE KEYS */;
INSERT INTO `media_book` VALUES (1,'Think and Grow Rich',1),(2,'Mastery: The Keys to Success and Long-Term Fulfillment ',2),(3,'Comedy Writing Secrets',3),(4,'The Sound of Your Voice',4);
/*!40000 ALTER TABLE `media_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_composer`
--

DROP TABLE IF EXISTS `media_composer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_composer` (
  `person_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`person_ptr_id`),
  CONSTRAINT `media_composer_person_ptr_id_1f81dea014e86edf_fk_core_person_id` FOREIGN KEY (`person_ptr_id`) REFERENCES `core_person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_composer`
--

LOCK TABLES `media_composer` WRITE;
/*!40000 ALTER TABLE `media_composer` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_composer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_episode`
--

DROP TABLE IF EXISTS `media_episode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_episode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `episode_number` int(11) NOT NULL,
  `viewed` tinyint(1) NOT NULL,
  `season_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_episode_b11701f0` (`season_id`),
  CONSTRAINT `media_episode_season_id_32663c181a7092d4_fk_media_season_id` FOREIGN KEY (`season_id`) REFERENCES `media_season` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1462 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_episode`
--

LOCK TABLES `media_episode` WRITE;
/*!40000 ALTER TABLE `media_episode` DISABLE KEYS */;
INSERT INTO `media_episode` VALUES (1,1,1,1),(2,2,1,1),(3,3,1,1),(4,4,1,1),(5,5,1,1),(6,6,1,1),(7,7,1,1),(8,8,1,1),(9,9,1,1),(10,10,1,1),(11,11,1,1),(12,1,1,2),(13,2,1,2),(14,3,1,2),(15,4,1,2),(16,5,1,2),(17,6,1,2),(18,7,1,2),(19,8,1,2),(20,9,1,2),(21,10,1,2),(22,11,1,2),(23,1,1,3),(24,2,1,3),(25,3,1,3),(26,4,1,3),(27,5,1,3),(28,6,1,3),(29,7,1,3),(30,1,1,4),(31,2,1,4),(32,3,1,4),(33,4,1,4),(34,5,1,4),(35,6,1,4),(36,7,1,4),(37,8,1,4),(38,9,1,4),(39,10,1,4),(40,11,1,4),(41,12,1,4),(42,13,1,4),(43,14,1,4),(44,15,1,4),(45,16,1,4),(46,17,1,4),(47,18,1,4),(48,19,1,4),(49,20,1,4),(50,21,1,4),(51,22,1,4),(52,1,1,5),(53,2,1,5),(54,3,1,5),(55,4,1,5),(56,5,1,5),(57,6,1,5),(58,7,1,5),(59,8,1,5),(60,9,1,5),(61,10,1,5),(62,11,1,5),(63,12,1,5),(64,13,1,5),(65,14,1,5),(66,15,1,5),(67,16,1,5),(68,17,1,5),(69,18,1,5),(70,1,0,6),(71,2,0,6),(72,3,0,6),(73,4,0,6),(74,5,0,6),(75,6,0,6),(76,7,0,6),(77,8,0,6),(78,9,0,6),(79,10,0,6),(80,11,0,6),(81,12,0,6),(82,13,0,6),(83,14,0,6),(84,15,0,6),(85,16,0,6),(86,17,0,6),(87,18,0,6),(88,19,0,6),(89,20,0,6),(90,21,0,6),(91,22,0,6),(92,1,1,7),(93,2,1,7),(94,3,1,7),(95,4,1,7),(96,5,1,7),(97,6,1,7),(98,7,1,7),(99,8,1,7),(100,9,1,7),(101,10,1,7),(102,11,1,7),(103,12,1,7),(104,13,1,7),(105,1,1,8),(106,2,1,8),(107,3,1,8),(108,4,1,8),(109,5,1,8),(110,6,1,8),(111,7,1,8),(112,8,1,8),(113,9,1,8),(114,10,1,8),(115,11,1,8),(116,12,1,8),(117,13,1,8),(118,1,1,9),(119,2,1,9),(120,3,1,9),(121,4,1,9),(122,5,1,9),(123,6,1,9),(124,7,1,9),(125,8,1,9),(126,9,1,9),(127,10,1,9),(128,11,1,9),(129,12,1,9),(130,13,1,9),(131,1,0,10),(132,2,0,10),(133,3,0,10),(134,4,0,10),(135,5,0,10),(136,6,0,10),(137,7,0,10),(138,8,0,10),(139,9,0,10),(140,10,0,10),(141,11,0,10),(142,12,0,10),(143,13,0,10),(144,1,0,11),(145,2,0,11),(146,3,0,11),(147,4,0,11),(148,5,0,11),(149,6,0,11),(150,7,0,11),(151,8,0,11),(152,9,0,11),(153,10,0,11),(154,11,0,11),(155,12,0,11),(156,13,0,11),(157,1,0,12),(158,2,0,12),(159,3,0,12),(160,4,0,12),(161,5,0,12),(162,6,0,12),(163,7,0,12),(164,8,0,12),(165,9,0,12),(166,10,0,12),(167,11,0,12),(168,12,0,12),(169,13,0,12),(170,1,0,13),(171,2,0,13),(172,3,0,13),(173,4,0,13),(174,5,0,13),(175,6,0,13),(176,7,0,13),(177,8,0,13),(178,9,0,13),(179,10,0,13),(180,11,0,13),(181,12,0,13),(182,13,0,13),(183,14,0,13),(184,1,1,14),(185,2,1,14),(186,3,1,14),(187,4,1,14),(188,5,1,14),(189,6,1,14),(190,1,1,15),(191,2,1,15),(192,3,1,15),(193,4,1,15),(194,5,1,15),(195,6,1,15),(196,1,1,16),(197,2,1,16),(198,3,1,16),(199,4,1,16),(200,5,1,16),(201,6,1,16),(202,1,1,17),(203,2,1,17),(204,3,1,17),(205,4,1,17),(206,5,1,17),(207,6,1,17),(208,1,1,18),(209,2,1,18),(210,3,1,18),(211,4,1,18),(212,5,1,18),(213,6,1,18),(214,7,1,18),(215,8,1,18),(216,9,1,18),(217,10,1,18),(218,11,1,18),(219,12,1,18),(220,13,1,18),(221,14,1,18),(222,15,1,18),(223,16,1,18),(224,17,1,18),(225,18,1,18),(226,19,1,18),(227,20,1,18),(228,21,1,18),(229,22,1,18),(230,23,1,18),(231,24,1,18),(232,25,1,18),(233,26,1,18),(234,27,1,18),(235,28,1,18),(236,29,1,18),(237,30,1,18),(238,31,1,18),(239,32,1,18),(240,33,1,18),(241,34,1,18),(242,35,1,18),(243,36,1,18),(244,37,1,18),(245,38,1,18),(246,39,1,18),(247,40,1,18),(248,41,1,18),(249,42,1,18),(250,43,1,18),(251,44,1,18),(252,45,1,18),(253,46,1,18),(254,47,1,18),(255,48,0,18),(256,1,1,19),(257,2,1,19),(258,3,1,19),(259,4,1,19),(260,5,1,19),(261,6,1,19),(262,7,1,19),(263,8,1,19),(264,9,1,19),(265,10,1,19),(266,11,1,19),(267,12,1,19),(268,13,1,19),(269,14,1,19),(270,15,1,19),(271,16,1,19),(272,17,1,19),(273,18,1,19),(274,19,1,19),(275,20,1,19),(276,21,1,19),(277,22,1,19),(278,1,1,20),(279,2,1,20),(280,3,1,20),(281,4,1,20),(282,5,1,20),(283,6,1,20),(284,7,1,20),(285,8,1,20),(286,9,1,20),(287,10,1,20),(288,11,1,20),(289,12,1,20),(290,13,1,20),(291,14,1,20),(292,15,1,20),(293,16,1,20),(294,17,1,20),(295,18,1,20),(296,19,1,20),(297,20,1,20),(298,21,1,20),(299,22,1,20),(300,1,1,21),(301,2,1,21),(302,3,1,21),(303,4,1,21),(304,5,1,21),(305,6,1,21),(306,7,1,21),(307,8,1,21),(308,9,1,21),(309,10,1,21),(310,11,1,21),(311,12,1,21),(312,13,1,21),(313,14,1,21),(314,15,1,21),(315,16,1,21),(316,17,1,21),(317,18,1,21),(318,19,1,21),(319,20,1,21),(320,21,1,21),(321,22,1,21),(322,1,1,22),(323,2,1,22),(324,3,1,22),(325,4,1,22),(326,5,1,22),(327,6,1,22),(328,7,1,22),(329,8,1,22),(330,9,1,22),(331,10,1,22),(332,11,1,22),(333,12,1,22),(334,13,0,22),(335,14,0,22),(336,15,0,22),(337,16,0,22),(338,17,0,22),(339,18,0,22),(340,19,0,22),(341,20,0,22),(342,21,0,22),(343,22,0,22),(366,1,1,24),(367,2,1,24),(368,3,1,24),(369,1,1,25),(370,2,1,25),(371,3,1,25),(372,1,1,26),(373,2,1,26),(374,3,1,26),(375,1,1,27),(376,2,1,27),(377,3,1,27),(378,4,1,27),(379,5,1,27),(380,6,1,27),(381,7,1,27),(382,8,1,27),(383,9,1,27),(384,10,1,27),(385,11,1,27),(386,12,1,27),(387,1,1,28),(388,2,1,28),(389,3,1,28),(390,4,1,28),(391,5,1,28),(392,6,1,28),(393,7,1,28),(394,8,1,28),(395,9,1,28),(396,10,1,28),(397,11,1,28),(398,12,1,28),(399,1,0,29),(400,2,0,29),(401,3,0,29),(402,4,0,29),(403,5,0,29),(404,6,0,29),(405,7,0,29),(406,8,0,29),(407,9,0,29),(408,10,0,29),(409,11,0,29),(410,12,0,29),(411,1,1,30),(412,2,1,30),(413,3,1,30),(414,4,1,30),(415,5,1,30),(416,6,1,30),(417,7,1,30),(418,8,1,30),(419,9,1,30),(420,10,1,30),(421,11,1,30),(422,12,1,30),(423,13,1,30),(424,1,1,31),(425,2,1,31),(426,3,1,31),(427,4,1,31),(428,5,1,31),(429,6,1,31),(430,7,1,31),(431,8,1,31),(432,9,1,31),(433,10,1,31),(434,11,1,31),(435,12,1,31),(436,13,1,31),(437,1,1,32),(438,2,1,32),(439,3,1,32),(440,4,1,32),(441,5,1,32),(442,6,1,32),(443,7,1,32),(444,8,1,32),(445,9,1,32),(446,10,1,32),(447,11,1,32),(448,12,1,32),(449,13,1,32),(450,1,1,33),(451,2,1,33),(452,3,1,33),(453,4,1,33),(454,5,1,33),(455,6,1,33),(456,7,1,33),(457,8,1,33),(458,9,1,33),(459,10,1,33),(460,11,1,33),(461,12,1,33),(462,13,1,33),(463,1,1,34),(464,2,1,34),(465,3,1,34),(466,4,1,34),(467,5,1,34),(468,6,1,34),(469,7,1,34),(470,8,1,34),(471,9,1,34),(472,10,1,34),(473,11,1,34),(474,12,1,34),(475,13,1,34),(476,1,1,35),(477,2,1,35),(478,3,1,35),(479,4,1,35),(480,5,1,35),(481,6,1,35),(482,7,1,35),(483,8,1,35),(484,9,1,35),(485,10,1,35),(486,11,1,35),(487,12,1,35),(488,13,1,35),(489,1,1,36),(490,2,1,36),(491,3,1,36),(492,4,1,36),(493,5,1,36),(494,6,1,36),(495,7,1,36),(496,8,1,36),(497,9,1,36),(498,10,1,36),(499,11,1,36),(500,12,1,36),(501,13,1,36),(502,1,1,37),(503,2,1,37),(504,3,1,37),(505,4,1,37),(506,5,1,37),(507,6,1,37),(508,7,1,37),(509,8,1,37),(510,9,1,37),(511,10,1,37),(512,11,1,37),(513,12,1,37),(514,13,1,37),(515,1,1,38),(516,2,1,38),(517,3,1,38),(518,4,1,38),(519,5,1,38),(520,6,1,38),(521,7,1,38),(522,8,1,38),(523,9,1,38),(524,10,1,38),(525,11,1,38),(526,12,1,38),(527,13,1,38),(528,14,1,38),(529,15,0,38),(530,16,0,38),(531,17,0,38),(532,18,0,38),(533,19,0,38),(534,20,0,38),(535,21,0,38),(536,22,0,38),(537,1,1,39),(538,2,1,39),(539,3,1,39),(540,4,1,39),(541,5,1,39),(542,6,1,39),(543,7,1,39),(544,8,1,39),(545,9,1,39),(546,10,1,39),(547,11,1,39),(548,12,1,39),(549,13,1,39),(550,14,1,39),(551,15,1,39),(552,16,1,39),(553,17,1,39),(554,18,1,39),(555,19,1,39),(556,20,1,39),(557,1,1,40),(558,2,1,40),(559,3,1,40),(560,4,1,40),(561,5,1,40),(562,6,1,40),(563,7,1,40),(564,8,1,40),(565,9,1,40),(566,10,1,40),(567,11,1,40),(568,12,1,40),(569,13,1,40),(570,14,1,40),(571,15,1,40),(572,16,1,40),(573,17,1,40),(574,18,1,40),(575,19,1,40),(576,20,1,40),(577,1,1,41),(578,2,1,41),(579,3,1,41),(580,4,1,41),(581,5,1,41),(582,6,1,41),(583,7,1,41),(584,8,1,41),(585,9,1,41),(586,10,1,41),(587,11,1,41),(588,12,1,41),(589,13,1,41),(590,14,1,41),(591,15,1,41),(592,16,1,41),(593,17,1,41),(594,18,1,41),(595,19,1,41),(596,20,1,41),(597,21,1,41),(598,1,1,42),(599,2,1,42),(600,3,1,42),(601,4,1,42),(602,5,1,42),(603,6,1,42),(604,7,1,42),(605,8,1,42),(606,9,1,42),(607,10,1,42),(608,11,1,42),(609,12,1,42),(610,13,1,42),(611,14,1,42),(612,15,1,42),(613,16,1,42),(614,17,1,42),(615,18,1,42),(616,19,1,42),(617,20,1,42),(618,21,1,42),(619,22,1,42),(620,23,1,42),(621,24,1,42),(622,25,1,42),(623,26,1,42),(624,27,1,42),(625,28,1,42),(626,29,1,42),(627,30,1,42),(628,31,1,42),(629,32,1,42),(630,33,1,42),(631,34,1,42),(632,35,1,42),(633,36,1,42),(634,37,1,42),(635,38,1,42),(636,39,1,42),(637,40,1,42),(638,41,1,42),(639,42,1,42),(640,43,1,42),(641,44,1,42),(642,45,1,42),(643,46,1,42),(644,47,1,42),(645,48,1,42),(646,49,1,42),(647,50,1,42),(648,51,1,42),(649,52,1,42),(650,53,1,42),(651,54,1,42),(652,55,1,42),(653,56,1,42),(654,57,1,42),(655,58,1,42),(656,59,1,42),(657,60,1,42),(658,61,1,42),(659,62,1,42),(660,63,1,42),(661,64,1,42),(662,65,1,42),(663,66,1,42),(664,67,1,42),(665,68,1,42),(666,69,1,42),(667,70,1,42),(668,71,1,42),(669,72,1,42),(670,73,1,42),(671,74,1,42),(672,1,1,43),(673,2,1,43),(674,3,1,43),(675,4,1,43),(676,5,1,43),(677,6,1,43),(678,7,1,43),(679,8,1,43),(680,9,1,43),(681,10,1,43),(682,11,1,43),(683,12,1,43),(684,13,1,43),(685,1,1,44),(686,2,1,44),(687,3,1,44),(688,4,1,44),(689,5,1,44),(690,6,1,44),(691,7,1,44),(692,8,1,44),(693,9,1,44),(694,10,1,44),(695,11,1,44),(696,12,1,44),(697,13,1,44),(698,1,1,45),(699,2,1,45),(700,3,1,45),(701,4,1,45),(702,5,1,45),(703,6,1,45),(704,7,1,45),(705,8,1,45),(706,9,1,45),(707,10,1,45),(708,11,1,45),(709,12,1,45),(710,13,1,45),(711,14,1,45),(712,15,1,45),(713,16,1,45),(714,17,1,45),(715,18,1,45),(716,19,1,45),(717,20,1,45),(718,21,1,45),(719,22,1,45),(720,23,1,45),(721,24,1,45),(722,25,1,45),(723,26,1,45),(724,27,1,45),(725,28,1,45),(726,29,1,45),(727,30,1,45),(728,31,1,45),(729,32,1,45),(730,33,1,45),(731,34,1,45),(732,35,1,45),(733,36,1,45),(734,37,1,45),(735,38,1,45),(736,39,1,45),(737,40,1,45),(738,41,1,45),(739,42,1,45),(740,43,1,45),(741,44,1,45),(742,45,1,45),(743,46,1,45),(744,47,1,45),(745,48,1,45),(746,49,1,45),(747,50,1,45),(748,51,1,45),(749,1,1,46),(750,2,1,46),(751,3,1,46),(752,4,1,46),(753,5,1,46),(754,6,1,46),(755,7,1,46),(756,8,1,46),(757,9,1,46),(758,10,1,46),(759,11,1,46),(760,12,1,46),(761,13,1,46),(762,14,1,46),(763,15,1,46),(764,16,1,46),(765,17,1,46),(766,18,1,46),(767,19,1,46),(768,20,1,46),(769,21,1,46),(770,22,1,46),(771,23,1,46),(772,24,1,46),(773,25,1,46),(774,1,1,47),(775,2,1,47),(776,3,1,47),(777,4,1,47),(778,5,1,47),(779,6,1,47),(780,7,1,47),(781,8,1,47),(782,9,1,47),(783,10,1,47),(784,11,1,47),(785,12,1,47),(786,13,1,47),(787,14,1,47),(788,15,1,47),(789,16,1,47),(790,17,1,47),(791,18,1,47),(792,19,1,47),(793,20,1,47),(794,21,1,47),(795,22,1,47),(796,23,1,47),(797,24,1,47),(798,1,0,48),(799,2,0,48),(800,3,0,48),(801,4,0,48),(802,5,0,48),(803,6,0,48),(804,7,0,48),(805,8,0,48),(806,9,0,48),(807,10,0,48),(808,11,0,48),(809,12,0,48),(810,13,0,48),(811,14,0,48),(812,15,0,48),(813,16,0,48),(814,17,0,48),(815,18,0,48),(816,19,0,48),(817,20,0,48),(818,21,0,48),(819,22,0,48),(820,1,0,49),(821,2,0,49),(822,3,0,49),(823,4,0,49),(824,5,0,49),(825,6,0,49),(826,7,0,49),(827,8,0,49),(828,9,0,49),(829,10,0,49),(830,11,0,49),(831,12,0,49),(832,13,0,49),(833,1,0,50),(834,2,0,50),(835,3,0,50),(836,4,0,50),(837,5,0,50),(838,6,0,50),(839,7,0,50),(840,8,0,50),(841,9,0,50),(842,10,0,50),(843,11,0,50),(844,12,0,50),(845,13,0,50),(846,1,0,51),(847,2,0,51),(848,3,0,51),(849,4,0,51),(850,5,0,51),(851,6,0,51),(852,7,0,51),(853,8,0,51),(854,9,0,51),(855,10,0,51),(856,11,0,51),(857,12,0,51),(858,13,0,51),(859,1,1,52),(860,2,1,52),(861,3,1,52),(862,4,1,52),(863,5,1,52),(864,6,1,52),(865,7,1,52),(866,8,1,52),(867,9,1,52),(868,10,1,52),(869,11,1,52),(870,1,0,53),(871,2,0,53),(872,3,0,53),(873,4,0,53),(874,5,0,53),(875,6,0,53),(876,7,0,53),(877,8,0,53),(878,9,0,53),(879,10,0,53),(880,1,1,54),(881,2,1,54),(882,3,1,54),(883,4,1,54),(884,5,1,54),(885,6,1,54),(886,7,1,54),(887,8,1,54),(888,9,1,54),(889,10,1,54),(890,11,1,54),(891,12,1,54),(892,1,1,55),(893,2,1,55),(894,3,1,55),(895,4,1,55),(896,5,1,55),(897,6,1,55),(898,7,1,55),(899,8,1,55),(900,9,1,55),(901,10,1,55),(902,11,1,55),(903,12,1,55),(904,13,1,55),(905,1,1,56),(906,2,1,56),(907,3,1,56),(908,4,1,56),(909,5,1,56),(910,6,1,56),(911,7,1,56),(912,8,1,56),(913,9,1,56),(914,1,1,57),(915,2,1,57),(916,3,1,57),(917,4,1,57),(918,5,1,57),(919,6,1,57),(920,7,1,57),(921,8,1,57),(922,9,1,57),(923,10,1,57),(924,11,1,57),(925,12,1,57),(926,13,1,57),(927,14,1,57),(928,15,1,57),(929,16,1,57),(930,17,1,57),(931,18,1,57),(932,19,1,57),(933,20,1,57),(934,21,1,57),(935,22,1,57),(936,23,1,57),(937,1,0,58),(938,2,0,58),(939,3,0,58),(940,4,0,58),(941,5,0,58),(942,6,0,58),(943,7,0,58),(944,8,0,58),(945,9,0,58),(946,10,0,58),(947,11,0,58),(948,12,0,58),(949,13,0,58),(950,14,0,58),(951,15,0,58),(952,16,0,58),(953,17,0,58),(954,18,0,58),(955,19,0,58),(956,20,0,58),(957,21,0,58),(958,22,0,58),(959,1,1,59),(960,2,1,59),(961,3,1,59),(962,4,1,59),(963,5,1,59),(964,6,1,59),(965,7,1,59),(966,8,1,59),(967,9,1,59),(968,10,1,59),(969,1,1,60),(970,2,1,60),(971,3,1,60),(972,4,1,60),(973,5,1,60),(974,6,1,60),(975,7,1,60),(976,8,1,60),(977,9,1,60),(978,10,1,60),(979,1,1,61),(980,2,1,61),(981,3,1,61),(982,4,1,61),(983,5,1,61),(984,6,1,61),(985,7,1,61),(986,8,1,61),(987,9,1,61),(988,10,1,61),(989,1,1,62),(990,2,1,62),(991,3,1,62),(992,4,1,62),(993,5,1,62),(994,6,1,62),(995,7,1,62),(996,8,1,62),(997,9,1,62),(998,10,1,62),(999,1,1,63),(1000,2,1,63),(1001,3,1,63),(1002,4,1,63),(1003,5,1,63),(1004,6,1,63),(1005,7,1,63),(1006,8,1,63),(1007,9,1,63),(1008,10,1,63),(1009,1,1,64),(1010,2,1,64),(1011,3,1,64),(1012,4,1,64),(1013,5,1,64),(1014,6,1,64),(1015,7,1,64),(1016,8,1,64),(1017,9,1,64),(1018,10,1,64),(1019,11,1,64),(1020,12,1,64),(1021,13,1,64),(1022,1,1,65),(1023,2,1,65),(1024,3,1,65),(1025,4,1,65),(1026,5,1,65),(1027,6,1,65),(1028,7,1,65),(1029,8,1,65),(1030,9,1,65),(1031,10,1,65),(1032,11,1,65),(1033,12,1,65),(1034,13,1,65),(1035,1,1,66),(1036,2,1,66),(1037,3,1,66),(1038,4,1,66),(1039,5,1,66),(1040,6,1,66),(1041,7,1,66),(1042,8,1,66),(1043,9,1,66),(1044,10,1,66),(1045,11,1,66),(1046,12,1,66),(1047,13,1,66),(1048,1,0,67),(1049,2,0,67),(1050,3,0,67),(1051,4,0,67),(1052,5,0,67),(1053,6,0,67),(1054,7,0,67),(1055,8,0,67),(1056,9,0,67),(1057,10,0,67),(1058,11,0,67),(1059,12,0,67),(1060,13,0,67),(1061,1,0,68),(1062,2,0,68),(1063,3,0,68),(1064,4,0,68),(1065,5,0,68),(1066,6,0,68),(1067,7,0,68),(1068,8,0,68),(1069,9,0,68),(1070,10,0,68),(1071,11,0,68),(1072,12,0,68),(1073,13,0,68),(1074,1,1,69),(1075,2,1,69),(1076,3,1,69),(1077,4,1,69),(1078,5,1,69),(1079,6,1,69),(1080,7,1,69),(1081,8,1,69),(1082,9,1,69),(1083,10,1,69),(1084,1,1,70),(1085,2,1,70),(1086,3,1,70),(1087,4,1,70),(1088,5,1,70),(1089,6,1,70),(1090,7,1,70),(1091,8,1,70),(1092,9,1,70),(1093,10,1,70),(1094,1,1,71),(1095,2,1,71),(1096,3,1,71),(1097,4,1,71),(1098,5,1,71),(1099,6,1,71),(1100,7,1,71),(1101,8,1,71),(1102,1,1,72),(1103,2,1,72),(1104,3,1,72),(1105,4,1,72),(1106,5,1,72),(1107,6,1,72),(1108,7,1,72),(1109,8,1,72),(1110,9,1,72),(1111,10,1,72),(1112,1,0,73),(1113,2,0,73),(1114,3,0,73),(1115,4,0,73),(1116,5,0,73),(1117,6,0,73),(1118,7,0,73),(1119,8,0,73),(1120,9,0,73),(1121,10,0,73),(1122,1,0,74),(1123,2,0,74),(1124,3,0,74),(1125,4,0,74),(1126,5,0,74),(1127,6,0,74),(1128,7,0,74),(1129,8,0,74),(1130,9,0,74),(1131,10,0,74),(1132,1,0,75),(1133,2,0,75),(1134,3,0,75),(1135,4,0,75),(1136,5,0,75),(1137,6,0,75),(1138,7,0,75),(1139,8,0,75),(1140,9,0,75),(1141,10,0,75),(1142,1,0,76),(1143,2,0,76),(1144,3,0,76),(1145,4,0,76),(1146,5,0,76),(1147,6,0,76),(1148,7,0,76),(1149,8,0,76),(1150,9,0,76),(1151,10,0,76),(1152,1,0,77),(1153,2,0,77),(1154,3,0,77),(1155,4,0,77),(1156,5,0,77),(1157,6,0,77),(1158,7,0,77),(1159,8,0,77),(1160,9,0,77),(1161,10,0,77),(1162,11,0,77),(1163,12,0,77),(1164,13,0,77),(1165,1,1,78),(1166,2,1,78),(1167,3,1,78),(1168,4,1,78),(1169,5,1,78),(1170,6,1,78),(1171,7,1,78),(1172,8,1,78),(1173,9,1,78),(1174,10,1,78),(1175,11,1,78),(1176,12,1,78),(1177,13,1,78),(1178,14,1,78),(1179,15,1,78),(1180,16,1,78),(1181,17,1,78),(1182,18,1,78),(1183,19,1,78),(1184,20,1,78),(1185,21,1,78),(1186,22,1,78),(1187,1,1,79),(1188,2,1,79),(1189,3,1,79),(1190,4,1,79),(1191,5,1,79),(1192,6,1,79),(1193,7,1,79),(1194,8,1,79),(1195,9,1,79),(1196,10,1,79),(1197,11,1,79),(1198,12,1,79),(1199,13,1,79),(1200,14,1,79),(1201,15,1,79),(1202,16,1,79),(1203,17,1,79),(1204,18,1,79),(1205,19,1,79),(1206,20,1,79),(1207,21,1,79),(1208,22,1,79),(1209,23,1,79),(1210,24,1,79),(1211,1,1,80),(1212,2,1,80),(1213,3,1,80),(1214,4,1,80),(1215,5,1,80),(1216,6,1,80),(1217,7,1,80),(1218,8,1,80),(1219,9,1,80),(1220,10,1,80),(1221,11,1,80),(1222,12,1,80),(1223,13,1,80),(1224,14,1,80),(1225,15,1,80),(1226,16,1,80),(1227,17,1,80),(1228,18,1,80),(1229,19,1,80),(1230,20,1,80),(1231,21,1,80),(1232,22,1,80),(1233,23,1,80),(1234,24,1,80),(1235,1,1,81),(1236,2,1,81),(1237,3,1,81),(1238,4,1,81),(1239,5,1,81),(1240,6,1,81),(1241,7,1,81),(1242,8,1,81),(1243,9,1,81),(1244,10,1,81),(1245,11,1,81),(1246,12,1,81),(1247,13,1,81),(1248,14,1,81),(1249,15,1,81),(1250,16,1,81),(1251,1,1,82),(1252,2,1,82),(1253,3,1,82),(1254,4,1,82),(1255,5,1,82),(1256,6,1,82),(1257,7,1,82),(1258,8,1,82),(1259,9,1,82),(1260,10,1,82),(1261,11,0,82),(1262,12,0,82),(1263,13,0,82),(1264,14,0,82),(1265,15,0,82),(1266,16,0,82),(1267,17,0,82),(1268,18,0,82),(1269,19,0,82),(1270,20,0,82),(1271,21,0,82),(1272,22,0,82),(1273,23,0,82),(1274,24,0,82),(1275,1,0,83),(1276,2,0,83),(1277,3,0,83),(1278,4,0,83),(1279,5,0,83),(1280,6,0,83),(1281,7,0,83),(1282,8,0,83),(1283,9,0,83),(1284,10,0,83),(1285,11,0,83),(1286,12,0,83),(1287,13,0,83),(1288,14,0,83),(1289,15,0,83),(1290,16,0,83),(1291,17,0,83),(1292,18,0,83),(1293,19,0,83),(1294,20,0,83),(1295,21,0,83),(1296,22,0,83),(1297,1,0,84),(1298,2,0,84),(1299,3,0,84),(1300,4,0,84),(1301,5,0,84),(1302,6,0,84),(1303,7,0,84),(1304,8,0,84),(1305,9,0,84),(1306,10,0,84),(1307,11,0,84),(1308,12,0,84),(1309,13,0,84),(1310,14,0,84),(1311,15,0,84),(1312,16,0,84),(1313,17,0,84),(1314,18,0,84),(1315,19,0,84),(1316,20,0,84),(1317,21,0,84),(1318,22,0,84),(1319,23,0,84),(1320,1,0,85),(1321,2,0,85),(1322,3,0,85),(1323,4,0,85),(1324,5,0,85),(1325,6,0,85),(1326,7,0,85),(1327,8,0,85),(1328,9,0,85),(1329,10,0,85),(1330,11,0,85),(1331,12,0,85),(1332,13,0,85),(1333,14,0,85),(1334,15,0,85),(1335,16,0,85),(1336,17,0,85),(1337,18,0,85),(1338,19,0,85),(1339,20,0,85),(1340,21,0,85),(1341,22,0,85),(1342,1,1,92),(1343,2,1,92),(1344,3,1,92),(1345,4,1,92),(1346,5,1,92),(1347,6,1,92),(1348,7,1,92),(1349,8,1,92),(1350,9,1,92),(1351,10,1,92),(1352,11,1,92),(1353,12,1,92),(1354,13,1,92),(1355,14,1,92),(1356,1,1,93),(1357,2,1,93),(1358,3,1,93),(1359,4,1,93),(1360,5,1,93),(1361,6,1,93),(1362,7,1,93),(1363,8,1,93),(1364,9,1,93),(1365,10,1,93),(1366,11,1,93),(1367,12,1,93),(1368,13,1,93),(1369,14,1,93),(1370,15,1,93),(1371,16,1,93),(1372,1,1,94),(1373,2,1,94),(1374,3,1,94),(1375,4,1,94),(1376,5,1,94),(1377,6,1,94),(1378,7,0,94),(1379,8,0,94),(1380,9,0,94),(1381,10,0,94),(1382,11,0,94),(1383,12,0,94),(1384,13,0,94),(1385,14,0,94),(1386,15,0,94),(1387,16,0,94),(1388,1,0,95),(1389,2,0,95),(1390,3,0,95),(1391,4,0,95),(1392,5,0,95),(1393,6,0,95),(1394,7,0,95),(1395,8,0,95),(1396,9,0,95),(1397,10,0,95),(1398,11,0,95),(1399,12,0,95),(1400,13,0,95),(1401,14,0,95),(1402,15,0,95),(1403,16,0,95),(1404,1,0,96),(1405,2,0,96),(1406,3,0,96),(1407,4,0,96),(1408,5,0,96),(1409,6,0,96),(1410,7,0,96),(1411,8,0,96),(1412,9,0,96),(1413,10,0,96),(1414,11,0,96),(1415,12,0,96),(1416,13,0,96),(1417,1,0,97),(1418,2,0,97),(1419,3,0,97),(1420,4,0,97),(1421,5,0,97),(1422,6,0,97),(1423,1,0,98),(1424,2,0,98),(1425,3,0,98),(1426,4,0,98),(1427,5,0,98),(1428,6,0,98),(1429,7,0,98),(1430,8,0,98),(1431,9,0,98),(1432,10,0,98),(1433,11,0,98),(1434,12,0,98),(1435,13,0,98),(1436,14,0,98),(1437,15,0,98),(1438,16,0,98),(1439,17,0,98),(1440,18,0,98),(1441,19,0,98),(1442,20,0,98),(1443,21,0,98),(1444,22,0,98),(1445,23,0,98),(1446,24,0,98),(1447,25,0,98),(1448,26,0,98),(1449,1,0,99),(1450,2,0,99),(1451,3,0,99),(1452,4,0,99),(1453,5,0,99),(1454,6,0,99),(1455,7,0,99),(1456,8,0,99),(1457,9,0,99),(1458,10,0,99),(1459,11,0,99),(1460,12,0,99),(1461,13,0,99);
/*!40000 ALTER TABLE `media_episode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_lecture`
--

DROP TABLE IF EXISTS `media_lecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_lecture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lecture_number` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `viewed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_lecture_ea134da7` (`course_id`),
  CONSTRAINT `media_lect_course_id_773d7d0205a4d619_fk_media_academiccourse_id` FOREIGN KEY (`course_id`) REFERENCES `media_academiccourse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_lecture`
--

LOCK TABLES `media_lecture` WRITE;
/*!40000 ALTER TABLE `media_lecture` DISABLE KEYS */;
INSERT INTO `media_lecture` VALUES (3,1,2,1),(4,2,2,1),(5,3,2,1),(6,4,2,1),(7,5,2,1),(8,6,2,0),(9,7,2,0),(10,8,2,0),(11,9,2,0),(12,10,2,0),(13,11,2,0),(14,12,2,0),(15,13,2,0),(16,14,2,0),(17,15,2,0),(18,16,2,0),(19,17,2,0),(20,18,2,0),(21,19,2,0),(22,20,2,0),(23,21,2,0),(24,22,2,0),(25,23,2,0),(26,24,2,0),(27,1,3,1),(28,2,3,1),(29,3,3,1),(30,4,3,1),(31,5,3,1),(32,6,3,1),(33,7,3,1),(34,8,3,1),(35,9,3,1),(36,10,3,1),(37,11,3,1),(38,12,3,1),(39,13,3,1),(40,14,3,1),(41,15,3,1),(42,16,3,1),(43,17,3,1),(44,18,3,1),(45,19,3,1),(46,20,3,1),(47,21,3,1),(48,22,3,1),(49,23,3,1),(50,24,3,1),(51,25,3,1),(52,1,4,1),(53,2,4,1),(54,3,4,1),(55,4,4,1),(56,5,4,1),(57,6,4,1),(58,7,4,1),(59,8,4,1),(60,9,4,1),(61,10,4,1),(62,11,4,1),(63,1,5,0),(64,2,5,0),(65,3,5,0),(66,4,5,0),(67,5,5,0),(68,6,5,0),(69,7,5,0),(70,8,5,0),(71,9,5,0),(72,10,5,0),(73,11,5,0),(74,12,5,0),(75,13,5,0),(76,14,5,0),(77,15,5,0),(78,16,5,0),(79,17,5,0),(80,18,5,0),(81,19,5,0),(82,20,5,0),(83,21,5,0),(84,22,5,0),(85,23,5,0),(86,24,5,0),(87,25,5,0),(88,26,5,0),(89,27,5,0),(90,28,5,0),(91,29,5,0),(92,30,5,0),(93,31,5,0),(94,32,5,0),(95,33,5,0),(96,34,5,0),(97,35,5,0),(98,36,5,0),(99,37,5,0),(100,38,5,0),(101,39,5,0),(102,40,5,0),(103,41,5,0),(104,42,5,0),(105,43,5,0),(106,44,5,0),(107,45,5,0),(108,46,5,0),(109,47,5,0),(110,48,5,0),(111,49,5,0),(112,50,5,0),(113,51,5,0),(114,52,5,0),(115,53,5,0),(116,54,5,0),(117,1,6,1),(118,2,6,1),(119,3,6,1),(120,4,6,1),(121,5,6,1),(122,6,6,1),(123,7,6,1),(124,8,6,1),(125,9,6,1),(126,10,6,1),(127,11,6,1),(128,12,6,1),(129,13,6,1),(130,14,6,1),(131,15,6,1),(132,16,6,1),(133,17,6,1),(134,18,6,1),(135,19,6,1),(136,20,6,1),(137,21,6,1),(138,22,6,1),(139,23,6,1),(140,24,6,1);
/*!40000 ALTER TABLE `media_lecture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_movie`
--

DROP TABLE IF EXISTS `media_movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `rating` decimal(3,2) NOT NULL,
  `seen` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_movie`
--

LOCK TABLES `media_movie` WRITE;
/*!40000 ALTER TABLE `media_movie` DISABLE KEYS */;
INSERT INTO `media_movie` VALUES (1,'Saturday Night Fever',0.71,1),(2,'The Wolf of Wallstreet',0.87,1);
/*!40000 ALTER TABLE `media_movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_season`
--

DROP TABLE IF EXISTS `media_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `season_number` int(11) NOT NULL,
  `tvshow_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_season_6e4f2578` (`tvshow_id`),
  CONSTRAINT `media_seas_tvshow_id_4325dd6b2e21dd6d_fk_media_televisionshow_id` FOREIGN KEY (`tvshow_id`) REFERENCES `media_televisionshow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_season`
--

LOCK TABLES `media_season` WRITE;
/*!40000 ALTER TABLE `media_season` DISABLE KEYS */;
INSERT INTO `media_season` VALUES (1,1,1),(2,2,1),(3,1,2),(4,2,2),(5,3,2),(6,4,2),(7,1,3),(8,2,3),(9,3,3),(10,4,3),(11,5,3),(12,6,3),(13,7,3),(14,1,4),(15,2,4),(16,3,4),(17,4,4),(18,1,5),(19,1,6),(20,2,6),(21,3,6),(22,4,6),(24,1,8),(25,2,8),(26,3,8),(27,1,9),(28,2,9),(29,3,9),(30,1,10),(31,2,10),(32,3,10),(33,4,10),(34,5,10),(35,1,11),(36,2,11),(37,3,11),(38,1,12),(39,1,13),(40,2,13),(41,3,13),(42,1,14),(43,1,15),(44,2,15),(45,1,16),(46,1,17),(47,2,17),(48,3,17),(49,4,17),(50,5,17),(51,6,17),(52,1,18),(53,2,18),(54,1,19),(55,1,20),(56,2,20),(57,3,20),(58,4,20),(59,1,21),(60,2,21),(61,3,21),(62,4,21),(63,1,22),(64,2,22),(65,3,22),(66,4,22),(67,5,22),(68,6,22),(69,1,23),(70,2,23),(71,3,23),(72,4,23),(73,1,24),(74,1,25),(75,2,25),(76,3,25),(77,4,25),(78,1,26),(79,2,26),(80,3,26),(81,4,26),(82,5,26),(83,6,26),(84,7,26),(85,8,26),(92,1,28),(93,2,28),(94,3,28),(95,4,28),(96,5,28),(97,6,28),(98,1,29),(99,3,11);
/*!40000 ALTER TABLE `media_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_song`
--

DROP TABLE IF EXISTS `media_song`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_song` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `album_id` int(11) NOT NULL,
  `rating` decimal(3,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_song_95c3b9df` (`album_id`),
  CONSTRAINT `media_song_album_id_4824eb3363b8cb90_fk_media_album_id` FOREIGN KEY (`album_id`) REFERENCES `media_album` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_song`
--

LOCK TABLES `media_song` WRITE;
/*!40000 ALTER TABLE `media_song` DISABLE KEYS */;
INSERT INTO `media_song` VALUES (1,'Hey, Soul Sister',1,0.50),(2,'Dog Days Are Over',2,0.50),(3,'Icarus',3,0.94),(4,'The City',4,0.96),(5,'Faure\'s Requiem in D Minor',5,0.85);
/*!40000 ALTER TABLE `media_song` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_televisionshow`
--

DROP TABLE IF EXISTS `media_televisionshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_televisionshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `season_count` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_televisionshow`
--

LOCK TABLES `media_televisionshow` WRITE;
/*!40000 ALTER TABLE `media_televisionshow` DISABLE KEYS */;
INSERT INTO `media_televisionshow` VALUES (1,'Psycho-Pass',2,0),(2,'Scandal',4,0),(3,'Mad Men',7,0),(4,'The IT Crowd',4,0),(5,'Fairy Tail',1,1),(6,'Glee',4,1),(8,'Sherlock',3,0),(9,'Borgia',3,0),(10,'Merlin',5,0),(11,'House of Cards',3,0),(12,'Reign',1,1),(13,'Avatar: The Last Airbender',3,0),(14,'Monster',1,0),(15,'Vampire Knight',2,0),(16,'Soul Eater',1,0),(17,'Community',6,0),(18,'Rick and Morty',2,0),(19,'BoJack Horseman',1,0),(20,'Bob\'s Burgers',4,0),(21,'Game of Thrones',4,0),(22,'Archer',6,0),(23,'The Tudors',4,0),(24,'Marco Polo',1,0),(25,'Hell on Wheels',4,0),(26,'House',8,1),(28,'White Collar',6,1),(29,'Ouran High School Host Club',1,0);
/*!40000 ALTER TABLE `media_televisionshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_project`
--

DROP TABLE IF EXISTS `projects_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_project`
--

LOCK TABLES `projects_project` WRITE;
/*!40000 ALTER TABLE `projects_project` DISABLE KEYS */;
INSERT INTO `projects_project` VALUES (1,'Learning Management System'),(2,'Courtier'),(3,'Clockwork'),(4,'Job hunting'),(5,'Lumosity'),(6,'Duolingo'),(7,'Coursera');
/*!40000 ALTER TABLE `projects_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_binarymood`
--

DROP TABLE IF EXISTS `psychology_binarymood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_binarymood` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `mood` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_binarymood`
--

LOCK TABLES `psychology_binarymood` WRITE;
/*!40000 ALTER TABLE `psychology_binarymood` DISABLE KEYS */;
INSERT INTO `psychology_binarymood` VALUES (1,'2015-02-22 04:08:36',0),(2,'2015-02-22 04:09:31',0),(3,'2015-02-22 04:09:33',1);
/*!40000 ALTER TABLE `psychology_binarymood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_conversation`
--

DROP TABLE IF EXISTS `psychology_conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_conversation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_conversation`
--

LOCK TABLES `psychology_conversation` WRITE;
/*!40000 ALTER TABLE `psychology_conversation` DISABLE KEYS */;
/*!40000 ALTER TABLE `psychology_conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_happinessmoment`
--

DROP TABLE IF EXISTS `psychology_happinessmoment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_happinessmoment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `happiness` decimal(3,2) DEFAULT NULL,
  `notes` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_happinessmoment`
--

LOCK TABLES `psychology_happinessmoment` WRITE;
/*!40000 ALTER TABLE `psychology_happinessmoment` DISABLE KEYS */;
INSERT INTO `psychology_happinessmoment` VALUES (1,'2015-01-01 02:06:12',0.96,'It was during my first or second set of benching. I had just started working out. I had taken two droplets of caffeine just before my workout, but this was absolutely not the only cause. I\'ve been taking caffeine all day and most of the day has been miserable (this is most likely the cause for the peak). I was listening to house music - this had a great affect. The particular song was \"The City\" by Madeon, but there was also \"Silhouettes\" by Avicii playing just before it.'),(2,'2015-01-01 03:59:25',0.83,'Lighting the Hanukkah candles on the \"last night\", December 31, New Years Eve, listening to a good, peaceful album I found on Spotify.'),(3,'2015-01-02 07:09:36',0.91,'Watching House, after midnight, realizing how much we are alike');
/*!40000 ALTER TABLE `psychology_happinessmoment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_journal`
--

DROP TABLE IF EXISTS `psychology_journal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_journal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` longtext,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_journal`
--

LOCK TABLES `psychology_journal` WRITE;
/*!40000 ALTER TABLE `psychology_journal` DISABLE KEYS */;
INSERT INTO `psychology_journal` VALUES (1,'The Project (on the flight back from San Francisco)','The path for Tier 27 is becoming clearer each day. From this point, we need to hold tight onto our developing team. With Eric and Andreea on the table for potential work, we can now advertise a team of seven. Our team members include myself, Ryan Douvlos, Dylan Moore, Matt Getz, Omar Jatoi, Andreea Manolache, and Eric Habich. I,  Andreea, and Eric all hold degrees from Carnegie Mellon University, with degrees in Mathematics, Architecture, and Design. Ryan holds a degree from Pennsylvania State University in Finance. Matt holds a degree Mathematics and Biology from the University of Florida. Omar is in his final year, working towards a degree in logic, with a specialty in security.\r\n\r\nAvailable hours per week by staff:\r\nMyself: 70 (35)\r\nRyan: 60 (30)\r\nMatt: 40\r\nAndreea: 20\r\nDylan: 15\r\nEric: 15\r\nOmar: 5\r\n\r\nTotal available hours per week: 225\r\nHours per week needed to maintain normal business operations: 65, split 35 to myself, and 30 to Ryan.\r\nRemaining hours which may be devoted to an application: 160 / week\r\n\r\nProposed wages for applications: \r\nMyself: $40 / hour\r\nRyan: $40 / hour\r\nMatt: $15 / hour (with a strong consideration for $20 / hour)\r\nAndreea: $15 / hour (with a strong consideration for $20 / hour)\r\nEric: $20 / hour (with a strong consideration for $25 / hour)\r\n**Note: Eric is to get more money because his line of work is more specialized, and the overall time requirement is much lower. Eric also has a full time job paying him $80k / year ($40 / hour)\r\nOmar: $15 / hour\r\nDylan: $12 / hour\r\n\r\nWeekly labor costs for an application\r\nMyself: 35 hours * $40 / hour = $1,400\r\nRyan: 30 hours * $40 / hour = $1,200\r\nMatt: 40 hours * $15 ($20) / hour = $600 ($800)\r\nAndreea: 20 hours * $15 ($20) / hour = $300 ($400)\r\nDylan: 15 hours * $12 / hour = $180\r\nEric: 15 hours * $20 ($25) / hour = $300 ($375)\r\nOmar: 5 hours * $15 / hour = $75\r\nTotal labor cost: $4,055 ($4,430)\r\n\r\nWith the proposed team and hours, we may complete a project on a larger scale than the LMS in a shorter period of time. We can consider an application which takes 1,600 hours to develop. This project might span 10 weeks, with the distribution of hours simply a multiplication of the weekly distribution by a factor of 10:\r\n\r\nDistribution of hours over a 1,600 hour project:\r\nMyself: 350\r\nRyan: 300\r\nMatt: 400\r\nAndreea: 200\r\nDylan: 150\r\nEric: 150\r\nOmar: 50\r\n\r\nA possible distribution of hours across each task category may be:\r\nProject planning: 100 hours\r\nProject management: 200 hours\r\nArchitecture: 200 hours\r\nDesign: 200 hours\r\nDatabase development: 400 hours\r\nFront end development: 400 hours\r\nResearch: 50 hours\r\nTesting: 50 hours\r\n\r\nWe may bill our highly qualified team at $50 / hour, yielding $80,000 for this 1,600 hour project. The project may be completed in ten weeks, with a weekly salary $4,055 ($4,430), and thus a project work cost of $40,550 ($44,300), which is just over half of the budget.\r\n\r\nAll that is left is to find such a project, and there are number of steps which should and in fact must be taken, and in a timely manner, if we are to achieve this goal on schedule.\r\n\r\nActions to be taken:\r\n1. Pull Dylan off of everything and channel all of his weekly hours into finding this project. He can look wherever he wants. We must meet with him frequently, possibly even every day, to see what his progress is like.\r\n2. Drop all clients which are not paying us enough for our attention. This includes losing any of Glenn\'s network which is not fully  supportive of our agreed rate. We can help achieve this by raising our rates, and offering those who cannot meet it other solutions\r\n3. Take on no work which might distract us from any of finding this project, working on this project, or preparing to work on this project.\r\n4. Focus primarily on the Learning Management System, working to make exclusive.\r\n5. Work on agile development techniques internally\r\n6. Discuss the situation with Jim as soon as possible, for the purpose of securing an appropriate window. Negotiate to pay him back off of the remainder of the project balance.\r\n7. Focus on restructuring our web presence to cater towards the precise audience we are targeting. Redesign the website. Add all team members, along with profiles for each. Add a list of every framework we can possibly work with. Have Dylan write up some content justifying these claims. Connect our site to the Learning Management System. Request permission from the LMS clients that we allow private access to this system while we continue to develop it, for the purpose of securing another similar project.\r\n8. Finish all outstanding work.\r\n9. Throw away hosting clients if necessary.\r\n10. Hire a server administrator if necessary.','2015-01-28 15:16:00');
/*!40000 ALTER TABLE `psychology_journal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_linearenergy`
--

DROP TABLE IF EXISTS `psychology_linearenergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_linearenergy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `energy` decimal(3,2),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_linearenergy`
--

LOCK TABLES `psychology_linearenergy` WRITE;
/*!40000 ALTER TABLE `psychology_linearenergy` DISABLE KEYS */;
INSERT INTO `psychology_linearenergy` VALUES (1,'2015-01-03 23:32:23',0.34),(2,'2015-01-04 00:43:55',0.43),(3,'2015-01-04 01:46:16',0.78),(4,'2015-01-04 01:51:57',0.58),(5,'2015-01-04 03:50:33',0.59),(6,'2015-01-04 03:53:35',0.73),(7,'2015-01-04 05:39:24',0.73),(8,'2015-01-04 08:44:19',0.81),(9,'2015-01-05 14:33:53',0.52),(10,'2015-01-05 16:10:13',0.31),(11,'2015-01-05 17:28:50',0.77),(12,'2015-01-06 05:50:50',0.91),(13,'2015-01-06 05:52:13',0.91),(14,'2015-01-06 06:12:29',0.91),(15,'2015-01-06 06:12:41',0.84),(16,'2015-01-06 06:22:58',0.77),(17,'2015-01-06 06:36:40',0.64),(18,'2015-01-06 06:48:57',0.47),(19,'2015-01-06 07:16:52',0.41),(20,'2015-01-06 15:30:00',0.61),(21,'2015-01-06 19:31:20',0.81),(22,'2015-01-07 02:33:52',0.87),(23,'2015-01-07 07:44:10',0.75),(24,'2015-01-19 18:57:23',0.74),(25,'2015-01-19 21:04:45',0.81),(26,'2015-01-20 00:02:53',0.89),(27,'2015-01-21 04:39:14',0.65),(28,'2015-01-21 07:09:19',0.61),(29,'2015-01-26 19:45:31',0.77),(30,'2015-01-29 15:53:28',0.39),(31,'2015-01-29 17:03:52',0.61),(32,'2015-01-31 15:43:26',0.59),(33,'2015-01-31 18:19:50',0.53),(34,'2015-02-14 21:39:11',0.83);
/*!40000 ALTER TABLE `psychology_linearenergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_linearmood`
--

DROP TABLE IF EXISTS `psychology_linearmood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_linearmood` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `mood` decimal(3,2),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_linearmood`
--

LOCK TABLES `psychology_linearmood` WRITE;
/*!40000 ALTER TABLE `psychology_linearmood` DISABLE KEYS */;
INSERT INTO `psychology_linearmood` VALUES (1,'2015-01-01 23:55:49',0.61),(2,'2015-01-01 23:57:53',0.58),(3,'2015-01-02 01:10:25',0.73),(4,'2015-01-02 02:15:41',0.77),(5,'2015-01-02 02:19:31',0.81),(6,'2015-01-02 07:09:30',0.87),(7,'2015-01-02 17:10:22',0.67),(8,'2015-01-02 20:32:58',0.43),(9,'2015-01-02 20:55:11',0.63),(10,'2015-01-02 21:48:46',0.61),(11,'2015-01-03 02:14:09',0.69),(12,'2015-01-03 17:21:25',0.78),(13,'2015-01-03 18:35:24',0.70),(14,'2015-01-03 19:42:44',0.68),(15,'2015-01-03 20:54:36',0.72),(16,'2015-01-03 20:59:47',0.73),(17,'2015-01-03 22:51:49',0.53),(18,'2015-01-04 01:46:11',0.82),(19,'2015-01-04 01:51:50',0.73),(20,'2015-01-04 03:50:29',0.51),(21,'2015-01-04 05:39:13',0.73),(22,'2015-01-04 05:49:22',0.57),(23,'2015-01-04 06:26:25',0.47),(24,'2015-01-04 08:44:15',0.64),(25,'2015-01-05 14:33:48',0.72),(26,'2015-01-05 16:10:20',0.63),(27,'2015-01-05 16:50:30',0.51),(28,'2015-01-05 17:28:43',0.67),(29,'2015-01-06 05:50:44',0.87),(30,'2015-01-06 05:52:13',0.87),(31,'2015-01-06 06:12:28',0.87),(32,'2015-01-06 06:12:37',0.87),(33,'2015-01-06 06:23:03',0.84),(34,'2015-01-06 06:36:37',0.72),(35,'2015-01-06 06:49:02',0.77),(36,'2015-01-06 07:16:45',0.61),(37,'2015-01-06 15:30:06',0.67),(38,'2015-01-06 19:31:15',0.87),(39,'2015-01-07 02:33:48',0.89),(40,'2015-01-07 07:44:06',0.81),(41,'2015-01-13 02:41:04',0.71),(42,'2015-01-13 04:08:48',0.37),(43,'2015-01-19 18:57:12',0.83),(44,'2015-01-19 21:04:42',0.87),(45,'2015-01-20 00:02:49',0.83),(46,'2015-01-20 17:26:32',0.61),(47,'2015-01-21 04:39:10',0.59),(48,'2015-01-21 07:09:13',0.68),(49,'2015-01-26 19:45:27',0.83),(50,'2015-01-29 15:53:21',0.51),(51,'2015-01-29 17:03:43',0.62),(52,'2015-01-31 15:43:19',0.71),(53,'2015-01-31 18:19:44',0.52),(54,'2015-02-14 21:39:05',0.83),(55,'2015-02-22 04:05:57',0.33),(56,'2015-03-23 04:08:46',0.73),(57,'2015-04-11 22:16:00',0.81),(58,'2015-04-11 22:50:43',0.93),(59,'2015-04-11 22:54:10',0.86),(60,'2015-04-11 23:16:13',0.81),(61,'2015-04-11 23:30:21',0.63),(62,'2015-04-11 23:45:53',0.59),(63,'2015-04-12 00:00:26',0.81),(64,'2015-04-12 00:16:05',0.79),(65,'2015-04-12 00:16:59',0.79),(66,'2015-04-12 00:30:46',0.82),(67,'2015-04-12 00:46:13',0.72),(68,'2015-04-12 01:13:54',0.69),(69,'2015-04-12 01:16:12',0.64),(70,'2015-04-12 01:31:02',0.82),(71,'2015-04-12 01:45:29',0.83),(72,'2015-04-12 02:00:40',0.81),(73,'2015-04-12 02:15:20',0.91),(74,'2015-04-12 02:45:54',0.84),(75,'2015-04-12 03:00:30',0.81),(76,'2015-04-12 03:16:33',0.77),(77,'2015-04-12 16:19:02',0.76),(78,'2015-04-12 23:52:38',0.81),(79,'2015-04-13 01:33:22',0.81),(80,'2015-04-13 03:30:12',0.81),(81,'2015-04-13 04:45:30',0.77);
/*!40000 ALTER TABLE `psychology_linearmood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_notecard`
--

DROP TABLE IF EXISTS `psychology_notecard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_notecard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(200) NOT NULL,
  `notes` longtext,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_notecard`
--

LOCK TABLES `psychology_notecard` WRITE;
/*!40000 ALTER TABLE `psychology_notecard` DISABLE KEYS */;
INSERT INTO `psychology_notecard` VALUES (1,'Digitalize everything','','2015-01-01 02:28:32'),(2,'I can leave the business when I am ready','','2015-01-01 02:29:07'),(3,'Use up what you have first','I wrote this a while ago, talking of things in my apartment, to use what I have before getting more.','2015-01-01 02:46:26'),(4,'I can leave Warren when I have a plan and a living','','2015-01-01 02:46:55'),(5,'I can leave warren when I find myself and my debt is paid off','','2015-01-01 02:47:07'),(6,'Develop a reputation','','2015-01-01 02:47:28'),(7,'Stay in one place','','2015-01-01 02:47:37'),(8,'Exercise good posture','','2015-01-01 02:47:44'),(9,'Research, mathematics, memory, and language','','2015-01-01 02:47:59'),(10,'I have a lot to say, and I love people','','2015-01-01 02:48:15'),(11,'Fill the walls with cards','I meant this literally when I wrote it, but I will take it figuratively from this point forward','2015-01-01 02:48:24'),(12,'Things I love','This card read (and it is true, and it was dated):\r\nI love when everything has palces!\r\nI love remembering\r\nI love cleaning\r\nI love reading my cards\r\nI love people\r\nI love practice','2014-01-21 03:12:54'),(13,'My goal with women','I want to find a partner, and on the way, I want to meet and sleep with many beautiful women','2015-01-01 03:13:57'),(14,'Qualities I want in a girl','Skinny\r\nFrisky\r\nAdventurous\r\nMysterious\r\nCute\r\nBrilliant\r\nSly / cunning\r\nSassy\r\nActive\r\nMotivated\r\nOutgoing','2015-01-01 03:14:22'),(15,'Programmers are the modern day composers','Write a blog post about this.','2015-01-04 02:11:52'),(16,'Adjusting schedule backwards is near impossible','','2015-01-05 16:21:21'),(17,'A daily decision','Every day i wake up, and i have something I need to do which is the direction of things, and other things I need to do to keep things flowing. I always want to do the former, and never want to do the latter, and every day I have a decision to make about which to do first, and which to do last. there is always a risk involved in the decision. If I choose the latter option and lose time for the former, I fall apart emotionally. if i choose the former option and I become too absorbed, I fall apart practically. Every day I have to make this decision, and the right choice is always the one that is the hardest to make.','2015-01-07 21:38:58'),(18,'Little poem about the night','The only path to Morning\'s light\r\nIs through the darkness of the night\r\nBy seeking out another way\r\nOne robs the spirit of the day','2015-01-20 16:49:55'),(19,'','This is a first note','2015-04-11 23:01:16'),(20,'','This is a first note','2015-04-11 23:01:26'),(21,'This is a second note','','2015-04-11 23:03:25'),(22,'Remaining time methoc, Atom.clock(), working great!','','2015-04-12 00:13:46'),(23,'Remaining time method, Atom.clock(), working great!','','2015-04-12 00:18:15'),(24,'Remaining time method, Atom.clock(), working great!','','2015-04-12 00:21:10'),(25,'Need more caffeine','','2015-04-12 00:30:57'),(26,'Pair tasks with atoms','','2015-04-12 01:03:57');
/*!40000 ALTER TABLE `psychology_notecard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychology_rule`
--

DROP TABLE IF EXISTS `psychology_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychology_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clause` varchar(200) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychology_rule`
--

LOCK TABLES `psychology_rule` WRITE;
/*!40000 ALTER TABLE `psychology_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `psychology_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_administrator`
--

DROP TABLE IF EXISTS `roles_administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_administrator` (
  `user_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`user_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_administrator`
--

LOCK TABLES `roles_administrator` WRITE;
/*!40000 ALTER TABLE `roles_administrator` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_administrator_districts`
--

DROP TABLE IF EXISTS `roles_administrator_districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_administrator_districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `administrator_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrator_id` (`administrator_id`,`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_administrator_districts`
--

LOCK TABLES `roles_administrator_districts` WRITE;
/*!40000 ALTER TABLE `roles_administrator_districts` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_administrator_districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_administrator_schools`
--

DROP TABLE IF EXISTS `roles_administrator_schools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_administrator_schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `administrator_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrator_id` (`administrator_id`,`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_administrator_schools`
--

LOCK TABLES `roles_administrator_schools` WRITE;
/*!40000 ALTER TABLE `roles_administrator_schools` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_administrator_schools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_student`
--

DROP TABLE IF EXISTS `roles_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_student` (
  `user_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`user_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_student`
--

LOCK TABLES `roles_student` WRITE;
/*!40000 ALTER TABLE `roles_student` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_teacher`
--

DROP TABLE IF EXISTS `roles_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_teacher` (
  `user_ptr_id` int(11) NOT NULL,
  `details` longtext,
  PRIMARY KEY (`user_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_teacher`
--

LOCK TABLES `roles_teacher` WRITE;
/*!40000 ALTER TABLE `roles_teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_atom`
--

DROP TABLE IF EXISTS `schedule_atom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_atom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `interval` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `mood_id` int(11),
  `absolute_index` int(11),
  `activity_id` int(11),
  `project_id` int(11),
  PRIMARY KEY (`id`),
  KEY `schedule_intervallog_dc08dca5` (`mood_id`),
  KEY `schedule_atom_f8a3193a` (`activity_id`),
  KEY `schedule_atom_b098ad43` (`project_id`),
  CONSTRAINT `schedule_atom_activity_id_7a3d3a2a5fd449b7_fk_tasks_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `tasks_activity` (`id`),
  CONSTRAINT `schedule_atom_project_id_58fc94ae06079f3e_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `schedule_in_mood_id_390d5562c56f9092_fk_psychology_linearmood_id` FOREIGN KEY (`mood_id`) REFERENCES `psychology_linearmood` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1013 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_atom`
--

LOCK TABLES `schedule_atom` WRITE;
/*!40000 ALTER TABLE `schedule_atom` DISABLE KEYS */;
INSERT INTO `schedule_atom` VALUES (1,'2015-04-11',33,'Creating the interval log model',NULL,2,NULL,NULL),(2,'2015-04-11',35,'Cleaning',NULL,3,NULL,NULL),(3,'2015-04-11',36,'Run',NULL,4,NULL,NULL),(4,'2015-04-11',37,'Work out',59,5,NULL,NULL),(5,'2015-04-11',38,'Workout',NULL,6,NULL,NULL),(6,'2015-04-11',39,'Workout, shower, and rest',60,7,NULL,NULL),(7,'2015-04-11',40,'Resting in bed',61,8,NULL,NULL),(8,'2015-04-11',41,'Converting nomenclature of the interval to the atom',62,9,NULL,NULL),(9,'2015-04-11',42,'Continuing work on the Atom',63,10,NULL,NULL),(10,'2015-04-11',43,'Continuing work on the Atom',65,11,NULL,NULL),(11,'2015-04-11',44,'Continuing work on the atom, calculating points possible based on the number of intervals which have occurred since the first was recorded',66,12,NULL,NULL),(12,'2015-04-11',45,'Continuing, still, the atom. The score method works',67,13,NULL,NULL),(13,'2015-04-11',46,'Pairing tasks with atoms, adding estimates, etc',NULL,14,NULL,NULL),(14,'2015-04-11',47,'Continuing the development of the task-atom relationship',69,15,NULL,NULL),(15,'2015-04-11',48,'Task dependency mapping via a readable hash table (Task.hash())',70,16,NULL,NULL),(16,'2015-04-11',49,'Continuing task dependencies, etc, filling data as it comes',71,17,NULL,NULL),(17,'2015-04-11',50,'Continue adding and modifying tasks',72,18,NULL,NULL),(18,'2015-04-11',51,'Rest',73,19,NULL,NULL),(19,'2015-04-11',52,'Cache activities',NULL,20,NULL,NULL),(20,'2015-04-11',53,'Working with atomic indexes, to create by index, difference, etc.',74,21,NULL,NULL),(21,'2015-04-11',54,'Finishing up atom creation by atomic index',75,22,NULL,NULL),(22,'2015-04-11',56,'Dinner',NULL,24,2,NULL),(27,'2015-04-11',55,'Atomic index creation touch ups and the beginning of dinner',76,23,NULL,NULL),(28,'2015-04-11',57,'Dinner',NULL,25,2,NULL),(29,'2015-04-11',58,'Taxes',NULL,26,NULL,NULL),(30,'2015-04-11',59,'House and relax',NULL,27,NULL,NULL),(31,'2015-04-11',60,'Relax',NULL,28,NULL,NULL),(32,'2015-04-11',61,'Recreation',NULL,29,NULL,NULL),(33,'2015-04-11',62,'Recreation',NULL,30,NULL,NULL),(34,'2015-04-11',63,'Recreation',NULL,31,NULL,NULL),(44,'2015-04-12',5,'Wake',NULL,37,NULL,NULL),(45,'2015-04-12',11,'Walk',NULL,43,7,NULL),(46,'2015-04-12',12,'Eat',NULL,44,NULL,NULL),(47,'2015-04-12',15,'Lumosity',NULL,47,13,5),(48,'2015-04-12',17,'Duolingo',NULL,49,13,6),(49,'2015-04-12',20,'Coursera',NULL,52,NULL,NULL),(50,'2015-04-12',0,'Sleep',NULL,32,NULL,NULL),(51,'2015-04-12',1,'Sleep',NULL,33,NULL,NULL),(52,'2015-04-12',2,'Sleep',NULL,34,NULL,NULL),(53,'2015-04-12',3,'Sleep',NULL,35,NULL,NULL),(54,'2015-04-12',4,'Sleep',NULL,36,NULL,NULL),(55,'2015-04-12',6,'Atomic shifting to account for loss of sleep',NULL,38,NULL,NULL),(56,'2015-04-12',7,'Atomic shifting to account for loss of sleep',NULL,39,NULL,NULL),(58,'2015-04-12',8,'Atomic shifting to account for loss of sleep',NULL,40,NULL,NULL),(59,'2015-04-12',21,'Algorithm studying',NULL,53,NULL,NULL),(60,'2015-04-12',22,'Algorithm studying',NULL,54,NULL,NULL),(61,'2015-04-12',34,'Learning Management System',NULL,66,11,1),(62,'2015-04-12',35,'Learning Management System',NULL,67,11,1),(63,'2015-04-12',36,'Learning Management System',NULL,68,11,1),(64,'2015-04-12',37,'Learning Management System',NULL,69,11,1),(65,'2015-04-12',9,'Fixing timezone issue with datetime',NULL,41,NULL,NULL),(66,'2015-04-12',10,'Fixing timezone issue with datetime',NULL,42,NULL,NULL),(67,'2015-04-12',14,'Pull, inverse of push, on atom, and finishing breakfast',77,46,NULL,NULL),(68,'2015-04-12',13,'Pull, inverse of push, on atom, and finishing breakfast',NULL,45,NULL,NULL),(69,'2015-04-12',38,'Learning Management System',NULL,70,11,1),(70,'2015-04-12',39,'Learning Management System',NULL,71,11,1),(71,'2015-04-12',40,'Learning Management System',NULL,72,11,1),(72,'2015-04-12',41,'Learning Management System',NULL,73,11,1),(73,'2015-04-12',16,'Lumosity and Duolingo, and inject',NULL,48,NULL,NULL),(74,'2015-04-12',18,'Duolingo',NULL,50,13,6),(75,'2015-04-12',19,'Duolingo',NULL,51,13,6),(76,'2015-04-12',23,'Break',NULL,55,NULL,NULL),(77,'2015-04-12',24,'Walk',NULL,56,7,NULL),(78,'2015-04-12',25,'Shower, prepare for lunch, and set up pinetrees.space',NULL,57,NULL,NULL),(79,'2015-04-12',29,'Lunch',NULL,61,2,NULL),(80,'2015-04-12',28,'Lunch',NULL,60,2,NULL),(81,'2015-04-12',26,'A bit of work on pinetrees.space',NULL,58,NULL,NULL),(82,'2015-04-12',27,'A bit of work on pinetrees.space',NULL,59,NULL,NULL),(83,'2015-04-12',31,'Lunch',NULL,63,2,NULL),(84,'2015-04-12',30,'Lunch',NULL,62,2,NULL),(85,'2015-04-12',32,'pinetrees.space',NULL,64,NULL,NULL),(86,'2015-04-12',33,'Rest',NULL,65,NULL,NULL),(87,'2015-04-12',43,'Learning Management System',78,75,11,1),(88,'2015-04-12',44,'Walk',NULL,76,7,NULL),(89,'2015-04-12',45,'Dinner',NULL,77,2,NULL),(90,'2015-04-12',46,'Dinner',NULL,78,2,NULL),(91,'2015-04-12',47,'Dinner',NULL,79,2,NULL),(92,'2015-04-12',48,'Dinner',NULL,80,2,NULL),(93,'2015-04-12',49,'Dinner',NULL,81,2,NULL),(94,'2015-04-12',50,'Algorithms',79,82,NULL,NULL),(95,'2015-04-12',51,'Relax',NULL,83,NULL,NULL),(96,'2015-04-12',52,'Algorithm studying',NULL,84,NULL,NULL),(97,'2015-04-12',53,'Algorithm studying',NULL,85,NULL,NULL),(98,'2015-04-12',54,'Algorithm studying',NULL,86,NULL,NULL),(99,'2015-04-12',55,'Algorithm studying',NULL,87,NULL,NULL),(100,'2015-04-12',56,'Sleep',NULL,88,NULL,NULL),(101,'2015-04-12',57,'Sleep',NULL,89,NULL,NULL),(102,'2015-04-12',58,'Walk',80,90,7,NULL),(103,'2015-04-12',59,'Sleep',NULL,91,NULL,NULL),(104,'2015-04-12',60,'Sleep',NULL,92,NULL,NULL),(105,'2015-04-12',61,'Clockwork',NULL,93,11,3),(106,'2015-04-12',62,'Clockwork',NULL,94,11,3),(107,'2015-04-12',63,'Clockwork',81,95,11,3),(108,'2015-04-13',7,'Wake',NULL,103,6,NULL),(109,'2015-04-13',8,'Walk',NULL,104,7,NULL),(111,'2015-04-13',10,'Lumosity',NULL,106,13,5),(112,'2015-04-13',11,'Duolingo',NULL,107,13,6),(118,'2015-04-13',12,'Find a recruiter in CA',NULL,108,11,4),(119,'2015-04-13',13,'Find a recruiter in CA',NULL,109,11,4),(120,'2015-04-13',14,'Find a recruiter in NC',NULL,110,11,4),(121,'2015-04-13',15,'Find a recruiter in NC',NULL,111,11,4),(122,'2015-04-13',53,'Learning Management System',NULL,149,11,4),(123,'2015-04-13',54,'Learning Management System',NULL,150,11,4),(126,'2015-04-13',17,'Courtier',NULL,113,11,2),(127,'2015-04-13',18,'Courtier',NULL,114,11,2),(128,'2015-04-13',19,'Courtier',NULL,115,11,2),(129,'2015-04-13',20,'Courtier',NULL,116,11,2),(130,'2015-04-13',21,'Courtier',NULL,117,11,2),(131,'2015-04-13',22,'Lunch',NULL,118,2,NULL),(132,'2015-04-13',23,'Lunch',NULL,119,2,NULL),(133,'2015-04-13',24,'Job hunting - Mindy',NULL,120,11,1),(134,'2015-04-13',25,'Shower',NULL,121,11,1),(135,'2015-04-13',26,'Job hunting - Mindy',NULL,122,11,1),(136,'2015-04-13',27,'Job hunting - Mindy',NULL,123,11,1),(137,'2015-04-13',28,'Job hunting - Will',NULL,124,11,1),(138,'2015-04-13',29,'Job hunting - Will',NULL,125,11,1),(139,'2015-04-13',30,'pinetrees.space',NULL,126,11,1),(140,'2015-04-13',31,'pinetrees.space',NULL,127,11,1),(141,'2015-04-13',32,'pinetrees.space',NULL,128,11,1),(142,'2015-04-13',33,'pinetrees.space',NULL,129,11,1),(143,'2015-04-13',34,'pinetrees.space',NULL,130,11,1),(144,'2015-04-13',35,'pinetrees.space',NULL,131,11,1),(145,'2015-04-13',36,'pinetrees.space',NULL,132,11,1),(146,'2015-04-13',37,'pinetrees.space',NULL,133,11,1),(147,'2015-04-13',38,'pinetrees.space',NULL,134,2,NULL),(148,'2015-04-13',42,'Dinner',NULL,138,2,NULL),(149,'2015-04-13',43,'Dinner',NULL,139,2,NULL),(150,'2015-04-13',44,'Dinner',NULL,140,2,NULL),(151,'2015-04-13',45,'pinetrees.space',NULL,141,11,1),(152,'2015-04-13',46,'pinetrees.space',NULL,142,11,1),(153,'2015-04-13',47,'pinetrees.space',NULL,143,11,1),(154,'2015-04-13',48,'pinetrees.space',NULL,144,11,1),(155,'2015-04-13',49,'pinetrees.space',NULL,145,11,1),(156,'2015-04-13',50,'Learning Management System',NULL,146,11,1),(157,'2015-04-13',51,'Learning Management System',NULL,147,11,1),(158,'2015-04-13',52,'Learning Management System',NULL,148,11,1),(160,'2015-04-13',16,'Walk',NULL,112,NULL,NULL),(161,'2015-04-13',6,'Sleep',NULL,102,NULL,NULL),(162,'2015-04-13',5,'Sleep',NULL,101,NULL,NULL),(163,'2015-04-13',4,'Sleep',NULL,100,NULL,NULL),(164,'2015-04-13',3,'Sleep',NULL,99,NULL,NULL),(165,'2015-04-13',2,'Sleep',NULL,98,NULL,NULL),(166,'2015-04-13',1,'Sleep',NULL,97,NULL,NULL),(167,'2015-04-13',0,'Sleep',NULL,96,NULL,NULL),(168,'2015-04-13',9,'Breakfast',NULL,105,NULL,NULL),(169,'2015-04-13',41,'Walk',NULL,137,NULL,NULL),(170,'2015-04-13',40,'pinetrees.space',NULL,136,NULL,NULL),(171,'2015-04-13',39,'pinetrees.space',NULL,135,NULL,NULL),(172,'2015-04-13',55,'Walk',NULL,151,NULL,NULL),(173,'2015-04-13',57,'Relax',NULL,153,NULL,NULL),(174,'2015-04-13',58,'Relax',NULL,154,NULL,NULL),(175,'2015-04-13',59,'Relax',NULL,155,NULL,NULL),(176,'2015-04-13',60,'Relax',NULL,156,NULL,NULL),(177,'2015-04-13',61,'Relax',NULL,157,NULL,NULL),(178,'2015-04-13',62,'Relax',NULL,158,NULL,NULL),(179,'2015-04-13',63,'Relax',NULL,159,NULL,NULL),(181,'2015-04-14',1,'Walk',NULL,161,NULL,NULL),(182,'2015-04-14',2,'Breakfast',NULL,162,NULL,NULL),(183,'2015-04-14',3,'Shower',NULL,163,NULL,NULL),(184,'2015-04-14',4,'Do something special',NULL,164,NULL,NULL),(185,'2015-04-14',5,'Plan for Carnival',NULL,165,NULL,NULL),(186,'2015-04-14',6,'Learning Management System',NULL,166,NULL,NULL),(187,'2015-04-14',7,'Learning Management System',NULL,167,NULL,NULL),(188,'2015-04-14',8,'Job hunting',NULL,168,NULL,NULL),(189,'2015-04-14',9,'Job hunting',NULL,169,NULL,NULL),(190,'2015-04-14',10,'Job hunting',NULL,170,NULL,NULL),(191,'2015-04-14',11,'Job hunting',NULL,171,NULL,NULL),(192,'2015-04-14',12,'Job hunting',NULL,172,NULL,NULL),(193,'2015-04-14',13,'Job hunting',NULL,173,NULL,NULL),(194,'2015-04-14',0,'Wake',NULL,160,NULL,NULL),(195,'2015-04-14',14,'Job hunting',NULL,174,NULL,NULL),(196,'2015-04-14',15,'Job hunting',NULL,175,NULL,NULL),(197,'2015-04-14',16,'Tier 27 website',NULL,176,NULL,NULL),(198,'2015-04-14',17,'Tier 27 website',NULL,177,NULL,NULL),(199,'2015-04-14',18,'Tier 27 website',NULL,178,NULL,NULL),(200,'2015-04-14',19,'Tier 27 website',NULL,179,NULL,NULL),(201,'2015-04-14',20,'Tier 27 website',NULL,180,NULL,NULL),(202,'2015-04-14',21,'Tier 27 website',NULL,181,NULL,NULL),(203,'2015-04-14',22,'Tier 27 website',NULL,182,NULL,NULL),(204,'2015-04-14',23,'Tier 27 website',NULL,183,NULL,NULL),(205,'2015-04-14',24,'Tier 27 website',NULL,184,NULL,NULL),(206,'2015-04-14',25,'Tier 27 website',NULL,185,NULL,NULL),(207,'2015-04-14',26,'Lunch',NULL,186,NULL,NULL),(208,'2015-04-14',27,'Lunch',NULL,187,NULL,NULL),(209,'2015-04-14',28,'Learning Management System',NULL,188,NULL,NULL),(210,'2015-04-14',29,'Learning Management System',NULL,189,NULL,NULL),(211,'2015-04-14',30,'Learning Management System',NULL,190,NULL,NULL),(212,'2015-04-14',31,'Learning Management System',NULL,191,NULL,NULL),(213,'2015-04-14',32,'Learning Management System',NULL,192,NULL,NULL),(214,'2015-04-14',33,'Learning Management System',NULL,193,NULL,NULL),(215,'2015-04-14',34,'Learning Management System',NULL,194,NULL,NULL),(216,'2015-04-14',35,'Learning Management System',NULL,195,NULL,NULL),(217,'2015-04-14',36,'Learning Management System',NULL,196,NULL,NULL),(218,'2015-04-14',37,'Learning Management System',NULL,197,NULL,NULL),(219,'2015-04-14',38,'Learning Management System',NULL,198,NULL,NULL),(220,'2015-04-14',39,'Learning Management System',NULL,199,NULL,NULL),(221,'2015-04-14',40,'Learning Management System',NULL,200,NULL,NULL),(222,'2015-04-14',41,'Learning Management System',NULL,201,NULL,NULL),(223,'2015-04-14',42,'Dinner',NULL,202,NULL,NULL),(224,'2015-04-14',43,'Dinner',NULL,203,NULL,NULL),(225,'2015-04-14',44,'Dinner',NULL,204,NULL,NULL),(226,'2015-04-14',45,'Dinner',NULL,205,NULL,NULL),(227,'2015-04-14',46,'Learning Management System',NULL,206,NULL,NULL),(228,'2015-04-14',47,'Learning Management System',NULL,207,NULL,NULL),(229,'2015-04-14',48,'Learning Management System',NULL,208,NULL,NULL),(230,'2015-04-14',49,'Learning Management System',NULL,209,NULL,NULL),(231,'2015-04-14',50,'Rest',NULL,210,NULL,NULL),(232,'2015-04-14',51,'Rest',NULL,211,NULL,NULL),(233,'2015-04-14',52,'Rest',NULL,212,NULL,NULL),(234,'2015-04-14',53,'Rest',NULL,213,NULL,NULL),(235,'2015-04-14',54,'Rest',NULL,214,NULL,NULL),(236,'2015-04-14',55,'Rest',NULL,215,NULL,NULL),(237,'2015-04-14',56,'Rest',NULL,216,NULL,NULL),(238,'2015-04-14',57,'Rest',NULL,217,NULL,NULL),(239,'2015-04-14',58,'Rest',NULL,218,NULL,NULL),(240,'2015-04-14',59,'Rest',NULL,219,NULL,NULL),(241,'2015-04-14',60,'Rest',NULL,220,NULL,NULL),(242,'2015-04-14',61,'Rest',NULL,221,NULL,NULL),(243,'2015-04-14',62,'Rest',NULL,222,NULL,NULL),(244,'2015-04-14',63,'Rest',NULL,223,NULL,NULL),(245,'2015-04-15',0,'Wake',NULL,224,NULL,NULL),(246,'2015-04-15',1,'Walk',NULL,225,NULL,NULL),(247,'2015-04-15',2,'Plan',NULL,226,NULL,NULL),(248,'2015-04-15',3,'Eat',NULL,227,NULL,NULL),(249,'2015-04-15',4,'Resume',NULL,228,NULL,NULL),(250,'2015-04-15',5,'Angel List',NULL,229,NULL,NULL),(251,'2015-04-15',6,'Github',NULL,230,NULL,NULL),(252,'2015-04-15',7,'Enterprise',NULL,231,NULL,NULL),(253,'2015-04-15',8,'Learning Management System',NULL,232,NULL,NULL),(254,'2015-04-15',9,'Learning Management System',NULL,233,NULL,NULL),(255,'2015-04-15',10,'Learning Management System',NULL,234,NULL,NULL),(256,'2015-04-15',11,'Learning Management System',NULL,235,NULL,NULL),(257,'2015-04-15',16,'Lunch',NULL,240,NULL,NULL),(258,'2015-04-15',17,'Lunch',NULL,241,NULL,NULL),(259,'2015-04-15',18,'Lunch',NULL,242,NULL,NULL),(260,'2015-04-15',19,'Lunch',NULL,243,NULL,NULL),(261,'2015-04-15',15,'Duolingo',NULL,239,NULL,NULL),(262,'2015-04-15',20,'Fantasy camp',NULL,244,NULL,NULL),(263,'2015-04-15',21,'Fantasy camp',NULL,245,NULL,NULL),(264,'2015-04-15',22,'Fantasy camp',NULL,246,NULL,NULL),(265,'2015-04-15',23,'Fantasy camp',NULL,247,NULL,NULL),(266,'2015-04-15',24,'Fantasy camp',NULL,248,NULL,NULL),(267,'2015-04-15',25,'Fantasy camp',NULL,249,NULL,NULL),(268,'2015-04-15',26,'Fantasy camp',NULL,250,NULL,NULL),(269,'2015-04-15',27,'Lunch, for real',NULL,251,NULL,NULL),(270,'2015-04-15',28,'Lunch, for real',NULL,252,NULL,NULL),(271,'2015-04-15',29,'Lunch, for real',NULL,253,NULL,NULL),(272,'2015-04-15',33,'Learning Management System',NULL,257,NULL,NULL),(273,'2015-04-15',37,'Taxes',NULL,261,NULL,NULL),(274,'2015-04-15',38,'Taxes',NULL,262,NULL,NULL),(275,'2015-04-15',39,'Taxes',NULL,263,NULL,NULL),(276,'2015-04-15',41,'Learning Management System',NULL,265,NULL,NULL),(277,'2015-04-15',42,'Learning Management System',NULL,266,NULL,NULL),(278,'2015-04-15',43,'Learning Management System',NULL,267,NULL,NULL),(279,'2015-04-15',30,'Lunch',NULL,254,NULL,NULL),(280,'2015-04-15',31,'Mission Bowling Club',NULL,255,NULL,NULL),(281,'2015-04-15',32,'Walk',NULL,256,NULL,NULL),(282,'2015-04-15',36,'Walk',NULL,260,NULL,NULL),(283,'2015-04-15',35,'Mission Bowling Club',NULL,259,NULL,NULL),(284,'2015-04-15',34,'Mission Bowling Club',NULL,258,NULL,NULL),(285,'2015-04-15',40,'Taxes',NULL,264,NULL,NULL),(286,'2015-04-15',48,'Learning Management System',NULL,272,NULL,NULL),(287,'2015-04-15',49,'Learning Management System',NULL,273,NULL,NULL),(288,'2015-04-15',50,'Learning Management System',NULL,274,NULL,NULL),(289,'2015-04-15',51,'Learning Management System',NULL,275,NULL,NULL),(290,'2015-04-15',52,'Learning Management System',NULL,276,NULL,NULL),(291,'2015-04-15',53,'Learning Management System',NULL,277,NULL,NULL),(292,'2015-04-15',54,'Learning Management System',NULL,278,NULL,NULL),(293,'2015-04-15',55,'Learning Management System',NULL,279,NULL,NULL),(294,'2015-04-15',47,'Dinner',NULL,271,NULL,NULL),(295,'2015-04-15',46,'Dinner',NULL,270,NULL,NULL),(296,'2015-04-15',45,'Dinner',NULL,269,NULL,NULL),(297,'2015-04-15',44,'Walk',NULL,268,NULL,NULL),(298,'2015-04-15',56,'Programming theory',NULL,280,NULL,NULL),(299,'2015-04-15',57,'Programming theory',NULL,281,NULL,NULL),(300,'2015-04-15',58,'Programming theory',NULL,282,NULL,NULL),(301,'2015-04-15',59,'Programming theory',NULL,283,NULL,NULL),(302,'2015-04-15',60,'Programming theory',NULL,284,NULL,NULL),(303,'2015-04-15',61,'Programming theory',NULL,285,NULL,NULL),(304,'2015-04-15',62,'Recreation',NULL,286,NULL,NULL),(305,'2015-04-15',63,'Recreation',NULL,287,NULL,NULL),(306,'2015-04-16',0,'Wake',NULL,288,NULL,NULL),(307,'2015-04-16',1,'Walk',NULL,289,NULL,NULL),(308,'2015-04-16',2,'Breakfast',NULL,290,NULL,NULL),(309,'2015-04-16',3,'Programming theory',NULL,291,NULL,NULL),(310,'2015-04-16',4,'Programming theory',NULL,292,NULL,NULL),(311,'2015-04-16',5,'Learning Management System',NULL,293,NULL,NULL),(312,'2015-04-16',6,'Learning Management System',NULL,294,NULL,NULL),(313,'2015-04-16',7,'Programming',NULL,295,NULL,NULL),(314,'2015-04-16',8,'Programming',NULL,296,NULL,NULL),(315,'2015-04-16',9,'Programming',NULL,297,NULL,NULL),(316,'2015-04-16',10,'Programming',NULL,298,NULL,NULL),(317,'2015-04-16',11,'Programming',NULL,299,NULL,NULL),(318,'2015-04-16',12,'Programming',NULL,300,NULL,NULL),(319,'2015-04-16',13,'Programming',NULL,301,NULL,NULL),(320,'2015-04-16',14,'Programming',NULL,302,NULL,NULL),(321,'2015-04-16',15,'Programming',NULL,303,NULL,NULL),(322,'2015-04-16',16,'Programming',NULL,304,NULL,NULL),(323,'2015-04-16',17,'Programming',NULL,305,NULL,NULL),(324,'2015-04-16',18,'Programming',NULL,306,NULL,NULL),(325,'2015-04-16',19,'Lunch',NULL,307,NULL,NULL),(326,'2015-04-16',20,'Lunch',NULL,308,NULL,NULL),(327,'2015-04-16',21,'Lunch',NULL,309,NULL,NULL),(328,'2015-04-16',22,'Interview preparation',NULL,310,NULL,NULL),(329,'2015-04-16',23,'Interview preparation',NULL,311,NULL,NULL),(330,'2015-04-16',24,'Interview preparation',NULL,312,NULL,NULL),(331,'2015-04-16',25,'Interview preparation',NULL,313,NULL,NULL),(332,'2015-04-16',26,'Interview',NULL,314,NULL,NULL),(333,'2015-04-16',27,'Interview',NULL,315,NULL,NULL),(334,'2015-04-16',28,'Interview',NULL,316,NULL,NULL),(335,'2015-04-16',29,'Interview',NULL,317,NULL,NULL),(336,'2015-04-16',30,'Programming',NULL,318,NULL,NULL),(337,'2015-04-16',31,'Programming',NULL,319,NULL,NULL),(338,'2015-04-16',32,'Programming',NULL,320,NULL,NULL),(339,'2015-04-16',33,'Programming',NULL,321,NULL,NULL),(340,'2015-04-16',34,'Programming',NULL,322,NULL,NULL),(341,'2015-04-16',35,'Programming',NULL,323,NULL,NULL),(342,'2015-04-16',36,'Programming',NULL,324,NULL,NULL),(343,'2015-04-16',37,'Programming',NULL,325,NULL,NULL),(344,'2015-04-16',38,'Learning Management System',NULL,326,NULL,NULL),(345,'2015-04-16',39,'Learning Management System',NULL,327,NULL,NULL),(346,'2015-04-16',40,'Learning Management System',NULL,328,NULL,NULL),(347,'2015-04-16',41,'Learning Management System',NULL,329,NULL,NULL),(348,'2015-04-16',42,'Learning Management System',NULL,330,NULL,NULL),(349,'2015-04-16',43,'Learning Management System',NULL,331,NULL,NULL),(350,'2015-04-16',44,'Learning Management System',NULL,332,NULL,NULL),(351,'2015-04-16',45,'Learning Management System',NULL,333,NULL,NULL),(352,'2015-04-16',46,'Dinner',NULL,334,NULL,NULL),(353,'2015-04-16',47,'Dinner',NULL,335,NULL,NULL),(354,'2015-04-16',48,'Learning Management System',NULL,336,NULL,NULL),(355,'2015-04-16',49,'Learning Management System',NULL,337,NULL,NULL),(356,'2015-04-16',50,'Learning Management System',NULL,338,NULL,NULL),(357,'2015-04-16',51,'Learning Management System',NULL,339,NULL,NULL),(358,'2015-04-16',52,'Programming theory',NULL,340,NULL,NULL),(359,'2015-04-16',53,'Programming theory',NULL,341,NULL,NULL),(360,'2015-04-16',54,'Programming theory',NULL,342,NULL,NULL),(361,'2015-04-16',55,'Programming theory',NULL,343,NULL,NULL),(362,'2015-04-16',56,'Rest',NULL,344,NULL,NULL),(363,'2015-04-16',57,'Rest',NULL,345,NULL,NULL),(364,'2015-04-16',58,'Rest',NULL,346,NULL,NULL),(365,'2015-04-16',59,'Rest',NULL,347,NULL,NULL),(366,'2015-04-16',60,'Rest',NULL,348,NULL,NULL),(367,'2015-04-16',61,'Rest',NULL,349,NULL,NULL),(368,'2015-04-16',62,'Rest',NULL,350,NULL,NULL),(369,'2015-04-16',63,'Rest',NULL,351,NULL,NULL),(370,'2015-04-17',0,'Wake',NULL,352,NULL,NULL),(371,'2015-04-17',1,'Walk',NULL,353,NULL,NULL),(372,'2015-04-17',2,'Breakfast',NULL,354,NULL,NULL),(373,'2015-04-17',3,'Pack',NULL,355,NULL,NULL),(374,'2015-04-17',4,'Travel',NULL,356,NULL,NULL),(375,'2015-04-17',5,'Travel',NULL,357,NULL,NULL),(376,'2015-04-17',6,'Travel',NULL,358,NULL,NULL),(377,'2015-04-17',7,'Travel',NULL,359,NULL,NULL),(378,'2015-04-17',8,'Travel',NULL,360,NULL,NULL),(379,'2015-04-17',9,'Travel',NULL,361,NULL,NULL),(380,'2015-04-17',10,'Travel',NULL,362,NULL,NULL),(381,'2015-04-17',11,'Travel',NULL,363,NULL,NULL),(382,'2015-04-17',12,'Travel',NULL,364,NULL,NULL),(383,'2015-04-17',13,'Travel',NULL,365,NULL,NULL),(384,'2015-04-17',14,'Travel',NULL,366,NULL,NULL),(385,'2015-04-17',15,'Travel',NULL,367,NULL,NULL),(386,'2015-04-17',16,'Travel',NULL,368,NULL,NULL),(387,'2015-04-17',17,'Travel',NULL,369,NULL,NULL),(388,'2015-04-17',18,'Travel',NULL,370,NULL,NULL),(389,'2015-04-17',19,'Travel',NULL,371,NULL,NULL),(390,'2015-04-17',20,'Interview preparation',NULL,372,NULL,NULL),(391,'2015-04-17',21,'Interview preparation',NULL,373,NULL,NULL),(392,'2015-04-17',22,'Interview preparation',NULL,374,NULL,NULL),(393,'2015-04-17',23,'Interview preparation',NULL,375,NULL,NULL),(394,'2015-04-17',24,'Interview preparation',NULL,376,NULL,NULL),(395,'2015-04-17',25,'Interview preparation',NULL,377,NULL,NULL),(396,'2015-04-17',26,'Interview preparation',NULL,378,NULL,NULL),(397,'2015-04-17',27,'Interview preparation',NULL,379,NULL,NULL),(398,'2015-04-17',28,'Learning Management System',NULL,380,NULL,NULL),(399,'2015-04-17',29,'Learning Management System',NULL,381,NULL,NULL),(400,'2015-04-17',30,'Learning Management System',NULL,382,NULL,NULL),(401,'2015-04-17',31,'Learning Management System',NULL,383,NULL,NULL),(402,'2015-04-17',32,'Learning Management System',NULL,384,NULL,NULL),(403,'2015-04-17',33,'Learning Management System',NULL,385,NULL,NULL),(404,'2015-04-17',34,'Learning Management System',NULL,386,NULL,NULL),(405,'2015-04-17',35,'Learning Management System',NULL,387,NULL,NULL),(406,'2015-04-17',36,'Break',NULL,388,NULL,NULL),(407,'2015-04-17',37,'Break',NULL,389,NULL,NULL),(408,'2015-04-17',38,'Break',NULL,390,NULL,NULL),(409,'2015-04-17',39,'Break',NULL,391,NULL,NULL),(410,'2015-04-17',40,'Break',NULL,392,NULL,NULL),(411,'2015-04-17',41,'Break',NULL,393,NULL,NULL),(412,'2015-04-17',42,'Break',NULL,394,NULL,NULL),(413,'2015-04-17',43,'Break',NULL,395,NULL,NULL),(414,'2015-04-17',44,'Break',NULL,396,NULL,NULL),(415,'2015-04-17',45,'Break',NULL,397,NULL,NULL),(416,'2015-04-17',46,'Break',NULL,398,NULL,NULL),(417,'2015-04-17',47,'Break',NULL,399,NULL,NULL),(418,'2015-04-17',48,'Break',NULL,400,NULL,NULL),(419,'2015-04-17',49,'Break',NULL,401,NULL,NULL),(420,'2015-04-17',50,'Break',NULL,402,NULL,NULL),(421,'2015-04-17',51,'Break',NULL,403,NULL,NULL),(422,'2015-04-17',52,'Break',NULL,404,NULL,NULL),(423,'2015-04-17',53,'Break',NULL,405,NULL,NULL),(424,'2015-04-17',54,'Break',NULL,406,NULL,NULL),(425,'2015-04-17',55,'Break',NULL,407,NULL,NULL),(426,'2015-04-17',56,'Break',NULL,408,NULL,NULL),(427,'2015-04-17',57,'Break',NULL,409,NULL,NULL),(428,'2015-04-17',58,'Break',NULL,410,NULL,NULL),(429,'2015-04-17',59,'Break',NULL,411,NULL,NULL),(430,'2015-04-17',60,'Rest',NULL,412,NULL,NULL),(431,'2015-04-17',61,'Rest',NULL,413,NULL,NULL),(432,'2015-04-17',62,'Rest',NULL,414,NULL,NULL),(433,'2015-04-17',63,'Rest',NULL,415,NULL,NULL),(434,'2015-04-18',0,'Wake',NULL,416,NULL,NULL),(435,'2015-04-18',27,'Rest',NULL,443,NULL,NULL),(436,'2015-04-18',28,'Program',NULL,444,NULL,NULL),(437,'2015-04-18',29,'Program',NULL,445,NULL,NULL),(438,'2015-04-18',30,'Program',NULL,446,NULL,NULL),(439,'2015-04-18',31,'Program',NULL,447,NULL,NULL),(440,'2015-04-18',32,'Break',NULL,448,NULL,NULL),(441,'2015-04-18',33,'Break',NULL,449,NULL,NULL),(442,'2015-04-18',34,'Break',NULL,450,NULL,NULL),(443,'2015-04-18',35,'Break',NULL,451,NULL,NULL),(444,'2015-04-18',36,'Break',NULL,452,NULL,NULL),(445,'2015-04-18',37,'Break',NULL,453,NULL,NULL),(446,'2015-04-18',38,'Break',NULL,454,NULL,NULL),(447,'2015-04-18',39,'Break',NULL,455,NULL,NULL),(448,'2015-04-18',40,'Break',NULL,456,NULL,NULL),(449,'2015-04-18',41,'Break',NULL,457,NULL,NULL),(450,'2015-04-18',42,'Break',NULL,458,NULL,NULL),(451,'2015-04-18',43,'Break',NULL,459,NULL,NULL),(452,'2015-04-18',44,'Break',NULL,460,NULL,NULL),(453,'2015-04-18',45,'Break',NULL,461,NULL,NULL),(454,'2015-04-18',46,'Break',NULL,462,NULL,NULL),(455,'2015-04-18',47,'Break',NULL,463,NULL,NULL),(456,'2015-04-18',48,'Break',NULL,464,NULL,NULL),(457,'2015-04-18',49,'Break',NULL,465,NULL,NULL),(458,'2015-04-18',50,'Break',NULL,466,NULL,NULL),(459,'2015-04-18',51,'Break',NULL,467,NULL,NULL),(460,'2015-04-18',52,'Break',NULL,468,NULL,NULL),(461,'2015-04-18',53,'Break',NULL,469,NULL,NULL),(462,'2015-04-18',54,'Break',NULL,470,NULL,NULL),(463,'2015-04-18',55,'Break',NULL,471,NULL,NULL),(464,'2015-04-18',56,'Break',NULL,472,NULL,NULL),(465,'2015-04-18',57,'Break',NULL,473,NULL,NULL),(466,'2015-04-18',58,'Break',NULL,474,NULL,NULL),(467,'2015-04-18',59,'Break',NULL,475,NULL,NULL),(468,'2015-04-18',60,'Break',NULL,476,NULL,NULL),(469,'2015-04-18',61,'Break',NULL,477,NULL,NULL),(470,'2015-04-18',62,'Break',NULL,478,NULL,NULL),(471,'2015-04-18',63,'Break',NULL,479,NULL,NULL),(472,'2015-04-19',0,'Wake',NULL,480,NULL,NULL),(473,'2015-04-19',9,'Rest',NULL,489,NULL,NULL),(474,'2015-04-19',10,'Rest',NULL,490,NULL,NULL),(475,'2015-04-19',11,'Rest',NULL,491,NULL,NULL),(476,'2015-04-19',12,'Rest',NULL,492,NULL,NULL),(477,'2015-04-19',13,'Rest',NULL,493,NULL,NULL),(478,'2015-04-19',14,'Rest',NULL,494,NULL,NULL),(479,'2015-04-19',15,'Rest',NULL,495,NULL,NULL),(480,'2015-04-19',16,'Rest',NULL,496,NULL,NULL),(481,'2015-04-19',17,'Rest',NULL,497,NULL,NULL),(482,'2015-04-19',18,'Rest',NULL,498,NULL,NULL),(483,'2015-04-19',19,'Rest',NULL,499,NULL,NULL),(484,'2015-04-19',20,'Rest',NULL,500,NULL,NULL),(485,'2015-04-19',21,'Rest',NULL,501,NULL,NULL),(486,'2015-04-19',22,'Rest',NULL,502,NULL,NULL),(487,'2015-04-19',23,'Rest',NULL,503,NULL,NULL),(488,'2015-04-19',24,'Rest',NULL,504,NULL,NULL),(489,'2015-04-19',25,'Rest',NULL,505,NULL,NULL),(490,'2015-04-19',26,'Rest',NULL,506,NULL,NULL),(491,'2015-04-19',27,'Rest',NULL,507,NULL,NULL),(492,'2015-04-19',28,'Rest',NULL,508,NULL,NULL),(493,'2015-04-19',29,'Rest',NULL,509,NULL,NULL),(494,'2015-04-19',30,'Rest',NULL,510,NULL,NULL),(495,'2015-04-19',31,'Rest',NULL,511,NULL,NULL),(496,'2015-04-19',32,'Rest',NULL,512,NULL,NULL),(497,'2015-04-19',33,'Rest',NULL,513,NULL,NULL),(498,'2015-04-19',34,'Rest',NULL,514,NULL,NULL),(499,'2015-04-19',35,'Learning Management System',NULL,515,NULL,NULL),(500,'2015-04-19',36,'Learning Management System',NULL,516,NULL,NULL),(501,'2015-04-19',37,'Learning Management System',NULL,517,NULL,NULL),(502,'2015-04-19',38,'Learning Management System',NULL,518,NULL,NULL),(503,'2015-04-19',39,'Learning Management System',NULL,519,NULL,NULL),(504,'2015-04-19',40,'Learning Management System',NULL,520,NULL,NULL),(505,'2015-04-19',41,'Learning Management System',NULL,521,NULL,NULL),(506,'2015-04-19',42,'Learning Management System',NULL,522,NULL,NULL),(507,'2015-04-19',47,'Learning Management System',NULL,527,NULL,NULL),(508,'2015-04-19',48,'Learning Management System',NULL,528,NULL,NULL),(509,'2015-04-19',49,'Learning Management System',NULL,529,NULL,NULL),(510,'2015-04-19',50,'Learning Management System',NULL,530,NULL,NULL),(511,'2015-04-19',51,'Learning Management System',NULL,531,NULL,NULL),(512,'2015-04-19',52,'Learning Management System',NULL,532,NULL,NULL),(513,'2015-04-19',53,'Learning Management System',NULL,533,NULL,NULL),(514,'2015-04-19',54,'Learning Management System',NULL,534,NULL,NULL),(515,'2015-04-19',55,'Learning Management System',NULL,535,NULL,NULL),(516,'2015-04-19',56,'Learning Management System',NULL,536,NULL,NULL),(517,'2015-04-19',57,'Learning Management System',NULL,537,NULL,NULL),(518,'2015-04-19',58,'Learning Management System',NULL,538,NULL,NULL),(519,'2015-04-19',46,'Dinner',NULL,526,NULL,NULL),(520,'2015-04-19',45,'Dinner',NULL,525,NULL,NULL),(521,'2015-04-19',44,'Dinner',NULL,524,NULL,NULL),(522,'2015-04-19',43,'Dinner',NULL,523,NULL,NULL),(523,'2015-04-19',59,'Rest',NULL,539,NULL,NULL),(524,'2015-04-19',60,'Rest',NULL,540,NULL,NULL),(525,'2015-04-19',61,'Rest',NULL,541,NULL,NULL),(526,'2015-04-19',62,'Rest',NULL,542,NULL,NULL),(527,'2015-04-19',63,'Rest',NULL,543,NULL,NULL),(528,'2015-04-20',0,'Wake',NULL,544,NULL,NULL),(529,'2015-04-20',1,'Walk',NULL,545,NULL,NULL),(530,'2015-04-20',2,'Breakfast',NULL,546,NULL,NULL),(531,'2015-04-20',3,'Cleaning',NULL,547,NULL,NULL),(532,'2015-04-20',4,'Scheduling',NULL,548,NULL,NULL),(533,'2015-04-20',5,'Scheduling',NULL,549,NULL,NULL),(534,'2015-04-20',6,'Learning Management System',NULL,550,NULL,NULL),(535,'2015-04-20',7,'Learning Management System',NULL,551,NULL,NULL),(536,'2015-04-20',8,'Learning Management System',NULL,552,NULL,NULL),(537,'2015-04-20',9,'Learning Management System',NULL,553,NULL,NULL),(538,'2015-04-20',10,'Learning Management System',NULL,554,NULL,NULL),(539,'2015-04-20',11,'Learning Management System',NULL,555,NULL,NULL),(540,'2015-04-20',12,'Learning Management System',NULL,556,NULL,NULL),(541,'2015-04-20',13,'Learning Management System',NULL,557,NULL,NULL),(542,'2015-04-20',14,'Lunch?',NULL,558,NULL,NULL),(543,'2015-04-20',15,'Lunch?',NULL,559,NULL,NULL),(544,'2015-04-20',16,'Interview preparation',NULL,560,NULL,NULL),(545,'2015-04-20',17,'Interview preparation',NULL,561,NULL,NULL),(546,'2015-04-20',18,'Interview preparation',NULL,562,NULL,NULL),(547,'2015-04-20',19,'Interview preparation',NULL,563,NULL,NULL),(548,'2015-04-20',20,'Interview preparation',NULL,564,NULL,NULL),(549,'2015-04-20',21,'Interview preparation',NULL,565,NULL,NULL),(550,'2015-04-20',22,'Interview preparation',NULL,566,NULL,NULL),(551,'2015-04-20',23,'Interview preparation',NULL,567,NULL,NULL),(552,'2015-04-20',24,'Interview',NULL,568,NULL,NULL),(553,'2015-04-20',25,'Interview',NULL,569,NULL,NULL),(554,'2015-04-20',26,'Interview',NULL,570,NULL,NULL),(555,'2015-04-20',27,'Interview',NULL,571,NULL,NULL),(556,'2015-04-20',31,'Learning Management System',NULL,575,NULL,NULL),(557,'2015-04-20',32,'Learning Management System',NULL,576,NULL,NULL),(558,'2015-04-20',33,'Learning Management System',NULL,577,NULL,NULL),(559,'2015-04-20',34,'Learning Management System',NULL,578,NULL,NULL),(560,'2015-04-20',35,'Learning Management System',NULL,579,NULL,NULL),(561,'2015-04-20',36,'Learning Management System',NULL,580,NULL,NULL),(562,'2015-04-20',37,'Learning Management System',NULL,581,NULL,NULL),(563,'2015-04-20',38,'Learning Management System',NULL,582,NULL,NULL),(564,'2015-04-20',39,'Learning Management System',NULL,583,NULL,NULL),(565,'2015-04-20',40,'Learning Management System',NULL,584,NULL,NULL),(566,'2015-04-20',41,'Learning Management System',NULL,585,NULL,NULL),(567,'2015-04-20',42,'Learning Management System',NULL,586,NULL,NULL),(568,'2015-04-20',43,'Learning Management System',NULL,587,NULL,NULL),(569,'2015-04-20',44,'Learning Management System',NULL,588,NULL,NULL),(570,'2015-04-20',45,'Learning Management System',NULL,589,NULL,NULL),(571,'2015-04-20',46,'Learning Management System',NULL,590,NULL,NULL),(572,'2015-04-21',9,'Schedule',NULL,617,NULL,NULL),(573,'2015-04-21',10,'Line server',NULL,618,NULL,NULL),(574,'2015-04-21',11,'Line server',NULL,619,NULL,NULL),(575,'2015-04-21',12,'Line server',NULL,620,NULL,NULL),(576,'2015-04-21',13,'Line server',NULL,621,NULL,NULL),(577,'2015-04-21',14,'Interview preparation',NULL,622,NULL,NULL),(578,'2015-04-21',15,'Interview preparation',NULL,623,NULL,NULL),(579,'2015-04-21',16,'Interview',NULL,624,NULL,NULL),(580,'2015-04-21',17,'Interview',NULL,625,NULL,NULL),(581,'2015-04-21',18,'Interview',NULL,626,NULL,NULL),(582,'2015-04-21',19,'Line server',NULL,627,NULL,NULL),(583,'2015-04-21',20,'Line server',NULL,628,NULL,NULL),(584,'2015-04-21',21,'Line server',NULL,629,NULL,NULL),(585,'2015-04-21',22,'Line server',NULL,630,NULL,NULL),(586,'2015-04-21',23,'Break',NULL,631,NULL,NULL),(587,'2015-04-21',24,'Break',NULL,632,NULL,NULL),(588,'2015-04-21',25,'Break',NULL,633,NULL,NULL),(589,'2015-04-21',26,'Break',NULL,634,NULL,NULL),(590,'2015-04-21',27,'Interview preparation',NULL,635,NULL,NULL),(591,'2015-04-21',28,'Interview',NULL,636,NULL,NULL),(592,'2015-04-21',29,'Learning Management System',NULL,637,NULL,NULL),(593,'2015-04-21',30,'Learning Management System',NULL,638,NULL,NULL),(594,'2015-04-21',31,'Learning Management System',NULL,639,NULL,NULL),(595,'2015-04-21',32,'Learning Management System',NULL,640,NULL,NULL),(596,'2015-04-21',33,'Learning Management System',NULL,641,NULL,NULL),(597,'2015-04-21',34,'Learning Management System',NULL,642,NULL,NULL),(598,'2015-04-21',35,'Learning Management System',NULL,643,NULL,NULL),(599,'2015-04-21',36,'Learning Management System',NULL,644,NULL,NULL),(600,'2015-04-21',37,'Learning Management System',NULL,645,NULL,NULL),(601,'2015-04-21',38,'Learning Management System',NULL,646,NULL,NULL),(602,'2015-04-21',39,'Learning Management System',NULL,647,NULL,NULL),(603,'2015-04-21',40,'Learning Management System',NULL,648,NULL,NULL),(604,'2015-04-21',41,'Learning Management System',NULL,649,NULL,NULL),(605,'2015-04-21',42,'Learning Management System',NULL,650,NULL,NULL),(606,'2015-04-21',43,'Learning Management System',NULL,651,NULL,NULL),(607,'2015-04-21',44,'Learning Management System',NULL,652,NULL,NULL),(608,'2015-04-21',45,'Learning Management System',NULL,653,NULL,NULL),(609,'2015-04-21',46,'Learning Management System',NULL,654,NULL,NULL),(610,'2015-04-21',47,'Learning Management System',NULL,655,NULL,NULL),(611,'2015-04-21',48,'Learning Management System',NULL,656,NULL,NULL),(612,'2015-04-21',49,'Learning Management System',NULL,657,NULL,NULL),(613,'2015-04-21',50,'Learning Management System',NULL,658,NULL,NULL),(614,'2015-04-21',51,'Learning Management System',NULL,659,NULL,NULL),(615,'2015-04-21',52,'Learning Management System',NULL,660,NULL,NULL),(616,'2015-04-21',53,'Salsify programming',NULL,661,NULL,NULL),(617,'2015-04-21',54,'Salsify programming',NULL,662,NULL,NULL),(618,'2015-04-21',55,'Salsify programming',NULL,663,NULL,NULL),(619,'2015-04-21',56,'Salsify programming',NULL,664,NULL,NULL),(620,'2015-04-21',57,'Salsify programming',NULL,665,NULL,NULL),(621,'2015-04-21',58,'Salsify programming',NULL,666,NULL,NULL),(622,'2015-04-21',59,'Salsify programming',NULL,667,NULL,NULL),(623,'2015-04-21',60,'Recreation',NULL,668,NULL,NULL),(624,'2015-04-21',61,'Recreation',NULL,669,NULL,NULL),(625,'2015-04-21',62,'Recreation',NULL,670,NULL,NULL),(626,'2015-04-21',63,'Recreation',NULL,671,NULL,NULL),(627,'2015-04-22',0,'Wake',NULL,672,NULL,NULL),(628,'2015-04-22',1,'Walk',NULL,673,NULL,NULL),(629,'2015-04-22',2,'Schedule',NULL,674,NULL,NULL),(630,'2015-04-22',3,'Schedule',NULL,675,NULL,NULL),(631,'2015-04-22',4,'Rest',NULL,676,NULL,NULL),(632,'2015-04-22',5,'Rest',NULL,677,NULL,NULL),(633,'2015-04-22',6,'Duolingo',NULL,678,NULL,NULL),(634,'2015-04-22',7,'Breakfast',NULL,679,NULL,NULL),(635,'2015-04-22',8,'Salsify',NULL,680,NULL,NULL),(636,'2015-04-22',9,'Salsify',NULL,681,NULL,NULL),(637,'2015-04-22',10,'Learning Management System',NULL,682,NULL,NULL),(638,'2015-04-22',11,'Learning Management System',NULL,683,NULL,NULL),(639,'2015-04-22',12,'Learning Management System',NULL,684,NULL,NULL),(640,'2015-04-22',13,'Learning Management System',NULL,685,NULL,NULL),(641,'2015-04-22',14,'Learning Management System',NULL,686,NULL,NULL),(642,'2015-04-22',15,'Learning Management System',NULL,687,NULL,NULL),(643,'2015-04-22',16,'Learning Management System',NULL,688,NULL,NULL),(644,'2015-04-22',17,'Learning Management System',NULL,689,NULL,NULL),(645,'2015-04-22',18,'Learning Management System',NULL,690,NULL,NULL),(646,'2015-04-22',19,'Learning Management System',NULL,691,NULL,NULL),(647,'2015-04-22',20,'Learning Management System',NULL,692,NULL,NULL),(648,'2015-04-22',21,'Learning Management System',NULL,693,NULL,NULL),(649,'2015-04-22',22,'Break',NULL,694,NULL,NULL),(650,'2015-04-22',24,'Lunch',NULL,696,NULL,NULL),(651,'2015-04-22',25,'Lunch',NULL,697,NULL,NULL),(652,'2015-04-22',26,'Lunch',NULL,698,NULL,NULL),(653,'2015-04-22',27,'Learning Management System',NULL,699,NULL,NULL),(654,'2015-04-22',28,'Learning Management System',NULL,700,NULL,NULL),(655,'2015-04-22',29,'Learning Management System',NULL,701,NULL,NULL),(656,'2015-04-22',30,'Learning Management System',NULL,702,NULL,NULL),(657,'2015-04-22',31,'Learning Management System',NULL,703,NULL,NULL),(658,'2015-04-22',32,'Learning Management System',NULL,704,NULL,NULL),(659,'2015-04-22',33,'Learning Management System',NULL,705,NULL,NULL),(660,'2015-04-22',34,'Learning Management System',NULL,706,NULL,NULL),(661,'2015-04-22',35,'Learning Management System',NULL,707,NULL,NULL),(662,'2015-04-22',36,'Learning Management System',NULL,708,NULL,NULL),(663,'2015-04-22',37,'Learning Management System',NULL,709,NULL,NULL),(664,'2015-04-22',38,'Learning Management System',NULL,710,NULL,NULL),(665,'2015-04-22',39,'Programming',NULL,711,NULL,NULL),(666,'2015-04-22',40,'Programming',NULL,712,NULL,NULL),(667,'2015-04-22',41,'Programming',NULL,713,NULL,NULL),(668,'2015-04-22',42,'Programming',NULL,714,NULL,NULL),(669,'2015-04-22',43,'Dinner',NULL,715,NULL,NULL),(670,'2015-04-22',44,'Dinner',NULL,716,NULL,NULL),(671,'2015-04-22',45,'Dinner',NULL,717,NULL,NULL),(672,'2015-04-22',46,'Dinner',NULL,718,NULL,NULL),(673,'2015-04-22',47,'Learning Management System',NULL,719,NULL,NULL),(674,'2015-04-22',48,'Learning Management System',NULL,720,NULL,NULL),(675,'2015-04-22',49,'Learning Management System',NULL,721,NULL,NULL),(676,'2015-04-22',50,'Learning Management System',NULL,722,NULL,NULL),(677,'2015-04-22',51,'Learning Management System',NULL,723,NULL,NULL),(678,'2015-04-22',52,'Learning Management System',NULL,724,NULL,NULL),(679,'2015-04-22',53,'Learning Management System',NULL,725,NULL,NULL),(680,'2015-04-22',54,'Learning Management System',NULL,726,NULL,NULL),(681,'2015-04-22',55,'Programming',NULL,727,NULL,NULL),(682,'2015-04-22',56,'Programming',NULL,728,NULL,NULL),(683,'2015-04-22',57,'Programming',NULL,729,NULL,NULL),(684,'2015-04-22',58,'Programming',NULL,730,NULL,NULL),(685,'2015-04-22',59,'Rest',NULL,731,NULL,NULL),(686,'2015-04-22',60,'Rest',NULL,732,NULL,NULL),(687,'2015-04-22',61,'Rest',NULL,733,NULL,NULL),(688,'2015-04-22',62,'Rest',NULL,734,NULL,NULL),(689,'2015-04-22',63,'Rest',NULL,735,NULL,NULL),(690,'2015-04-23',0,'Rest',NULL,736,NULL,NULL),(691,'2015-04-23',1,'Wake',NULL,737,NULL,NULL),(692,'2015-04-22',23,'Walk',NULL,695,NULL,NULL),(693,'2015-04-23',2,'Walk',NULL,738,NULL,NULL),(694,'2015-04-23',3,'Salsify',NULL,739,NULL,NULL),(695,'2015-04-23',4,'Salsify',NULL,740,NULL,NULL),(696,'2015-04-23',5,'Schedule',NULL,741,NULL,NULL),(697,'2015-04-23',6,'Breakfast',NULL,742,NULL,NULL),(698,'2015-04-23',7,'Teespring',NULL,743,NULL,NULL),(699,'2015-04-23',8,'Teespring',NULL,744,NULL,NULL),(700,'2015-04-23',9,'Teespring',NULL,745,NULL,NULL),(701,'2015-04-23',10,'Teespring',NULL,746,NULL,NULL),(702,'2015-04-23',11,'Teespring',NULL,747,NULL,NULL),(703,'2015-04-23',12,'Teespring',NULL,748,NULL,NULL),(704,'2015-04-23',13,'Teespring',NULL,749,NULL,NULL),(705,'2015-04-23',14,'Teespring',NULL,750,NULL,NULL),(706,'2015-04-23',15,'Learning Management System',NULL,751,NULL,NULL),(707,'2015-04-23',16,'Learning Management System',NULL,752,NULL,NULL),(708,'2015-04-23',17,'Learning Management System',NULL,753,NULL,NULL),(709,'2015-04-23',18,'Learning Management System',NULL,754,NULL,NULL),(710,'2015-04-23',19,'Learning Management System',NULL,755,NULL,NULL),(711,'2015-04-23',20,'Learning Management System',NULL,756,NULL,NULL),(712,'2015-04-23',21,'Learning Management System',NULL,757,NULL,NULL),(713,'2015-04-23',22,'Red Hat',NULL,758,NULL,NULL),(714,'2015-04-23',23,'Red Hat',NULL,759,NULL,NULL),(715,'2015-04-23',36,'Rest',NULL,772,NULL,NULL),(716,'2015-04-23',37,'Rest',NULL,773,NULL,NULL),(717,'2015-04-23',38,'Rest',NULL,774,NULL,NULL),(718,'2015-04-23',39,'Learning Management System',NULL,775,NULL,NULL),(719,'2015-04-23',40,'Learning Management System',NULL,776,NULL,NULL),(720,'2015-04-23',41,'Learning Management System',NULL,777,NULL,NULL),(721,'2015-04-23',42,'Learning Management System',NULL,778,NULL,NULL),(722,'2015-04-23',43,'Learning Management System',NULL,779,NULL,NULL),(723,'2015-04-23',44,'Learning Management System',NULL,780,NULL,NULL),(724,'2015-04-23',45,'Learning Management System',NULL,781,NULL,NULL),(725,'2015-04-23',46,'Learning Management System',NULL,782,NULL,NULL),(726,'2015-04-23',47,'Courtier',NULL,783,NULL,NULL),(727,'2015-04-23',48,'Courtier',NULL,784,NULL,NULL),(728,'2015-04-23',49,'Courtier',NULL,785,NULL,NULL),(729,'2015-04-23',50,'Courtier',NULL,786,NULL,NULL),(730,'2015-04-23',51,'Learning Management System',NULL,787,NULL,NULL),(731,'2015-04-23',52,'Learning Management System',NULL,788,NULL,NULL),(732,'2015-04-23',53,'Learning Management System',NULL,789,NULL,NULL),(733,'2015-04-23',54,'Learning Management System',NULL,790,NULL,NULL),(734,'2015-04-23',55,'Learning Management System',NULL,791,NULL,NULL),(735,'2015-04-23',56,'Learning Management System',NULL,792,NULL,NULL),(736,'2015-04-23',57,'Learning Management System',NULL,793,NULL,NULL),(737,'2015-04-23',58,'Learning Management System',NULL,794,NULL,NULL),(738,'2015-04-23',59,'Rest',NULL,795,NULL,NULL),(739,'2015-04-23',60,'Rest',NULL,796,NULL,NULL),(740,'2015-04-23',61,'Rest',NULL,797,NULL,NULL),(741,'2015-04-23',62,'Rest',NULL,798,NULL,NULL),(742,'2015-04-23',63,'Rest',NULL,799,NULL,NULL),(743,'2015-04-24',0,'Wake',NULL,800,NULL,NULL),(744,'2015-04-24',1,'Walk',NULL,801,NULL,NULL),(745,'2015-04-24',11,'Teespring',NULL,811,NULL,NULL),(746,'2015-04-24',12,'Teespring',NULL,812,NULL,NULL),(747,'2015-04-24',13,'Teespring',NULL,813,NULL,NULL),(748,'2015-04-24',14,'Teespring',NULL,814,NULL,NULL),(749,'2015-04-24',15,'Teespring',NULL,815,NULL,NULL),(750,'2015-04-24',16,'Teespring',NULL,816,NULL,NULL),(751,'2015-04-24',17,'Teespring',NULL,817,NULL,NULL),(752,'2015-04-24',18,'Teespring',NULL,818,NULL,NULL),(753,'2015-04-24',19,'Teespring',NULL,819,NULL,NULL),(754,'2015-04-24',20,'Teespring',NULL,820,NULL,NULL),(755,'2015-04-24',21,'Teespring',NULL,821,NULL,NULL),(756,'2015-04-24',22,'Teespring',NULL,822,NULL,NULL),(757,'2015-04-24',23,'Learning Management System',NULL,823,NULL,NULL),(758,'2015-04-24',24,'Learning Management System',NULL,824,NULL,NULL),(759,'2015-04-24',25,'Learning Management System',NULL,825,NULL,NULL),(760,'2015-04-24',26,'Learning Management System',NULL,826,NULL,NULL),(761,'2015-04-24',27,'Learning Management System',NULL,827,NULL,NULL),(762,'2015-04-24',28,'Learning Management System',NULL,828,NULL,NULL),(763,'2015-04-24',29,'Learning Management System',NULL,829,NULL,NULL),(764,'2015-04-24',30,'Learning Management System',NULL,830,NULL,NULL),(765,'2015-04-24',31,'Learning Management System',NULL,831,NULL,NULL),(766,'2015-04-24',32,'Learning Management System',NULL,832,NULL,NULL),(767,'2015-04-24',33,'Learning Management System',NULL,833,NULL,NULL),(768,'2015-04-24',34,'Learning Management System',NULL,834,NULL,NULL),(769,'2015-04-24',35,'Learning Management System',NULL,835,NULL,NULL),(770,'2015-04-24',36,'Learning Management System',NULL,836,NULL,NULL),(771,'2015-04-24',37,'Learning Management System',NULL,837,NULL,NULL),(772,'2015-04-24',38,'Learning Management System',NULL,838,NULL,NULL),(773,'2015-04-24',39,'Dinner',NULL,839,NULL,NULL),(774,'2015-04-24',40,'Dinner',NULL,840,NULL,NULL),(775,'2015-04-24',41,'Dinner',NULL,841,NULL,NULL),(776,'2015-04-24',42,'Dinner',NULL,842,NULL,NULL),(777,'2015-04-24',43,'Learning Management System',NULL,843,NULL,NULL),(778,'2015-04-24',44,'Learning Management System',NULL,844,NULL,NULL),(779,'2015-04-24',45,'Learning Management System',NULL,845,NULL,NULL),(780,'2015-04-24',46,'Learning Management System',NULL,846,NULL,NULL),(781,'2015-04-24',47,'Learning Management System',NULL,847,NULL,NULL),(782,'2015-04-24',48,'Learning Management System',NULL,848,NULL,NULL),(783,'2015-04-24',49,'Learning Management System',NULL,849,NULL,NULL),(784,'2015-04-24',50,'Learning Management System',NULL,850,NULL,NULL),(785,'2015-04-24',51,'Programming',NULL,851,NULL,NULL),(786,'2015-04-24',52,'Programming',NULL,852,NULL,NULL),(787,'2015-04-24',53,'Programming',NULL,853,NULL,NULL),(788,'2015-04-24',54,'Programming',NULL,854,NULL,NULL),(789,'2015-04-24',55,'Programming',NULL,855,NULL,NULL),(790,'2015-04-24',56,'Programming',NULL,856,NULL,NULL),(791,'2015-04-24',57,'Programming',NULL,857,NULL,NULL),(792,'2015-04-24',58,'Programming',NULL,858,NULL,NULL),(793,'2015-04-24',59,'Rest',NULL,859,NULL,NULL),(794,'2015-04-24',60,'Rest',NULL,860,NULL,NULL),(795,'2015-04-24',61,'Rest',NULL,861,NULL,NULL),(796,'2015-04-24',62,'Rest',NULL,862,NULL,NULL),(797,'2015-04-24',63,'Rest',NULL,863,NULL,NULL),(798,'2015-04-25',0,'Wake',NULL,864,NULL,NULL),(799,'2015-04-25',1,'Walk',NULL,865,NULL,NULL),(800,'2015-04-25',2,'Teespring Tic Tac Toe',NULL,866,NULL,NULL),(801,'2015-04-25',3,'Teespring Tic Tac Toe',NULL,867,NULL,NULL),(802,'2015-04-25',4,'Teespring Tic Tac Toe',NULL,868,NULL,NULL),(803,'2015-04-25',5,'Teespring Tic Tac Toe',NULL,869,NULL,NULL),(804,'2015-04-25',6,'Teespring Tic Tac Toe',NULL,870,NULL,NULL),(805,'2015-04-25',7,'Teespring Tic Tac Toe',NULL,871,NULL,NULL),(806,'2015-04-25',8,'Teespring Tic Tac Toe',NULL,872,NULL,NULL),(807,'2015-04-25',9,'Teespring Tic Tac Toe',NULL,873,NULL,NULL),(808,'2015-04-25',10,'Teespring Tic Tac Toe',NULL,874,NULL,NULL),(809,'2015-04-25',11,'Teespring Tic Tac Toe',NULL,875,NULL,NULL),(810,'2015-04-25',12,'Teespring Tic Tac Toe',NULL,876,NULL,NULL),(811,'2015-04-25',13,'Teespring Tic Tac Toe',NULL,877,NULL,NULL),(812,'2015-04-25',14,'Teespring Tic Tac Toe',NULL,878,NULL,NULL),(813,'2015-04-25',15,'Teespring Tic Tac Toe',NULL,879,NULL,NULL),(814,'2015-04-25',16,'Teespring Tic Tac Toe',NULL,880,NULL,NULL),(815,'2015-04-25',17,'Teespring Tic Tac Toe',NULL,881,NULL,NULL),(816,'2015-04-25',18,'Teespring Tic Tac Toe',NULL,882,NULL,NULL),(817,'2015-04-25',19,'Teespring Tic Tac Toe',NULL,883,NULL,NULL),(818,'2015-04-25',20,'Teespring Tic Tac Toe',NULL,884,NULL,NULL),(819,'2015-04-25',21,'Teespring Tic Tac Toe',NULL,885,NULL,NULL),(820,'2015-04-25',22,'Teespring Tic Tac Toe',NULL,886,NULL,NULL),(821,'2015-04-25',23,'Teespring Tic Tac Toe',NULL,887,NULL,NULL),(822,'2015-04-25',24,'Teespring Tic Tac Toe',NULL,888,NULL,NULL),(823,'2015-04-25',25,'Teespring Tic Tac Toe',NULL,889,NULL,NULL),(824,'2015-04-25',29,'Teespring Tic Tac Toe',NULL,893,NULL,NULL),(825,'2015-04-25',30,'Teespring Tic Tac Toe',NULL,894,NULL,NULL),(826,'2015-04-25',31,'Teespring Tic Tac Toe',NULL,895,NULL,NULL),(827,'2015-04-25',32,'Teespring Tic Tac Toe',NULL,896,NULL,NULL),(828,'2015-04-25',33,'Teespring Tic Tac Toe',NULL,897,NULL,NULL),(829,'2015-04-25',34,'Teespring Tic Tac Toe',NULL,898,NULL,NULL),(830,'2015-04-25',35,'Teespring Tic Tac Toe',NULL,899,NULL,NULL),(831,'2015-04-25',36,'Teespring Tic Tac Toe',NULL,900,NULL,NULL),(832,'2015-04-25',37,'Teespring Tic Tac Toe',NULL,901,NULL,NULL),(833,'2015-04-25',38,'Teespring Tic Tac Toe',NULL,902,NULL,NULL),(834,'2015-04-25',39,'Teespring Tic Tac Toe',NULL,903,NULL,NULL),(835,'2015-04-25',40,'Teespring Tic Tac Toe',NULL,904,NULL,NULL),(836,'2015-04-25',41,'Teespring Tic Tac Toe',NULL,905,NULL,NULL),(837,'2015-04-25',42,'Teespring Tic Tac Toe',NULL,906,NULL,NULL),(838,'2015-04-25',43,'Teespring Tic Tac Toe',NULL,907,NULL,NULL),(839,'2015-04-25',44,'Teespring Tic Tac Toe',NULL,908,NULL,NULL),(840,'2015-04-25',45,'Teespring Tic Tac Toe',NULL,909,NULL,NULL),(841,'2015-04-25',46,'Teespring Tic Tac Toe',NULL,910,NULL,NULL),(842,'2015-04-25',47,'Teespring Tic Tac Toe',NULL,911,NULL,NULL),(843,'2015-04-25',48,'Teespring Tic Tac Toe',NULL,912,NULL,NULL),(844,'2015-04-25',49,'Teespring Tic Tac Toe',NULL,913,NULL,NULL),(845,'2015-04-25',50,'Teespring Tic Tac Toe',NULL,914,NULL,NULL),(846,'2015-04-25',51,'Teespring Tic Tac Toe',NULL,915,NULL,NULL),(847,'2015-04-25',52,'Teespring Tic Tac Toe',NULL,916,NULL,NULL),(848,'2015-04-26',6,'Sleep',NULL,934,NULL,NULL),(849,'2015-04-26',7,'Sleep',NULL,935,NULL,NULL),(850,'2015-04-26',8,'Sleep',NULL,936,NULL,NULL),(851,'2015-04-26',9,'Sleep',NULL,937,NULL,NULL),(852,'2015-04-26',10,'Sleep',NULL,938,NULL,NULL),(853,'2015-04-26',11,'Sleep',NULL,939,NULL,NULL),(854,'2015-04-26',12,'Sleep',NULL,940,NULL,NULL),(855,'2015-04-26',13,'Sleep',NULL,941,NULL,NULL),(856,'2015-04-26',14,'Sleep',NULL,942,NULL,NULL),(857,'2015-04-26',15,'Sleep',NULL,943,NULL,NULL),(858,'2015-04-26',16,'Sleep',NULL,944,NULL,NULL),(859,'2015-04-26',17,'Sleep',NULL,945,NULL,NULL),(860,'2015-04-26',18,'Tic Tac Toe',NULL,946,NULL,NULL),(861,'2015-04-26',19,'Tic Tac Toe',NULL,947,NULL,NULL),(862,'2015-04-26',20,'Tic Tac Toe',NULL,948,NULL,NULL),(863,'2015-04-26',21,'Tic Tac Toe',NULL,949,NULL,NULL),(864,'2015-04-26',22,'Tic Tac Toe',NULL,950,NULL,NULL),(865,'2015-04-26',23,'Tic Tac Toe',NULL,951,NULL,NULL),(866,'2015-04-26',24,'Tic Tac Toe',NULL,952,NULL,NULL),(867,'2015-04-26',25,'Tic Tac Toe',NULL,953,NULL,NULL),(868,'2015-04-26',26,'Tic Tac Toe',NULL,954,NULL,NULL),(869,'2015-04-26',27,'Tic Tac Toe',NULL,955,NULL,NULL),(870,'2015-04-26',28,'Tic Tac Toe',NULL,956,NULL,NULL),(871,'2015-04-26',29,'Tic Tac Toe',NULL,957,NULL,NULL),(872,'2015-04-26',30,'Tic Tac Toe',NULL,958,NULL,NULL),(873,'2015-04-26',31,'Tic Tac Toe',NULL,959,NULL,NULL),(874,'2015-04-26',32,'Tic Tac Toe',NULL,960,NULL,NULL),(875,'2015-04-26',33,'Tic Tac Toe',NULL,961,NULL,NULL),(876,'2015-04-26',34,'Tic Tac Toe',NULL,962,NULL,NULL),(877,'2015-04-26',35,'Tic Tac Toe',NULL,963,NULL,NULL),(878,'2015-04-26',36,'Tic Tac Toe',NULL,964,NULL,NULL),(879,'2015-04-26',37,'Tic Tac Toe',NULL,965,NULL,NULL),(880,'2015-04-26',38,'Tic Tac Toe',NULL,966,NULL,NULL),(881,'2015-04-26',39,'Tic Tac Toe',NULL,967,NULL,NULL),(882,'2015-04-26',40,'Tic Tac Toe',NULL,968,NULL,NULL),(883,'2015-04-26',41,'Tic Tac Toe',NULL,969,NULL,NULL),(884,'2015-04-27',24,'Learning Management System',NULL,1016,NULL,NULL),(885,'2015-04-27',25,'Learning Management System',NULL,1017,NULL,NULL),(886,'2015-04-27',26,'Learning Management System',NULL,1018,NULL,NULL),(887,'2015-04-27',27,'Learning Management System',NULL,1019,NULL,NULL),(888,'2015-04-27',28,'Learning Management System',NULL,1020,NULL,NULL),(889,'2015-04-27',29,'Learning Management System',NULL,1021,NULL,NULL),(890,'2015-04-27',30,'Learning Management System',NULL,1022,NULL,NULL),(891,'2015-04-27',31,'Learning Management System',NULL,1023,NULL,NULL),(892,'2015-04-27',32,'Learning Management System',NULL,1024,NULL,NULL),(893,'2015-04-27',33,'Learning Management System',NULL,1025,NULL,NULL),(894,'2015-04-27',34,'Learning Management System',NULL,1026,NULL,NULL),(895,'2015-04-27',35,'Learning Management System',NULL,1027,NULL,NULL),(896,'2015-04-27',36,'Learning Management System',NULL,1028,NULL,NULL),(897,'2015-04-27',37,'Learning Management System',NULL,1029,NULL,NULL),(898,'2015-04-27',38,'Learning Management System',NULL,1030,NULL,NULL),(899,'2015-04-27',39,'Learning Management System',NULL,1031,NULL,NULL),(900,'2015-04-27',40,'Learning Management System',NULL,1032,NULL,NULL),(901,'2015-04-27',41,'Learning Management System',NULL,1033,NULL,NULL),(902,'2015-04-27',42,'Learning Management System',NULL,1034,NULL,NULL),(903,'2015-04-27',43,'Learning Management System',NULL,1035,NULL,NULL),(904,'2015-04-27',44,'Learning Management System',NULL,1036,NULL,NULL),(905,'2015-04-27',45,'Learning Management System',NULL,1037,NULL,NULL),(906,'2015-04-27',46,'Learning Management System',NULL,1038,NULL,NULL),(907,'2015-04-27',47,'Learning Management System',NULL,1039,NULL,NULL),(908,'2015-04-27',48,'Learning Management System',NULL,1040,NULL,NULL),(909,'2015-04-27',49,'Learning Management System',NULL,1041,NULL,NULL),(910,'2015-04-27',50,'Learning Management System',NULL,1042,NULL,NULL),(911,'2015-04-27',51,'Learning Management System',NULL,1043,NULL,NULL),(912,'2015-04-27',52,'Learning Management System',NULL,1044,NULL,NULL),(913,'2015-04-27',53,'Learning Management System',NULL,1045,NULL,NULL),(914,'2015-04-27',54,'Learning Management System',NULL,1046,NULL,NULL),(915,'2015-04-27',55,'Learning Management System',NULL,1047,NULL,NULL),(916,'2015-04-27',56,'Learning Management System',NULL,1048,NULL,NULL),(917,'2015-04-27',57,'Learning Management System',NULL,1049,NULL,NULL),(918,'2015-04-27',58,'Learning Management System',NULL,1050,NULL,NULL),(919,'2015-04-27',59,'Learning Management System',NULL,1051,NULL,NULL),(920,'2015-04-27',60,'Learning Management System',NULL,1052,NULL,NULL),(921,'2015-04-27',61,'Learning Management System',NULL,1053,NULL,NULL),(922,'2015-04-27',62,'Learning Management System',NULL,1054,NULL,NULL),(923,'2015-04-27',63,'Learning Management System',NULL,1055,NULL,NULL),(924,'2015-04-28',0,'Learning Management System',NULL,1056,NULL,NULL),(925,'2015-04-28',1,'Learning Management System',NULL,1057,NULL,NULL),(926,'2015-04-28',2,'Learning Management System',NULL,1058,NULL,NULL),(927,'2015-04-28',3,'Learning Management System',NULL,1059,NULL,NULL),(928,'2015-04-28',4,'Learning Management System',NULL,1060,NULL,NULL),(929,'2015-04-28',5,'Learning Management System',NULL,1061,NULL,NULL),(930,'2015-04-28',6,'Learning Management System',NULL,1062,NULL,NULL),(931,'2015-04-28',7,'Learning Management System',NULL,1063,NULL,NULL),(932,'2015-04-28',8,'Rest',NULL,1064,NULL,NULL),(933,'2015-04-28',9,'Rest',NULL,1065,NULL,NULL),(934,'2015-04-28',10,'Rest',NULL,1066,NULL,NULL),(935,'2015-04-28',11,'Rest',NULL,1067,NULL,NULL),(936,'2015-04-28',12,'Rest',NULL,1068,NULL,NULL),(937,'2015-04-28',13,'Rest',NULL,1069,NULL,NULL),(938,'2015-04-28',14,'Rest',NULL,1070,NULL,NULL),(939,'2015-04-28',15,'Rest',NULL,1071,NULL,NULL),(940,'2015-04-28',16,'Wake',NULL,1072,NULL,NULL),(941,'2015-04-28',17,'Walk',NULL,1073,NULL,NULL),(942,'2015-04-28',18,'Breakfast',NULL,1074,NULL,NULL),(943,'2015-04-28',19,'Learning Management System',NULL,1075,NULL,NULL),(944,'2015-04-28',20,'Learning Management System',NULL,1076,NULL,NULL),(945,'2015-04-28',21,'Learning Management System',NULL,1077,NULL,NULL),(946,'2015-04-28',22,'Learning Management System',NULL,1078,NULL,NULL),(947,'2015-04-28',23,'Interviews',NULL,1079,NULL,NULL),(948,'2015-04-28',24,'Interviews',NULL,1080,NULL,NULL),(949,'2015-04-28',25,'Interviews',NULL,1081,NULL,NULL),(950,'2015-04-28',26,'Interviews',NULL,1082,NULL,NULL),(951,'2015-04-28',27,'Interviews',NULL,1083,NULL,NULL),(952,'2015-04-28',28,'Interviews',NULL,1084,NULL,NULL),(953,'2015-04-28',29,'Interviews',NULL,1085,NULL,NULL),(954,'2015-04-28',30,'Interviews',NULL,1086,NULL,NULL),(955,'2015-04-28',31,'Interviews',NULL,1087,NULL,NULL),(956,'2015-04-28',32,'Interviews',NULL,1088,NULL,NULL),(957,'2015-04-28',33,'Interviews',NULL,1089,NULL,NULL),(958,'2015-04-28',34,'Interviews',NULL,1090,NULL,NULL),(959,'2015-04-28',35,'Interviews',NULL,1091,NULL,NULL),(960,'2015-04-28',36,'Interviews',NULL,1092,NULL,NULL),(961,'2015-04-28',37,'Interviews',NULL,1093,NULL,NULL),(962,'2015-04-28',38,'Interviews',NULL,1094,NULL,NULL),(963,'2015-04-28',39,'Interviews',NULL,1095,NULL,NULL),(964,'2015-04-28',40,'Interviews',NULL,1096,NULL,NULL),(965,'2015-04-28',41,'Interviews',NULL,1097,NULL,NULL),(966,'2015-04-28',42,'Interviews',NULL,1098,NULL,NULL),(967,'2015-04-28',43,'Interviews',NULL,1099,NULL,NULL),(968,'2015-04-28',44,'Interviews',NULL,1100,NULL,NULL),(969,'2015-04-28',45,'Interviews',NULL,1101,NULL,NULL),(970,'2015-04-28',46,'Interviews',NULL,1102,NULL,NULL),(971,'2015-04-28',47,'Learning Management System',NULL,1103,NULL,NULL),(972,'2015-04-28',48,'Learning Management System',NULL,1104,NULL,NULL),(973,'2015-04-28',49,'Learning Management System',NULL,1105,NULL,NULL),(974,'2015-04-28',50,'Learning Management System',NULL,1106,NULL,NULL),(975,'2015-04-28',51,'Learning Management System',NULL,1107,NULL,NULL),(976,'2015-04-28',52,'Learning Management System',NULL,1108,NULL,NULL),(977,'2015-04-28',53,'Courtier',NULL,1109,NULL,NULL),(978,'2015-04-28',54,'Courtier',NULL,1110,NULL,NULL),(979,'2015-04-28',55,'Courtier',NULL,1111,NULL,NULL),(980,'2015-04-28',56,'Courtier',NULL,1112,NULL,NULL),(981,'2015-04-28',57,'Programming',NULL,1113,NULL,NULL),(982,'2015-04-28',58,'Programming',NULL,1114,NULL,NULL),(983,'2015-04-28',59,'Programming',NULL,1115,NULL,NULL),(984,'2015-04-28',60,'Programming',NULL,1116,NULL,NULL),(985,'2015-04-28',61,'Programming',NULL,1117,NULL,NULL),(986,'2015-04-28',62,'Programming',NULL,1118,NULL,NULL),(987,'2015-04-28',63,'Programming',NULL,1119,NULL,NULL),(988,'2015-04-29',0,'Wake',NULL,1120,NULL,NULL),(989,'2015-04-29',1,'Walk',NULL,1121,NULL,NULL),(990,'2015-04-29',2,'Breakfast',NULL,1122,NULL,NULL),(991,'2015-04-29',3,'Beautiful Soup',NULL,1123,NULL,NULL),(992,'2015-04-29',4,'Beautiful Soup',NULL,1124,NULL,NULL),(993,'2015-04-29',5,'Beautiful Soup',NULL,1125,NULL,NULL),(994,'2015-04-29',6,'Beautiful Soup',NULL,1126,NULL,NULL),(995,'2015-04-29',7,'Beautiful Soup',NULL,1127,NULL,NULL),(996,'2015-04-29',8,'Beautiful Soup',NULL,1128,NULL,NULL),(997,'2015-04-29',9,'Beautiful Soup',NULL,1129,NULL,NULL),(998,'2015-04-29',10,'Beautiful Soup',NULL,1130,NULL,NULL),(999,'2015-04-29',11,'Beautiful Soup',NULL,1131,NULL,NULL),(1000,'2015-04-29',12,'Beautiful Soup',NULL,1132,NULL,NULL),(1001,'2015-04-29',13,'Beautiful Soup',NULL,1133,NULL,NULL),(1002,'2015-04-29',14,'Beautiful Soup',NULL,1134,NULL,NULL),(1003,'2015-04-29',15,'Beautiful Soup',NULL,1135,NULL,NULL),(1004,'2015-04-29',16,'Beautiful Soup',NULL,1136,NULL,NULL),(1005,'2015-04-29',17,'Beautiful Soup',NULL,1137,NULL,NULL),(1006,'2015-04-29',18,'Beautiful Soup',NULL,1138,NULL,NULL),(1007,'2015-04-30',12,'General Assembly',NULL,1196,NULL,NULL),(1008,'2015-04-30',13,'General Assembly',NULL,1197,NULL,NULL),(1009,'2015-04-30',14,'General Assembly',NULL,1198,NULL,NULL),(1010,'2015-04-30',15,'General Assembly',NULL,1199,NULL,NULL),(1011,'2015-05-24',41,'Schedule',NULL,2761,NULL,NULL),(1012,'2015-05-24',42,'Schedule',NULL,2762,NULL,NULL);
/*!40000 ALTER TABLE `schedule_atom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_atom_notes`
--

DROP TABLE IF EXISTS `schedule_atom_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_atom_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atom_id` int(11) NOT NULL,
  `notecard_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `atom_id` (`atom_id`,`notecard_id`),
  KEY `schedule_atom_notes_bc95ff85` (`atom_id`),
  KEY `schedule_atom_notes_c25588b2` (`notecard_id`),
  CONSTRAINT `schedule_atom_notes_atom_id_9a3503e8e79f9eb_fk_schedule_atom_id` FOREIGN KEY (`atom_id`) REFERENCES `schedule_atom` (`id`),
  CONSTRAINT `schedule__notecard_id_609b31f04e144f8c_fk_psychology_notecard_id` FOREIGN KEY (`notecard_id`) REFERENCES `psychology_notecard` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_atom_notes`
--

LOCK TABLES `schedule_atom_notes` WRITE;
/*!40000 ALTER TABLE `schedule_atom_notes` DISABLE KEYS */;
INSERT INTO `schedule_atom_notes` VALUES (1,10,24),(2,11,25),(3,13,26);
/*!40000 ALTER TABLE `schedule_atom_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_atom_tasks`
--

DROP TABLE IF EXISTS `schedule_atom_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_atom_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atom_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `atom_id` (`atom_id`,`task_id`),
  KEY `schedule_atom_tasks_bc95ff85` (`atom_id`),
  KEY `schedule_atom_tasks_57746cc8` (`task_id`),
  CONSTRAINT `schedule_atom_tasks_atom_id_5654c2109eb3b7d8_fk_schedule_atom_id` FOREIGN KEY (`atom_id`) REFERENCES `schedule_atom` (`id`),
  CONSTRAINT `schedule_atom_tasks_task_id_deabe8264bf63dc_fk_tasks_task_id` FOREIGN KEY (`task_id`) REFERENCES `tasks_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_atom_tasks`
--

LOCK TABLES `schedule_atom_tasks` WRITE;
/*!40000 ALTER TABLE `schedule_atom_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule_atom_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_dailysurvey`
--

DROP TABLE IF EXISTS `schedule_dailysurvey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_dailysurvey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nap` tinyint(1) NOT NULL,
  `good` tinyint(1) NOT NULL,
  `happy` tinyint(1) NOT NULL,
  `breakfast` tinyint(1) NOT NULL,
  `dinner` tinyint(1) NOT NULL,
  `lunch` tinyint(1) NOT NULL,
  `evening_caffeine` tinyint(1) NOT NULL,
  `morning_caffeine` tinyint(1) NOT NULL,
  `television` tinyint(1) NOT NULL,
  `wake` tinyint(1) NOT NULL,
  `cleaned` tinyint(1) DEFAULT NULL,
  `exercise` tinyint(1) NOT NULL,
  `french` tinyint(1) NOT NULL,
  `lumosity` tinyint(1) NOT NULL,
  `piano` tinyint(1) NOT NULL,
  `energy` tinyint(1) NOT NULL,
  `calm` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `peak_happiness` decimal(3,2),
  `peak_energy` decimal(3,2),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_dailysurvey`
--

LOCK TABLES `schedule_dailysurvey` WRITE;
/*!40000 ALTER TABLE `schedule_dailysurvey` DISABLE KEYS */;
INSERT INTO `schedule_dailysurvey` VALUES (5,0,0,0,1,1,1,0,1,1,1,1,1,0,1,0,0,0,'2014-12-23',0.50,0.50),(6,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,1,1,'2014-12-24',0.50,0.50),(8,0,0,1,1,1,1,1,1,1,0,1,0,0,1,0,0,1,'2014-12-25',0.50,0.50),(9,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,'2014-12-26',0.50,0.50),(10,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,0,0,'2014-12-27',0.50,0.50),(11,0,1,1,1,0,0,0,1,0,1,1,0,0,1,0,1,0,'2014-12-28',0.92,0.92),(12,0,0,1,0,1,1,1,0,1,0,1,0,0,0,0,0,1,'2014-12-29',0.60,0.60),(13,0,1,0,1,1,1,1,1,1,0,1,0,0,0,0,1,1,'2014-12-30',0.50,0.50),(14,0,0,0,1,0,1,0,0,1,0,1,0,1,1,0,0,0,'2014-12-31',0.50,0.50),(15,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,'2015-01-01',0.50,0.50),(16,0,1,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,'2015-01-02',0.50,0.50);
/*!40000 ALTER TABLE `schedule_dailysurvey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks_activity`
--

DROP TABLE IF EXISTS `tasks_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks_activity`
--

LOCK TABLES `tasks_activity` WRITE;
/*!40000 ALTER TABLE `tasks_activity` DISABLE KEYS */;
INSERT INTO `tasks_activity` VALUES (1,'Juice'),(2,'Eat'),(3,'Lumosity'),(4,'Duolingo'),(5,'Clean'),(6,'Rest'),(7,'Walk'),(8,'Exercise'),(9,'Read'),(10,'Program'),(11,'Work'),(12,'Relax'),(13,'Education');
/*!40000 ALTER TABLE `tasks_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks_date`
--

DROP TABLE IF EXISTS `tasks_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks_date`
--

LOCK TABLES `tasks_date` WRITE;
/*!40000 ALTER TABLE `tasks_date` DISABLE KEYS */;
INSERT INTO `tasks_date` VALUES (1,'2015-01-26');
/*!40000 ALTER TABLE `tasks_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks_task`
--

DROP TABLE IF EXISTS `tasks_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `parent_id` int(11),
  `position` int(11) NOT NULL,
  `date_id` int(11),
  `complete` tinyint(1) NOT NULL,
  `time_estimate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_task_6be37982` (`parent_id`),
  KEY `tasks_task_f60fa4d8` (`date_id`),
  CONSTRAINT `tasks_task_date_id_65ac73afe296eb6e_fk_tasks_date_id` FOREIGN KEY (`date_id`) REFERENCES `tasks_date` (`id`),
  CONSTRAINT `tasks_task_parent_id_7bc817ecf72d1140_fk_tasks_task_id` FOREIGN KEY (`parent_id`) REFERENCES `tasks_task` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks_task`
--

LOCK TABLES `tasks_task` WRITE;
/*!40000 ALTER TABLE `tasks_task` DISABLE KEYS */;
INSERT INTO `tasks_task` VALUES (23,'Change IntervalLog to Atom',NULL,1,NULL,1,15),(24,'Estimate life atoms',NULL,1,NULL,1,15),(25,'Create atomic point system',NULL,1,NULL,1,15),(26,'Add an absolute index to the atom',NULL,1,NULL,1,15),(27,'Create a personal website',NULL,1,NULL,0,240),(28,'Revise resume',NULL,1,NULL,0,15),(29,'Find a recruiter in California',NULL,1,NULL,0,15),(30,'Update the development plan for the LMS',NULL,1,NULL,0,60),(31,'Create a task list which will get me out of Warren',NULL,1,NULL,0,180),(32,'Find a job',NULL,1,NULL,0,12000),(33,'Move out of Warren',NULL,1,NULL,0,2400),(34,'Add interval and date arguments to the atom methods to plan ahead. Warn against overwriting data.',NULL,1,NULL,0,15),(35,'Write a relevant cover letter',NULL,1,NULL,0,30),(36,'Work with LinkedIn profile',NULL,1,NULL,0,30),(37,'Apply to jobs on Angel List',NULL,1,NULL,0,45),(38,'Practice technical programming questions',NULL,1,NULL,0,1200),(39,'Find and read a book on Javascript',NULL,1,NULL,0,600),(40,'Add a recurring activity model to bind to atoms',NULL,1,NULL,0,45),(41,'Create method to list atoms',NULL,1,NULL,0,15),(42,'File my taxes',NULL,1,NULL,0,90),(43,'Calculate how long I have before I require income',NULL,1,NULL,0,90),(44,'Release clockwork',NULL,1,NULL,0,15),(45,'CLean and push Organize',NULL,1,NULL,0,15),(46,'Clean and push PB&J',NULL,1,NULL,0,15),(47,'Clean up github.com/pinetrees',NULL,1,NULL,0,15),(48,'Create pinetrees.io, or otherwise...',NULL,1,NULL,0,15),(49,'Clean and push stashed object to django package',NULL,1,NULL,0,15),(50,'Add resume to pinetrees.space',NULL,1,NULL,0,15),(51,'Continue development on pinetrees.space',NULL,1,NULL,0,15);
/*!40000 ALTER TABLE `tasks_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks_task_dependencies`
--

DROP TABLE IF EXISTS `tasks_task_dependencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks_task_dependencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_task_id` int(11) NOT NULL,
  `to_task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `from_task_id` (`from_task_id`,`to_task_id`),
  KEY `tasks_task_dependencies_5db94e1d` (`from_task_id`),
  KEY `tasks_task_dependencies_e303ac75` (`to_task_id`),
  CONSTRAINT `tasks_task_dependen_to_task_id_4cdf75ea54914e6d_fk_tasks_task_id` FOREIGN KEY (`to_task_id`) REFERENCES `tasks_task` (`id`),
  CONSTRAINT `tasks_task_depend_from_task_id_4e4c02e38bc08cc2_fk_tasks_task_id` FOREIGN KEY (`from_task_id`) REFERENCES `tasks_task` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks_task_dependencies`
--

LOCK TABLES `tasks_task_dependencies` WRITE;
/*!40000 ALTER TABLE `tasks_task_dependencies` DISABLE KEYS */;
INSERT INTO `tasks_task_dependencies` VALUES (2,32,35),(3,32,36),(4,32,37),(5,32,38),(6,32,39),(1,33,32);
/*!40000 ALTER TABLE `tasks_task_dependencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_chemical`
--

DROP TABLE IF EXISTS `units_chemical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_chemical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_chemical`
--

LOCK TABLES `units_chemical` WRITE;
/*!40000 ALTER TABLE `units_chemical` DISABLE KEYS */;
INSERT INTO `units_chemical` VALUES (1,'Caffeine');
/*!40000 ALTER TABLE `units_chemical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_content`
--

DROP TABLE IF EXISTS `units_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` decimal(6,2) NOT NULL,
  `container_id` int(11) NOT NULL,
  `measure_id` int(11) NOT NULL,
  `chemical_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `units_content_5733dad4` (`container_id`),
  KEY `units_content_80c371ce` (`measure_id`),
  KEY `units_content_a69d813a` (`chemical_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_content`
--

LOCK TABLES `units_content` WRITE;
/*!40000 ALTER TABLE `units_content` DISABLE KEYS */;
INSERT INTO `units_content` VALUES (1,1200.00,1,3,1),(2,1000.00,4,3,1);
/*!40000 ALTER TABLE `units_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_conversion`
--

DROP TABLE IF EXISTS `units_conversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_conversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `multiplier` decimal(14,8),
  `from_unit_id` int(11) NOT NULL,
  `to_unit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `units_conversion_eb0cac85` (`from_unit_id`),
  KEY `units_conversion_725b5a87` (`to_unit_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_conversion`
--

LOCK TABLES `units_conversion` WRITE;
/*!40000 ALTER TABLE `units_conversion` DISABLE KEYS */;
INSERT INTO `units_conversion` VALUES (1,80.00000000,2,1),(2,12.00000000,5,4),(3,16.00000000,10,9),(5,453.59200000,11,7);
/*!40000 ALTER TABLE `units_conversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_unit`
--

DROP TABLE IF EXISTS `units_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200),
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_unit`
--

LOCK TABLES `units_unit` WRITE;
/*!40000 ALTER TABLE `units_unit` DISABLE KEYS */;
INSERT INTO `units_unit` VALUES (1,'Dropper Bottle','A 2 ounce dropper bottle, primarily used for storing and measuring precise quantities of caffeine.'),(2,'Half Droplet','A half filled dropper from the dropper bottle. This is the standard fill when the appropriate volume of air is pushed out of the dropper.'),(3,'Milligram','A standard milligram.'),(4,'Ice Cube Tray','Primarily used for storing caffeinated ice cubes.'),(5,'Ice Cube','Primarily caffeinated ice cubes.'),(6,'Unit','This is one unit relative to the object to which it is applied.'),(7,'Pound',''),(8,'Stalk','This applies to celery.'),(9,'Gallon',''),(10,'Cup',''),(11,'Gram',''),(12,'Box','A box of pasta?'),(13,'Spoon','A spoon of cream cheese?'),(14,'Can','A can of tomato paste?'),(15,'Tablespoon','');
/*!40000 ALTER TABLE `units_unit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-27 18:10:39
