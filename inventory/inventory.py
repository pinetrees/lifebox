from .models import Category, Label, Attribute, ContainerPossession, Possession, Location

IC = Category
IL = Label
IA = Attribute
ICP = ContainerPossession
IP = Possession
IL = Location


class InventoryPopulation:
	categories = [
		'Memorabilia',
		'Trinket',
		'Electronics',
		'Book',
		'Utility',
		'Disposable',
		'Health',
		'ContainerPossession',
		'Cookware',
		'Food',
		'Clothing',
		'Music',
		'Stationary',
		'Furniture',
		'Gift',
		'Accessory',
	]

	containers = [
		'steel oats',
	]

	locations = [
		'standing desk shelf one',
		'standing desk shelf two',
		'standing desk shelf three',
		'standing desk drawer one',
		'standing desk drawer two',
		'standing desk wall hook',
		'standing desk bottom rack',
		'standing desk floorspace',
		'standing desk surrounding area',
		'bathroom sink drawer one',
		'bathroom sink drawer two',
		'bathroom sink drawer three',
		'bathroom sink cabinet',
		'refrigerator top',
        'dresser desk top',
        'dresser cubby',
        'dresser drawer one',
        'dresser drawer two',
        'dresser drawer three',
        'dresser drawer four',
        'dresser drawer five',
        'dresser drawer six',
	]

	def set_possessions(self):
		self.possessions = [
			{
				'attributes': {
					'name': 'anime girl',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Trinket'])
			},
			{
				'attributes': {
					'name': 'glass egg',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Trinket'])
			},
			{
				'attributes': {
					'name': 'wolf medallion',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Trinket'])
			},
			{
				'attributes': {
					'name': 'orange dice set',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Trinket'])
			},
			{
				'attributes': {
					'name': 'gemstone',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Trinket'])
			},
			{
				'attributes': {
					'name': 'harmonica',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Trinket', 'Music'])
			},
			{
				'attributes': {
					'name': 'carnegie mellon graduation pen',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Memorabilia', 'Stationary'])
			},
			{
				'attributes': {
					'name': 'graduation tassle',
					'container': self.get_container('steel oats'),
				},
				'categories': self.get_categories(['Memorabilia'])
			},
			{
				'attributes': {
					'name': 'toe nail clippers',
					'location': self.get_location('bathroom sink drawer one'),
				},
				'categories': self.get_categories(['Memorabilia'])
			},
			{
				'attributes': {
					'name': 'loose index cards',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility', 'Stationary'])
			},
			{
				'attributes': {
					'name': 'small portable clock',
					'location': self.get_location('standing desk shelf one'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'amazon bluetooth speaker',
					'location': self.get_location('standing desk shelf one'),
				},
				'categories': self.get_categories(['Utility', 'Electronics', 'Gift'])
			},
			{
				'attributes': {
					'name': 'packaged index cards',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility', 'Stationary'])
			},
			{
				'attributes': {
					'name': 'amazon bluetooth speaker charger',
					'location': self.get_location('standing desk wall hook'),
				},
				'categories': self.get_categories(['Utility', 'Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'lenovo t430 laptop',
					'location': self.get_location('standing desk shelf one'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'lenovo t430 laptop charger',
					'location': self.get_location('standing desk floorspace'),
				},
				'categories': self.get_categories(['Utility', 'Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'logitech wireless mouse and keyboard',
					'location': self.get_location('standing desk shelf two'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'liquid caffeine dropper bottle',
					'location': self.get_location('standing desk shelf two'),
				},
				'categories': self.get_categories(['Health'])
			},
			{
				'attributes': {
					'name': 'blue teapot',
					'location': self.get_location('standing desk shelf two'),
				},
				'categories': self.get_categories(['Health', 'Utility', 'Gift'])
			},
			{
				'attributes': {
					'name': 'red cup',
					'location': self.get_location('standing desk shelf two'),
				},
				'categories': self.get_categories(['Health', 'Utility'])
			},
			{
				'attributes': {
					'name': 'black pen',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility', 'Stationary'])
			},
			{
				'attributes': {
					'name': 'postit notes',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility', 'Stationary'])
			},
			{
				'attributes': {
					'name': 'code complete 2',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility'])
			},
			{
				'attributes': {
					'name': 'men of mathematics',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility'])
			},
			{
				'attributes': {
					'name': 'an introduction to database systems',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility'])
			},
			{
				'attributes': {
					'name': 'you are your own gym',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility'])
			},
			{
				'attributes': {
					'name': 'the little prince',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility'])
			},
			{
				'attributes': {
					'name': 'schumann piano concerto',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility', 'Music'])
			},
			{
				'attributes': {
					'name': 'bach goldberg variations',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility', 'Music'])
			},
			{
				'attributes': {
					'name': 'bach partitas',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility', 'Music'])
			},
			{
				'attributes': {
					'name': 'chopin nocturnes',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Book', 'Utility', 'Music'])
			},
			{
				'attributes': {
					'name': 'yellow legal pads',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility', 'Stationary'])
			},
			{
				'attributes': {
					'name': 'nexus 7',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Electronics'])
			},
			{
				'attributes': {
					'name': 'nexus 7 charger block',
					'location': self.get_location('standing desk floor space'),
				},
				'categories': self.get_categories(['Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'wrist watch',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'wallet',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility'])
			},
			{
				'attributes': {
					'name': 'cell phone',
					'location': self.get_location('standing desk shelf three'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'cell phone charger',
					'location': self.get_location('standing desk floorspace'),
				},
				'categories': self.get_categories(['Utility', 'Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'balancing board',
					'location': self.get_location('standing desk floorspace'),
				},
				'categories': self.get_categories(['Utility'])
			},
			{
				'attributes': {
					'name': 'pet vacuum',
					'location': self.get_location('standing desk bottom rack'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'tall fan',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'oil bin seat',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Furniture'])
			},
			{
				'attributes': {
					'name': 'oil bin seat clothing pillow',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Furniture'])
			},
			{
				'attributes': {
					'name': 'large surge protector',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'power strip',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'keyboard',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Music', 'Electronics'])
			},
			{
				'attributes': {
					'name': 'keyboard charger',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'keyboard stand',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'keyboard pedal',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'keyboard chair',
					'location': self.get_location('standing desk surrounding area'),
				},
				'categories': self.get_categories(['Utility', 'Furniture'])
			},
			{
				'attributes': {
					'name': 'cutting knife',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'eating fork',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'meat fork',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'spoon',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'bowl',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'small plate',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'large plate',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'frying pan',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'wok top',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'large metal bowl',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'salad bowl',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'small metal bowl',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'large measuring cup',
					'location': self.get_location('refrigerator top'),
				},
				'categories': self.get_categories(['Utility', 'COokware'])
			},
			{
				'attributes': {
					'name': 'lenovo x61 laptop',
					'location': self.get_location('dresser desk top'),
				},
				'categories': self.get_categories(['Electronics'])
			},
			{
				'attributes': {
					'name': 'lenovo x61 laptop charger',
					'location': self.get_location('dresser desk top'),
				},
				'categories': self.get_categories(['Electronics', 'Accessory'])
			},
			{
				'attributes': {
					'name': 'computer speakers',
					'location': self.get_location('dresser desk top'),
				},
				'categories': self.get_categories(['Electronics', 'Music'])
			},
			{
				'attributes': {
					'name': 'audio- technica headphones',
					'location': self.get_location('dresser cubby'),
				},
				'categories': self.get_categories(['Electronics', 'Music'])
			},
		]

	def get_categories(self, categories):
		return Category.objects.filter(name__in=categories).all()

	def get_container(self, name):
		return ContainerPossession.objects.filter(name=name).first()

	def get_location(self, name):
		return Location.objects.filter(name=name).first()

	def create_categories(self):
		for category in self.categories:
			Category.objects.get_or_create(name=category)

	def create_containers(self):
		for container in self.containers:
			ContainerPossession.objects.get_or_create(name=container)

	def create_locations(self):
		for location in self.locations:
			Location.objects.get_or_create(name=location)

	def create_possessions(self):
		for possession in self.possessions:
			p = Possession.objects.get_or_create(**possession['attributes'])[0]
			p.categories = possession['categories']	

	def __init__(self):
		self.purge()
		self.create_categories()
		self.create_containers()
		self.create_locations()
		self.set_possessions()
		self.create_possessions()

	def purge(self):
		Category.purge()
		Label.purge()
		Attribute.purge()
		ContainerPossession.purge()
		Location.purge()
		Possession.purge()		

IPO = InventoryPopulation()
