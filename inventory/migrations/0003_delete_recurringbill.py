# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0002_auto_20141227_1526'),
    ]

    operations = [
        migrations.DeleteModel(
            name='RecurringBill',
        ),
    ]
