# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0004_possession'),
    ]

    operations = [
        migrations.AddField(
            model_name='possession',
            name='condition',
            field=models.DecimalField(default=0.5, max_digits=3, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='possession',
            name='practical',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='possession',
            name='sentimental',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
