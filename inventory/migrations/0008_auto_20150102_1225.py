# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0007_auto_20150102_1218'),
    ]

    operations = [
        migrations.AddField(
            model_name='cleaning',
            name='complete',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cleaning',
            name='frequency',
            field=models.IntegerField(default=1, choices=[(1, b'Once'), (2, b'Daily'), (3, b'Weekly'), (4, b'Monthly'), (5, b'Yearly')]),
            preserve_default=True,
        ),
    ]
