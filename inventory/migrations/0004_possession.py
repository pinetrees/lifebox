# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0003_delete_recurringbill'),
    ]

    operations = [
        migrations.CreateModel(
            name='Possession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('value', models.DecimalField(default=0.0, max_digits=7, decimal_places=2)),
                ('quality', models.DecimalField(default=0.5, max_digits=3, decimal_places=2)),
                ('utility', models.DecimalField(default=0.5, max_digits=3, decimal_places=2)),
                ('expected_expiration', models.DateField(null=True, blank=True)),
                ('sentimental', models.BooleanField()),
                ('practical', models.BooleanField()),
                ('categories', models.ManyToManyField(to='inventory.Category')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
