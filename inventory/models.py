from core.models import PurgeableModel, CoreModel, KeyValueModel, models
from django.utils import timezone

# Create your models here.
class Category(CoreModel):
	#description = models.CharField(null=True, max_length=200)

	class Meta:
		verbose_name_plural = "categories"

class Label(CoreModel):
	
	def generate_possessions(self, count):
		for i in range(0, count):
			p = Possession.objects.create()
			self.possessions.add(p)

class Attribute(PurgeableModel, KeyValueModel):
	pass

class FuturePurchase(CoreModel):
	categories = models.ManyToManyField(Category)
	budget = models.DecimalField(max_digits=5, decimal_places=2)

class Purchase(CoreModel):
	cost = models.DecimalField(max_digits=6, decimal_places=2)
	purchased_at = models.DateTimeField(default=timezone.now)
	reference = models.CharField(max_length=200, null=True, blank=True)

class Possession(CoreModel):
	categories = models.ManyToManyField(Category, related_name="possessions")
	labels = models.ManyToManyField(Label, related_name="possessions")
	attributes = models.ManyToManyField(Attribute)
	container = models.ForeignKey("ContainerPossession", null=True, related_name="contents")
	location = models.ForeignKey("Location", null=True, related_name="contents")
	value = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)
	quality = models.DecimalField(max_digits=3, decimal_places=2, default=0.50)
	condition = models.DecimalField(max_digits=3, decimal_places=2, default=0.50)
	utility = models.DecimalField(max_digits=3, decimal_places=2, default=0.50)
	expected_expiration = models.DateField(null=True, blank=True)
	sentimental = models.BooleanField(default=False)
	practical = models.BooleanField(default=False)

	@classmethod
	def find(self, param):
		return self.objects.filter(name__contains=param)

	#refactor this
	@classmethod
	def locate(self, param):
		possessions = self.find(param)
		return_set = []
		for possession in possessions:
		#	return_set.append({
		#		'possession': possession,
		#		'location': possession.location,
		#	})
			return_set.append([
				possession,
				possession.location,
			])
		return return_set

class ContainerPossession(Possession):
	pass

class Location(CoreModel):
	pass

class Room(CoreModel):
	pass

class Cleaning(CoreModel):
	FREQUENCIES = (
		(1, 'Once'),
		(2, 'Daily'),
		(3, 'Weekly'),
		(4, 'Monthly'),
		(5, 'Yearly')
	)
	room = models.ForeignKey(Room, null=True)
	frequency = models.IntegerField(choices=FREQUENCIES, default=1)
	complete = models.BooleanField(default=False)

