from django.contrib import admin
from .models import Category, FuturePurchase, Purchase, Possession, Room, Cleaning

class CleaningAdmin(admin.ModelAdmin):
	list_display = ["name", "room", "frequency", "complete"]

# Register your models here.
admin.site.register(Category)
admin.site.register(FuturePurchase)
admin.site.register(Purchase)
admin.site.register(Possession)
admin.site.register(Cleaning, CleaningAdmin)
admin.site.register(Room)
