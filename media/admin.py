from django.contrib import admin
from media.models import TelevisionShow, Season, Episode, Movie, AcademicCourse, Lecture, Author, Book, Artist, Album, Song
from django.core.urlresolvers import reverse

class SeasonInline(admin.TabularInline):
	model = Season

class TelevisionShowAdmin(admin.ModelAdmin):
	list_display = ["id", "name", "list_seasons", "active"]
	list_filter = ["active"]
	list_editable = ["active"]
	ordering = ['id']
	inlines = [SeasonInline]

	def list_seasons(self, obj):
		url = reverse('admin:media_season_changelist')
		return '<a href="{0}?tvshow__id__exact={1}">List seasons</a>'.format(url, obj.id)
	list_seasons.allow_tags = True
	list_seasons.short_description = 'Seasons'

class SeasonAdmin(admin.ModelAdmin):
	list_display = ["id", "__str__", "tvshow_link", "list_episodes"]
	ordering = ['id']

	def tvshow_link(self, obj):
		url = reverse('admin:media_televisionshow_changelist')
		return '<a href="{0}?id__exact={1}">{2}</a>'.format(url, obj.tvshow.id, obj.tvshow.name)
	tvshow_link.allow_tags = True
	tvshow_link.short_description = 'Television Show'
	def list_episodes(self, obj):
		url = reverse('admin:media_episode_changelist')
		return '<a href="{0}?season__id__exact={1}">List episodes</a>'.format(url, obj.id)
	list_episodes.allow_tags = True
	list_episodes.short_description = 'Episodes'

class EpisodeAdmin(admin.ModelAdmin):
	list_display = ["__str__", "season_link", "viewed"]
	list_filter = ["season"]
	list_editable = ["viewed"]
	ordering = ['id']

	def season_link(self, obj):
		url = reverse('admin:media_season_changelist')
		return '<a href="{0}?id__exact={1}">{2}</a>'.format(url, obj.season.id, obj.season)
	season_link.allow_tags = True
	season_link.short_description = 'Season'


class MovieAdmin(admin.ModelAdmin):
	list_display = ["name", "seen", "rating"]

class AcademicCourseAdmin(admin.ModelAdmin):
	list_display = ["__str__", "list_lectures"]
	ordering = ['id']

	def list_lectures(self, obj):
		url = reverse('admin:media_lecture_changelist')
		return '<a href="{0}?course__id__exact={1}">List lectures</a>'.format(url, obj.id)
	list_lectures.allow_tags = True
	list_lectures.short_description = 'Lectures'


class LectureAdmin(admin.ModelAdmin):
	list_display = ["__str__", "course_link", "viewed"]
	list_filter = ["course"]
	list_editable = ["viewed"]
	ordering = ['id']

	def course_link(self, obj):
		url = reverse('admin:media_academiccourse_changelist')
		return '<a href="{0}?id__exact={1}">{2}</a>'.format(url, obj.course.id, obj.course)
	course_link.allow_tags = True
	course_link.short_description = 'Course'


# Register your models here.
admin.site.register(TelevisionShow, TelevisionShowAdmin)
admin.site.register(Season, SeasonAdmin)
admin.site.register(Episode, EpisodeAdmin)

admin.site.register(Movie, MovieAdmin)

admin.site.register(AcademicCourse, AcademicCourseAdmin)
admin.site.register(Lecture, LectureAdmin)

admin.site.register(Author)
admin.site.register(Book)

admin.site.register(Artist)
admin.site.register(Album)
admin.site.register(Song)
