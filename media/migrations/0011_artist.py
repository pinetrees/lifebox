# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
        ('media', '0010_author_book'),
    ]

    operations = [
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('person_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Person')),
            ],
            options={
            },
            bases=('core.person',),
        ),
    ]
