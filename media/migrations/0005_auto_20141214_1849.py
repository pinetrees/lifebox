# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('academics', '0003_auto_20141214_1846'),
        ('media', '0004_academiccourse_lecture'),
    ]

    operations = [
        migrations.AddField(
            model_name='academiccourse',
            name='professor',
            field=models.ForeignKey(default=1, to='academics.Professor'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='academiccourse',
            name='university',
            field=models.ForeignKey(default=1, to='academics.University'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='academiccourse',
            name='year_taught',
            field=models.IntegerField(default=2014),
            preserve_default=True,
        ),
    ]
