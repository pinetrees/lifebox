# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('academics', '0003_auto_20141214_1846'),
        ('media', '0005_auto_20141214_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='academiccourse',
            name='term_taught',
            field=models.ForeignKey(default=1, to='academics.Term'),
            preserve_default=True,
        ),
    ]
