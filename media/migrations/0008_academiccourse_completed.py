# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0007_lecture_viewed'),
    ]

    operations = [
        migrations.AddField(
            model_name='academiccourse',
            name='completed',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
