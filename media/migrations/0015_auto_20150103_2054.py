# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0014_album_song'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='rating',
            field=models.DecimalField(default=0.5, max_digits=3, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='academiccourse',
            name='year_taught',
            field=models.IntegerField(default=2015),
            preserve_default=True,
        ),
    ]
