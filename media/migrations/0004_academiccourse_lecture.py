# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0003_televisionshow_season_count'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcademicCourse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('lecture_count', models.IntegerField(default=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lecture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lecture_number', models.IntegerField(default=1)),
                ('course', models.ForeignKey(to='media.AcademicCourse')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
