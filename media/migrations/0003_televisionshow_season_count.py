# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0002_auto_20141214_0442'),
    ]

    operations = [
        migrations.AddField(
            model_name='televisionshow',
            name='season_count',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
