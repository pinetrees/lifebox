# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0008_academiccourse_completed'),
    ]

    operations = [
        migrations.AddField(
            model_name='televisionshow',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
