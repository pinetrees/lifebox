# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0016_movie'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='rating',
            field=models.DecimalField(default=0.5, max_digits=3, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='movie',
            name='seen',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
