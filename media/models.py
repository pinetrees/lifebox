from core.models import models, CoreModel, Person
from datetime import datetime
from academics.models import University, Professor, Term

# Create your models here.
class TelevisionShow(CoreModel):
	season_count = models.IntegerField(default=1)
	active = models.BooleanField(default=False)

	def save_with_episodes(self, episodes_list):
		self.save()
		for index, item in enumerate(Season.objects.filter(tvshow=self)):  
			for j in range(0, episodes_list[index]):
				Episode(season=item, episode_number=j+1).save()

	@staticmethod
	#Change this to watch_episode_of
	def watch_episode(TelevisionShowName):
		show = TelevisionShow.objects.filter(name=TelevisionShowName).first()
		episode = Episode.objects.filter(season__tvshow=show).filter(viewed=False).first()
		episode.viewed = True
		episode.save()
		return episode

	@staticmethod
	def last_episode(TelevisionShowName):
		show = TelevisionShow.objects.filter(name=TelevisionShowName).first()
		return Episode.objects.filter(season__tvshow=show).filter(viewed=True).last()


class Season(models.Model):
	tvshow = models.ForeignKey(TelevisionShow)
	season_number = models.IntegerField(default=1)

	def __str__(self):
		return self.tvshow.name + " Season " + str(self.season_number)

class Episode(models.Model):
	season = models.ForeignKey(Season)
	episode_number = models.IntegerField(default=1)
	viewed = models.BooleanField(default=False)

	def __str__(self):
		return self.season.__str__() + " Episode " + str(self.episode_number)

class Movie(CoreModel):
	seen = models.BooleanField(default=False)
	rating = models.DecimalField(max_digits=3, decimal_places=2, default=0.5)

class AcademicCourse(models.Model):
	name = models.CharField(max_length=200)
	university = models.ForeignKey(University)
	professor = models.ForeignKey(Professor)
	year_taught = models.IntegerField(default=datetime.now().year)
	term_taught = models.ForeignKey(Term, default=1)
	lecture_count = models.IntegerField(default=1)
	completed = models.BooleanField(default=False)

	def __str__(self):
		return self.name

class Lecture(models.Model):
	course = models.ForeignKey(AcademicCourse)
	lecture_number = models.IntegerField(default=1)
	viewed = models.BooleanField(default=False)

	def __str__(self):
		return self.course.__str__() + " Lecture " + str(self.lecture_number)

class Author(models.Model):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)

	def __str__(self):
		return " ".join([self.first_name, self.last_name])

class Book(CoreModel):
	author = models.ForeignKey(Author)

class Artist(CoreModel):
	pass

class Album(CoreModel):
	artist = models.ForeignKey(Artist)

class Composer(Person):
	pass

class Song(CoreModel):
	album = models.ForeignKey(Album)
	rating = models.DecimalField(max_digits=3, decimal_places=2, default=0.5)

def create_tvshow_seasons(instance, created, raw, **kwargs):
	if not created or raw:
		return
	for i in range(0, instance.season_count):
		Season(tvshow=instance, season_number=i+1).save()
	return True

models.signals.post_save.connect(create_tvshow_seasons, sender=TelevisionShow, dispatch_uid="create_tvshow_seasons")

def create_course_lectures(instance, created, raw, **kwargs):
	for i in range(0, instance.lecture_count):
		Lecture.objects.update_or_create(course=instance, lecture_number=i+1)
	if instance.completed == True:
		Lecture.objects.filter(course=instance).update(viewed=True)
	return True

models.signals.post_save.connect(create_course_lectures, sender=AcademicCourse, dispatch_uid="create_course_lectures")
	
