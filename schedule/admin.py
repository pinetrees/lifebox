from django.contrib import admin
from schedule.models import DailySurvey

# Register your models here.
admin.site.register(DailySurvey)
