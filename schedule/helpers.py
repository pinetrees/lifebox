from datetime import date, time, datetime, timedelta
import pytz

WAKE = time(9, 0, 0)
TIMEZONE = pytz.timezone('US/Eastern')
TZ = TIMEZONE

def get_this_morning():
    today = date.today()
    wake = datetime.combine(today, WAKE)
    return TZ.localize(wake)

def get_interval():
    now = datetime.now(TZ)
    this_morning = get_this_morning()
    time_since_wake = now - this_morning
    minutes_since_wake = time_since_wake.seconds / 60.0
    interval = int(minutes_since_wake / 15)
    interval = interval % 64

    #print now
    #print WAKE
    #print this_morning
    #print time_since_wake
    #print minutes_since_wake
    #print interval

    return interval

def get_next_interval():
    return get_interval() + 1

def get_time_remaining_in_interval():
    this_morning = get_this_morning()
    last_interval_occurred_at = this_morning + timedelta(minutes=get_interval() * 15)
    next_interval_occurs_at = this_morning + timedelta(minutes=get_next_interval() * 15)
    now = datetime.now(TZ)
    time_since = now - last_interval_occurred_at
    time_remaining = next_interval_occurs_at - now
    return time_remaining

def print_time():
    print datetime.now(TZ).strftime("%H:%M")
