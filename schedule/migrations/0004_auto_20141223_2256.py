# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20141223_2251'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='back_to_bed',
            field=models.BooleanField(default=False, verbose_name=b'Did you go back to sleep?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='evening_caffeine',
            field=models.BooleanField(default=False, verbose_name=b'Did you have enough caffeine?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='morning_caffeine',
            field=models.BooleanField(default=False, verbose_name=b'Did you have caffeine early enough?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='television',
            field=models.BooleanField(default=False, verbose_name=b'Did you watch television?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='tired',
            field=models.BooleanField(default=False, verbose_name=b'Were you tired?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='wake',
            field=models.BooleanField(default=False, verbose_name=b'Did you wake up early enough?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='breakfast',
            field=models.BooleanField(default=False, verbose_name=b'Did you eat breakfast?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='dinner',
            field=models.BooleanField(default=False, verbose_name=b'Did you eat dinner?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='good',
            field=models.BooleanField(default=False, verbose_name=b'Did you feel good?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='happy',
            field=models.BooleanField(default=False, verbose_name=b'Were you happy?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='lunch',
            field=models.BooleanField(default=False, verbose_name=b'Did you eat lunch?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='nap',
            field=models.BooleanField(default=False, verbose_name=b'Did you take a nap?'),
            preserve_default=True,
        ),
    ]
