# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0006_auto_20141223_2306'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dailysurvey',
            name='back_to_bed',
        ),
        migrations.RemoveField(
            model_name='dailysurvey',
            name='nap_trouble',
        ),
        migrations.RemoveField(
            model_name='dailysurvey',
            name='tired',
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='energy',
            field=models.BooleanField(default=False, verbose_name=b'Did you have energy when you woke up?'),
            preserve_default=True,
        ),
    ]
