# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0010_dailysurvey_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='peak_happiness',
            field=models.DecimalField(decimal_places=2, default=0.5, max_digits=3, blank=True, null=True, verbose_name=b'What was your peak happiness?'),
            preserve_default=True,
        ),
    ]
