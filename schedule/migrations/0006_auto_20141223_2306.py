# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0005_auto_20141223_2300'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='clean',
            field=models.BooleanField(default=False, verbose_name=b'Did you clean?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='exercise',
            field=models.BooleanField(default=False, verbose_name=b'Did you exercise?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='french',
            field=models.BooleanField(default=False, verbose_name=b'Did you practice french?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='lumosity',
            field=models.BooleanField(default=False, verbose_name=b'Did you play lumosity?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='piano',
            field=models.BooleanField(default=False, verbose_name=b'Did you practice piano?'),
            preserve_default=True,
        ),
    ]
