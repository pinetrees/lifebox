# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='good',
            field=models.BooleanField(default=0, verbose_name=b'Did you feel good today today?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='happy',
            field=models.BooleanField(default=0, verbose_name=b'Were you happy today?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='nap',
            field=models.BooleanField(default=0, verbose_name=b'Did you take a nap today?'),
            preserve_default=True,
        ),
    ]
