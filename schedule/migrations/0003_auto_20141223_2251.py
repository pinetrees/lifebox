# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0002_auto_20141223_2247'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='breakfast',
            field=models.BooleanField(default=False, verbose_name=b'Did you eat breakfast today?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='dinner',
            field=models.BooleanField(default=False, verbose_name=b'Did you eat dinner today?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysurvey',
            name='lunch',
            field=models.BooleanField(default=False, verbose_name=b'Did you eat lunch today?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='good',
            field=models.BooleanField(default=False, verbose_name=b'Did you feel good today?'),
            preserve_default=True,
        ),
    ]
