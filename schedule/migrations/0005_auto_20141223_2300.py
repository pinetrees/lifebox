# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0004_auto_20141223_2256'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='nap_trouble',
            field=models.BooleanField(default=False, verbose_name=b'Did you have trouble falling asleep for a nap?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailysurvey',
            name='nap',
            field=models.BooleanField(default=False, verbose_name=b'Did you take a nap on time?'),
            preserve_default=True,
        ),
    ]
