# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0007_auto_20141223_2308'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dailysurvey',
            old_name='clean',
            new_name='cleaned',
        ),
    ]
