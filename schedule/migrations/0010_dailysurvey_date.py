# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0009_dailysurvey_calm'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='date',
            field=models.DateField(default=datetime.date.today),
            preserve_default=True,
        ),
    ]
