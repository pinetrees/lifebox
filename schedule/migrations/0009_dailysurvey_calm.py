# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0008_auto_20141223_2314'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailysurvey',
            name='calm',
            field=models.BooleanField(default=False, verbose_name=b'Did you remain calm?'),
            preserve_default=True,
        ),
    ]
