from django.db import models
from .helpers import *
import datetime
from dateutil.relativedelta import relativedelta
from core.models import PurgeableModel
from psychology.models import LinearMood, NoteCard
from tasks.models import *
from projects.models import *
from prettytable import PrettyTable

# Create your models here.
class DailySurvey(models.Model):
	date = models.DateField(default=datetime.date.today)
	wake = models.BooleanField(verbose_name="Did you wake up early enough?", default=False)
	energy = models.BooleanField(verbose_name="Did you have energy when you woke up?", default=False)
	morning_caffeine = models.BooleanField(verbose_name="Did you have caffeine early enough?", default=False)
	lumosity = models.BooleanField(verbose_name="Did you play lumosity?", default=False)
	french = models.BooleanField(verbose_name="Did you practice french?", default=False)
	nap = models.BooleanField(verbose_name="Did you take a nap on time?", default=False)
	happy = models.BooleanField(verbose_name="Were you happy?", default=False)
	good = models.BooleanField(verbose_name="Did you feel good?", default=False)
	breakfast = models.BooleanField(verbose_name="Did you eat breakfast?", default=False)
	lunch = models.BooleanField(verbose_name="Did you eat lunch?", default=False)
	dinner = models.BooleanField(verbose_name="Did you eat dinner?", default=False)
	cleaned = models.BooleanField(verbose_name="Did you clean?", default=False)
	exercise = models.BooleanField(verbose_name="Did you exercise?", default=False)
	television = models.BooleanField(verbose_name="Did you watch television?", default=False)
	piano = models.BooleanField(verbose_name="Did you practice piano?", default=False)
	evening_caffeine = models.BooleanField(verbose_name="Did you have enough caffeine?", default=False)
	calm = models.BooleanField(verbose_name="Did you remain calm?", default=False)

	peak_happiness = models.DecimalField(max_digits=3, decimal_places=2, verbose_name="What was your peak happiness?", null=True, blank=True, default=0.5)
	peak_energy = models.DecimalField(max_digits=3, decimal_places=2, verbose_name="What was your peak energy?", null=True, blank=True, default=0.5)
	pass

	def __str__(self):
		return str(self.date)

class AtomManager(models.Manager):
    def create(self, *args, **kwargs):
        kwargs = self._update_kwargs(**kwargs)
        atom = super(AtomManager, self).create(*args, **kwargs)
        atom = self._set_index(atom)

        return atom

    def get_or_create(self, *args, **kwargs):
        kwargs = self._update_kwargs(**kwargs)
        atom, created = super(AtomManager, self).get_or_create(*args, **kwargs)
        atom = self._set_index(atom)
        return atom, created

    def _update_kwargs(self, **kwargs):
        absolute_index = kwargs.get('absolute_index', None)
        if absolute_index is not None:
            date, interval = self._get_date_and_interval(absolute_index)
            kwargs['date'] = date
            kwargs['interval'] = interval
            
        return kwargs

    def _set_index(self, atom):
        if atom.absolute_index is None:
            atom.absolute_index = atom.get_absolute_index()
            atom.save()
        return atom


    def _get_date_and_interval(self, absolute_index):
        first_atom = self.first()
        date = first_atom.date
        interval = first_atom.interval
        for i in range(absolute_index - 1):
            interval += 1
            if interval == 64:
                date += timedelta(days=1)
                interval = 0

        return [date, interval]

    def LAI(self):
        return self.order_by('absolute_index').last()

    def delete_by_index(self, absolute_index):
        return self.filter(absolute_index=absolute_index).delete()

    def change(self, absolute_index, message):
        self.filter(absolute_index=absolute_index).update(message=message)

    def move(self, absolute_index, new_index):
        atom = self.filter(absolute_index=absolute_index).first()
        atom.update(absolute_index=new_index)
        atom.update_date_and_interval_from_index()

class Atom(PurgeableModel):
    date  = models.DateField(default=datetime.date.today)
    interval = models.IntegerField(default=get_interval)
    message = models.TextField(blank=True, default='')
    mood = models.ForeignKey(LinearMood, null=True)
    notes = models.ManyToManyField(NoteCard)
    absolute_index = models.IntegerField(null=True)
    tasks = models.ManyToManyField(Task, related_name="atoms")
    activity = models.ForeignKey(Activity, null=True, related_name="atoms")
    project = models.ForeignKey(Project, null=True, related_name="atoms")

    objects = AtomManager()

    def __str__(self):
        return self.message

    def get_name(self):
        return 'A' + self.absolute_index.__str__()

    name = property(get_name)

    @classmethod
    def write(self, message=None, mood=None):
        date = datetime.date.today()
        interval = get_interval()
        log, created = self.objects.get_or_create(date=date, interval=interval)
        if message is not None:
            log.message = message
        if mood is not None:
            linear_mood = LinearMood(mood=mood)
            linear_mood.save()
            log.mood = linear_mood
        log.save()

    @classmethod
    def fetch(self):
        date = datetime.date.today()
        interval = get_interval()
        log, created = self.objects.get_or_create(date=date, interval=interval)
        return log

    @classmethod
    def next_index(self, absolute_index=None):
        if absolute_index is None:
            absolute_index = self._get_absolute_index()
        while self.objects.filter(absolute_index=absolute_index).first() is not None:
            absolute_index += 1
        return absolute_index

    @classmethod
    def next_open(self, message, iterations=1, absolute_index=None):
        for i in range(iterations):
            next_index = self.next_index(absolute_index)
            obj = self.objects.create(absolute_index=next_index, message=message)
        return obj

    schedule = next_open

    @classmethod
    def read(self):
        return self.fetch().message

    def note(self, subject, notes=''):
        note = NoteCard.objects.create(subject=subject, notes=notes)
        self.notes.add(note)

    @classmethod
    def last(self, count=1):
        intervals = self.objects.order_by('-interval').all()[:count]
        return intervals

    @classmethod
    def death(self):
        return datetime.datetime(1989, 9, 3) + relativedelta(years=85)

    @classmethod
    def remaining(self):
        time_remaining = self.death() - datetime.datetime.now()
        minutes = time_remaining.days * 24 * 60 + time_remaining.seconds / 60.0
        atoms = minutes / 15.0
        return atoms

    @classmethod
    def clock(self):
        time_remaining = get_time_remaining_in_interval()
        print time_remaining - datetime.timedelta(microseconds=time_remaining.microseconds)

    def get_absolute_index(self):
        absolute_index = 0
        first_atom = Atom.objects.first()
        days_passed = (self.date - first_atom.date).days
        if days_passed > 1:
            absolute_index += 64 * (days_passed - 1)
        if days_passed > 0:
            absolute_index += 64 - first_atom.interval
            absolute_index += self.interval
        else:
            absolute_index += self.interval - first_atom.interval

        absolute_index += 1

        return absolute_index

    @classmethod
    #Please please please refactor
    def _get_absolute_index(self):
        absolute_index = 0
        first_atom = Atom.objects.first()
        dates_passed = (datetime.date.today() - first_atom.date).days
        days_passed = (datetime.datetime.now() - first_atom.time()).days
        #if days_passed > 1:
        if dates_passed > 1:
            #absolute_index += 64 * (days_passed - 1)
            absolute_index += 64 * (dates_passed - 1)
        if dates_passed > 0:
            absolute_index += 64 - first_atom.interval
            absolute_index += get_interval()
        else:
            absolute_index += get_interval() - first_atom.interval
        
        absolute_index += 1

        return absolute_index

    @classmethod
    #This has a bug - we take from the last created atom, which may not have been created for some time. We need the current atomic index
    def points_possible(self):
        return self._get_absolute_index()
        #Calculate the number of full days which have passed, and multiply this number times 64
        #Add in the number of intervals in the first day, and the number of intervals in the last day
        #Consider the case (today) when these days are the same
        #total_possible_atoms = 0
        #first_atom = self.objects.first()
        #most_recent_atom = self.objects.last()
        #days_passed = (most_recent_atom.date - first_atom.date).days
        #if days_passed > 1:
        #    pass
        #    total_possible_atoms += 64 * (days_passed - 1)
        #if days_passed > 0:
        #    total_possible_atoms += 64 - first_atom.interval
        #    total_possible_atoms += most_recent_atom.interval
        #else:
        #    total_possible_atoms += most_recent_atom.interval - first_atom.interval

        ##A global offset, consider starting (as I have) on interval 32, and being presently on interval 42. This applies throughout (I hope!)
        #total_possible_atoms += 1

        #return total_possible_atoms

    @classmethod
    def points_earned(self):
        absolute_index = self._get_absolute_index()
        return self.objects.filter(absolute_index__lte=absolute_index).count()

    @classmethod
    def score(self):
        return float(self.points_earned()) / float(self.points_possible())

    def time(self):
        relative_wake = datetime.datetime.combine(self.date, WAKE)
        return relative_wake + datetime.timedelta(minutes = self.interval*15)

    @classmethod
    def list(self, absolute=True, today=False, time=False):
        atoms = self.objects.all()
        if today:
            atoms = atoms.filter(date=date.today())
        atoms = atoms.order_by('absolute_index')
        for atom in atoms:
            if today:
                if time:
                    prefix = atom.time().strftime('%H:%M')
                else:
                    prefix = atom.interval.__str__()
            elif time:
                prefix = atom.time().__str__()
            elif absolute:
                prefix = 'a' + atom.absolute_index.__str__()
            else:
                prefix = atom.date.__str__() + ":" + atom.interval.__str__()
            if atom.interval == get_interval():
                prefix = '>' + prefix
            print prefix + " : " + atom.__str__()

    @classmethod
    def list_as_matrix(self):
        atoms = self.objects.order_by('absolute_index').all()
        index = self.index()
        x = PrettyTable(["Date", "Time", "Index", "Message", "Project", "Activity", "Mood"])
        for atom in atoms:
            text = atom.message[:32]
            if atom.absolute_index == index:
                text = '>>>' + text
            data = [atom.date.__str__(), atom.time().strftime('%H:%M'), atom.absolute_index.__str__(), text, atom.project.__str__(), atom.activity.__str__(), atom.mood.__str__()]
            x.add_row(data)
            line = "\t".join(data)
            #if atom.absolute_index == index:
            #    print len(line) * '='
            #print line
            #if atom.absolute_index == index:
            #    print len(line) * '='
        print x


    @classmethod
    def today(self, time=False):
        return self.list(today=True, time=time)

    @classmethod
    def index(self):
        return self.points_possible()

    @classmethod
    def next(self):
        return self.index() + 1

    @classmethod
    def shift(self, absolute_index, shift_length=1):
        for i in range(shift_length):
            next_index = self.next_index(absolute_index)
            queryset = Atom.objects.filter(absolute_index__gte=absolute_index, absolute_index__lt=next_index)
            #Fairly good algorithm! Unfortunately, I need to patch update all of the date and interval after this op
            #queryset.update(absolute_index=models.expressions.F('absolute_index')+1)
            atoms = queryset.all()
            for atom in atoms:
                atom.absolute_index += 1
                atom.update_date_and_interval_from_index()

    @classmethod
    def inject(self, message, absolute_index=None):
        if absolute_index is None:
            absolute_index = self.index()
        self.shift(absolute_index=absolute_index)
        self.objects.create(absolute_index=absolute_index, message=message)

    @classmethod
    def eject(self, absolute_index=None, _range=1):
        #Inverse of inject, requires documentation for push and shift in order to differentiate the two
        if absolute_index is None:
            print 'You have to provide me with an absolute index to start from'
            return False
        self.objects.delete_by_index(absolute_index)
        self._pull_range(absolute_index, absolute_index + _range)

    def update_date_and_interval_from_index(self):
        date, interval = Atom.objects._get_date_and_interval(self.absolute_index)
        self.date = date
        self.interval = interval
        self.save()

    @classmethod
    def swap(self, a1, a2):
        atom_one = self.objects.filter(absolute_index=a1).first()
        atom_two = self.objects.filter(absolute_index=a2).first()

        atom_one.absolute_index = a2
        atom_one.update_date_and_interval_from_index()

        atom_two.absolute_index = a1
        atom_two.update_date_and_interval_from_index()
        self.list()

    @classmethod
    def push(self, a1, intervals):
        this_atom = self.objects.filter(absolute_index=a1).first()
        atoms = self.objects.filter(absolute_index__gt=a1, absolute_index__lte=a1+intervals).all()
        for atom in atoms:
            atom.absolute_index -= 1
            atom.update_date_and_interval_from_index()
        this_atom.absolute_index += intervals
        this_atom.update_date_and_interval_from_index()
        
    @classmethod
    def pull(self, a1, intervals):
        this_atom = self.objects.filter(absolute_index=a1).first()
        atoms = self.objects.filter(absolute_index__lt=a1, absolute_index__gte=a1-intervals).all()
        for atom in atoms:
            atom.absolute_index += 1
            atom.update_date_and_interval_from_index()
        this_atom.absolute_index -= intervals
        this_atom.update_date_and_interval_from_index()
        
    @classmethod
    #This method pays no regard to objects in the way of the atom, in contrast to push, which pulls all atoms back to make room - push could be changed to cycle
    def _push(self, a1, intervals=1):
        atom = self.objects.filter(absolute_index=a1).first()
        atom.absolute_index += intervals
        atom.update_date_and_interval_from_index()

    @classmethod
    def _pull(self, a1, intervals=1):
        atom = self.objects.filter(absolute_index=a1).first()
        atom.absolute_index -= intervals
        atom.update_date_and_interval_from_index()

    @classmethod
    def _pull_range(self, a1, aN, intervals=1):
        atoms = self.objects.filter(absolute_index__gte=a1, absolute_index__lte=aN).all()
        for atom in atoms:
            atom.absolute_index -= intervals
            atom.update_date_and_interval_from_index()

    @classmethod
    def copy(self, a1, a2):
        atom = self.objects.filter(absolute_index=a1).first()
        return self.objects.get_or_create(absolute_index=a2, message=atom.message)
