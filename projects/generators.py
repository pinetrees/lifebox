from .models import *

PROJECTS = ['Learning Management System', 'Courtier', 'Clockwork', 'Job hunting', 'Lumosity', 'Duolingo', 'Coursera']
for project in PROJECTS:
    Project.objects.get_or_create(name=project)
