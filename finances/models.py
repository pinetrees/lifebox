from core.models import CoreModel, models

# Create your models here.
class RecurringBill(CoreModel):
	amount = models.DecimalField(max_digits=6, decimal_places=2)
	periods_per_year = models.IntegerField(default=12)

class BudgetAllocation(CoreModel):
	amount = models.DecimalField(max_digits=6, decimal_places=2)
	periods_per_year = models.IntegerField(default=12)


