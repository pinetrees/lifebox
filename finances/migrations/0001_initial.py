# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RecurringBill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('amount', models.DecimalField(max_digits=6, decimal_places=2)),
                ('periods_per_year', models.IntegerField(default=12)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
