from django.contrib import admin
from finances.models import RecurringBill, BudgetAllocation

class RecurringBillAdmin(admin.ModelAdmin):
	list_display = ["name", "amount"]

class BudgetAllocationAdmin(admin.ModelAdmin):
	list_display = ["name", "amount", "periods_per_year"]

# Register your models here.
admin.site.register(RecurringBill, RecurringBillAdmin)
admin.site.register(BudgetAllocation, BudgetAllocationAdmin)
