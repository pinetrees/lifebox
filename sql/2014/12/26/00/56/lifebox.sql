-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: lifebox
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `academics_professor`
--

DROP TABLE IF EXISTS `academics_professor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academics_professor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `current_university_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `academics_professor_a8ebd7b9` (`current_university_id`),
  CONSTRAINT `D12faad85e3c4b33773b3d0d2e362223` FOREIGN KEY (`current_university_id`) REFERENCES `academics_university` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academics_professor`
--

LOCK TABLES `academics_professor` WRITE;
/*!40000 ALTER TABLE `academics_professor` DISABLE KEYS */;
INSERT INTO `academics_professor` VALUES (1,'Ken','Train',1),(2,'Eric','Grimson',2),(3,'Dan','Grossman',3),(4,'Martin','Haugh',4),(5,'Ben','Polak',5);
/*!40000 ALTER TABLE `academics_professor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `academics_term`
--

DROP TABLE IF EXISTS `academics_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academics_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academics_term`
--

LOCK TABLES `academics_term` WRITE;
/*!40000 ALTER TABLE `academics_term` DISABLE KEYS */;
INSERT INTO `academics_term` VALUES (1,'Spring'),(2,'Fall');
/*!40000 ALTER TABLE `academics_term` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `academics_university`
--

DROP TABLE IF EXISTS `academics_university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academics_university` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academics_university`
--

LOCK TABLES `academics_university` WRITE;
/*!40000 ALTER TABLE `academics_university` DISABLE KEYS */;
INSERT INTO `academics_university` VALUES (1,'University of California, Berkeley'),(2,'Massachusetts Institute of Technology'),(3,'University of Washington'),(4,'Columbia University'),(5,'Yale University');
/*!40000 ALTER TABLE `academics_university` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add television show',7,'add_televisionshow'),(20,'Can change television show',7,'change_televisionshow'),(21,'Can delete television show',7,'delete_televisionshow'),(22,'Can add season',8,'add_season'),(23,'Can change season',8,'change_season'),(24,'Can delete season',8,'delete_season'),(25,'Can add episode',9,'add_episode'),(26,'Can change episode',9,'change_episode'),(27,'Can delete episode',9,'delete_episode'),(28,'Can add academic course',10,'add_academiccourse'),(29,'Can change academic course',10,'change_academiccourse'),(30,'Can delete academic course',10,'delete_academiccourse'),(31,'Can add lecture',11,'add_lecture'),(32,'Can change lecture',11,'change_lecture'),(33,'Can delete lecture',11,'delete_lecture'),(34,'Can add professor',12,'add_professor'),(35,'Can change professor',12,'change_professor'),(36,'Can delete professor',12,'delete_professor'),(37,'Can add university',13,'add_university'),(38,'Can change university',13,'change_university'),(39,'Can delete university',13,'delete_university'),(40,'Can add term',14,'add_term'),(41,'Can change term',14,'change_term'),(42,'Can delete term',14,'delete_term'),(43,'Can add workout',15,'add_workout'),(44,'Can change workout',15,'change_workout'),(45,'Can delete workout',15,'delete_workout'),(46,'Can add exercise',16,'add_exercise'),(47,'Can change exercise',16,'change_exercise'),(48,'Can delete exercise',16,'delete_exercise'),(49,'Can add exercise set',17,'add_exerciseset'),(50,'Can change exercise set',17,'change_exerciseset'),(51,'Can delete exercise set',17,'delete_exerciseset'),(55,'Can add walk',19,'add_walk'),(56,'Can change walk',19,'change_walk'),(57,'Can delete walk',19,'delete_walk'),(58,'Can add food',20,'add_food'),(59,'Can change food',20,'change_food'),(60,'Can delete food',20,'delete_food'),(61,'Can add sleep',21,'add_sleep'),(62,'Can change sleep',21,'change_sleep'),(63,'Can delete sleep',21,'delete_sleep'),(64,'Can add cigarette',22,'add_cigarette'),(65,'Can change cigarette',22,'change_cigarette'),(66,'Can delete cigarette',22,'delete_cigarette'),(67,'Can add joint',23,'add_joint'),(68,'Can change joint',23,'change_joint'),(69,'Can delete joint',23,'delete_joint'),(70,'Can add drink',24,'add_drink'),(71,'Can change drink',24,'change_drink'),(72,'Can delete drink',24,'delete_drink'),(73,'Can add daily survey',25,'add_dailysurvey'),(74,'Can change daily survey',25,'change_dailysurvey'),(75,'Can delete daily survey',25,'delete_dailysurvey'),(76,'Can add unit',26,'add_unit'),(77,'Can change unit',26,'change_unit'),(78,'Can delete unit',26,'delete_unit'),(79,'Can add conversion',27,'add_conversion'),(80,'Can change conversion',27,'change_conversion'),(81,'Can delete conversion',27,'delete_conversion'),(82,'Can add chemical',28,'add_chemical'),(83,'Can change chemical',28,'change_chemical'),(84,'Can delete chemical',28,'delete_chemical'),(85,'Can add content',29,'add_content'),(86,'Can change content',29,'change_content'),(87,'Can delete content',29,'delete_content'),(88,'Can add caffeine',30,'add_caffeine'),(89,'Can change caffeine',30,'change_caffeine'),(90,'Can delete caffeine',30,'delete_caffeine');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$TXM8LOzcinf5$Gp21llf4N1cDEI1xzZqIPMbEPyKiIGOmozNxEesLcas=','2014-12-23 23:55:28',1,'admin','','','admin@example.com',1,1,'2014-12-14 05:15:17'),(3,'pbkdf2_sha256$12000$3mfOb3EdKWfu$wiqjFF0wSD0Bp9x7PHTJ+A22AIekwH01DNY4ZTODZwk=','2014-12-25 22:47:47',1,'joshua','','','',1,1,'2014-12-21 02:02:45'),(4,'pbkdf2_sha256$12000$xS5d8kyawiza$9A5oDvbivkG6M+xVvdyOgauA6gPnTsXWd2R7Xdhfphw=','2014-12-21 02:03:09',1,'ryan','','','',1,1,'2014-12-21 02:03:09');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-12-14 05:58:08','1','Psycho-Pass Season 1 Episode 1',2,'Changed viewed.',9,1),(2,'2014-12-14 05:58:08','2','Psycho-Pass Season 1 Episode 2',2,'Changed viewed.',9,1),(3,'2014-12-14 05:58:08','3','Psycho-Pass Season 1 Episode 3',2,'Changed viewed.',9,1),(4,'2014-12-14 05:58:08','4','Psycho-Pass Season 1 Episode 4',2,'Changed viewed.',9,1),(5,'2014-12-14 05:58:08','5','Psycho-Pass Season 1 Episode 5',2,'Changed viewed.',9,1),(6,'2014-12-14 05:58:08','6','Psycho-Pass Season 1 Episode 6',2,'Changed viewed.',9,1),(7,'2014-12-14 05:58:08','7','Psycho-Pass Season 1 Episode 7',2,'Changed viewed.',9,1),(8,'2014-12-14 05:58:08','8','Psycho-Pass Season 1 Episode 8',2,'Changed viewed.',9,1),(9,'2014-12-14 05:58:09','9','Psycho-Pass Season 1 Episode 9',2,'Changed viewed.',9,1),(10,'2014-12-14 06:11:07','92','Mad Men Season 1 Episode 1',2,'Changed viewed.',9,1),(11,'2014-12-14 06:11:07','93','Mad Men Season 1 Episode 2',2,'Changed viewed.',9,1),(12,'2014-12-14 06:11:07','94','Mad Men Season 1 Episode 3',2,'Changed viewed.',9,1),(13,'2014-12-14 06:11:07','95','Mad Men Season 1 Episode 4',2,'Changed viewed.',9,1),(14,'2014-12-14 06:11:08','96','Mad Men Season 1 Episode 5',2,'Changed viewed.',9,1),(15,'2014-12-14 06:11:08','97','Mad Men Season 1 Episode 6',2,'Changed viewed.',9,1),(16,'2014-12-14 06:11:08','98','Mad Men Season 1 Episode 7',2,'Changed viewed.',9,1),(17,'2014-12-14 06:11:08','99','Mad Men Season 1 Episode 8',2,'Changed viewed.',9,1),(18,'2014-12-14 06:11:08','100','Mad Men Season 1 Episode 9',2,'Changed viewed.',9,1),(19,'2014-12-14 06:11:08','101','Mad Men Season 1 Episode 10',2,'Changed viewed.',9,1),(20,'2014-12-14 06:11:08','102','Mad Men Season 1 Episode 11',2,'Changed viewed.',9,1),(21,'2014-12-14 06:11:08','103','Mad Men Season 1 Episode 12',2,'Changed viewed.',9,1),(22,'2014-12-14 06:11:08','104','Mad Men Season 1 Episode 13',2,'Changed viewed.',9,1),(23,'2014-12-14 18:46:46','1','University object',1,'',13,1),(24,'2014-12-14 18:47:15','1','Professor object',1,'',12,1),(25,'2014-12-14 18:48:12','1','Term object',1,'',14,1),(26,'2014-12-14 18:48:15','2','Term object',1,'',14,1),(27,'2014-12-14 18:50:01','1','AcademicCourse object',1,'',10,1),(28,'2014-12-14 18:51:43','1','Introduction to Economics',2,'Changed term_taught.',10,1),(29,'2014-12-14 18:52:34','1','Introduction to Economics',2,'Changed lecture_count.',10,1),(30,'2014-12-14 19:03:56','1','Introduction to Economics',3,'',10,1),(31,'2014-12-14 19:04:11','2','Introduction to Economics',1,'',10,1),(32,'2014-12-14 19:04:40','3','Introduction to Economics Lecture 1',2,'Changed viewed.',11,1),(33,'2014-12-14 19:04:40','4','Introduction to Economics Lecture 2',2,'Changed viewed.',11,1),(34,'2014-12-14 19:04:40','5','Introduction to Economics Lecture 3',2,'Changed viewed.',11,1),(35,'2014-12-14 19:06:53','2','Massachusetts Institute of Technology',1,'',13,1),(36,'2014-12-14 19:07:03','2','Eric Grimson',1,'',12,1),(37,'2014-12-14 19:07:30','3','Introduction to Computer Science and Programming',1,'',10,1),(38,'2014-12-14 19:08:48','3','Introduction to Computer Science and Programming',2,'Changed lecture_count.',10,1),(39,'2014-12-14 19:09:05','3','Introduction to Computer Science and Programming',2,'Changed lecture_count.',10,1),(40,'2014-12-14 19:09:11','51','Introduction to Computer Science and Programming Lecture 25',2,'Changed viewed.',11,1),(41,'2014-12-14 19:10:41','3','Introduction to Computer Science and Programming',2,'Changed completed.',10,1),(42,'2014-12-14 19:12:00','3','University of Washington',1,'',13,1),(43,'2014-12-14 19:12:09','3','Dan Grossman',1,'',12,1),(44,'2014-12-14 19:16:53','4','Programming Languages',1,'',10,1),(45,'2014-12-14 19:17:44','4','Columbia University',1,'',13,1),(46,'2014-12-14 19:17:58','4','Martin Haugh',1,'',12,1),(47,'2014-12-14 19:18:57','5','Financial Engineering and Risk Management Part 1',1,'',10,1),(48,'2014-12-14 19:19:33','5','Yale University',1,'',13,1),(49,'2014-12-14 19:19:58','5','Ben Polak',1,'',12,1),(50,'2014-12-14 19:20:27','6','Game Theory',1,'',10,1),(51,'2014-12-14 20:23:09','6','Introduction to Economics Lecture 4',2,'Changed viewed.',11,1),(52,'2014-12-14 23:52:53','312','Glee Season 3 Episode 13',2,'Changed viewed.',9,1),(53,'2014-12-15 02:06:46','1','Exercise object',1,'',16,1),(54,'2014-12-15 02:06:57','2','Exercise object',1,'',16,1),(55,'2014-12-15 02:07:04','3','Exercise object',1,'',16,1),(56,'2014-12-15 02:07:54','1','Workout object',1,'',15,1),(57,'2014-12-15 02:12:31','1','ExerciseSet object',1,'',17,1),(58,'2014-12-15 02:12:40','2','ExerciseSet object',1,'',17,1),(59,'2014-12-15 02:12:47','3','ExerciseSet object',1,'',17,1),(60,'2014-12-15 02:12:54','4','ExerciseSet object',1,'',17,1),(61,'2014-12-15 02:13:03','5','ExerciseSet object',1,'',17,1),(62,'2014-12-15 02:13:08','6','ExerciseSet object',1,'',17,1),(63,'2014-12-15 02:13:15','7','ExerciseSet object',1,'',17,1),(64,'2014-12-15 02:13:23','8','ExerciseSet object',1,'',17,1),(65,'2014-12-15 02:13:31','9','ExerciseSet object',1,'',17,1),(66,'2014-12-15 02:13:37','10','ExerciseSet object',1,'',17,1),(67,'2014-12-15 02:13:43','11','ExerciseSet object',1,'',17,1),(68,'2014-12-15 02:13:49','12','ExerciseSet object',1,'',17,1),(69,'2014-12-15 15:59:16','1','Walk object',1,'',19,1),(70,'2014-12-15 16:14:35','1','Walk object',2,'Changed start_time and end_time.',19,1),(71,'2014-12-15 22:05:17','2','Walk object',1,'',19,1),(72,'2014-12-15 22:05:17','3','Walk object',1,'',19,1),(73,'2014-12-15 22:05:32','3','Walk object',3,'',19,1),(74,'2014-12-15 22:05:38','2','Walk object',3,'',19,1),(75,'2014-12-15 22:05:41','4','Walk object',1,'',19,1),(76,'2014-12-15 22:06:02','4','Walk object',2,'Changed start_time and blocks.',19,1),(77,'2014-12-15 23:45:39','515','Reign Season 1 Episode 1',2,'Changed viewed.',9,1),(78,'2014-12-15 23:45:59','7','Reign',3,'',7,1),(79,'2014-12-15 23:46:20','10','Psycho-Pass Season 1 Episode 10',2,'Changed viewed.',9,1),(80,'2014-12-15 23:46:20','11','Psycho-Pass Season 1 Episode 11',2,'Changed viewed.',9,1),(81,'2014-12-16 07:41:26','26','House',2,'Changed season_count.',7,1),(82,'2014-12-16 07:41:50','26','House',2,'No fields changed.',7,1),(83,'2014-12-16 07:43:29','1165','House Season 1 Episode 1',2,'Changed viewed.',9,1),(84,'2014-12-16 20:17:32','1166','House Season 1 Episode 2',2,'Changed viewed.',9,1),(85,'2014-12-16 20:18:41','2','Workout 2014-12-16 02:41:53-05:00',1,'',15,1),(86,'2014-12-16 20:18:51','2','Workout 2014-12-16 07:41:53+00:00',3,'',15,1),(87,'2014-12-20 17:45:38','12','Psycho-Pass Season 2 Episode 1',2,'Changed viewed.',9,1),(88,'2014-12-20 17:45:38','13','Psycho-Pass Season 2 Episode 2',2,'Changed viewed.',9,1),(89,'2014-12-20 17:47:51','1167','House Season 1 Episode 3',2,'Changed viewed.',9,1),(90,'2014-12-20 18:19:04','7','Introduction to Economics Lecture 5',2,'Changed viewed.',11,1),(91,'2014-12-23 04:27:04','27','White Collar',1,'',7,3),(92,'2014-12-23 04:29:29','27','White Collar',3,'',7,3),(93,'2014-12-23 04:31:09','1342','White Collar Season 1 Episode 1',2,'Changed viewed.',9,3),(94,'2014-12-23 04:31:09','1343','White Collar Season 1 Episode 2',2,'Changed viewed.',9,3),(95,'2014-12-23 04:31:09','1344','White Collar Season 1 Episode 3',2,'Changed viewed.',9,3),(96,'2014-12-23 04:31:09','1345','White Collar Season 1 Episode 4',2,'Changed viewed.',9,3),(97,'2014-12-23 04:31:09','1346','White Collar Season 1 Episode 5',2,'Changed viewed.',9,3),(98,'2014-12-23 04:31:09','1347','White Collar Season 1 Episode 6',2,'Changed viewed.',9,3),(99,'2014-12-23 04:31:09','1348','White Collar Season 1 Episode 7',2,'Changed viewed.',9,3),(100,'2014-12-23 04:31:09','1349','White Collar Season 1 Episode 8',2,'Changed viewed.',9,3),(101,'2014-12-23 04:31:09','1350','White Collar Season 1 Episode 9',2,'Changed viewed.',9,3),(102,'2014-12-23 04:31:09','1351','White Collar Season 1 Episode 10',2,'Changed viewed.',9,3),(103,'2014-12-23 04:31:09','1352','White Collar Season 1 Episode 11',2,'Changed viewed.',9,3),(104,'2014-12-23 04:31:09','1353','White Collar Season 1 Episode 12',2,'Changed viewed.',9,3),(105,'2014-12-23 04:31:09','1354','White Collar Season 1 Episode 13',2,'Changed viewed.',9,3),(106,'2014-12-23 04:31:09','1355','White Collar Season 1 Episode 14',2,'Changed viewed.',9,3),(107,'2014-12-23 04:31:39','1356','White Collar Season 2 Episode 1',2,'Changed viewed.',9,3),(108,'2014-12-23 04:31:39','1357','White Collar Season 2 Episode 2',2,'Changed viewed.',9,3),(109,'2014-12-23 04:31:39','1358','White Collar Season 2 Episode 3',2,'Changed viewed.',9,3),(110,'2014-12-23 04:31:39','1359','White Collar Season 2 Episode 4',2,'Changed viewed.',9,3),(111,'2014-12-23 04:31:39','1360','White Collar Season 2 Episode 5',2,'Changed viewed.',9,3),(112,'2014-12-23 04:31:39','1361','White Collar Season 2 Episode 6',2,'Changed viewed.',9,3),(113,'2014-12-23 04:31:39','1362','White Collar Season 2 Episode 7',2,'Changed viewed.',9,3),(114,'2014-12-23 04:31:39','1363','White Collar Season 2 Episode 8',2,'Changed viewed.',9,3),(115,'2014-12-23 04:31:39','1364','White Collar Season 2 Episode 9',2,'Changed viewed.',9,3),(116,'2014-12-23 04:31:39','1365','White Collar Season 2 Episode 10',2,'Changed viewed.',9,3),(117,'2014-12-23 04:31:39','1366','White Collar Season 2 Episode 11',2,'Changed viewed.',9,3),(118,'2014-12-23 04:31:39','1367','White Collar Season 2 Episode 12',2,'Changed viewed.',9,3),(119,'2014-12-23 04:31:39','1368','White Collar Season 2 Episode 13',2,'Changed viewed.',9,3),(120,'2014-12-23 04:31:39','1369','White Collar Season 2 Episode 14',2,'Changed viewed.',9,3),(121,'2014-12-23 04:31:39','1370','White Collar Season 2 Episode 15',2,'Changed viewed.',9,3),(122,'2014-12-23 04:31:39','1371','White Collar Season 2 Episode 16',2,'Changed viewed.',9,3),(123,'2014-12-24 00:02:19','1','Cigarette object',1,'',22,1),(124,'2014-12-24 00:02:41','1','Joint object',1,'',23,1),(125,'2014-12-24 04:15:14','4','DailySurvey object',1,'',25,1),(126,'2014-12-24 04:15:23','4','DailySurvey object',3,'',25,1),(127,'2014-12-24 04:15:23','3','DailySurvey object',3,'',25,1),(128,'2014-12-24 04:15:23','2','DailySurvey object',3,'',25,1),(129,'2014-12-24 04:15:23','1','DailySurvey object',3,'',25,1),(130,'2014-12-24 04:16:47','5','DailySurvey object',1,'',25,1),(131,'2014-12-24 04:40:28','5','DailySurvey object',2,'No fields changed.',25,1),(132,'2014-12-24 05:02:48','313','Glee Season 3 Episode 14',2,'Changed viewed.',9,1),(133,'2014-12-24 05:02:48','314','Glee Season 3 Episode 15',2,'Changed viewed.',9,1),(134,'2014-12-24 05:02:48','315','Glee Season 3 Episode 16',2,'Changed viewed.',9,1),(135,'2014-12-24 05:02:48','316','Glee Season 3 Episode 17',2,'Changed viewed.',9,1),(136,'2014-12-24 05:02:48','317','Glee Season 3 Episode 18',2,'Changed viewed.',9,1),(137,'2014-12-24 05:02:48','318','Glee Season 3 Episode 19',2,'Changed viewed.',9,1),(138,'2014-12-24 05:03:04','516','Reign Season 1 Episode 2',2,'Changed viewed.',9,1),(139,'2014-12-24 05:05:09','1','Psycho-Pass',2,'Changed active.',7,1),(140,'2014-12-24 05:05:09','4','The IT Crowd',2,'Changed active.',7,1),(141,'2014-12-24 05:05:09','5','Fairy Tail',2,'Changed active.',7,1),(142,'2014-12-24 05:05:09','6','Glee',2,'Changed active.',7,1),(143,'2014-12-24 05:05:09','12','Reign',2,'Changed active.',7,1),(144,'2014-12-24 05:05:09','26','House',2,'Changed active.',7,1),(145,'2014-12-24 18:41:31','14','Psycho-Pass Season 2 Episode 3',2,'Changed viewed.',9,1),(146,'2014-12-24 18:42:06','225','Fairy Tail Season 1 Episode 18',2,'Changed viewed.',9,1),(147,'2014-12-24 18:42:06','226','Fairy Tail Season 1 Episode 19',2,'Changed viewed.',9,1),(148,'2014-12-24 18:42:06','227','Fairy Tail Season 1 Episode 20',2,'Changed viewed.',9,1),(149,'2014-12-24 18:42:06','228','Fairy Tail Season 1 Episode 21',2,'Changed viewed.',9,1),(150,'2014-12-24 18:42:06','229','Fairy Tail Season 1 Episode 22',2,'Changed viewed.',9,1),(151,'2014-12-24 18:42:06','230','Fairy Tail Season 1 Episode 23',2,'Changed viewed.',9,1),(152,'2014-12-24 23:22:09','1','Unit object',1,'',26,3),(153,'2014-12-24 23:23:00','2','Unit object',1,'',26,3),(154,'2014-12-24 23:26:31','1','Conversion object',1,'',27,3),(155,'2014-12-24 23:33:22','1','Caffeine',1,'',28,3),(156,'2014-12-24 23:38:07','3','Milligram',1,'',26,3),(157,'2014-12-24 23:40:56','1','Content object',1,'',29,3),(158,'2014-12-25 00:28:19','1','Caffeine object',1,'',30,3),(159,'2014-12-25 06:03:46','2','Workout 2014-12-24 16:30:00-05:00',1,'',15,3),(160,'2014-12-25 06:04:37','13','Bench Press - 12x75',1,'',17,3),(161,'2014-12-25 06:04:45','14','Bench Press - 12x75',1,'',17,3),(162,'2014-12-25 06:04:52','15','Bench Press - 12x75',1,'',17,3),(163,'2014-12-25 06:05:03','16','Squats - 8x75',1,'',17,3),(164,'2014-12-25 06:05:11','17','Squats - 8x75',1,'',17,3),(165,'2014-12-25 06:05:17','18','Squats - 8x75',1,'',17,3),(166,'2014-12-25 06:05:24','19','Deadlift - 4x75',1,'',17,3),(167,'2014-12-25 06:05:31','20','Deadlift - 4x75',1,'',17,3),(168,'2014-12-25 06:05:38','21','Deadlift - 4x75',1,'',17,3),(169,'2014-12-25 06:06:43','7','2014-12-24',1,'',25,3),(170,'2014-12-25 06:06:59','7','2014-12-24',3,'',25,3),(171,'2014-12-25 06:07:42','5','Walk 2014-12-25 12:30:00-05:00',1,'',19,3),(172,'2014-12-25 15:25:55','2','Caffeine object',1,'',30,3),(173,'2014-12-25 17:38:09','4','Ice Cube Tray',1,'',26,3),(174,'2014-12-25 17:38:23','5','Ice Cube',1,'',26,3),(175,'2014-12-25 17:39:45','2','Conversion object',1,'',27,3),(176,'2014-12-25 17:40:03','2','Content object',1,'',29,3),(177,'2014-12-25 17:41:27','3','Caffeine object',1,'',30,3),(178,'2014-12-25 19:43:56','4','Caffeine object',1,'',30,3);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'television show','media','televisionshow'),(8,'season','media','season'),(9,'episode','media','episode'),(10,'academic course','media','academiccourse'),(11,'lecture','media','lecture'),(12,'professor','academics','professor'),(13,'university','academics','university'),(14,'term','academics','term'),(15,'workout','fitness','workout'),(16,'exercise','fitness','exercise'),(17,'exercise set','fitness','exerciseset'),(19,'walk','fitness','walk'),(20,'food','food','food'),(21,'sleep','fitness','sleep'),(22,'cigarette','fitness','cigarette'),(23,'joint','fitness','joint'),(24,'drink','fitness','drink'),(25,'daily survey','schedule','dailysurvey'),(26,'unit','units','unit'),(27,'conversion','units','conversion'),(28,'chemical','units','chemical'),(29,'content','units','content'),(30,'caffeine','fitness','caffeine');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2014-12-14 04:35:02'),(2,'auth','0001_initial','2014-12-14 04:35:06'),(3,'admin','0001_initial','2014-12-14 04:35:08'),(4,'sessions','0001_initial','2014-12-14 04:35:08'),(5,'media','0001_initial','2014-12-14 04:41:57'),(6,'media','0002_auto_20141214_0442','2014-12-14 04:42:46'),(7,'media','0003_televisionshow_season_count','2014-12-14 04:50:16'),(8,'media','0004_academiccourse_lecture','2014-12-14 18:36:08'),(9,'academics','0001_initial','2014-12-14 18:42:00'),(10,'academics','0002_term_university','2014-12-14 18:43:28'),(11,'academics','0003_auto_20141214_1846','2014-12-14 18:46:09'),(12,'media','0005_auto_20141214_1849','2014-12-14 18:49:41'),(13,'media','0006_academiccourse_term_taught','2014-12-14 18:50:44'),(14,'media','0007_lecture_viewed','2014-12-14 18:59:14'),(15,'media','0008_academiccourse_completed','2014-12-14 19:09:44'),(16,'fitness','0001_initial','2014-12-15 02:05:04'),(17,'fitness','0002_auto_20141215_0209','2014-12-15 02:09:52'),(18,'fitness','0003_auto_20141215_0211','2014-12-15 02:11:28'),(19,'fitness','0004_auto_20141215_1558','2014-12-15 15:58:06'),(20,'food','0001_initial','2014-12-20 20:52:25'),(21,'fitness','0005_auto_20141223_1857','2014-12-23 23:57:19'),(22,'fitness','0006_auto_20141223_1900','2014-12-24 00:00:51'),(23,'fitness','0007_auto_20141223_1903','2014-12-24 00:03:26'),(24,'schedule','0001_initial','2014-12-24 03:31:05'),(25,'schedule','0002_auto_20141223_2247','2014-12-24 03:47:16'),(26,'schedule','0003_auto_20141223_2251','2014-12-24 03:51:46'),(27,'schedule','0004_auto_20141223_2256','2014-12-24 03:56:38'),(28,'schedule','0005_auto_20141223_2300','2014-12-24 04:00:20'),(29,'schedule','0006_auto_20141223_2306','2014-12-24 04:06:41'),(30,'schedule','0007_auto_20141223_2308','2014-12-24 04:08:18'),(31,'schedule','0008_auto_20141223_2314','2014-12-24 04:14:58'),(32,'schedule','0009_dailysurvey_calm','2014-12-24 04:18:42'),(33,'schedule','0010_dailysurvey_date','2014-12-24 04:38:13'),(34,'media','0009_televisionshow_active','2014-12-24 05:03:46'),(35,'units','0001_initial','2014-12-24 23:20:54'),(36,'units','0002_conversion','2014-12-24 23:26:03'),(37,'units','0003_chemical','2014-12-24 23:32:53'),(38,'units','0004_content','2014-12-24 23:38:20'),(39,'units','0005_content_chemical','2014-12-24 23:40:37'),(40,'fitness','0008_auto_20141224_1926','2014-12-25 00:26:39'),(41,'fitness','0009_auto_20141225_1249','2014-12-25 17:49:28');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('2l3kgvs7qen6a783gfgsj2z2ktf54pzv','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 19:19:54'),('39b7ub1e0htpoby7ykbjua7mbbyfgrsi','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-03 17:52:07'),('4ntqcbpubjkdihhd6nvnfy4pdz24bmff','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 20:16:19'),('7acvg7i4mq8umt7mg350hahojq4h9tlu','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 22:05:00'),('agglfjr54pn6ia3n79guhqv6w80ftqiw','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-07 23:16:14'),('aubcdlxxqzbt5memdxetfss20zxnhx54','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-08 06:01:46'),('b9rip46uknb0mftgfzkgzad8fv7ilmqa','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-06 04:25:25'),('bamgm1i9jpw0jfhqpp0fbnpib54vurfb','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 01:59:38'),('c8gwc5ur3z6yk0b7g0inf6qw5q5552ju','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-03 17:44:05'),('db6mp58yzqkhd8qvnewizou1n5rs8a05','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 20:16:10'),('dyia8782amp6c9pg9wa5j37cb3bpz0t8','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-04 21:05:35'),('give7n5nlt7tzrj5qqtipejomlf4f3sn','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-07 23:16:09'),('hoiel1orb4505amao9paedx49kwugnb5','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2015-01-06 04:22:37'),('kdduk8flz68jxbxjnr2mja0ubjt8d4oi','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 15:58:52'),('lg8j2o8p9u1rzvzxx5h9kw6waa22w8dp','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-28 16:52:09'),('m2787ciihpxymxyuzmdb8mjaa8kt41hs','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-06 23:55:28'),('obfw0yrw24zqj8kqn2ixjcbrfw4ml2xh','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-28 23:51:12'),('oho2rxka63fhzcjcd4a1awu64l839l09','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-29 02:22:32'),('qviwsmflca06lhjwk4gjtabvgf9k7z9p','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-06 17:12:37'),('rkqhinaqk8ikxjq5z597zuavstvl9idp','YTUxNmYwNWU4MWRhNGE1MDFiZDczYWFkNTBlMDY3MDdmZDk2YWM3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjRhMjJiOGMxOTU3MTllMTdjYTMxOTZlMjYzY2U1MWNmYjNlZDA5OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-08 22:47:47'),('x3ihchue6cv5zbna9i58tti2heurdfm4','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 05:28:19'),('ye0fqbjnxcnq3re28lcutdmdy7eq43yt','MzRiNTk2NGFkODMzZjJhNTdiNDkyMDAxZmQ1MzkyY2UxOWI3ZTE3Mzp7fQ==','2014-12-30 20:16:06'),('z30fphzs7r8ph244p8ggp69x6wz1klvb','MmU4NjJlMTYyN2E1YmUyNzJlZmM2ZTEwZDIzY2Q1NGQwMjAxZWY1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImMwYzFkM2I1NDVkNjFjNmY0YzgzNjZkNTRmMDk0MTNiOWMyOWUzYWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-30 19:44:04');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_caffeine`
--

DROP TABLE IF EXISTS `fitness_caffeine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_caffeine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` decimal(6,2) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fitness_caffeine_e8175980` (`unit_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_caffeine`
--

LOCK TABLES `fitness_caffeine` WRITE;
/*!40000 ALTER TABLE `fitness_caffeine` DISABLE KEYS */;
INSERT INTO `fitness_caffeine` VALUES (1,5.00,2,'2014-12-25 17:49:23'),(2,3.00,2,'2014-12-25 17:49:23'),(3,1.00,5,'2014-12-25 17:49:23'),(4,2.00,2,'2014-12-25 19:43:49');
/*!40000 ALTER TABLE `fitness_caffeine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_cigarette`
--

DROP TABLE IF EXISTS `fitness_cigarette`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_cigarette` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_cigarette`
--

LOCK TABLES `fitness_cigarette` WRITE;
/*!40000 ALTER TABLE `fitness_cigarette` DISABLE KEYS */;
INSERT INTO `fitness_cigarette` VALUES (1,'2014-12-22 01:00:00');
/*!40000 ALTER TABLE `fitness_cigarette` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_drink`
--

DROP TABLE IF EXISTS `fitness_drink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_drink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_drink`
--

LOCK TABLES `fitness_drink` WRITE;
/*!40000 ALTER TABLE `fitness_drink` DISABLE KEYS */;
/*!40000 ALTER TABLE `fitness_drink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_exercise`
--

DROP TABLE IF EXISTS `fitness_exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_exercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_exercise`
--

LOCK TABLES `fitness_exercise` WRITE;
/*!40000 ALTER TABLE `fitness_exercise` DISABLE KEYS */;
INSERT INTO `fitness_exercise` VALUES (1,'Bench Press'),(2,'Deadlift'),(3,'Squats');
/*!40000 ALTER TABLE `fitness_exercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_exerciseset`
--

DROP TABLE IF EXISTS `fitness_exerciseset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_exerciseset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repetitions` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `exercise_id` int(11) NOT NULL,
  `workout_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fitness_exerciseset_f57b1ff6` (`exercise_id`),
  KEY `fitness_exerciseset_c1bc7825` (`workout_id`),
  CONSTRAINT `fitness_exerci_workout_id_79cc1d3b42b702ac_fk_fitness_workout_id` FOREIGN KEY (`workout_id`) REFERENCES `fitness_workout` (`id`),
  CONSTRAINT `fitness_exer_exercise_id_73480ec38390ac7c_fk_fitness_exercise_id` FOREIGN KEY (`exercise_id`) REFERENCES `fitness_exercise` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_exerciseset`
--

LOCK TABLES `fitness_exerciseset` WRITE;
/*!40000 ALTER TABLE `fitness_exerciseset` DISABLE KEYS */;
INSERT INTO `fitness_exerciseset` VALUES (1,12,75,1,1),(2,12,75,1,1),(3,12,75,1,1),(4,12,75,1,1),(5,4,75,2,1),(6,4,75,2,1),(7,4,75,2,1),(8,4,75,2,1),(9,8,75,3,1),(10,8,75,3,1),(11,8,75,3,1),(12,8,75,3,1),(13,12,75,1,2),(14,12,75,1,2),(15,12,75,1,2),(16,8,75,3,2),(17,8,75,3,2),(18,8,75,3,2),(19,4,75,2,2),(20,4,75,2,2),(21,4,75,2,2);
/*!40000 ALTER TABLE `fitness_exerciseset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_joint`
--

DROP TABLE IF EXISTS `fitness_joint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_joint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_joint`
--

LOCK TABLES `fitness_joint` WRITE;
/*!40000 ALTER TABLE `fitness_joint` DISABLE KEYS */;
INSERT INTO `fitness_joint` VALUES (1,'2014-12-22 06:00:00');
/*!40000 ALTER TABLE `fitness_joint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_sleep`
--

DROP TABLE IF EXISTS `fitness_sleep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_sleep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_sleep`
--

LOCK TABLES `fitness_sleep` WRITE;
/*!40000 ALTER TABLE `fitness_sleep` DISABLE KEYS */;
/*!40000 ALTER TABLE `fitness_sleep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_walk`
--

DROP TABLE IF EXISTS `fitness_walk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_walk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `blocks` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_walk`
--

LOCK TABLES `fitness_walk` WRITE;
/*!40000 ALTER TABLE `fitness_walk` DISABLE KEYS */;
INSERT INTO `fitness_walk` VALUES (1,'2014-12-15 16:14:10','2014-12-15 16:29:10',6),(4,'2014-12-15 19:12:42','2014-12-15 19:34:42',11),(5,'2014-12-25 17:30:00','2014-12-25 17:45:00',6);
/*!40000 ALTER TABLE `fitness_walk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitness_workout`
--

DROP TABLE IF EXISTS `fitness_workout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitness_workout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitness_workout`
--

LOCK TABLES `fitness_workout` WRITE;
/*!40000 ALTER TABLE `fitness_workout` DISABLE KEYS */;
INSERT INTO `fitness_workout` VALUES (1,'2014-12-14 20:30:00'),(2,'2014-12-24 21:30:00');
/*!40000 ALTER TABLE `fitness_workout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_food`
--

DROP TABLE IF EXISTS `food_food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_food` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_food`
--

LOCK TABLES `food_food` WRITE;
/*!40000 ALTER TABLE `food_food` DISABLE KEYS */;
INSERT INTO `food_food` VALUES (1,'banana'),(2,'apple'),(3,'carrot'),(4,'celery'),(5,'milk'),(6,'cheese'),(7,'egg'),(8,'kale'),(9,'ranch dressing'),(10,'clif bar'),(11,'nature valley bar'),(12,'chicken'),(13,'potato salad'),(14,'baked bean'),(15,'potato');
/*!40000 ALTER TABLE `food_food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_academiccourse`
--

DROP TABLE IF EXISTS `media_academiccourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_academiccourse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `lecture_count` int(11) NOT NULL,
  `professor_id` int(11) NOT NULL,
  `university_id` int(11) NOT NULL,
  `year_taught` int(11) NOT NULL,
  `term_taught_id` int(11) NOT NULL,
  `completed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_academiccourse_fa68148b` (`professor_id`),
  KEY `media_academiccourse_890553cf` (`university_id`),
  KEY `media_academiccourse_a038e4cc` (`term_taught_id`),
  CONSTRAINT `media_acade_term_taught_id_12eef5812e130334_fk_academics_term_id` FOREIGN KEY (`term_taught_id`) REFERENCES `academics_term` (`id`),
  CONSTRAINT `media_ac_professor_id_358b591c883b5001_fk_academics_professor_id` FOREIGN KEY (`professor_id`) REFERENCES `academics_professor` (`id`),
  CONSTRAINT `media__university_id_32d46e89ec2151eb_fk_academics_university_id` FOREIGN KEY (`university_id`) REFERENCES `academics_university` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_academiccourse`
--

LOCK TABLES `media_academiccourse` WRITE;
/*!40000 ALTER TABLE `media_academiccourse` DISABLE KEYS */;
INSERT INTO `media_academiccourse` VALUES (2,'Introduction to Economics',24,1,1,2011,2,0),(3,'Introduction to Computer Science and Programming',24,2,2,2008,2,1),(4,'Programming Languages',11,3,3,2013,2,1),(5,'Financial Engineering and Risk Management Part 1',54,4,4,2013,2,0),(6,'Game Theory',24,5,5,2014,1,1);
/*!40000 ALTER TABLE `media_academiccourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_episode`
--

DROP TABLE IF EXISTS `media_episode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_episode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `episode_number` int(11) NOT NULL,
  `viewed` tinyint(1) NOT NULL,
  `season_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_episode_b11701f0` (`season_id`),
  CONSTRAINT `media_episode_season_id_32663c181a7092d4_fk_media_season_id` FOREIGN KEY (`season_id`) REFERENCES `media_season` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1449 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_episode`
--

LOCK TABLES `media_episode` WRITE;
/*!40000 ALTER TABLE `media_episode` DISABLE KEYS */;
INSERT INTO `media_episode` VALUES (1,1,1,1),(2,2,1,1),(3,3,1,1),(4,4,1,1),(5,5,1,1),(6,6,1,1),(7,7,1,1),(8,8,1,1),(9,9,1,1),(10,10,1,1),(11,11,1,1),(12,1,1,2),(13,2,1,2),(14,3,1,2),(15,4,0,2),(16,5,0,2),(17,6,0,2),(18,7,0,2),(19,8,0,2),(20,9,0,2),(21,10,0,2),(22,11,0,2),(23,1,1,3),(24,2,1,3),(25,3,1,3),(26,4,1,3),(27,5,1,3),(28,6,1,3),(29,7,1,3),(30,1,1,4),(31,2,1,4),(32,3,1,4),(33,4,1,4),(34,5,1,4),(35,6,1,4),(36,7,1,4),(37,8,1,4),(38,9,1,4),(39,10,1,4),(40,11,1,4),(41,12,1,4),(42,13,1,4),(43,14,1,4),(44,15,1,4),(45,16,1,4),(46,17,1,4),(47,18,1,4),(48,19,1,4),(49,20,1,4),(50,21,1,4),(51,22,1,4),(52,1,1,5),(53,2,1,5),(54,3,1,5),(55,4,1,5),(56,5,1,5),(57,6,1,5),(58,7,1,5),(59,8,1,5),(60,9,1,5),(61,10,1,5),(62,11,1,5),(63,12,1,5),(64,13,1,5),(65,14,1,5),(66,15,1,5),(67,16,1,5),(68,17,1,5),(69,18,1,5),(70,1,0,6),(71,2,0,6),(72,3,0,6),(73,4,0,6),(74,5,0,6),(75,6,0,6),(76,7,0,6),(77,8,0,6),(78,9,0,6),(79,10,0,6),(80,11,0,6),(81,12,0,6),(82,13,0,6),(83,14,0,6),(84,15,0,6),(85,16,0,6),(86,17,0,6),(87,18,0,6),(88,19,0,6),(89,20,0,6),(90,21,0,6),(91,22,0,6),(92,1,1,7),(93,2,1,7),(94,3,1,7),(95,4,1,7),(96,5,1,7),(97,6,1,7),(98,7,1,7),(99,8,1,7),(100,9,1,7),(101,10,1,7),(102,11,1,7),(103,12,1,7),(104,13,1,7),(105,1,1,8),(106,2,1,8),(107,3,1,8),(108,4,1,8),(109,5,1,8),(110,6,1,8),(111,7,1,8),(112,8,1,8),(113,9,1,8),(114,10,1,8),(115,11,1,8),(116,12,1,8),(117,13,1,8),(118,1,1,9),(119,2,1,9),(120,3,1,9),(121,4,1,9),(122,5,1,9),(123,6,1,9),(124,7,1,9),(125,8,1,9),(126,9,1,9),(127,10,1,9),(128,11,1,9),(129,12,1,9),(130,13,1,9),(131,1,0,10),(132,2,0,10),(133,3,0,10),(134,4,0,10),(135,5,0,10),(136,6,0,10),(137,7,0,10),(138,8,0,10),(139,9,0,10),(140,10,0,10),(141,11,0,10),(142,12,0,10),(143,13,0,10),(144,1,0,11),(145,2,0,11),(146,3,0,11),(147,4,0,11),(148,5,0,11),(149,6,0,11),(150,7,0,11),(151,8,0,11),(152,9,0,11),(153,10,0,11),(154,11,0,11),(155,12,0,11),(156,13,0,11),(157,1,0,12),(158,2,0,12),(159,3,0,12),(160,4,0,12),(161,5,0,12),(162,6,0,12),(163,7,0,12),(164,8,0,12),(165,9,0,12),(166,10,0,12),(167,11,0,12),(168,12,0,12),(169,13,0,12),(170,1,0,13),(171,2,0,13),(172,3,0,13),(173,4,0,13),(174,5,0,13),(175,6,0,13),(176,7,0,13),(177,8,0,13),(178,9,0,13),(179,10,0,13),(180,11,0,13),(181,12,0,13),(182,13,0,13),(183,14,0,13),(184,1,1,14),(185,2,1,14),(186,3,1,14),(187,4,1,14),(188,5,1,14),(189,6,1,14),(190,1,1,15),(191,2,1,15),(192,3,1,15),(193,4,1,15),(194,5,1,15),(195,6,1,15),(196,1,1,16),(197,2,1,16),(198,3,1,16),(199,4,1,16),(200,5,1,16),(201,6,1,16),(202,1,0,17),(203,2,0,17),(204,3,0,17),(205,4,0,17),(206,5,0,17),(207,6,0,17),(208,1,1,18),(209,2,1,18),(210,3,1,18),(211,4,1,18),(212,5,1,18),(213,6,1,18),(214,7,1,18),(215,8,1,18),(216,9,1,18),(217,10,1,18),(218,11,1,18),(219,12,1,18),(220,13,1,18),(221,14,1,18),(222,15,1,18),(223,16,1,18),(224,17,1,18),(225,18,1,18),(226,19,1,18),(227,20,1,18),(228,21,1,18),(229,22,1,18),(230,23,1,18),(231,24,1,18),(232,25,0,18),(233,26,0,18),(234,27,0,18),(235,28,0,18),(236,29,0,18),(237,30,0,18),(238,31,0,18),(239,32,0,18),(240,33,0,18),(241,34,0,18),(242,35,0,18),(243,36,0,18),(244,37,0,18),(245,38,0,18),(246,39,0,18),(247,40,0,18),(248,41,0,18),(249,42,0,18),(250,43,0,18),(251,44,0,18),(252,45,0,18),(253,46,0,18),(254,47,0,18),(255,48,0,18),(256,1,1,19),(257,2,1,19),(258,3,1,19),(259,4,1,19),(260,5,1,19),(261,6,1,19),(262,7,1,19),(263,8,1,19),(264,9,1,19),(265,10,1,19),(266,11,1,19),(267,12,1,19),(268,13,1,19),(269,14,1,19),(270,15,1,19),(271,16,1,19),(272,17,1,19),(273,18,1,19),(274,19,1,19),(275,20,1,19),(276,21,1,19),(277,22,1,19),(278,1,1,20),(279,2,1,20),(280,3,1,20),(281,4,1,20),(282,5,1,20),(283,6,1,20),(284,7,1,20),(285,8,1,20),(286,9,1,20),(287,10,1,20),(288,11,1,20),(289,12,1,20),(290,13,1,20),(291,14,1,20),(292,15,1,20),(293,16,1,20),(294,17,1,20),(295,18,1,20),(296,19,1,20),(297,20,1,20),(298,21,1,20),(299,22,1,20),(300,1,1,21),(301,2,1,21),(302,3,1,21),(303,4,1,21),(304,5,1,21),(305,6,1,21),(306,7,1,21),(307,8,1,21),(308,9,1,21),(309,10,1,21),(310,11,1,21),(311,12,1,21),(312,13,1,21),(313,14,1,21),(314,15,1,21),(315,16,1,21),(316,17,1,21),(317,18,1,21),(318,19,1,21),(319,20,1,21),(320,21,1,21),(321,22,1,21),(322,1,0,22),(323,2,0,22),(324,3,0,22),(325,4,0,22),(326,5,0,22),(327,6,0,22),(328,7,0,22),(329,8,0,22),(330,9,0,22),(331,10,0,22),(332,11,0,22),(333,12,0,22),(334,13,0,22),(335,14,0,22),(336,15,0,22),(337,16,0,22),(338,17,0,22),(339,18,0,22),(340,19,0,22),(341,20,0,22),(342,21,0,22),(343,22,0,22),(366,1,1,24),(367,2,1,24),(368,3,1,24),(369,1,1,25),(370,2,1,25),(371,3,1,25),(372,1,1,26),(373,2,1,26),(374,3,1,26),(375,1,1,27),(376,2,1,27),(377,3,1,27),(378,4,1,27),(379,5,1,27),(380,6,1,27),(381,7,1,27),(382,8,1,27),(383,9,1,27),(384,10,1,27),(385,11,1,27),(386,12,1,27),(387,1,1,28),(388,2,1,28),(389,3,1,28),(390,4,1,28),(391,5,1,28),(392,6,1,28),(393,7,1,28),(394,8,1,28),(395,9,1,28),(396,10,1,28),(397,11,1,28),(398,12,1,28),(399,1,0,29),(400,2,0,29),(401,3,0,29),(402,4,0,29),(403,5,0,29),(404,6,0,29),(405,7,0,29),(406,8,0,29),(407,9,0,29),(408,10,0,29),(409,11,0,29),(410,12,0,29),(411,1,1,30),(412,2,1,30),(413,3,1,30),(414,4,1,30),(415,5,1,30),(416,6,1,30),(417,7,1,30),(418,8,1,30),(419,9,1,30),(420,10,1,30),(421,11,1,30),(422,12,1,30),(423,13,1,30),(424,1,1,31),(425,2,1,31),(426,3,1,31),(427,4,1,31),(428,5,1,31),(429,6,1,31),(430,7,1,31),(431,8,1,31),(432,9,1,31),(433,10,1,31),(434,11,1,31),(435,12,1,31),(436,13,1,31),(437,1,1,32),(438,2,1,32),(439,3,1,32),(440,4,1,32),(441,5,1,32),(442,6,1,32),(443,7,1,32),(444,8,1,32),(445,9,1,32),(446,10,1,32),(447,11,1,32),(448,12,1,32),(449,13,1,32),(450,1,1,33),(451,2,1,33),(452,3,1,33),(453,4,1,33),(454,5,1,33),(455,6,1,33),(456,7,1,33),(457,8,1,33),(458,9,1,33),(459,10,1,33),(460,11,1,33),(461,12,1,33),(462,13,1,33),(463,1,1,34),(464,2,1,34),(465,3,1,34),(466,4,1,34),(467,5,1,34),(468,6,1,34),(469,7,1,34),(470,8,1,34),(471,9,1,34),(472,10,1,34),(473,11,1,34),(474,12,1,34),(475,13,1,34),(476,1,1,35),(477,2,1,35),(478,3,1,35),(479,4,1,35),(480,5,1,35),(481,6,1,35),(482,7,1,35),(483,8,1,35),(484,9,1,35),(485,10,1,35),(486,11,1,35),(487,12,1,35),(488,13,1,35),(489,1,1,36),(490,2,1,36),(491,3,1,36),(492,4,1,36),(493,5,1,36),(494,6,1,36),(495,7,1,36),(496,8,1,36),(497,9,1,36),(498,10,1,36),(499,11,1,36),(500,12,1,36),(501,13,1,36),(502,1,0,37),(503,2,0,37),(504,3,0,37),(505,4,0,37),(506,5,0,37),(507,6,0,37),(508,7,0,37),(509,8,0,37),(510,9,0,37),(511,10,0,37),(512,11,0,37),(513,12,0,37),(514,13,0,37),(515,1,1,38),(516,2,1,38),(517,3,0,38),(518,4,0,38),(519,5,0,38),(520,6,0,38),(521,7,0,38),(522,8,0,38),(523,9,0,38),(524,10,0,38),(525,11,0,38),(526,12,0,38),(527,13,0,38),(528,14,0,38),(529,15,0,38),(530,16,0,38),(531,17,0,38),(532,18,0,38),(533,19,0,38),(534,20,0,38),(535,21,0,38),(536,22,0,38),(537,1,1,39),(538,2,1,39),(539,3,1,39),(540,4,1,39),(541,5,1,39),(542,6,1,39),(543,7,1,39),(544,8,1,39),(545,9,1,39),(546,10,1,39),(547,11,1,39),(548,12,1,39),(549,13,1,39),(550,14,1,39),(551,15,1,39),(552,16,1,39),(553,17,1,39),(554,18,1,39),(555,19,1,39),(556,20,1,39),(557,1,1,40),(558,2,1,40),(559,3,1,40),(560,4,1,40),(561,5,1,40),(562,6,1,40),(563,7,1,40),(564,8,1,40),(565,9,1,40),(566,10,1,40),(567,11,1,40),(568,12,1,40),(569,13,1,40),(570,14,1,40),(571,15,1,40),(572,16,1,40),(573,17,1,40),(574,18,1,40),(575,19,1,40),(576,20,1,40),(577,1,1,41),(578,2,1,41),(579,3,1,41),(580,4,1,41),(581,5,1,41),(582,6,1,41),(583,7,1,41),(584,8,1,41),(585,9,1,41),(586,10,1,41),(587,11,1,41),(588,12,1,41),(589,13,1,41),(590,14,1,41),(591,15,1,41),(592,16,1,41),(593,17,1,41),(594,18,1,41),(595,19,1,41),(596,20,1,41),(597,21,1,41),(598,1,1,42),(599,2,1,42),(600,3,1,42),(601,4,1,42),(602,5,1,42),(603,6,1,42),(604,7,1,42),(605,8,1,42),(606,9,1,42),(607,10,1,42),(608,11,1,42),(609,12,1,42),(610,13,1,42),(611,14,1,42),(612,15,1,42),(613,16,1,42),(614,17,1,42),(615,18,1,42),(616,19,1,42),(617,20,1,42),(618,21,1,42),(619,22,1,42),(620,23,1,42),(621,24,1,42),(622,25,1,42),(623,26,1,42),(624,27,1,42),(625,28,1,42),(626,29,1,42),(627,30,1,42),(628,31,1,42),(629,32,1,42),(630,33,1,42),(631,34,1,42),(632,35,1,42),(633,36,1,42),(634,37,1,42),(635,38,1,42),(636,39,1,42),(637,40,1,42),(638,41,1,42),(639,42,1,42),(640,43,1,42),(641,44,1,42),(642,45,1,42),(643,46,1,42),(644,47,1,42),(645,48,1,42),(646,49,1,42),(647,50,1,42),(648,51,1,42),(649,52,1,42),(650,53,1,42),(651,54,1,42),(652,55,1,42),(653,56,1,42),(654,57,1,42),(655,58,1,42),(656,59,1,42),(657,60,1,42),(658,61,1,42),(659,62,1,42),(660,63,1,42),(661,64,1,42),(662,65,1,42),(663,66,1,42),(664,67,1,42),(665,68,1,42),(666,69,1,42),(667,70,1,42),(668,71,1,42),(669,72,1,42),(670,73,1,42),(671,74,1,42),(672,1,1,43),(673,2,1,43),(674,3,1,43),(675,4,1,43),(676,5,1,43),(677,6,1,43),(678,7,1,43),(679,8,1,43),(680,9,1,43),(681,10,1,43),(682,11,1,43),(683,12,1,43),(684,13,1,43),(685,1,1,44),(686,2,1,44),(687,3,1,44),(688,4,1,44),(689,5,1,44),(690,6,1,44),(691,7,1,44),(692,8,1,44),(693,9,1,44),(694,10,1,44),(695,11,1,44),(696,12,1,44),(697,13,1,44),(698,1,1,45),(699,2,1,45),(700,3,1,45),(701,4,1,45),(702,5,1,45),(703,6,1,45),(704,7,1,45),(705,8,1,45),(706,9,1,45),(707,10,1,45),(708,11,1,45),(709,12,1,45),(710,13,1,45),(711,14,1,45),(712,15,1,45),(713,16,1,45),(714,17,1,45),(715,18,1,45),(716,19,1,45),(717,20,1,45),(718,21,1,45),(719,22,1,45),(720,23,1,45),(721,24,1,45),(722,25,1,45),(723,26,1,45),(724,27,1,45),(725,28,1,45),(726,29,1,45),(727,30,1,45),(728,31,1,45),(729,32,1,45),(730,33,1,45),(731,34,1,45),(732,35,1,45),(733,36,1,45),(734,37,1,45),(735,38,1,45),(736,39,1,45),(737,40,1,45),(738,41,1,45),(739,42,1,45),(740,43,1,45),(741,44,1,45),(742,45,1,45),(743,46,1,45),(744,47,1,45),(745,48,1,45),(746,49,1,45),(747,50,1,45),(748,51,1,45),(749,1,1,46),(750,2,1,46),(751,3,1,46),(752,4,1,46),(753,5,1,46),(754,6,1,46),(755,7,1,46),(756,8,1,46),(757,9,1,46),(758,10,1,46),(759,11,1,46),(760,12,1,46),(761,13,1,46),(762,14,1,46),(763,15,1,46),(764,16,1,46),(765,17,1,46),(766,18,1,46),(767,19,1,46),(768,20,1,46),(769,21,1,46),(770,22,1,46),(771,23,1,46),(772,24,1,46),(773,25,1,46),(774,1,1,47),(775,2,1,47),(776,3,1,47),(777,4,1,47),(778,5,1,47),(779,6,1,47),(780,7,1,47),(781,8,1,47),(782,9,1,47),(783,10,1,47),(784,11,1,47),(785,12,1,47),(786,13,1,47),(787,14,1,47),(788,15,1,47),(789,16,1,47),(790,17,1,47),(791,18,1,47),(792,19,1,47),(793,20,1,47),(794,21,1,47),(795,22,1,47),(796,23,1,47),(797,24,1,47),(798,1,0,48),(799,2,0,48),(800,3,0,48),(801,4,0,48),(802,5,0,48),(803,6,0,48),(804,7,0,48),(805,8,0,48),(806,9,0,48),(807,10,0,48),(808,11,0,48),(809,12,0,48),(810,13,0,48),(811,14,0,48),(812,15,0,48),(813,16,0,48),(814,17,0,48),(815,18,0,48),(816,19,0,48),(817,20,0,48),(818,21,0,48),(819,22,0,48),(820,1,0,49),(821,2,0,49),(822,3,0,49),(823,4,0,49),(824,5,0,49),(825,6,0,49),(826,7,0,49),(827,8,0,49),(828,9,0,49),(829,10,0,49),(830,11,0,49),(831,12,0,49),(832,13,0,49),(833,1,0,50),(834,2,0,50),(835,3,0,50),(836,4,0,50),(837,5,0,50),(838,6,0,50),(839,7,0,50),(840,8,0,50),(841,9,0,50),(842,10,0,50),(843,11,0,50),(844,12,0,50),(845,13,0,50),(846,1,0,51),(847,2,0,51),(848,3,0,51),(849,4,0,51),(850,5,0,51),(851,6,0,51),(852,7,0,51),(853,8,0,51),(854,9,0,51),(855,10,0,51),(856,11,0,51),(857,12,0,51),(858,13,0,51),(859,1,1,52),(860,2,1,52),(861,3,1,52),(862,4,1,52),(863,5,1,52),(864,6,1,52),(865,7,1,52),(866,8,1,52),(867,9,1,52),(868,10,1,52),(869,11,1,52),(870,1,0,53),(871,2,0,53),(872,3,0,53),(873,4,0,53),(874,5,0,53),(875,6,0,53),(876,7,0,53),(877,8,0,53),(878,9,0,53),(879,10,0,53),(880,1,1,54),(881,2,1,54),(882,3,1,54),(883,4,1,54),(884,5,1,54),(885,6,1,54),(886,7,1,54),(887,8,1,54),(888,9,1,54),(889,10,1,54),(890,11,1,54),(891,12,1,54),(892,1,1,55),(893,2,1,55),(894,3,1,55),(895,4,1,55),(896,5,1,55),(897,6,1,55),(898,7,1,55),(899,8,1,55),(900,9,1,55),(901,10,1,55),(902,11,1,55),(903,12,1,55),(904,13,1,55),(905,1,1,56),(906,2,1,56),(907,3,1,56),(908,4,1,56),(909,5,1,56),(910,6,1,56),(911,7,1,56),(912,8,1,56),(913,9,1,56),(914,1,1,57),(915,2,1,57),(916,3,1,57),(917,4,1,57),(918,5,1,57),(919,6,1,57),(920,7,1,57),(921,8,1,57),(922,9,1,57),(923,10,1,57),(924,11,1,57),(925,12,1,57),(926,13,1,57),(927,14,1,57),(928,15,1,57),(929,16,1,57),(930,17,1,57),(931,18,1,57),(932,19,1,57),(933,20,1,57),(934,21,1,57),(935,22,1,57),(936,23,1,57),(937,1,0,58),(938,2,0,58),(939,3,0,58),(940,4,0,58),(941,5,0,58),(942,6,0,58),(943,7,0,58),(944,8,0,58),(945,9,0,58),(946,10,0,58),(947,11,0,58),(948,12,0,58),(949,13,0,58),(950,14,0,58),(951,15,0,58),(952,16,0,58),(953,17,0,58),(954,18,0,58),(955,19,0,58),(956,20,0,58),(957,21,0,58),(958,22,0,58),(959,1,1,59),(960,2,1,59),(961,3,1,59),(962,4,1,59),(963,5,1,59),(964,6,1,59),(965,7,1,59),(966,8,1,59),(967,9,1,59),(968,10,1,59),(969,1,1,60),(970,2,1,60),(971,3,1,60),(972,4,1,60),(973,5,1,60),(974,6,1,60),(975,7,1,60),(976,8,1,60),(977,9,1,60),(978,10,1,60),(979,1,1,61),(980,2,1,61),(981,3,1,61),(982,4,1,61),(983,5,1,61),(984,6,1,61),(985,7,1,61),(986,8,1,61),(987,9,1,61),(988,10,1,61),(989,1,1,62),(990,2,1,62),(991,3,1,62),(992,4,1,62),(993,5,1,62),(994,6,1,62),(995,7,1,62),(996,8,1,62),(997,9,1,62),(998,10,1,62),(999,1,1,63),(1000,2,1,63),(1001,3,1,63),(1002,4,1,63),(1003,5,1,63),(1004,6,1,63),(1005,7,1,63),(1006,8,1,63),(1007,9,1,63),(1008,10,1,63),(1009,1,1,64),(1010,2,1,64),(1011,3,1,64),(1012,4,1,64),(1013,5,1,64),(1014,6,1,64),(1015,7,1,64),(1016,8,1,64),(1017,9,1,64),(1018,10,1,64),(1019,11,1,64),(1020,12,1,64),(1021,13,1,64),(1022,1,1,65),(1023,2,1,65),(1024,3,1,65),(1025,4,1,65),(1026,5,1,65),(1027,6,1,65),(1028,7,1,65),(1029,8,1,65),(1030,9,1,65),(1031,10,1,65),(1032,11,1,65),(1033,12,1,65),(1034,13,1,65),(1035,1,1,66),(1036,2,1,66),(1037,3,1,66),(1038,4,1,66),(1039,5,1,66),(1040,6,1,66),(1041,7,1,66),(1042,8,1,66),(1043,9,1,66),(1044,10,1,66),(1045,11,1,66),(1046,12,1,66),(1047,13,1,66),(1048,1,0,67),(1049,2,0,67),(1050,3,0,67),(1051,4,0,67),(1052,5,0,67),(1053,6,0,67),(1054,7,0,67),(1055,8,0,67),(1056,9,0,67),(1057,10,0,67),(1058,11,0,67),(1059,12,0,67),(1060,13,0,67),(1061,1,0,68),(1062,2,0,68),(1063,3,0,68),(1064,4,0,68),(1065,5,0,68),(1066,6,0,68),(1067,7,0,68),(1068,8,0,68),(1069,9,0,68),(1070,10,0,68),(1071,11,0,68),(1072,12,0,68),(1073,13,0,68),(1074,1,1,69),(1075,2,1,69),(1076,3,1,69),(1077,4,1,69),(1078,5,1,69),(1079,6,1,69),(1080,7,1,69),(1081,8,1,69),(1082,9,1,69),(1083,10,1,69),(1084,1,1,70),(1085,2,1,70),(1086,3,1,70),(1087,4,1,70),(1088,5,1,70),(1089,6,1,70),(1090,7,1,70),(1091,8,1,70),(1092,9,1,70),(1093,10,1,70),(1094,1,1,71),(1095,2,1,71),(1096,3,1,71),(1097,4,1,71),(1098,5,1,71),(1099,6,1,71),(1100,7,1,71),(1101,8,1,71),(1102,1,1,72),(1103,2,1,72),(1104,3,1,72),(1105,4,1,72),(1106,5,1,72),(1107,6,1,72),(1108,7,1,72),(1109,8,1,72),(1110,9,1,72),(1111,10,1,72),(1112,1,0,73),(1113,2,0,73),(1114,3,0,73),(1115,4,0,73),(1116,5,0,73),(1117,6,0,73),(1118,7,0,73),(1119,8,0,73),(1120,9,0,73),(1121,10,0,73),(1122,1,0,74),(1123,2,0,74),(1124,3,0,74),(1125,4,0,74),(1126,5,0,74),(1127,6,0,74),(1128,7,0,74),(1129,8,0,74),(1130,9,0,74),(1131,10,0,74),(1132,1,0,75),(1133,2,0,75),(1134,3,0,75),(1135,4,0,75),(1136,5,0,75),(1137,6,0,75),(1138,7,0,75),(1139,8,0,75),(1140,9,0,75),(1141,10,0,75),(1142,1,0,76),(1143,2,0,76),(1144,3,0,76),(1145,4,0,76),(1146,5,0,76),(1147,6,0,76),(1148,7,0,76),(1149,8,0,76),(1150,9,0,76),(1151,10,0,76),(1152,1,0,77),(1153,2,0,77),(1154,3,0,77),(1155,4,0,77),(1156,5,0,77),(1157,6,0,77),(1158,7,0,77),(1159,8,0,77),(1160,9,0,77),(1161,10,0,77),(1162,11,0,77),(1163,12,0,77),(1164,13,0,77),(1165,1,1,78),(1166,2,1,78),(1167,3,1,78),(1168,4,1,78),(1169,5,1,78),(1170,6,0,78),(1171,7,0,78),(1172,8,0,78),(1173,9,0,78),(1174,10,0,78),(1175,11,0,78),(1176,12,0,78),(1177,13,0,78),(1178,14,0,78),(1179,15,0,78),(1180,16,0,78),(1181,17,0,78),(1182,18,0,78),(1183,19,0,78),(1184,20,0,78),(1185,21,0,78),(1186,22,0,78),(1187,1,0,79),(1188,2,0,79),(1189,3,0,79),(1190,4,0,79),(1191,5,0,79),(1192,6,0,79),(1193,7,0,79),(1194,8,0,79),(1195,9,0,79),(1196,10,0,79),(1197,11,0,79),(1198,12,0,79),(1199,13,0,79),(1200,14,0,79),(1201,15,0,79),(1202,16,0,79),(1203,17,0,79),(1204,18,0,79),(1205,19,0,79),(1206,20,0,79),(1207,21,0,79),(1208,22,0,79),(1209,23,0,79),(1210,24,0,79),(1211,1,0,80),(1212,2,0,80),(1213,3,0,80),(1214,4,0,80),(1215,5,0,80),(1216,6,0,80),(1217,7,0,80),(1218,8,0,80),(1219,9,0,80),(1220,10,0,80),(1221,11,0,80),(1222,12,0,80),(1223,13,0,80),(1224,14,0,80),(1225,15,0,80),(1226,16,0,80),(1227,17,0,80),(1228,18,0,80),(1229,19,0,80),(1230,20,0,80),(1231,21,0,80),(1232,22,0,80),(1233,23,0,80),(1234,24,0,80),(1235,1,0,81),(1236,2,0,81),(1237,3,0,81),(1238,4,0,81),(1239,5,0,81),(1240,6,0,81),(1241,7,0,81),(1242,8,0,81),(1243,9,0,81),(1244,10,0,81),(1245,11,0,81),(1246,12,0,81),(1247,13,0,81),(1248,14,0,81),(1249,15,0,81),(1250,16,0,81),(1251,1,0,82),(1252,2,0,82),(1253,3,0,82),(1254,4,0,82),(1255,5,0,82),(1256,6,0,82),(1257,7,0,82),(1258,8,0,82),(1259,9,0,82),(1260,10,0,82),(1261,11,0,82),(1262,12,0,82),(1263,13,0,82),(1264,14,0,82),(1265,15,0,82),(1266,16,0,82),(1267,17,0,82),(1268,18,0,82),(1269,19,0,82),(1270,20,0,82),(1271,21,0,82),(1272,22,0,82),(1273,23,0,82),(1274,24,0,82),(1275,1,0,83),(1276,2,0,83),(1277,3,0,83),(1278,4,0,83),(1279,5,0,83),(1280,6,0,83),(1281,7,0,83),(1282,8,0,83),(1283,9,0,83),(1284,10,0,83),(1285,11,0,83),(1286,12,0,83),(1287,13,0,83),(1288,14,0,83),(1289,15,0,83),(1290,16,0,83),(1291,17,0,83),(1292,18,0,83),(1293,19,0,83),(1294,20,0,83),(1295,21,0,83),(1296,22,0,83),(1297,1,0,84),(1298,2,0,84),(1299,3,0,84),(1300,4,0,84),(1301,5,0,84),(1302,6,0,84),(1303,7,0,84),(1304,8,0,84),(1305,9,0,84),(1306,10,0,84),(1307,11,0,84),(1308,12,0,84),(1309,13,0,84),(1310,14,0,84),(1311,15,0,84),(1312,16,0,84),(1313,17,0,84),(1314,18,0,84),(1315,19,0,84),(1316,20,0,84),(1317,21,0,84),(1318,22,0,84),(1319,23,0,84),(1320,1,0,85),(1321,2,0,85),(1322,3,0,85),(1323,4,0,85),(1324,5,0,85),(1325,6,0,85),(1326,7,0,85),(1327,8,0,85),(1328,9,0,85),(1329,10,0,85),(1330,11,0,85),(1331,12,0,85),(1332,13,0,85),(1333,14,0,85),(1334,15,0,85),(1335,16,0,85),(1336,17,0,85),(1337,18,0,85),(1338,19,0,85),(1339,20,0,85),(1340,21,0,85),(1341,22,0,85),(1342,1,1,92),(1343,2,1,92),(1344,3,1,92),(1345,4,1,92),(1346,5,1,92),(1347,6,1,92),(1348,7,1,92),(1349,8,1,92),(1350,9,1,92),(1351,10,1,92),(1352,11,1,92),(1353,12,1,92),(1354,13,1,92),(1355,14,1,92),(1356,1,1,93),(1357,2,1,93),(1358,3,1,93),(1359,4,1,93),(1360,5,1,93),(1361,6,1,93),(1362,7,1,93),(1363,8,1,93),(1364,9,1,93),(1365,10,1,93),(1366,11,1,93),(1367,12,1,93),(1368,13,1,93),(1369,14,1,93),(1370,15,1,93),(1371,16,1,93),(1372,1,0,94),(1373,2,0,94),(1374,3,0,94),(1375,4,0,94),(1376,5,0,94),(1377,6,0,94),(1378,7,0,94),(1379,8,0,94),(1380,9,0,94),(1381,10,0,94),(1382,11,0,94),(1383,12,0,94),(1384,13,0,94),(1385,14,0,94),(1386,15,0,94),(1387,16,0,94),(1388,1,0,95),(1389,2,0,95),(1390,3,0,95),(1391,4,0,95),(1392,5,0,95),(1393,6,0,95),(1394,7,0,95),(1395,8,0,95),(1396,9,0,95),(1397,10,0,95),(1398,11,0,95),(1399,12,0,95),(1400,13,0,95),(1401,14,0,95),(1402,15,0,95),(1403,16,0,95),(1404,1,0,96),(1405,2,0,96),(1406,3,0,96),(1407,4,0,96),(1408,5,0,96),(1409,6,0,96),(1410,7,0,96),(1411,8,0,96),(1412,9,0,96),(1413,10,0,96),(1414,11,0,96),(1415,12,0,96),(1416,13,0,96),(1417,1,0,97),(1418,2,0,97),(1419,3,0,97),(1420,4,0,97),(1421,5,0,97),(1422,6,0,97),(1423,1,0,98),(1424,2,0,98),(1425,3,0,98),(1426,4,0,98),(1427,5,0,98),(1428,6,0,98),(1429,7,0,98),(1430,8,0,98),(1431,9,0,98),(1432,10,0,98),(1433,11,0,98),(1434,12,0,98),(1435,13,0,98),(1436,14,0,98),(1437,15,0,98),(1438,16,0,98),(1439,17,0,98),(1440,18,0,98),(1441,19,0,98),(1442,20,0,98),(1443,21,0,98),(1444,22,0,98),(1445,23,0,98),(1446,24,0,98),(1447,25,0,98),(1448,26,0,98);
/*!40000 ALTER TABLE `media_episode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_lecture`
--

DROP TABLE IF EXISTS `media_lecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_lecture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lecture_number` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `viewed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_lecture_ea134da7` (`course_id`),
  CONSTRAINT `media_lect_course_id_773d7d0205a4d619_fk_media_academiccourse_id` FOREIGN KEY (`course_id`) REFERENCES `media_academiccourse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_lecture`
--

LOCK TABLES `media_lecture` WRITE;
/*!40000 ALTER TABLE `media_lecture` DISABLE KEYS */;
INSERT INTO `media_lecture` VALUES (3,1,2,1),(4,2,2,1),(5,3,2,1),(6,4,2,1),(7,5,2,1),(8,6,2,0),(9,7,2,0),(10,8,2,0),(11,9,2,0),(12,10,2,0),(13,11,2,0),(14,12,2,0),(15,13,2,0),(16,14,2,0),(17,15,2,0),(18,16,2,0),(19,17,2,0),(20,18,2,0),(21,19,2,0),(22,20,2,0),(23,21,2,0),(24,22,2,0),(25,23,2,0),(26,24,2,0),(27,1,3,1),(28,2,3,1),(29,3,3,1),(30,4,3,1),(31,5,3,1),(32,6,3,1),(33,7,3,1),(34,8,3,1),(35,9,3,1),(36,10,3,1),(37,11,3,1),(38,12,3,1),(39,13,3,1),(40,14,3,1),(41,15,3,1),(42,16,3,1),(43,17,3,1),(44,18,3,1),(45,19,3,1),(46,20,3,1),(47,21,3,1),(48,22,3,1),(49,23,3,1),(50,24,3,1),(51,25,3,1),(52,1,4,1),(53,2,4,1),(54,3,4,1),(55,4,4,1),(56,5,4,1),(57,6,4,1),(58,7,4,1),(59,8,4,1),(60,9,4,1),(61,10,4,1),(62,11,4,1),(63,1,5,0),(64,2,5,0),(65,3,5,0),(66,4,5,0),(67,5,5,0),(68,6,5,0),(69,7,5,0),(70,8,5,0),(71,9,5,0),(72,10,5,0),(73,11,5,0),(74,12,5,0),(75,13,5,0),(76,14,5,0),(77,15,5,0),(78,16,5,0),(79,17,5,0),(80,18,5,0),(81,19,5,0),(82,20,5,0),(83,21,5,0),(84,22,5,0),(85,23,5,0),(86,24,5,0),(87,25,5,0),(88,26,5,0),(89,27,5,0),(90,28,5,0),(91,29,5,0),(92,30,5,0),(93,31,5,0),(94,32,5,0),(95,33,5,0),(96,34,5,0),(97,35,5,0),(98,36,5,0),(99,37,5,0),(100,38,5,0),(101,39,5,0),(102,40,5,0),(103,41,5,0),(104,42,5,0),(105,43,5,0),(106,44,5,0),(107,45,5,0),(108,46,5,0),(109,47,5,0),(110,48,5,0),(111,49,5,0),(112,50,5,0),(113,51,5,0),(114,52,5,0),(115,53,5,0),(116,54,5,0),(117,1,6,1),(118,2,6,1),(119,3,6,1),(120,4,6,1),(121,5,6,1),(122,6,6,1),(123,7,6,1),(124,8,6,1),(125,9,6,1),(126,10,6,1),(127,11,6,1),(128,12,6,1),(129,13,6,1),(130,14,6,1),(131,15,6,1),(132,16,6,1),(133,17,6,1),(134,18,6,1),(135,19,6,1),(136,20,6,1),(137,21,6,1),(138,22,6,1),(139,23,6,1),(140,24,6,1);
/*!40000 ALTER TABLE `media_lecture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_season`
--

DROP TABLE IF EXISTS `media_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `season_number` int(11) NOT NULL,
  `tvshow_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_season_6e4f2578` (`tvshow_id`),
  CONSTRAINT `media_seas_tvshow_id_4325dd6b2e21dd6d_fk_media_televisionshow_id` FOREIGN KEY (`tvshow_id`) REFERENCES `media_televisionshow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_season`
--

LOCK TABLES `media_season` WRITE;
/*!40000 ALTER TABLE `media_season` DISABLE KEYS */;
INSERT INTO `media_season` VALUES (1,1,1),(2,2,1),(3,1,2),(4,2,2),(5,3,2),(6,4,2),(7,1,3),(8,2,3),(9,3,3),(10,4,3),(11,5,3),(12,6,3),(13,7,3),(14,1,4),(15,2,4),(16,3,4),(17,4,4),(18,1,5),(19,1,6),(20,2,6),(21,3,6),(22,4,6),(24,1,8),(25,2,8),(26,3,8),(27,1,9),(28,2,9),(29,3,9),(30,1,10),(31,2,10),(32,3,10),(33,4,10),(34,5,10),(35,1,11),(36,2,11),(37,3,11),(38,1,12),(39,1,13),(40,2,13),(41,3,13),(42,1,14),(43,1,15),(44,2,15),(45,1,16),(46,1,17),(47,2,17),(48,3,17),(49,4,17),(50,5,17),(51,6,17),(52,1,18),(53,2,18),(54,1,19),(55,1,20),(56,2,20),(57,3,20),(58,4,20),(59,1,21),(60,2,21),(61,3,21),(62,4,21),(63,1,22),(64,2,22),(65,3,22),(66,4,22),(67,5,22),(68,6,22),(69,1,23),(70,2,23),(71,3,23),(72,4,23),(73,1,24),(74,1,25),(75,2,25),(76,3,25),(77,4,25),(78,1,26),(79,2,26),(80,3,26),(81,4,26),(82,5,26),(83,6,26),(84,7,26),(85,8,26),(92,1,28),(93,2,28),(94,3,28),(95,4,28),(96,5,28),(97,6,28),(98,1,29);
/*!40000 ALTER TABLE `media_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_televisionshow`
--

DROP TABLE IF EXISTS `media_televisionshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_televisionshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `season_count` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_televisionshow`
--

LOCK TABLES `media_televisionshow` WRITE;
/*!40000 ALTER TABLE `media_televisionshow` DISABLE KEYS */;
INSERT INTO `media_televisionshow` VALUES (1,'Psycho-Pass',2,1),(2,'Scandal',4,0),(3,'Mad Men',7,0),(4,'The IT Crowd',4,1),(5,'Fairy Tail',1,1),(6,'Glee',4,0),(8,'Sherlock',3,0),(9,'Borgia',3,0),(10,'Merlin',5,0),(11,'House of Cards',3,0),(12,'Reign',1,1),(13,'Avatar: The Last Airbender',3,0),(14,'Monster',1,0),(15,'Vampire Knight',2,0),(16,'Soul Eater',1,0),(17,'Community',6,0),(18,'Rick and Morty',2,0),(19,'BoJack Horseman',1,0),(20,'Bob\'s Burgers',4,0),(21,'Game of Thrones',4,0),(22,'Archer',6,0),(23,'The Tudors',4,0),(24,'Marco Polo',1,0),(25,'Hell on Wheels',4,0),(26,'House',8,1),(28,'White Collar',6,0),(29,'Ouran High School Host Club',1,0);
/*!40000 ALTER TABLE `media_televisionshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_dailysurvey`
--

DROP TABLE IF EXISTS `schedule_dailysurvey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_dailysurvey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nap` tinyint(1) NOT NULL,
  `good` tinyint(1) NOT NULL,
  `happy` tinyint(1) NOT NULL,
  `breakfast` tinyint(1) NOT NULL,
  `dinner` tinyint(1) NOT NULL,
  `lunch` tinyint(1) NOT NULL,
  `evening_caffeine` tinyint(1) NOT NULL,
  `morning_caffeine` tinyint(1) NOT NULL,
  `television` tinyint(1) NOT NULL,
  `wake` tinyint(1) NOT NULL,
  `cleaned` tinyint(1),
  `exercise` tinyint(1) NOT NULL,
  `french` tinyint(1) NOT NULL,
  `lumosity` tinyint(1) NOT NULL,
  `piano` tinyint(1) NOT NULL,
  `energy` tinyint(1) NOT NULL,
  `calm` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_dailysurvey`
--

LOCK TABLES `schedule_dailysurvey` WRITE;
/*!40000 ALTER TABLE `schedule_dailysurvey` DISABLE KEYS */;
INSERT INTO `schedule_dailysurvey` VALUES (5,0,0,0,1,1,1,0,1,1,1,1,1,0,1,0,0,0,'2014-12-23'),(6,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,1,1,'2014-12-24');
/*!40000 ALTER TABLE `schedule_dailysurvey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_chemical`
--

DROP TABLE IF EXISTS `units_chemical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_chemical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_chemical`
--

LOCK TABLES `units_chemical` WRITE;
/*!40000 ALTER TABLE `units_chemical` DISABLE KEYS */;
INSERT INTO `units_chemical` VALUES (1,'Caffeine');
/*!40000 ALTER TABLE `units_chemical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_content`
--

DROP TABLE IF EXISTS `units_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` decimal(6,2) NOT NULL,
  `container_id` int(11) NOT NULL,
  `measure_id` int(11) NOT NULL,
  `chemical_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `units_content_5733dad4` (`container_id`),
  KEY `units_content_80c371ce` (`measure_id`),
  KEY `units_content_a69d813a` (`chemical_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_content`
--

LOCK TABLES `units_content` WRITE;
/*!40000 ALTER TABLE `units_content` DISABLE KEYS */;
INSERT INTO `units_content` VALUES (1,1200.00,1,3,1),(2,1000.00,4,3,1);
/*!40000 ALTER TABLE `units_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_conversion`
--

DROP TABLE IF EXISTS `units_conversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_conversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `multiplier` decimal(8,2) NOT NULL,
  `from_unit_id` int(11) NOT NULL,
  `to_unit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `units_conversion_eb0cac85` (`from_unit_id`),
  KEY `units_conversion_725b5a87` (`to_unit_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_conversion`
--

LOCK TABLES `units_conversion` WRITE;
/*!40000 ALTER TABLE `units_conversion` DISABLE KEYS */;
INSERT INTO `units_conversion` VALUES (1,80.00,2,1),(2,12.00,5,4);
/*!40000 ALTER TABLE `units_conversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units_unit`
--

DROP TABLE IF EXISTS `units_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units_unit`
--

LOCK TABLES `units_unit` WRITE;
/*!40000 ALTER TABLE `units_unit` DISABLE KEYS */;
INSERT INTO `units_unit` VALUES (1,'Dropper Bottle','A 2 ounce dropper bottle, primarily used for storing and measuring precise quantities of caffeine.'),(2,'Half Droplet','A half filled dropper from the dropper bottle. This is the standard fill when the appropriate volume of air is pushed out of the dropper.'),(3,'Milligram','A standard milligram.'),(4,'Ice Cube Tray','Primarily used for storing caffeinated ice cubes.'),(5,'Ice Cube','Primarily caffeinated ice cubes.');
/*!40000 ALTER TABLE `units_unit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-26  0:56:35
