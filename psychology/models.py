from core.models import CoreModel, PurgeableModel, DateStampedModel, models
from django.utils import timezone
import datetime

# Create your models here.
class HappinessMoment(models.Model):
    time = models.DateTimeField(default=timezone.now)
    happiness = models.DecimalField(max_digits=3, decimal_places=2, verbose_name="What was your peak happiness?", null=True, blank=True, default=0.8)
    notes = models.TextField()

class NoteCard(models.Model):
    subject = models.CharField(max_length=200)
    notes = models.TextField(null=True, blank=True)
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.subject

class Journal(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField(null=True, blank=True)
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

class LinearMood(models.Model):
    time = models.DateTimeField(default=timezone.now)
    mood = models.DecimalField(max_digits=3, decimal_places=2, verbose_name="What is your current linear mood?", null=True, blank=True, default=0.6)

    @classmethod
    def record(self, mood = 0.5):
        return self(mood=mood).save()

    @classmethod
    def last(self, count=1):
        moods = self.objects.order_by('-time').all()[:count]
        for mood in reversed(moods):
            mood.time_ago()

    def time_ago(self):
        td = timezone.now() - self.time
        td = td - datetime.timedelta(microseconds=td.microseconds)
        last_output = td
        last_output = str(last_output) + ": " + str(self.mood)
        print last_output 

    def __str__(self):
        return self.mood.__str__()


class LinearEnergy(models.Model):
    time = models.DateTimeField(default=timezone.now)
    energy = models.DecimalField(max_digits=3, decimal_places=2, verbose_name="What is your current linear energy?", null=True, blank=True, default=0.6)

    @classmethod
    def record(self, energy = 0.5):
        return self(energy=energy).save()

    @classmethod
    def last(self, count=1):
        energies = self.objects.order_by('-time').all()[:count]
        for energy in reversed(energies):
            energy.time_ago()

    def time_ago(self):
        td = timezone.now() - self.time
        td = td - datetime.timedelta(microseconds=td.microseconds)
        last_output = td
        last_output = str(last_output) + ": " + str(self.energy)
        print last_output 

class BinaryMood(models.Model):
    time = models.DateTimeField(default=timezone.now)
    mood = models.BooleanField(default=True)

    @classmethod
    def record(self, mood = True):
        return self(mood=mood).save()

    @classmethod
    def last(self, count=1):
        moods= self.objects.order_by('-time').all()[:count]
        for mood in reversed(moods):
            mood.time_ago()

    def time_ago(self):
        td = timezone.now() - self.time
        td = td - datetime.timedelta(microseconds=td.microseconds)
        last_output = td
        last_output = str(last_output) + ": " + str(self.mood)
        print last_output 

class Rule(models.Model):
    clause = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.clause

class Conversation(DateStampedModel, PurgeableModel):
    pass
