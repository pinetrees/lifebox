# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('psychology', '0003_auto_20141231_2129'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinearMood',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(default=django.utils.timezone.now)),
                ('mood', models.DecimalField(decimal_places=2, default=0.8, max_digits=3, blank=True, null=True, verbose_name=b'What is your current linear mood?')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
