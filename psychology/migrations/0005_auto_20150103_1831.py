# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('psychology', '0004_linearmood'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinearEnergy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(default=django.utils.timezone.now)),
                ('energy', models.DecimalField(decimal_places=2, default=0.6, max_digits=3, blank=True, null=True, verbose_name=b'What is your current linear mood?')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='linearmood',
            name='mood',
            field=models.DecimalField(decimal_places=2, default=0.6, max_digits=3, blank=True, null=True, verbose_name=b'What is your current linear mood?'),
            preserve_default=True,
        ),
    ]
