from django.contrib import admin
from psychology.models import HappinessMoment, NoteCard, Journal, LinearMood, LinearEnergy, Rule

class LinearMoodAdmin(admin.ModelAdmin):
	list_display = ["mood", "time"]

class LinearEnergyAdmin(admin.ModelAdmin):
	list_display = ["energy", "time"]

# Register your models here.
admin.site.register(HappinessMoment)
admin.site.register(NoteCard)
admin.site.register(Journal)
admin.site.register(LinearMood, LinearMoodAdmin)
admin.site.register(LinearEnergy, LinearEnergyAdmin)
admin.site.register(Rule)
