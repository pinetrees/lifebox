from django.contrib import admin
from fitness.models import Workout, Exercise, ExerciseSet, Walk, Sleep, Substance, Caffeine, WaterPitcher
from daterange_filter.filter import DateRangeFilter

class ExerciseSetInline(admin.TabularInline):
	model = ExerciseSet

class WorkoutAdmin(admin.ModelAdmin):
	list_display = ["datetime", "__str__"]
	ordering = ['-datetime']
	inlines = [ExerciseSetInline,]

class ExerciseAdmin(admin.ModelAdmin):
	pass

class WalkAdmin(admin.ModelAdmin):
	list_display = ["start_time", "end_time", "blocks"]

class SleepAdmin(admin.ModelAdmin):
	list_display = ["start_time", "end_time", "duration"]

class SubstanceAdmin(admin.ModelAdmin):
	list_display = ["substance_type", "time"]

class CaffeineAdmin(admin.ModelAdmin):
	list_display = ["datetime", "quantity", "unit", "milligrams"]
	list_filter = [["datetime", DateRangeFilter]]

# Register your models here.
admin.site.register(Workout, WorkoutAdmin)
admin.site.register(Exercise, ExerciseAdmin)
admin.site.register(Walk, WalkAdmin)
admin.site.register(Sleep, SleepAdmin)
admin.site.register(Substance, SubstanceAdmin)
admin.site.register(Caffeine, CaffeineAdmin)
admin.site.register(WaterPitcher)
