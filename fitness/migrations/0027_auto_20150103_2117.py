# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0026_auto_20150103_2114'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Drink',
        ),
        migrations.DeleteModel(
            name='Joint',
        ),
    ]
