# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0025_substance_substance_type'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Cigarette',
        ),
        migrations.AlterField(
            model_name='substance',
            name='substance_type',
            field=models.IntegerField(choices=[(1, b'Cigarette'), (2, b'Joint'), (3, b'Drink')]),
            preserve_default=True,
        ),
    ]
