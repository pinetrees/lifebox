# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0006_auto_20141223_1900'),
    ]

    operations = [
        migrations.CreateModel(
            name='Drink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 3, 22, 991, tzinfo=utc))),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 3, 22, 148, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 3, 22, 634, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 3, 21, 999782, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 23, 16, 3, 21, 999751, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 18, 21, 999346, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 3, 21, 999321, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 3, 21, 998076, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
