# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import lifebox._time_


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0021_auto_20150102_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=lifebox._time_.eight_hours_ago),
            preserve_default=True,
        ),
    ]
