# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0003_auto_20141215_0211'),
    ]

    operations = [
        migrations.CreateModel(
            name='Walk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_time', models.DateTimeField(default=datetime.datetime(2014, 12, 15, 15, 58, 3, 8379, tzinfo=utc))),
                ('end_time', models.DateTimeField(default=datetime.datetime(2014, 12, 15, 16, 13, 3, 8406, tzinfo=utc))),
                ('blocks', models.IntegerField(default=6)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 15, 58, 3, 7211, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
