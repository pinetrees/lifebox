# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0015_auto_20141227_1708'),
    ]

    operations = [
        migrations.CreateModel(
            name='WaterPitcher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='caffeine',
            name='datetime',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 17, 30, 44, 259232, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='drink',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 17, 30, 44, 259868, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 17, 30, 44, 259584, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 17, 30, 44, 258900, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 9, 30, 44, 258870, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 17, 30, 44, 258413, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 17, 15, 44, 258377, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
