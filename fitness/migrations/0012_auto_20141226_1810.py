# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0011_auto_20141226_1809'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caffeine',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 10, 30, 674149, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 10, 30, 673210, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='drink',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 10, 30, 673779, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 10, 30, 673497, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 10, 30, 672909, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 15, 10, 30, 672880, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 25, 30, 672544, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 10, 30, 672519, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 10, 30, 671423, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
