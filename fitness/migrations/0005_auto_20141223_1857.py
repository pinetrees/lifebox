# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0004_auto_20141215_1558'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sleep',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_time', models.DateTimeField(default=datetime.datetime(2014, 12, 23, 15, 57, 5, 481155, tzinfo=utc))),
                ('end_time', models.DateTimeField(default=datetime.datetime(2014, 12, 23, 23, 57, 5, 481186, tzinfo=utc))),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 0, 12, 5, 480749, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 23, 23, 57, 5, 480721, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 23, 23, 57, 5, 479485, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
