# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0014_auto_20141226_1811'),
    ]

    operations = [
        migrations.AddField(
            model_name='sleep',
            name='slept_through',
            field=models.BooleanField(default=False, verbose_name=b'Did you sleep through the night?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='caffeine',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 22, 8, 39, 364963, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 22, 8, 39, 363852, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='drink',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 22, 8, 39, 364426, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 22, 8, 39, 364142, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 22, 8, 39, 363521, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 14, 8, 39, 363485, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 22, 8, 39, 363117, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 21, 53, 39, 363085, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 27, 22, 8, 39, 362050, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
