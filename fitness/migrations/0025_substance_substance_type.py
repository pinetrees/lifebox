# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0024_substance'),
    ]

    operations = [
        migrations.AddField(
            model_name='substance',
            name='substance_type',
            field=models.IntegerField(default=1, choices=[(1, b'Cigarette'), (1, b'Joint'), (1, b'Drink')]),
            preserve_default=False,
        ),
    ]
