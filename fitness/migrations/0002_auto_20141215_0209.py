# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 2, 9, 49, 824894, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
