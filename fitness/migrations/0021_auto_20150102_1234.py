# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0020_auto_20150102_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 2, 9, 34, 44, 442831, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
