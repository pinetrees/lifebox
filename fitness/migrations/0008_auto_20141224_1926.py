# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('units', '0005_content_chemical'),
        ('fitness', '0007_auto_20141223_1903'),
    ]

    operations = [
        migrations.CreateModel(
            name='Caffeine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 26, 31, 841972, tzinfo=utc))),
                ('quantity', models.DecimalField(max_digits=6, decimal_places=2)),
                ('unit', models.ForeignKey(to='units.Unit')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 26, 31, 840773, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='drink',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 26, 31, 841498, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 26, 31, 841136, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 26, 31, 840397, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 24, 16, 26, 31, 840366, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 41, 31, 839946, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 26, 31, 839918, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 0, 26, 31, 838645, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
