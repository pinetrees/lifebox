# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0002_auto_20141215_0209'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='workoutlog',
            name='Set',
        ),
        migrations.RemoveField(
            model_name='workoutlog',
            name='exercise',
        ),
        migrations.RemoveField(
            model_name='workoutlog',
            name='workout',
        ),
        migrations.DeleteModel(
            name='WorkoutLog',
        ),
        migrations.AddField(
            model_name='exerciseset',
            name='exercise',
            field=models.ForeignKey(default=1, to='fitness.Exercise'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='exerciseset',
            name='workout',
            field=models.ForeignKey(default=1, to='fitness.Workout'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='exerciseset',
            name='repetitions',
            field=models.IntegerField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 2, 11, 20, 543840, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
