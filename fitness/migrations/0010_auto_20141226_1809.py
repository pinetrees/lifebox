# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0009_auto_20141225_1249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caffeine',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 28, 248566, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 28, 247626, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='drink',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 28, 248196, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='exerciseset',
            name='weight',
            field=models.IntegerField(default=75, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 28, 247916, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 28, 247296, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 15, 9, 28, 247268, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 24, 28, 246937, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 28, 246913, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 28, 245882, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
