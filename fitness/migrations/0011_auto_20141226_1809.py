# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0010_auto_20141226_1809'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caffeine',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 34, 506341, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 34, 505342, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='drink',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 34, 505947, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 34, 505648, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 34, 505025, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 15, 9, 34, 504995, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 24, 34, 504639, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 34, 504613, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 23, 9, 34, 503478, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
