# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0022_auto_20150102_1235'),
    ]

    operations = [
        migrations.AddField(
            model_name='exerciseset',
            name='datetime',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
