# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0008_auto_20141224_1926'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='caffeine',
            options={'verbose_name_plural': 'Caffeine'},
        ),
        migrations.RemoveField(
            model_name='caffeine',
            name='time',
        ),
        migrations.AddField(
            model_name='caffeine',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 17, 49, 23, 513002, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cigarette',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 17, 49, 23, 511814, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='drink',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 17, 49, 23, 512542, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joint',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 17, 49, 23, 512168, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 17, 49, 23, 511445, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sleep',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 9, 49, 23, 511413, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 18, 4, 23, 511004, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='walk',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 17, 49, 23, 510974, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='workout',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 25, 17, 49, 23, 509737, tzinfo=utc), verbose_name=b'date and time'),
            preserve_default=True,
        ),
    ]
