from django.db import models
from core.models import PurgeableModel
from django.utils import timezone
from django.utils.timezone import activate
from lifebox import settings, _time_
import datetime
from units.models import Unit


activate(settings.TIME_ZONE)

# Create your models here.
class Workout(PurgeableModel):
	datetime = models.DateTimeField("date and time", default=timezone.now)

	def __str__(self):
		return "Workout " + str(self.datetime)

	def do(self, exercise, repetitions = 8, weight = 75):
		exercise_set = ExerciseSet(workout=self, exercise=exercise, repetitions=repetitions, weight=weight, datetime=timezone.now())
		exercise_set.save()
		return exercise_set

	@classmethod
	def latest(self):
		return self.objects.last()

class Exercise(PurgeableModel):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name

class ExerciseSet(PurgeableModel):
	workout = models.ForeignKey(Workout, related_name="sets")
	exercise = models.ForeignKey(Exercise)
	repetitions = models.IntegerField()
	weight = models.IntegerField(null=True, blank=True, default=75)
	datetime = models.DateTimeField("date and time", default=timezone.now)

	def __str__(self):
		return self.exercise.__str__() + ' - ' + str(self.repetitions) + 'x' + str(self.weight)

class Walk(PurgeableModel):
	start_time = models.DateTimeField(default=_time_.fifteen_minutes_ago)
	end_time = models.DateTimeField(default=timezone.now)
	blocks = models.IntegerField(default=6)

	def __str__(self):
		return " ".join(["Walk", str(self.start_time)])

	@classmethod
	def walked(self, from_hour = 0, from_minute = 0, to_hour = 0, to_minute = 0, blocks = 6):
		#dt = timezone.now()
		start_time = datetime.datetime.now().replace(hour=from_hour).replace(minute=from_minute).replace(second=0).replace(microsecond=0)
		end_time = datetime.datetime.now().replace(hour=to_hour).replace(minute=to_minute).replace(second=0).replace(microsecond=0)
		walk = self(start_time=start_time, end_time=end_time, blocks=blocks)
		walk.save()
		return walk

	@classmethod
	def last(self, count=1):
		walks = self.objects.order_by('-end_time').all()[:count]
		for walk in reversed(walks):
			walk.time_ago()

	def time_ago(self):
		td = timezone.now() - self.end_time
		td = td - datetime.timedelta(microseconds=td.microseconds)
		last_output = td
		last_output = str(last_output) + ": " + str(self.blocks)
		print last_output 

class Sleep(PurgeableModel):
	start_time = models.DateTimeField(default=_time_.eight_hours_ago)
	end_time = models.DateTimeField(default=timezone.now)
	slept_through = models.BooleanField(verbose_name="Did you sleep through the night?", default=False)

	def duration(self):
		return self.end_time - self.start_time

	@classmethod
	def slept(self, from_hour = 0, from_minute = 0, to_hour = 0, to_minute = 0, from_days_ago = 0, to_days_ago = 0, slept_through = 0):
		#dt = timezone.now()
		start_time = datetime.datetime.now().replace(hour=from_hour).replace(minute=from_minute).replace(second=0).replace(microsecond=0) - datetime.timedelta(days=from_days_ago)
		end_time = datetime.datetime.now().replace(hour=to_hour).replace(minute=to_minute).replace(second=0).replace(microsecond=0) - datetime.timedelta(days=to_days_ago)
		sleep = self(start_time=start_time, end_time=end_time, slept_through=slept_through)
		sleep.save()
		return sleep

	@classmethod
	def nap(self, duration, minutes_ago = 0):
		end_time = datetime.datetime.now() - datetime.timedelta(minutes=minutes_ago)
		start_time = end_time - datetime.timedelta(minutes=duration)

		sleep = self(start_time=start_time, end_time=end_time)
		sleep.save()
		return sleep
		

class Substance(PurgeableModel):
	SUBSTANCES = (
		(1, 'Cigarette'),
		(2, 'Joint'),
		(3, 'Drink'),
	)
	substance_type = models.IntegerField(choices=SUBSTANCES)
	time = models.DateTimeField(default=timezone.now)

class Caffeine(PurgeableModel):
	datetime = models.DateTimeField(default=timezone.now)
	unit = models.ForeignKey(Unit)
	quantity = models.DecimalField(max_digits=6, decimal_places=2)

	class Meta:
		verbose_name_plural = "Caffeine"

	#This is more general than miligrams, and needs to be adjusted in the Units model Unit
	def milligrams(self):
		return self.unit.find_chemical_contents('Caffeine').quantity * self.quantity

	def date(self):
		return self.datetime.date()

	
	@classmethod
	def by_date(self, date):
		return self.objects.raw("SELECT * FROM lifebox.fitness_caffeine WHERE date(CONVERT_TZ(datetime, 'GMT', 'EST')) = %s", [date]);

	@classmethod 
	def today(self):
		doses = self.by_date(datetime.date.today())
		milligrams = 0
		for dose in doses:
			milligrams += dose.milligrams()
		return milligrams

	@classmethod
	def yesterday(self):
		doses = self.by_date(datetime.date.today() - datetime.timedelta(1))
		milligrams = 0
		for dose in doses:
			milligrams += dose.milligrams()
		return milligrams
	
	@classmethod
	def days_ago(self, days_ago):
		doses = self.by_date(datetime.date.today() - datetime.timedelta(days_ago))
		milligrams = 0
		for dose in doses:
			milligrams += dose.milligrams()
		return milligrams
	
	@classmethod
	def week(self, total=False, by_day=False):
		#There is a much better way to total this, once we figure out how to traverse the conversion behind the scenes
		doses = self.objects.filter(datetime__lt=timezone.now(), datetime__gt=timezone.now() - datetime.timedelta(weeks=1)).all()		
		if total is True:
			milligrams = 0
			for dose in doses:
				milligrams += dose.milligrams()
			return milligrams
		elif by_day is True:
			for i in range(0, 7):
				print self.days_ago(i)
		else:
			for dose in doses:
				dose.render()
			

	@classmethod
	def drop(self, quantity):
		unit = Unit.objects.filter(name='Half Droplet').first()
		return self(unit=unit, quantity=quantity).save()

	@classmethod
	def cube(self, quantity=1, minutes_ago=0):
		unit = Unit.objects.filter(name='Ice Cube').first()
		return self(unit=unit, quantity=quantity, datetime=timezone.now() - datetime.timedelta(minutes=minutes_ago)).save()

	@classmethod
	def last(self, count=1, dosage=True):
		doses = self.objects.order_by('-datetime').all()[:count]
		for dose in reversed(doses):
			dose.time_ago(dosage)

	def time_ago(self, dosage=True):
		td = timezone.now() - self.datetime
		td = td - datetime.timedelta(microseconds=td.microseconds)
		last_output = td
		if dosage is True:
			last_output = str(last_output) + ": " + str(self.milligrams())
		print last_output 

	def render(self):
		print str(self.datetime) + ": " + str(self.milligrams())

class WaterFilter(PurgeableModel):
	pass

class WaterPitcher(PurgeableModel):
	""" Put these in just after a pitcher is emptied. Each sixty requires a change in filter."""
	datetime = models.DateTimeField(default=timezone.now)
	water_filter = models.ForeignKey(WaterFilter, null=True, related_name="pitchers")

	def save(self, *args, **kwargs):
		water_filter = WaterFilter.objects.last()
		if water_filter.pitchers.count() >= 60:
			water_filter = WaterFilter.objects.create()
		self.water_filter = water_filter
		super(WaterPitcher, self).save(*args, **kwargs)

