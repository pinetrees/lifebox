from core.models import CoreModel, models
from units.models import Unit
from decimal import Decimal
import datetime
from django.utils import timezone

# Create your models here.
class Food(CoreModel):

	def __str__(self):
		return self.name.title()

	def weight(self, unit = None):
		try:
			foodweight = FoodWeight.objects.get(food=self)
		except:
			return False
		if unit is not None:
			return foodweight.convert(unit)
		else:
			return foodweight

	def pantry_unit(self):
		try:
			return PantryItem.objects.get(food=self).unit
		except:
			pass

	def pantry_weight(self):
		unit = self.pantry_unit()
		if unit.name == 'Unit':
			return FoodWeight(food=self, unit=unit)
		else:
			return self.weight(unit)

class Pantry(CoreModel):

	class Meta:
		verbose_name_plural = "pantries"

class PantryItem(models.Model):
	pantry = models.ForeignKey(Pantry)
	food = models.ForeignKey(Food)
	quantity = models.DecimalField(max_digits=14, decimal_places=8, default=1.00000000)
	unit = models.ForeignKey(Unit)


class Recipe(CoreModel):

	def consume(self):
		for recipefood in self.recipefood_set.all():
			recipefood.consume()

	def replenish(self):
		for recipefood in self.recipefood_set.all():
			recipefood.replenish()

class RecipeFood(models.Model):
	recipe = models.ForeignKey(Recipe)
	food = models.ForeignKey(Food)
	quantity = models.DecimalField(max_digits=3, decimal_places=2, default=1.00)
	unit = models.ForeignKey(Unit)

	#This method is fishy
	def pantry_weight(self):
		weight = self.food.weight()
		if weight is not False and (weight.unit == self.unit or self.food.pantry_weight().unit == self.unit or self.unit.name == 'Unit'):
			foodweight = self.food.pantry_weight()
			foodweight.multiplier = foodweight.multiplier * self.quantity
			return foodweight
		else:
			return False

	def pantry_item(self):
		try:
			return PantryItem.objects.get(food=self.food)
		except:
			return False

	def consume(self):
		pantryitem = self.pantry_item()
		pantryweight = self.pantry_weight()
		if pantryitem is False or pantryweight is False:
			return False
		if pantryitem.unit == pantryweight.unit:
			pantryitem.quantity = pantryitem.quantity - pantryweight.multiplier
			pantryitem.save()
		else:
			print "The units do not match."

	#This method is the inverse of consume. Both methods should be built upon a helper function to reduce code
	def replenish(self):
		pantryitem = self.pantry_item()
		pantryweight = self.pantry_weight()
		if pantryitem.unit == pantryweight.unit:
			pantryitem.quantity = pantryitem.quantity + pantryweight.multiplier
			pantryitem.save()
		else:
			print "The units do not match."

class FoodWeight(models.Model):
	food = models.ForeignKey(Food)
	multiplier = models.DecimalField(max_digits=14, decimal_places=8, default=Decimal('1.00000000'))
	unit = models.ForeignKey(Unit)

	def convert(self, to_unit_name):
		conversion = self.unit.convert(to_unit_name)
		return FoodWeight(food=self.food, multiplier=self.multiplier/conversion.multiplier, unit=conversion.to_unit)

class Meal(models.Model):
	recipe = models.ForeignKey(Recipe)
	portion = models.DecimalField(max_digits=3, decimal_places = 2, default=1.00)
	datetime = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.recipe.__str__()

	@classmethod
	def have(self, recipe_name, hours_ago = 0, minutes_ago = 0, days_ago = 0, portion = 1):
		recipe = Recipe.objects.filter(name=recipe_name).first()
		if recipe is None:
			return False
		else:
			_datetime_ = timezone.now() - datetime.timedelta(hours=hours_ago) - datetime.timedelta(minutes=minutes_ago) - datetime.timedelta(days=days_ago)
			return self(recipe=recipe, portion=portion, datetime=_datetime_).save()

def consume_meal(instance, created, raw, **kwargs):
	if not created or raw:
		return
	instance.recipe.consume()
	return True

def replenish_meal(sender, instance, using, **kwargs):
	instance.recipe.replenish()
	return True

models.signals.post_save.connect(consume_meal, sender=Meal, dispatch_uid="consume_meal")
models.signals.post_delete.connect(replenish_meal, sender=Meal, dispatch_uid="replenish_meal")
