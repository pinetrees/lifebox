from food.data import *
from food.models import Food

def generate_foods():
	for food in FOODS:
		Food.objects.create(name=food)	
