# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pantryitem',
            name='quantity',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
