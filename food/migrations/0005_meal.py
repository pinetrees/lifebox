# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0004_auto_20141227_1321'),
    ]

    operations = [
        migrations.CreateModel(
            name='Meal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(default=datetime.datetime(2014, 12, 27, 18, 58, 16, 438864, tzinfo=utc))),
                ('recipe', models.ForeignKey(to='food.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
