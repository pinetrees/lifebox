# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0003_auto_20141227_1319'),
    ]

    operations = [
        migrations.AlterField(
            model_name='foodweight',
            name='multiplier',
            field=models.DecimalField(default=1.0, max_digits=14, decimal_places=8),
            preserve_default=True,
        ),
    ]
