# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0005_meal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meal',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 3, 22, 55, 6, 985609, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipefood',
            name='quantity',
            field=models.DecimalField(default=1.0, max_digits=3, decimal_places=2),
            preserve_default=True,
        ),
    ]
