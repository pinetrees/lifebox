# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0006_auto_20150103_1755'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal',
            name='portion',
            field=models.DecimalField(default=1.0, max_digits=3, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='meal',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 4, 1, 0, 43, 711670, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
