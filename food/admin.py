from django.contrib import admin
from food.models import Food, Pantry, PantryItem, Recipe, RecipeFood, FoodWeight, Meal

class PantryItemInline(admin.TabularInline):
	model = PantryItem

class PantryAdmin(admin.ModelAdmin):
	inlines = [PantryItemInline,]

class PantryItemAdmin(admin.ModelAdmin):
	list_display = ["food", "quantity", "unit"]

class RecipeFoodInline(admin.TabularInline):
	model = RecipeFood

class RecipeAdmin(admin.ModelAdmin):
	inlines = [RecipeFoodInline,]

class FoodWeightAdmin(admin.ModelAdmin):
	list_display = ["food", "multiplier", "unit"]

class MealAdmin(admin.ModelAdmin):
	list_display = ["__str__", "datetime"]
	list_filter = ["recipe"]
	ordering = ["-datetime"]

# Register your models here.
admin.site.register(Food)
admin.site.register(Pantry, PantryAdmin)
admin.site.register(PantryItem, PantryItemAdmin)
admin.site.register(Recipe, RecipeAdmin)
admin.site.register(FoodWeight, FoodWeightAdmin)
admin.site.register(Meal, MealAdmin)
