from django.db import models

class PurgeableModel(models.Model):
    class Meta:
        abstract = True

    @classmethod
    def purge(self):
        return self.objects.all().delete()

    @classmethod 
    def total(self):
        return self.objects.count()

    @classmethod
    def take(self):
        return self.objects.order_by('?').first()

    @classmethod
    def all(self):
        return self.objects.all()

    @classmethod
    def list(self):
        for object in self.objects.all():
            print object

    @classmethod
    def _(self, index):
        return self.objects.all()[index-1:index][0]

    @classmethod
    def I(self):
        return self.objects.first()

    @classmethod
    def L(self):
        return self.objects.last()

    @classmethod
    def G(self, pk):
        return self.objects.get(pk=pk)

    @classmethod
    def C(self, **kwargs):
        return self.objects.create(**kwargs)

    @classmethod
    def A(self):
        return self.objects.all()

    @classmethod
    def get(self, pk):
        return self.objects.get(pk=pk)

    def update(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])
        self.save()


class NamedModel(PurgeableModel):
    name = models.CharField(max_length=200, null=True, default='')
    
    def __str__(self):
        return self.name

    @classmethod
    def create(self, name):
        return self.objects.create(name=name)

    class Meta:
        abstract = True

class CoreModel(NamedModel):
    
    class Meta:
        abstract = True

class PersonModel(PurgeableModel):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)

    def __str__(self):
        return " ".join([self.first_name, self.last_name])

    def name(self):
        return self.__str__()
    
    class Meta:
        abstract = True

class Person(PersonModel):
    pass

class DateStampedModel(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

class KeyValueModel(models.Model):
    key = models.CharField(max_length=200)
    value = models.CharField(max_length=200)

    class Meta:
        abstract = True

class Node(CoreModel):
    pass
