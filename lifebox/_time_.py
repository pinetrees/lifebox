import datetime
from django.utils import timezone

def one_week_from_now():
	#return timezone.now() + datetime.timedelta(weeks=1)
	return one_week_from()

def one_week_from(time = timezone.now()):
	return time + datetime.timedelta(weeks=1)

def one_hour_from_now():
	return one_hour_from()

def one_hour_from(time = timezone.now()):
	return time + datetime.timedelta(hours=1)

def fifteen_minutes_ago():
	return timezone.now() - datetime.timedelta(minutes=15)

def eight_hours_ago():
	return timezone.now() - datetime.timedelta(hours=8)
