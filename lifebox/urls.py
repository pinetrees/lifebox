from django.conf.urls import patterns, include, url
from django.contrib import admin
#from tasks.urls import register_tasks_urls
from rest_framework import routers
from lifebox import views
router = routers.DefaultRouter()

#register_tasks_urls(router)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'lifebox.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    #url(r'^admin/', include(admin.site.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),
    url(r'^data/', views.data, name='data'),
    url(r'^tasks/', include('tasks.urls')),

)
