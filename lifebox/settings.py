"""
Django settings for lifebox project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_-lombo+@!!w@_***6i+gl0=^tlbeif16h0n3r=-&fv(5+#aup'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    #'django_admin_bootstrapped.bootstrap3',
    #'django_admin_bootstrapped',
    #'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'core',
    'academics',
    'media',
    'fitness',
    'food',
    'schedule',
    'units',
    'inventory',
    'finances',
    'psychology',
    'django_extensions',
    'daterange_filter',
	'tasks',
    'rest_framework',
    'projects',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'lifebox.urls'

WSGI_APPLICATION = 'lifebox.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
	'ENGINE': 	'django.db.backends.mysql',
	#'ENGINE': 	'django.db.backends.postgresql_psycopg2',
	#'ENGINE':	'django.contrib.gis.db.backends.postgis',
	'NAME':		'lifebox',
	'USER': 	'lifebox',
	'PASSWORD':	'lifebox',
	'HOST':		'localhost',
	'PORT':		'3306'
	#'PORT':		'5432'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'EST'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

SHELL_PLUS_PRE_IMPORTS = (
	('food.generators', '*'),
	('lifebox.interaction', '*'),
	('lifebox.interaction', '*'),
	('lifebox.generators', '*'),
	('inventory.inventory', '*'),
	('tasks.generators', '*'),
	('projects.generators', '*'),
)
