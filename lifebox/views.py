from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse

def data(request):
    data = 'test'
    return render(request, 'lifebox/data.html', {
        'data': data    
    })
