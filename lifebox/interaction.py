from fitness.models import Workout, Exercise, Sleep, Caffeine, WaterFilter, WaterPitcher
from units.models import Unit
from psychology.models import LinearMood, LinearEnergy, BinaryMood, Conversation
from media.models import TelevisionShow
from tasks.models import Task
import datetime
import time
from django.db import models
from tasks.models import Activity
from schedule.models import Atom
from schedule.helpers import *
from projects.models import Project

HALF_DROPLET = Unit.objects.filter(name='Half Droplet').first()

def cdrop(quantity):
	return Caffeine(unit=HALF_DROPLET, quantity=quantity).save()

def lmood(mood):
	return LinearMood(mood=mood).save()

def watch(televisionshow_name):
	return TelevisionShow.watch_episode(televisionshow_name)

def bmf():
	return BinaryMood.record(False)

def bmt():
	return BinaryMood.record(True)

C = Caffeine
WF = WaterFilter
WP = WaterPitcher
S = Sleep
LM = LinearMood
LE = LinearEnergy
BM = BinaryMood
TS = TelevisionShow
CON = Conversation
T = Task
WO = Workout

LWO = WO.latest()
BENCHPRESS = Exercise.objects.get(name='Bench Press')
DEADLIFT = Exercise.objects.get(name='Deadlift')
SQUAT = Exercise.objects.get(name='Squat')


Ts = []
for task in Task.objects.all():
	Ts.append(task)

E = exit
def cycle_model_names(times = 3, period=0.05):
    _ = map(lambda x: x.__name__, models.get_models())
    for i in range(0, times):
        for item in _:
            print item
            time.sleep(period)

_A = cycle_model_names


A = Atom
AC = A.clock
AF = A.fetch
AL = A.list_as_matrix
AW = A.write
D = print_time

class Stash(object):

    def __init__(self, objects=[], name='stash'):
        self.name = name
        dictionary = {}
        for obj in objects:
            key = obj.name.lower().replace(' ','_')
            dictionary[key] = obj
        self.__dict__.update(dictionary)

    def __repr__(self):
        return self.keys().__str__()

    def keys(self):
        return self.__dict__.keys()

    def length(self):
        return len(self.keys())


activities = Stash(objects=Activity.objects.all(), name='activities')
projects = Stash(objects=Project.objects.all(), name='projects')
tasks = Stash(objects=Task.objects.all(), name='tasks')
atoms = Stash(objects=Atom.objects.all(), name='atoms')
stash = Stash(objects=[activities, projects, tasks, atoms], name='stash')
#Please forgive me
globals().update(stash.activities.__dict__)
globals().update(stash.atoms.__dict__)
