from django.test import TestCase
from core.models import Node

# Create your tests here.
nodes = [
	"Felix Mendelssohn", 
	"Max Bruch",
]
for name in nodes:
	Node.objects.get_or_create(name=name)
