from django.db import models
from core.models import CoreModel, NamedModel
from datetime import date

# Create your models here.
class Date(models.Model):
    date = models.DateField(default=date.today)

    def __str__(self):
        return self.date.__str__()

    @classmethod
    def today(self):
        return self.objects.filter(date=date.today()).first()
    

class TaskManager(models.Manager):
    def create(self, *args, **kwargs):
        task = super(TaskManager, self).create(*args, **kwargs)
        return task

    def create_many(self, list_of_tasks):
        tasks = []
        for task_name in list_of_tasks:
            task = self.create(name=task_name)
            tasks.append(task)
        return tasks

    def take(self):
        return self.filter(complete=False).order_by('?').first()

    def I(self):
        return self.filter(complete=False).first()

    def all(self, complete=False):
        if complete:
            return super(TaskManager, self).all()
        else:
            return self.filter(complete=False).all()

    def total(self):
        return self.filter(complete=False).count()


class Task(NamedModel):
    parent = models.ForeignKey("Task", null=True, blank=True, related_name="tasks")
    dependencies = models.ManyToManyField("Task", related_name="dependents")
    position = models.IntegerField(default=1)
    date = models.ForeignKey(Date, null=True, blank=True)
    complete = models.BooleanField(default=False)
    time_estimate = models.IntegerField(default=15)

    objects = TaskManager()

    def children(self):
        return self.tasks.all()

    @classmethod
    def create_child(self, name, parent=0):
        return self.objects.create(name=name, parent=parent)

    @classmethod
    def _list(self, hide_complete=True):
        tasks = self.objects.filter(parent__isnull=True)
        if hide_complete is True:
            tasks = tasks.filter(complete=False)
        for task in tasks.all():
            print task
            task.nested_list(hide_complete)

    def nested_list(self, tabs=0):
        tabs += 1
        for task in self.children():
            prefix = tabs * "   "
            print prefix + task.__str__()
            task.nested_list(tabs)

    @classmethod
    def _list_dependencies(self):
        tasks = self.objects.all()
        for task in tasks:
            print task
            task.list_dependencies(1)

    def list_dependencies(self, tabs=0):
        for task in self.dependencies.all():
            prefix = tabs * "   "
            print prefix + task.__str__()

    @classmethod
    def _list_dependents(self):
        tasks = self.objects.all()
        for task in tasks:
            print task
            task.list_dependents(1)

    def list_dependents(self, tabs=0):
        for task in self.dependents.all():
            prefix = tabs * "   "
            print prefix + task.__str__()
            
    def list(self):
        return self.nested_list()

    def __str__(self):
        return self.id.__str__() + ". " + self.name

    def do(self):
        self.complete = True
        self.save()

    def undo(self):
        self.complete = False
        self.save()

    def is_fully_scheduled(self):
        return 15 * self.atoms.count() - self.time_estimate  > 0

    @classmethod
    def hash(self, time=True):
        if time:
            return self.list_time_estimates()
        else:
            return self._list_dependencies()

    @classmethod
    def create_dependency(self, dependent, dependency):
        Task.objects.get(pk=dependent).dependencies.add(dependency)

    @classmethod
    def list_time_estimates(self):
        for task in self.objects.all():
            print task.__str__() + ": " + task.time_estimate.__str__()

    @classmethod
    def update_time(self, pk, time_estimate):
        self.objects.filter(pk=pk).update(time_estimate=time_estimate)
        self.hash()

    @classmethod
    def time(self):
        time_hash = self.objects.all().aggregate(models.aggregates.Sum('time_estimate'))
        time = time_hash['time_estimate__sum']
        hours = time / 60.0
        return hours

class Activity(NamedModel):
    pass
