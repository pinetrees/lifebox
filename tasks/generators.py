from .models import *

ACTIVITIES = ['Juice', 'Eat', 'Lumosity', 'Duolingo', 'Clean', 'Rest', 'Walk', 'Exercise', 'Read', 'Program', 'Work', 'Relax', 'Education']
for activity in ACTIVITIES:
    Activity.objects.get_or_create(name=activity)
