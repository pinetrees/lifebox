from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from tasks import views

urlpatterns = patterns('',
    url(r'^data/', views.data, name='data'),
)
