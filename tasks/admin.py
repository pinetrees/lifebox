from django.contrib import admin
from django.forms import TextInput, Textarea
from django.db import models
from django.core.urlresolvers import reverse
from tasks.models import Task, Date

class TaskInline(admin.TabularInline):
	model = Task

class TaskAdmin(admin.ModelAdmin):
	list_display = ["name", "position", "complete", "tasks"]
	list_editable = ["position", "complete"]
	list_filter = ["date", "complete"]
	inlines = [TaskInline]
	ordering = ["position"]
	def tasks(self, obj):
		url = reverse('admin:tasks_task_changelist')
		return '<a href="{0}?parent__id__exact={1}">Tasks</a>'.format(url, obj.id)
	tasks.allow_tags = True
	tasks.short_description = 'Tasks'
	#formfield_overrides = {
	#models.CharField: {'widget': TextInput(attrs={'size':'40'})},
	#	models.TextField: {'widget': Textarea(attrs={'rows':4, 'cols':40})},
	#}

# Register your models here.
admin.site.register(Task, TaskAdmin)
admin.site.register(Date)
