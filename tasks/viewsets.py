from rest_framework import viewsets, permissions, status, exceptions
from .serializers import *
from rest_framework.response import Response


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

