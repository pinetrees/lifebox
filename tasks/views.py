from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.db import models
from media.models import TelevisionShow
import json
import pycountry
from lifebox.data import *
from .models import *
from academics.models import Question


def data(request):
    app_models = models.get_models()
    model_names = map(lambda x: x.__name__, app_models)
    show_names = list(TelevisionShow.objects.all().values_list('name', flat=True))
    countries = map(lambda x: x.name, list(pycountry.countries))
    languages = map(lambda x: x.name, list(pycountry.languages))
    schedule = schedules[0]
    activities = list(Activity.objects.values_list('name', flat=True).all())
    questions = list(Question.objects.values_list('question', 'answer').all())
    return render(request, 'tasks/data.html', {
        'data': json.dumps(questions)
    })
