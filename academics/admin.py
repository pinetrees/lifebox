from django.contrib import admin
from academics.models import Professor, University, Term

# Register your models here.
admin.site.register(Professor)
admin.site.register(University)
admin.site.register(Term)
