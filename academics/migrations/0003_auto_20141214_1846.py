# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('academics', '0002_term_university'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='university',
            options={'verbose_name_plural': 'universities'},
        ),
        migrations.AddField(
            model_name='professor',
            name='current_university',
            field=models.ForeignKey(default=1, to='academics.University'),
            preserve_default=False,
        ),
    ]
