from django.db import models
from core.models import PersonModel, PurgeableModel

# Create your models here.
class University(models.Model):
	name = models.CharField(max_length=200)

	class Meta:
		verbose_name_plural = "universities"

	def __str__(self):
		return self.name

class Professor(PersonModel):
	current_university = models.ForeignKey(University)

class Term(models.Model):
	name = models.CharField(max_length=32)

	def __str__(self):
		return self.name

class Question(PurgeableModel):
    question = models.CharField(max_length=256)
    answer = models.TextField()
