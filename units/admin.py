from django.contrib import admin
from units.models import Unit, Conversion, Chemical, Content

class ConversionAdmin(admin.ModelAdmin):
	list_display = ["from_unit", "to_unit", "multiplier"]

class ContentAdmin(admin.ModelAdmin):
	list_display = ["container", "chemical", "quantity", "measure"]

# Register your models here.
admin.site.register(Unit)
admin.site.register(Conversion, ConversionAdmin)
admin.site.register(Chemical)
admin.site.register(Content, ContentAdmin)
