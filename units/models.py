from core.models import models, CoreModel

# Create your models here.
class Unit(CoreModel):
	description = models.TextField(default='', blank=True)

	def convert(self, to_unit_name, quantity = 1):
		if isinstance(to_unit_name, Unit):
			to_unit = to_unit_name
		else:
			to_unit = Unit.objects.filter(name=to_unit_name).first()
		conversion = Conversion.objects.filter(from_unit=self).filter(to_unit=to_unit).first()
		conversion.multiplier = conversion.multiplier * quantity
		return conversion

	def chemical_contents(self, through_unit_name, chemical_name):
		conversion = self.convert(through_unit_name)
		chemical = Chemical.objects.filter(name=chemical_name).first()
		content = Content.objects.filter(container=conversion.to_unit).filter(chemical=chemical).first()
		return self.converted_content(conversion, chemical, content)

	def find_chemical_contents(self, chemical_name):
		chemical = Chemical.objects.filter(name=chemical_name).first()
		content = Content.objects.raw("SELECT ct.id as id, container_id, chemical_id, measure_id, quantity FROM units_conversion c JOIN units_content ct ON to_unit_id=container_id WHERE from_unit_id=%s AND chemical_id=%s", [self.id, chemical.id])[0]
		conversion = Conversion.objects.filter(from_unit=self).filter(to_unit=content.container).first()
		return self.converted_content(conversion, chemical, content)
		
	def converted_content(self, conversion, chemical, content):
		return Content(container=self, chemical=chemical, measure=content.measure, quantity=content.quantity / conversion.multiplier)

class Conversion(models.Model):
	from_unit = models.ForeignKey(Unit, related_name="to_unit")
	to_unit = models.ForeignKey(Unit, related_name="from_unit")
	multiplier = models.DecimalField(max_digits=14, decimal_places=8)

class Chemical(CoreModel):

	def __str__(self):
		return self.name

class Content(models.Model):
	container = models.ForeignKey(Unit, related_name="container")
	chemical = models.ForeignKey(Chemical)
	measure = models.ForeignKey(Unit, related_name="measure")
	quantity = models.DecimalField(max_digits=6, decimal_places=2)
	

#A fantastic idea, but not necessary!
#class ConvertedUnit(models.Model):
#	unit = models.ForeignKey(Unit)
#	conversion = models.ForeignKey(Conversion)
#	
#	class Meta:
#		abstract = True
