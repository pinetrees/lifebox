# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('units', '0003_chemical'),
    ]

    operations = [
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.DecimalField(max_digits=6, decimal_places=2)),
                ('container', models.ForeignKey(related_name='container', to='units.Unit')),
                ('measure', models.ForeignKey(related_name='measure', to='units.Unit')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
