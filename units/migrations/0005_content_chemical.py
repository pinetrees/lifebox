# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('units', '0004_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='content',
            name='chemical',
            field=models.ForeignKey(default=1, to='units.Chemical'),
            preserve_default=False,
        ),
    ]
