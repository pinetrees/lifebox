# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('units', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Conversion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('multiplier', models.DecimalField(max_digits=8, decimal_places=2)),
                ('from_unit', models.ForeignKey(related_name='to_unit', to='units.Unit')),
                ('to_unit', models.ForeignKey(related_name='from_unit', to='units.Unit')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
